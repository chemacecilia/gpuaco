#include <stdio.h>
#include <cuda_runtime.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <cuda_occupancy.h>
#define BLOCK_SIZE_ATOMIC_DEVICE 256
#define TAMBLOCK_SCATTER 16
#define SHARED_SIZE 256 //16*16
#define LENGHT 1.0f
#define DEVICE 0


__host__ void sacaCPU(float * pheromone, int num_cities, int num_ants){
	float aux=0;
	for (int i = 0; i<(num_cities*num_cities);i++){
		printf("%f\n",pheromone[i]);
		aux+=pheromone[i];
	}
	printf("\n SUMA:\t%f\n",aux);
}


__host__ void pheromoneOnCPU (float * pheromone, int * tours, int num_cities, int num_ants) {
  
  int pos = 0;
 
  for (int i = 0; i<num_ants; ++i) {
    for (int j = 0; j<num_cities; ++j) {
      int pos1 = tours[pos+j];
      int pos2 = tours[pos+j+1];
      
      pheromone[(pos1*num_cities)+pos2] += LENGHT;
      pheromone[(pos2*num_cities)+pos1] += LENGHT;
    }
    pos+=(num_cities+1);
  }
}


__global__ void pheromoneOnGPUScatterToGather (float * pheromone, int * tours, int num_cities, int num_ants) {
  
  int row =  blockIdx.y*blockDim.y+threadIdx.y; 
  int column = blockIdx.x*blockDim.x+threadIdx.x; 
  int index = (row*num_cities)+column;
  
  if ((row < num_cities) && (column < num_cities)) {
    
    for (int i = 0; i< num_ants; ++i) {      
      for (int j=0; j< num_cities; ++j) {
	
	if ((tours[i*(num_cities+1)+j] == row) && (tours [i*(num_cities+1)+j+1] == column) ||
	    (tours[i*(num_cities+1)+j] == column) && (tours [i*(num_cities+1)+j+1] == row)) {
	  //                   printf ("Row = %d and column = %d\n", row, column);
	  pheromone[index] += LENGHT;
	  
	}
      }
    }
  }
}




// pheromone_update_with_atomics
__global__ void pheromone_update_with_atomic (float * d_pheromone, const int number_of_cities, int * d_tour, const int number_of_ants, int ite){
  
  const int xIndex = blockIdx.x* (number_of_cities+1) + threadIdx.x;
  int index,indexThread;
  for (int i=0; i<ite; i++) {
    indexThread = i*blockDim.x;
    index =xIndex+(indexThread);
    if ((threadIdx.x+indexThread)<number_of_cities){
      unsigned int coordx=d_tour[index];
      unsigned int coordy=d_tour[index+1];
      unsigned int pos = (coordy*number_of_cities)+coordx;
      unsigned int posim = (coordx*number_of_cities)+coordy;
      float pheromone = 0.0;
      pheromone=LENGHT;
      atomicAdd(&d_pheromone[pos],pheromone);
      atomicAdd(&d_pheromone[posim],pheromone);
      //    printf ("Pheromone %f\n", d_pheromone[pos]);
    }
  }
}

// pheromone_update_with_atomics version two. No iterations
__global__ void pheromone_update_with_atomic_no_iter (float * d_pheromone, const int num_cities, int * d_tour, const int num_ants){
  
  int row = blockIdx.y*blockDim.y+threadIdx.y; 
  int column = blockIdx.x*blockDim.x+threadIdx.x; 
  int index = row*(num_cities+1)+column; 

  if ((row<num_ants) && (column< num_cities)){
    unsigned int coordx=d_tour[index];
    unsigned int coordy=d_tour[index+1];
    unsigned int pos = (coordy*num_cities)+coordx;
    unsigned int posim = (coordx*num_cities)+coordy;
    float pheromone = 0.0f;
    pheromone=LENGHT;
    atomicAdd(&d_pheromone[pos],pheromone);
    atomicAdd(&d_pheromone[posim],pheromone);
//    printf ("Pheromone %f\n", d_pheromone[pos]);
  } 
}


// pheromone_update_with_atomics version three. Using shared 
__global__ void pheromone_update_with_atomic_shared (float * d_pheromone, const int num_cities, int * d_tour, const int num_ants){
  
  /*  int tx = threadIdx.x;
  int ty = threadIdx.y;
  int row = blockIdx.y*blockDim.y+threadIdx.y; 
  int column = blockIdx.x*blockDim.x+threadIdx.x; 
  int index = row*(num_cities+1)+column; 
  int index_shared = threadIdx.y*blockDim.x+threadIdx.x; 

  if (row==0)
    printf ("index shared = %d\n", index_shared);
  
  __shared__ unsigned int s_tour[TAMBLOCK_SCATTER][TAMBLOCK_SCATTER];

  if ((row<num_ants) && (column< num_cities)){
    s_tour[ty][tx] = d_tour[index];
    __syncthreads();
    unsigned int coordx=s_tour[ty][tx];
    unsigned int coordy=s_tour[ty][tx+1];
    unsigned int pos = (coordy*num_cities)+coordx;
    unsigned int posim = (coordx*num_cities)+coordy;
    float pheromone = 0.0f;
    pheromone=LENGHT;
    atomicAdd(&d_pheromone[pos],pheromone);
    atomicAdd(&d_pheromone[posim],pheromone);
    printf ("Pheromone %f\n", d_pheromone[pos]);
    } 
  */
}


// pheromone_update_with_atomics no atomics 
__global__ void pheromone_update_no_atomic (float * d_pheromone, const int num_cities, int * d_tour, const int num_ants){
  
  int row = blockIdx.y*blockDim.y+threadIdx.y; 
  int column = blockIdx.x*blockDim.x+threadIdx.x; 
  int index = row*(num_cities+1)+column; 

  if ((row<num_ants) && (column< num_cities)){
    unsigned int coordx=d_tour[index];
    unsigned int coordy=d_tour[index+1];
    unsigned int pos = (coordy*num_cities)+coordx;
    unsigned int posim = (coordx*num_cities)+coordy;
    float pheromone = 0.0;
    pheromone=LENGHT;
    d_pheromone[pos]+=pheromone;
    d_pheromone[posim]+=pheromone; 
//    printf ("Pheromone %f\n", d_pheromone[pos]);
  } 
}



__host__ void loadfile (int * num_ants, int * num_cities, int** h_tours, char * name_f){
  
  
  FILE * fichero;
  fichero = fopen(name_f,"r");
  if (fichero == NULL){
    printf("cagada al abrir el fichero para leerlo");
    exit (-1);
  } 
  
  fscanf(fichero,"%d",num_ants);
  *num_cities=*num_ants;
  
  printf("NUMERO DE ANTS: %d \n", *num_ants);
  printf("NUMERO DE CITIES: %d \n", *num_cities);
  
  int aux;
  
  *h_tours = (int *) malloc ((*num_cities+1) * (*num_ants)*sizeof(int));
  
  if (*h_tours == NULL) {
    fprintf (stderr, "Malloc error\n");
    exit (-1);
    
  }
  
  for(int i=0; i < *num_ants;i++){
    for(int j=0; j <= *num_cities;j++){
      
      fscanf(fichero,"%d",&aux); 
	   
      *(*h_tours+(i*(*num_cities+1)+j)) = aux; 
    }
    if (feof(fichero)){
      break;
    }
  }
  fclose(fichero);
  
}



/**
 * This function sets the kernel parameter of pheromone update
 * kernel. We set one block per each ant and a thread per each
 * couples of cities.
 */
__host__ void setting_kernel_parameters_phero(dim3 * grid_phero, dim3 *block_phero, unsigned int * iterationsInPhero, int n, int n_ants){
  
  grid_phero->x = n_ants;
  block_phero->x  = (n<BLOCK_SIZE_ATOMIC_DEVICE)?n:BLOCK_SIZE_ATOMIC_DEVICE;
  *iterationsInPhero= (unsigned int)ceil((float)n/BLOCK_SIZE_ATOMIC_DEVICE);
  //    pdebug ("Threads in the Pheromone Update with atomic instructions kernel 6 (x)= (%d) and blocks (%d). Number of iterations per block is %d", block_phero->x, grid_phero->x, *iterationsInPhero);
}


__host__ void allocateHostMemory (float ** h_pheromone1, float ** h_pheromone2, 
				  int num_cities, int num_ants) {
  
  
  int memsize = num_cities*num_cities*sizeof(float);

  cudaMallocHost (h_pheromone1, memsize);
  cudaMallocHost (h_pheromone2, memsize);
 
}


__host__ void allocateDeviceMemory (int ** d_tours, float ** d_pheromone, 
                                    int num_cities, int num_ants) 
{
  
  
  // ahora sé lo que valen num_ants y num_cities
  int memsized_tours=num_ants*(num_cities+1)*sizeof(int);
  int memsized_pheromone=num_cities*num_cities*sizeof(float);
  
  
  //********* Reserva de memoria en el device
  if (cudaMalloc(d_tours,memsized_tours)!=cudaSuccess){
    fprintf(stderr,"Error reservando d_tours");
    exit (-1);
  }
  if (cudaMalloc(d_pheromone,memsized_pheromone)!=cudaSuccess){
    fprintf(stderr,"Error reservando d_pheromone");
    exit (-1);
  }
  
  
}


__host__ void deviceMemorySet (int * h_tours, int * d_tours, float * d_pheromone, 
			       int num_ants, int num_cities) {
  
  
  int memsized_tours=num_ants*(num_cities+1)*sizeof(int);
  int memsized_pheromone=num_cities*num_cities*sizeof(float);
  
  cudaMemset(d_pheromone,0,memsized_pheromone);
  
  if (cudaMemcpy(d_tours,h_tours,memsized_tours,cudaMemcpyHostToDevice)!=cudaSuccess){
    fprintf(stderr,"Error al transferir h_tours hacia d_tours");
    exit (-1);
  }
  
}



__host__ void checkErrors (float * h_pheromoneCPU2, float * h_pheromoneGPU, int num_cities) {
  
  float sum=0.0f;
  printf("....Checking Errors........\n"); 
  for (int i=0;i<(num_cities* num_cities);i++){    
    if (h_pheromoneCPU2[i] != h_pheromoneGPU[i]){
 //     printf("i = %d, CPU = %e and GPU %e\n",i, h_pheromoneCPU2[i], h_pheromoneGPU[i]);
      sum += abs(h_pheromoneCPU2[i] - h_pheromoneGPU[i]);
    }
  }
  
  printf (".......OK\n\n"); 
  printf("abs %f, de un total de: %d\n",sum,num_cities*num_cities*2);
  
  
}


int main (int argc, char** argv) {
  int num_cities = 0, num_ants;
  int *h_tours, *d_tours; 
  float *h_pheromoneCPU, *h_pheromoneGPU, *d_pheromone; 
  char *name_file;
  cudaEvent_t start, stop;

 
  dim3 grid_phero, block_phero;
  unsigned int iterationsInPhero=0;
  float msec = 0.0f; 
  
 
  cudaSetDevice(DEVICE);
  int device;
  cudaGetDevice(&device);
  cudaDeviceProp nombredev;
  cudaGetDeviceProperties(&nombredev, DEVICE);
  printf("GPU %s ->msecs\t\n", nombredev.name);





 
  name_file = argv[1];
  
  printf ("---------Reading File %s\n", name_file);
  
  loadfile (&num_ants, &num_cities, &h_tours, name_file);
  printf("-----------OK!\n");
  
  int memsize_pheromone=num_cities*num_cities*sizeof(float);
  allocateHostMemory (&h_pheromoneCPU, &h_pheromoneGPU, num_cities,num_ants);

  for (int p=0;p<(num_cities*num_cities);p++){
	h_pheromoneCPU[p]=0.0f;
	h_pheromoneGPU[p]=0.0f;
  }

  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  
  //////////////////////////////////////////////////////////////////////////
  ////////////////////////Pheromone on CPU/////////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  printf ("....Pheromone on CPU\n");
  cudaEventRecord (start);

  pheromoneOnCPU (h_pheromoneCPU, h_tours, num_cities, num_ants);

  cudaEventRecord (stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&msec, start, stop);
  printf ("....End Pheromone on CPU, elapsed time:\n%f\t CPU msecs\n", msec);
  
  ////////////////////////////////////////////////////////////////////////
  ////////////////////////Pheromone on GPU atomics///////////////////////
  //////////////////////////////////////////////////////////////////////
  
  allocateDeviceMemory (&d_tours, &d_pheromone, num_cities, num_ants);
  deviceMemorySet (h_tours, d_tours,d_pheromone, num_ants, num_cities);
  setting_kernel_parameters_phero(&grid_phero, &block_phero, &iterationsInPhero, num_cities, num_ants);
  printf ("\n....Pheromone on GPU atomics (ite) blocks (%d, %d) and threads (%d, %d)\n", grid_phero.x, grid_phero.y, block_phero.x, block_phero.y);
  cudaEventRecord (start);
 
  pheromone_update_with_atomic<<<grid_phero, block_phero>>> (d_pheromone, num_cities, d_tours, num_ants, iterationsInPhero);
  
  cudaEventRecord (stop);
  cudaEventSynchronize(stop);
  msec=0.0f;
  cudaEventElapsedTime(&msec, start, stop);
  printf ("....Pheromone on GPU atomics (ite), elapsed time:\n%f\t ATOMIC ITE msecs\n", msec);
  if (cudaMemcpy(h_pheromoneGPU,d_pheromone,memsize_pheromone,cudaMemcpyDeviceToHost)!=cudaSuccess){
    fprintf(stderr,"Error al trasferir d_pheromone hacia h_pheromone2");
    exit (-1);
  }
  checkErrors (h_pheromoneCPU, h_pheromoneGPU, num_cities);
  
  
  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////Pheromone on GPU Scatter To Gather////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  
  float gridx = ceilf((float) num_cities/TAMBLOCK_SCATTER);
  float gridy = ceilf((float)num_cities/TAMBLOCK_SCATTER);
  dim3 gridScatter (gridx, gridy);
  dim3 blockScatter (TAMBLOCK_SCATTER, TAMBLOCK_SCATTER); 
  cudaMemset(d_pheromone,0,memsize_pheromone);
  printf ("\n....Scatter to Gather Threads per block (%d, %d) and blocks (%d, %d)\n", blockScatter.x, blockScatter.y, gridScatter.x, gridScatter.y);
  cudaEventRecord (start);

  //pheromoneOnGPUScatterToGather <<<gridScatter, blockScatter>>> (d_pheromone, d_tours, num_cities, num_ants);
  
  cudaEventRecord (stop);
  cudaEventSynchronize(stop);
  msec=0.0f;
  cudaEventElapsedTime(&msec, start, stop);
  printf ("....Pheromone Scatter to Gather, elapsed time:\n%f\t GATHER msecs\n", msec);

  if (cudaMemcpy(h_pheromoneGPU,d_pheromone,memsize_pheromone,cudaMemcpyDeviceToHost)!=cudaSuccess){
    fprintf(stderr,"Error al trasferir d_pheromone hacia h_pheromone 3");
    exit (-1);
  }
  //checkErrors (h_pheromoneCPU, h_pheromoneGPU, num_cities);

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////Pheromone on GPU Atomics no iter////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  
 
  gridx = ceilf((float) num_cities/TAMBLOCK_SCATTER);
  gridy = ceilf((float)num_ants/TAMBLOCK_SCATTER);
  dim3 gridAtomic (gridx, gridy);
  dim3 blockAtomic (TAMBLOCK_SCATTER, TAMBLOCK_SCATTER); 
  cudaMemset(d_pheromone,0,memsize_pheromone);
  printf ("\n... Pheromone Atomic NO iter Threads per block (%d, %d) and blocks (%d, %d)\n", blockAtomic.x, blockAtomic.y, gridAtomic.x, gridAtomic.y);
  cudaEventRecord (start);

  pheromone_update_with_atomic_no_iter <<<gridAtomic, blockAtomic>>> (d_pheromone, num_cities, d_tours, num_ants);
  
  cudaEventRecord (stop);
  cudaEventSynchronize(stop);
  msec=0.0f;
  cudaEventElapsedTime(&msec, start, stop);
  printf ("....Pheromone Atomic NO iter, elapsed time:\n%f\t ATOMIC NO ITER msecs\n", msec); 
  cudaError_t err;
//  printf("\nmemsize_pheromone : %d\n",memsize_pheromone);

  err = cudaMemcpy(h_pheromoneGPU,d_pheromone,memsize_pheromone,cudaMemcpyDeviceToHost);
  if (err!=cudaSuccess)
	fprintf(stderr," %s Error al trasferir d_pheromone hacia h_pheromone 4\n", cudaGetErrorString(err));
  checkErrors(h_pheromoneCPU, h_pheromoneGPU, num_cities);


///
  int maxActiveBlocks;
  cudaOccupancyMaxActiveBlocksPerMultiprocessor( &maxActiveBlocks, pheromone_update_with_atomic_no_iter, (blockAtomic.x*blockAtomic.y), 0);
  printf("\nMAXActiveBlocks:%d\n",maxActiveBlocks);



  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////Pheromone on GPU no Atomics no iter////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  
 
  gridx = ceilf((float) num_cities/TAMBLOCK_SCATTER);
  gridy = ceilf((float)num_ants/TAMBLOCK_SCATTER);
  gridAtomic.x=gridx;
  gridAtomic.y= gridy;
  blockAtomic.x=TAMBLOCK_SCATTER;
  blockAtomic.y=TAMBLOCK_SCATTER; 
  cudaMemset(d_pheromone,0,memsize_pheromone);
  printf ("\n... NO Atomics, no Iter, Threads per block (%d, %d) and blocks (%d, %d)\n", blockAtomic.x, blockAtomic.y, gridAtomic.x, gridAtomic.y);
  cudaEventRecord (start);

  pheromone_update_no_atomic <<<gridAtomic, blockAtomic>>> (d_pheromone, num_cities, d_tours, num_ants);
  
  cudaEventRecord (stop);
  cudaEventSynchronize(stop);
  msec=0.0f;
  cudaEventElapsedTime(&msec, start, stop);
  printf ("....NO Atomics, no Iter, elapsed time:\n%f\t NO ATOMIC NO ITER msecs\n", msec); 
  err = cudaMemcpy(h_pheromoneGPU,d_pheromone,memsize_pheromone,cudaMemcpyDeviceToHost);
  if (err!=cudaSuccess)
	fprintf(stderr," %s Error al trasferir d_pheromone hacia h_pheromone 4\n", cudaGetErrorString(err));
    checkErrors(h_pheromoneCPU, h_pheromoneGPU, num_cities);


  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////Pheromone on GPU shared Atomics no iter////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////

  /*  gridx = ceilf((float) num_cities/TAMBLOCK_SCATTER);
  gridy = ceilf((float)num_ants/TAMBLOCK_SCATTER);
  gridAtomic.x=gridx;
  gridAtomic.y= gridy;
  blockAtomic.x=TAMBLOCK_SCATTER;
  blockAtomic.y=TAMBLOCK_SCATTER; 
  cudaMemset(d_pheromone,0,memsize_pheromone);
  printf ("\n... Atomics with shared no Iter, Threads per block (%d, %d) and blocks (%d, %d)\n", blockAtomic.x, blockAtomic.y, gridAtomic.x, gridAtomic.y);
  cudaEventRecord (start);

  pheromone_update_with_atomic_shared<<<gridAtomic, blockAtomic>>> (d_pheromone, num_cities, d_tours, num_ants);

  cudaEventRecord (stop);
  cudaEventSynchronize(stop);
  msec=0.0f;
  cudaEventElapsedTime(&msec, start, stop);
  printf ("....Atomics Shared, no Iter, elapsed time %f msecs\n", msec); 
  err = cudaMemcpy(h_pheromoneGPU,d_pheromone,memsize_pheromone,cudaMemcpyDeviceToHost);
  if (err!=cudaSuccess)
	fprintf(stderr," %s Error al trasferir d_pheromone hacia h_pheromone 4\n", cudaGetErrorString(err));
  checkErrors(h_pheromoneCPU, h_pheromoneGPU, num_cities);
  */

  ///LIBERAR MEMORIAS
  cudaFree (d_pheromone);
  cudaFree (d_tours);
  free (h_tours);
  cudaFreeHost(h_pheromoneCPU);
  cudaFreeHost(h_pheromoneGPU);
  
  
}

