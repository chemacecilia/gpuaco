/***************************************************************************

    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP. This code is adapted from the Ant Colony Optimization Book. Dorigo and
    Stuzle.

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Departmentt
                  Catholic University of Murcia
                  Campus de los Jeronimos
                  30107 Murcia
		          Spain

***************************************************************************/

#include "TSP.h"
#include "Colony.h"

#define PROG_ID_STR      "\nACO algorithm AS tailores to GPUs for the TSP, v1.0\n"
#define CALL_SYNTAX_STR  "call syntax:  acotsp <param-list>\n"

#define LINE_BUF_LEN     100

struct point * read_etsp(const char *tsp_file_name, problem * problem);

//extern FILE *report, *comp_report, *stat_report;

extern char name_buf[LINE_BUF_LEN];
extern int  opt;


void exit_program (problem * instance, colony * colony);
void init_program(int argc, char *argv[], problem * problem, colony * colony);

//void printDist(void);

//void printHeur(void);

//void printTrail(void);





