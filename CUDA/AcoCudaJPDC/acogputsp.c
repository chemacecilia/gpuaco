/*****************************************************************************
//////////////////////////////////////////////////////////////////////////////
////////////////// GPU ACO Ant System algorithm for the TSP///////////////////
/////////////////////////////////////////////////////////////////////////////
    Version: 1.0
    File acogputsp.c
    Author: Jose M. Cecilia
    Purpose: main routines and control for the ACO on  GPUs
    Check: README and gpl.txt
    Copyright (c) 2012 Jose M. Cecilia

    Program's name: gpuacotsp

    Ant Colony Optimization algorithm AS, tailored to GPUs for the
    symmetric TSP. These implementations are based on the sources codes
    provided by Thomas Stuetzle.

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos
                  30107 Murcia
                  Spain

***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "utilities.h"
#include "kernel.h"

/* -----------Main program: control for running the ACO algorithm on GPUs------------*/


int main (int argc, char ** argv) {

    problem instance;
    colony colony;
    int solution;

    pdebug("Start Debugging");


    //Initialize the parameters and TSP instance. In out File.
    init_program (argc, argv, &instance, &colony);

    pdebug ("Runnning the algorithm on the GPU");

    solution = antSystemTSP(instance, colony);
    printf ("Best solution found is %d\n",solution);
    exit_program (&instance, &colony);
    return 0;


}
