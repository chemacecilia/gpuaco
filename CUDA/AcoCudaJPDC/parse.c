#include <unistd.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include "parse.h"

/**
* This method initialize values by default
*/
void initialize_values (options * options) {
    options->arg_tries=3;
    options->device = 0;
    options->arg_tours=100;
    options->arg_tsplibfile=NULL;
    options->arg_ants=20;
    options->arg_alpha=0.5;
    options->arg_beta=0.5;
    options->arg_rho=0.1;
}

/**
* Diplay help information
*/
void display_info() {

    printf ("Number of tries is option -r \n");
    printf ("Number of tours is option -s\n");
    printf ("To indicate the input TSP file use -i option\n");
    printf ("Number of ants is option -m\n");
    printf ("Alpha is option -a\n");
    printf ("Beta is option -b\n");
    printf ("Number of the GPU device to run the algorithm on -d\n");
    printf ("Evaporation rate is option -e\n");
    exit(0);
}

/**
 * This function checks if the value introduced is an integer
 */
int check_valid (char * arg){

    int value;

    if ((value = atoi(arg)))
        return value;
    else{
         printf("You should include an integer instead of %s\n", arg);
         return -1;
    }
}

/**
 *This function prints the values of the ACO algorithm
 */
void printValues(options * options){

    printf ("Number of tries is %d\n",options->arg_tries);
    printf ("Number of tours is %d\n",options->arg_tours);
    printf ("The file is %s\n",options->arg_tsplibfile);
    printf ("Number of ants is %d\n",options->arg_ants);
    printf ("Alpha is %f\n",options->arg_alpha);
    printf ("Beta is %f\n",options->arg_beta);
    printf ("Evaporation rate %f\n",options->arg_rho);
}




void parseValues (int argc, char ** argv, options * options) {

	 //const char *progname;

    int c;
    int index;
    int value;

	//program name
   //  progname = argv [0] != NULL && *(argv [0]) != '\0 ? argv [0] : "gpuacotsp";

    initialize_values(options);

    opterr = 0;
	while((c=getopt (argc, argv, "r:s:i:m:a:b:e:h:d:")) != -1) {
		 switch(c){
		    case 'r'://number of tries
                options->arg_tries = check_valid(optarg);
			 	break;
		    case 'd'://number of tries
                options->device = check_valid(optarg);
			 	break;
            case 's': //number of tours
			    options->arg_tours = check_valid(optarg);
			    break;
		    case 'i': //tsplib file
			    options->arg_tsplibfile = optarg;
			    break;
		    case 'm': //number of ants
			    options->arg_ants = check_valid(optarg);
			    break;
		    case 'a': //alpha
			    options->arg_alpha = atof(optarg);
			    break;
		    case 'b': //beta
			    options->arg_beta = atof(optarg);
			    break;
            case 'e': //rho
                options->arg_rho = atof(optarg);
                break;
		    case 'h': // help
			    display_info();
                break;
		    case '?':
                display_info();
			    break;
		    default:
                display_info();
			    break;
		    }
    }

    printValues(options);

    for (index = optind; index < argc; index++) {
        printf ("Non-option argument %s\n", argv[index]);
    }

}
