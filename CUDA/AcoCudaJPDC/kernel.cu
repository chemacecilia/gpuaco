/*
 * Version: 1.0
 * File: kernel.cu
 * Author: Jose M. Cecilia
 * Purpose: TheGPU kernels to manage the kernel execution
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. cecilia
 */
/***************************************************************************
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain

*******************************************************************************/

#include <stdio.h>
#include <curand_kernel.h>
#include "kernel.h"
#include "utilities.h"





/****************** CUDA Kernels ***************/

/**
  * This kernel set ups the generation of random number on the GPU
  * It sets a different seed for each thread
  */
__global__ void setup_kernel(curandState *state, unsigned int m) {

  int id = threadIdx.x + blockIdx.x * blockDim.x; /* Each thread gets same seed, a different sequence number,
						     no offset */
  if (id<m)
    curand_init(1234*blockIdx.x, id, 0, &state[id]);
}


/**
  * FUCTION: This Kernel computes the total information and it also performs the evaporation stage.
  * INPUT: 
  * OUTPUT: _d_choiceInfo: Information about heuristic + pheromone, d_pheromone after evaporation 
  */
__global__ void computeTotalkernel (float *d_choiceInfo, float *d_pheromone, unsigned int * d_distance, unsigned int n, float alpha, float beta, float rho)
{

    int tid = (blockIdx.y*blockDim.y*n)+(blockDim.x*blockIdx.x)+threadIdx.x;

    if (tid<n*n){
        float pheromone;
        pheromone = d_pheromone[tid]+0.00001f;
        float heuristic = 1.0f / ((float) d_distance[tid] + 0.1f);
        d_choiceInfo[tid]=__powf(pheromone, alpha)*__powf(heuristic, beta);
        //printf ("El choice info es %f, la pheromona es %f  y la heuristica es %f, alpha %f and beta%f\n", d_choiceInfo[tid], pheromone, heuristic, alpha, beta);
        //Evaporation stage
        pheromone*=(1.0f-rho);
        d_pheromone[tid] = pheromone;
    }
}



/*
	FUNCTION:       This function obtains the maximum of the array and the index of that maximum
	INPUT:          pointer to the probabilistic information and the relative index
	OUTPUT:         Relative index of the maximum probability
	(SIDE)EFFECTS:  The maximum probability is in g_idata[0]
   	ORIGIN:         Partially based on NVIDIA_CUDA_SDK
*/
__device__ inline unsigned int reduceMax(float *g_idata, unsigned int index, unsigned int n)
{

  __shared__ unsigned int index_sh [BLOCK_SIZE_NEXT_TOUR];

  index_sh[threadIdx.x]= index;
  g_idata[threadIdx.x] = (index>=n)?0.0:g_idata[threadIdx.x];

  __syncthreads();

  // do reduction in shared mem
  for(unsigned int s=blockDim.x/2; s>0; s>>=1) {
    if (threadIdx.x < s){
      if (g_idata[threadIdx.x]<g_idata[threadIdx.x + s]) {
	    g_idata[threadIdx.x]=g_idata[threadIdx.x + s];
	    index_sh[threadIdx.x]=index_sh[threadIdx.x + s];
      }
    }
    __syncthreads();
  }

  // write result for this block to global mem
  return index_sh[0];
}

/**
 * Reduction
 */
__device__ inline void reduction(float *g_idata, unsigned int index, unsigned int n){

  int tid = threadIdx.x;
  //printf ("En reduction the n element is %d\n", n);

  g_idata[tid] = (index >= n) ? 0.0 : g_idata[tid];

  __syncthreads();

  // do reduction in shared mem
  for(unsigned int s=blockDim.x>>1; s>0; s>>=1) {
    if (tid < s){
	    g_idata[tid]+=g_idata[tid + s];
      }
    }
  __syncthreads();
}


/**
 * Simple parallel scan of 2exp(k) elements with 2exp(k) threads
 */
inline __device__ void simplescan(float * idata, unsigned int index, unsigned int n){

    int tid = threadIdx.x;
    idata[tid] = (index<n) ? idata[tid]:0.0f;//Some cities may be null

    for (int offset = 1; offset < n; offset <<= 1) {
        float temp;
        if (tid >= offset)
            temp = idata[tid-offset];
        __syncthreads();
        if (tid >= offset)
            idata[tid]+=temp;
        __syncthreads();
    }

}


/**
 * This funtion set a city as a visited in a bitwise
 */
__device__ inline void setCityAsVisited(unsigned int city, unsigned int * tabul){

  //Division is the bit that represents that city
  unsigned int bitTabul = (unsigned int) city>>LOG_BLOCK_SIZE_NEXT_TOUR;
  //Modulo is the thread that manages that city
  unsigned int threadTabul = (unsigned int) city & (BLOCK_SIZE_NEXT_TOUR-1);

  if (threadIdx.x == threadTabul) {
    //the bit that represents the city in this thread is marked as zero
    unsigned int aux = (unsigned int)MACROBITS<<bitTabul;
    *tabul = *tabul & (~aux);
  }
}


/**
 * This function do a stencil check in order to select a city. Only
 * one thread will be in the frontier between positive and negative
 * values.
 */
__device__ inline bool stencilCheck (float * sidata, float p, int n ,  int index) {

    int tid = threadIdx.x;
    bool check = false;
    //Stencil to check which city to go next. Only one thread can get in.
    //Thread 0 sh@ould be stencil

    if (index < n) {
        float left_value = (tid > 0) ? sidata[tid-1] : 0.0f; // Prepare the stencil for thread 0.
        left_value -= p;
        float center_value = sidata[tid] - p;
        check = ((left_value * center_value)<= 0);
    }

    return check;

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/******************************* TOUR IMPLEMENTATIONS *********************************************/
/////////////////////////////////////////////////////////////////////////////////////////////////////

/***********************************Implementation Scan + Stencil *********************************/

/**
  * This kernel performs the tour generation by m ants. An ant is represented by a block, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern.
  */
__global__ void nextTour_kernel_reducing_random(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

    // We need an extra cell to better do stencil
  __shared__ float probabilities_sh[BLOCK_SIZE_NEXT_TOUR];
  __shared__ float partial_sums_sh[BLOCK_SIZE_NEXT_TOUR];//This can be iterathreads width
  __shared__ unsigned int tour_sh [BLOCK_SIZE_NEXT_TOUR];

  int tid = threadIdx.x;
  __shared__ unsigned int tourLenghtCum_sh;
  __shared__ unsigned int current_chunk_sh;
  unsigned int current_city, beginning_city, current_chunk;
  unsigned int btourindex = blockIdx.x*(number_of_cities+1);
  unsigned int tabul=0xFFFFFFFF;
  __shared__ float random;
  __shared__ float p;
  curandState localState = state[blockIdx.x];

  //Each ant is placed in a initial random city.
  if (tid == 0){
    random = curand_uniform(&localState);
    tour_sh[0] = (unsigned int)number_of_cities*random;
    tourLenghtCum_sh=0;
  }
  __syncthreads();

  current_city = tour_sh[0];
  beginning_city = current_city;
  setCityAsVisited (beginning_city, &tabul);

  __syncthreads();

  //Repeat until tabu list is full
  for (int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){

    int indexChoiceInfo = current_city*number_of_cities;
    int indexaux;

    // Check if tour_sh is full.
    if ((tourIndex & (BLOCK_SIZE_NEXT_TOUR-1)) == 0) {
        int indexdtour = blockIdx.x*(number_of_cities+1) + (((tourIndex >> LOG_BLOCK_SIZE_NEXT_TOUR)-1) * BLOCK_SIZE_NEXT_TOUR)+ threadIdx.x;
        d_tour[indexdtour]= tour_sh[threadIdx.x];
    }

    if ( iterathreads == 1 ) { //There are more threads than cities, thus it is not needed hierarchical scan.
        if (threadIdx.x < number_of_cities){
            unsigned int auxtabul = tabul >>(iterathreads-1);
            auxtabul = (unsigned int) MACROBITS & auxtabul;
            float choice= choice_info[indexChoiceInfo+tid];
            probabilities_sh[threadIdx.x]= (float)choice*auxtabul;
        }
        __syncthreads();
        //Scan over the probabilities. The result is in probabilities_sh
        simplescan (probabilities_sh,threadIdx.x, number_of_cities);

        //Calculate the total probability to proceed with the roulette selection
        if (tid == (number_of_cities-1))
            p = random * probabilities_sh[tid];

        __syncthreads();

        if (stencilCheck(probabilities_sh, p, number_of_cities, tid)) {
           // printf ("I am the ant %d, thread %d in the tourIndex %d\n", blockIdx.x, tid, tourIndex);
            setCityAsVisited(tid, &tabul);
            tour_sh[tourIndex] = tid;
            tourLenghtCum_sh+= d_distance[indexChoiceInfo+tid];
        }
        __syncthreads();
        current_city = tour_sh[tourIndex];
    }
    else { // There are more cities than threads. A hierarchical reduction is performed before scan
        for (int i=0; i<iterathreads-1; ++i) {
            indexaux = threadIdx.x+(i*blockDim.x);
            unsigned int auxtabul = tabul>>i;
            auxtabul = (unsigned int) MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+(indexaux)];
            probabilities_sh[threadIdx.x]= (float) choice*auxtabul;
            
            reduction(probabilities_sh, indexaux, number_of_cities);
            
            if (threadIdx.x == 0){
                partial_sums_sh[i] = probabilities_sh[0];
            }
            __syncthreads();
        }
        // Last Chunk may be not divisible by the TAMBLOCK
        indexaux = threadIdx.x + ((iterathreads-1)*blockDim.x);
        if (indexaux < number_of_cities){
            unsigned int auxtabul = tabul >>(iterathreads-1);
            auxtabul = (unsigned int)MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+(indexaux)];
            probabilities_sh[threadIdx.x] = (float)choice*auxtabul;
        }
        
        __syncthreads ();
        //Reduction of the rest elements
        reduction(probabilities_sh, indexaux, number_of_cities);
        
        if (threadIdx.x == 0){
            partial_sums_sh[(iterathreads-1)] = probabilities_sh[0];
        }
        __syncthreads();

        // Now, scan indexes stored in partial_sums_sh
        simplescan (partial_sums_sh, threadIdx.x, iterathreads);

        //Calculate the last probabilities
        if (tid == (iterathreads-1)){
            p = random * partial_sums_sh[iterathreads-1];
        }
        __syncthreads();

        //Check in which chunk lies the probability in
        if (stencilCheck(partial_sums_sh, p, iterathreads, tid)) {
            current_chunk_sh = tid;
        }
        __syncthreads ();
        current_chunk = current_chunk_sh;

        //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk
        indexaux = threadIdx.x + current_chunk*blockDim.x;

        if (indexaux < number_of_cities){
            unsigned int auxtabul = tabul>>current_chunk;
            auxtabul = (unsigned int) MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+(indexaux)];
            probabilities_sh[threadIdx.x]= (float) choice*auxtabul;
        }

        __syncthreads ();
        //Scan over the probabilities. The result is in probabilities_sh
        simplescan (probabilities_sh, indexaux, number_of_cities);
         
        if (tid == 0) {
           p = random * probabilities_sh[BLOCK_SIZE_NEXT_TOUR-1];
        }        
        __syncthreads();

        //Check which city within the select chunk is being selected
        if (stencilCheck(probabilities_sh, p, number_of_cities, indexaux)) {
            setCityAsVisited (indexaux, &tabul);
            tour_sh[tourIndex & (BLOCK_SIZE_NEXT_TOUR-1)] = indexaux;
            tourLenghtCum_sh+= d_distance[indexChoiceInfo+indexaux];
        }
        __syncthreads();
        
        current_city = tour_sh[tourIndex & (BLOCK_SIZE_NEXT_TOUR-1)]; 
    }
  }

  int indexaux = threadIdx.x + ((iterathreads-1)* BLOCK_SIZE_NEXT_TOUR);

  //Write in device memory the tour
  if ((indexaux < number_of_cities)) {
     int indexdtour = blockIdx.x*(number_of_cities+1) + threadIdx.x + (iterathreads-1)* BLOCK_SIZE_NEXT_TOUR;
     d_tour[indexdtour]= tour_sh[threadIdx.x];
  }

  //Complete the tour getting the beginning city
  if (threadIdx.x==0) {
    d_tour[btourindex+number_of_cities]=beginning_city;
    tourLenghtCum_sh+= d_distance[current_city*number_of_cities+beginning_city];
    d_lenghtList[blockIdx.x]=tourLenghtCum_sh; // Keep the lenght of the current tour
  }
}

/***********************************Baseline Implementation (JPDC). Ant=Block; random per city*********************************/
/**
  * FUNCTION: This function performs the tour generation by m ants. Each ant is represented by one block
  * each thread represents one city or set of cities. The selection procedure is implemented by launching
  * one random number per thread and performing a hierarchical reduction.
  */
__global__ void nextTour_kernel(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

  __shared__ float probabilities_sh[BLOCK_SIZE_NEXT_TOUR];
  __shared__ unsigned int tourLenghtCum;
  __shared__ unsigned int current_city_sh;
  unsigned int current_city;
  unsigned int btourindex = blockIdx.x*(number_of_cities+1);
  unsigned int tabul=0xFFFFFFFF;
  float random;
  curandState localState = state[blockIdx.x];

  //Each ant is placed in a initial random city.
  if (threadIdx.x == 0){
    random = curand_uniform(&localState);
    current_city_sh = (unsigned int)number_of_cities*random;
    tourLenghtCum=0;
    d_tour[btourindex] = current_city_sh;
    //printf("I am block %d y la ciudad inicial es %d\n", blockIdx.x, current_city_sh);
  }

  __syncthreads();

  current_city = current_city_sh;
  unsigned int beginning_city = current_city;

  setCityAsVisited (beginning_city, &tabul);

  __syncthreads();

  //Repeat until tabu list is full
  for (int tourIndex=1; tourIndex<number_of_cities;++tourIndex){

    int indexChoiceInfo = current_city*number_of_cities;
    float current_prob = 0.0f;
    int indexaux;
   // printf ("EL numero de iteraciones es %d\n",iterathreads-1);
    for (int i=0; i<iterathreads-1; i++){

      indexaux = threadIdx.x+(i*blockDim.x);
      unsigned int auxtabul = tabul>>i; //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int) MACROBITS & auxtabul; //auxbits debe ser 0 o 1.
      float choice = choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float) choice*auxtabul*random;

      //printf("I am thread %d y auxtabul es %d, choice es %f y random %f y la probabilidad es %f\n", threadIdx.x, auxtabul, choice, random, probabilities_sh[threadIdx.x]);

      __syncthreads();

      unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);
      float prob_local = probabilities_sh[0];
      ////printf ("Para la ant %d la prob que se obtiene de reduccion es %f\n", blockIdx.x, prob_local);
      if (prob_local>=current_prob) {
  	    current_prob = prob_local;
  	    current_city = aux_city;
      }
    }

    indexaux = threadIdx.x+((iterathreads-1)*blockDim.x);
    if (indexaux < number_of_cities){
      unsigned int auxtabul = tabul >>(iterathreads-1); //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int)MACROBITS & auxtabul; //auxbits debe ser 0 o 1.
      float choice= choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float)choice*auxtabul*random;
    }
    __syncthreads();
    unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);
    float prob_local = probabilities_sh[0];
    if (prob_local>=current_prob) {
      current_prob = prob_local;
      current_city = aux_city;
    }
    __syncthreads();

    setCityAsVisited (current_city, &tabul);
    if (threadIdx.x==0){
      d_tour[btourindex+tourIndex]=current_city; //Adding the city to the tour
      tourLenghtCum+= d_distance[indexChoiceInfo+current_city];
    }
    __syncthreads();
  }

  //Complete the tour getting the beginning city
  if (threadIdx.x==0) {
    d_tour[btourindex+number_of_cities]=beginning_city;
    tourLenghtCum+= d_distance[current_city*number_of_cities+beginning_city];
    d_lenghtList[blockIdx.x]=tourLenghtCum; // Keep the lenght of the current tour
    //printf ("Para la ant %d La longitud del camino es %d\n", blockIdx.x,tourLenghtCum);
  }

}



/**
  * This kernel performs the pheromone stage
  */
__global__ void pheromone_update_with_atomic (float * d_pheromone, const unsigned int number_of_cities, unsigned int * d_tour, const unsigned int number_of_ants, unsigned int * d_lenghtList, int ite){

  const int xIndex = blockIdx.x* (number_of_cities+1) + threadIdx.x;
  int index,indexThread;

  for (int i=0; i<ite; i++) {
    indexThread = i*blockDim.x;
    index =xIndex+(indexThread);
    if ((threadIdx.x+indexThread)<number_of_cities){
        unsigned int coordx=d_tour[index];
        unsigned int coordy=d_tour[index+1];
        unsigned int pos = (coordy*number_of_cities)+coordx;
        unsigned int posim = (coordx*number_of_cities)+coordy;
        float pheromone = 0.0;
        pheromone=(float)1.0*(1.0/d_lenghtList[blockIdx.x]);
        d_pheromone[pos]+=pheromone;
        d_pheromone[posim]+=pheromone;
    }
  }
}

void deviceQuery () {

    // Number of CUDA devices
    int devCount;
    cudaGetDeviceCount(&devCount);
    printf("CUDA Device Query...\n");
    printf("There are %d CUDA devices.\n", devCount);

    //First, we check how many threads can be allocated per block
    cudaDeviceProp devProp;
    for (int i=0; i<devCount; ++i){
        cudaGetDeviceProperties(&devProp,i);
        printf("Major revision number:         %zd\n",  devProp.major);
        printf("Minor revision number:         %zd\n",  devProp.minor);
        printf("Name:                          %s\n",  devProp.name);
        printf("Total global memory:           %zd\n",  devProp.totalGlobalMem);
        printf("Total shared memory per block: %zd\n",  devProp.sharedMemPerBlock);
        printf("Total registers per block:     %zd\n",  devProp.regsPerBlock);
        printf("Warp size:                     %zd\n",  devProp.warpSize);
        printf("Maximum memory pitch:          %zd\n",  devProp.memPitch);
        printf("Maximum threads per block:     %zd\n",  devProp.maxThreadsPerBlock);
        for (int i = 0; i < 3; ++i)
            printf("Maximum dimension %d of block:  %zd\n", i, devProp.maxThreadsDim[i]);
        for (int i = 0; i < 3; ++i)
            printf("Maximum dimension %d of grid:   %zd\n", i, devProp.maxGridSize[i]);
        printf("Clock rate:                    %zd\n",  devProp.clockRate);
        printf("Total constant memory:         %zd\n",  devProp.totalConstMem);
        printf("Texture alignment:             %zd\n",  devProp.textureAlignment);
        printf("Concurrent copy and execution: %s\n",  (devProp.deviceOverlap ? "Yes" : "No"));
        printf("Number of multiprocessors:     %zd\n",  devProp.multiProcessorCount);
        printf("Kernel execution timeout:      %s\n",  (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
        printf("*************************************************\n");
    }


}

/***** External function to launch the kernels **********/
int antSystemTSP (problem instance, colony colony){

    dim3 grid_choice, block_choice;
    dim3 grid_tour, block_tour;
    dim3 grid_phero, block_phero;
    dim3 grid_random, block_random;
    unsigned int iterationsInTour = 0;
    unsigned int iterationsInPhero = 0;
    size_t mem_size_llist = (colony.n_ants)*sizeof(unsigned int);
    int i,j;
    unsigned int bestLength= UINT_MAX; // the length of the best tour so far
    unsigned int bestIt=0; // Length of the tour obtained in the current iteration
    unsigned int mem_size_seed = colony.n_ants*sizeof(curandState);
    unsigned int mem_size_h_tours = (instance.n+1) * colony.n_ants * sizeof (unsigned int);
    unsigned int * h_tours = (unsigned int *) malloc (mem_size_h_tours);
    unsigned int * h_bestTour = (unsigned int *) malloc ((instance.n+1)*sizeof(unsigned int));
    pdebug ("In the launching kernel function");
    curandState * devStates;
    #ifdef DEBUG
    deviceQuery();
    #endif
   
    
    #ifdef TIMING
    // Event creation
    cudaEvent_t start, stop; // events
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    float elapsedTime, total = 0.0f;
    #endif

    //initialize_random_numbers(devStates, colony.n_ants);

    CHECK_CUDA(cudaMalloc ((void **)&devStates, mem_size_seed)); 

    setting_kernel_parameters(&grid_random, &block_random, colony.n_ants, BLOCK_SIZE_CHOICE);
    setup_kernel<<<grid_random,block_random>>> (devStates, colony.n_ants);

    setting_kernel_parameters(&grid_choice, &block_choice, instance.n*instance.n, BLOCK_SIZE_CHOICE);
    setting_kernel_parameters_tour(&grid_tour, &block_tour, &iterationsInTour, instance.n, colony.n_ants);
    setting_kernel_parameters_phero(&grid_phero, &block_phero, &iterationsInPhero, instance.n, colony.n_ants);

    pdebug ("Compute Total parameters are number of blocks %d number of threads %d\n", grid_choice.x, block_choice.x);
    pdebug ("Next Tour parameters: blocks %d, threads %d, number of tiles %d\n", grid_tour.x, block_tour.x, iterationsInTour);
    pdebug ("Pheromone kernel parameters are number of blocks %d number of threads %d\n", grid_phero.x, block_phero.x);

    for (i = 0; i < colony.n_tries; ++i) {

        CHECK_CUDA(cudaMemset (d_pheromone,0, instance.n*instance.n*sizeof(float))); //Reset Pheromone matrix
        pdebug ("------Running the %d try, consisting on %d tours---------------\n ", i, colony.n_tours);
        for (j = 0; j< colony.n_tours; ++j){
            
            computeTotalkernel<<<grid_choice, block_choice>>>(d_total, d_pheromone, d_distance, instance.n, colony.alpha, colony.beta,colony.rho);
            cudaThreadSynchronize();          
            #ifdef TIMING
            cudaEventRecord(start,0);
            #endif

            nextTour_kernel<<< grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour);
            
            #ifdef TIMING        
            cudaDeviceSynchronize();
            cudaEventRecord (stop);
            cudaEventSynchronize(stop);

            cudaEventElapsedTime(&elapsedTime, start, stop);
            printf ("Elapsed Time is %f\n", elapsedTime);
            total+=elapsedTime;
            #endif

            CHECK_CUDA(cudaMemcpy(colony.lengthList, d_lengthList, mem_size_llist, cudaMemcpyDeviceToHost));

            #ifdef DEBUG
            CHECK_CUDA(cudaMemcpy(h_tours, d_tour, mem_size_h_tours, cudaMemcpyDeviceToHost));
            checkForErrors (h_tours, instance.n, colony.n_ants);
            #endif 

            bestIt = getLenghtIterationBestTour(colony.lengthList, colony.n_ants, d_tour, d_bestTour,&bestLength,instance.n, j);
            
            pheromone_update_with_atomic<<<grid_phero, block_phero>>> (d_pheromone, instance.n, d_tour, colony.n_ants, d_lengthList, iterationsInPhero);
            cudaThreadSynchronize();
        }
    }
    printf ("The best tour found is:\n");
    cudaMemcpy (h_bestTour, d_bestTour, (instance.n+1)*sizeof(unsigned int), cudaMemcpyDeviceToHost);
    for (int k=0; k<(instance.n+1); ++k) {
        printf ("%d, ", h_bestTour[k]);
    }
    printf ("\n");
    
    #ifdef TIMING
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    #endif
    CHECK_CUDA(cudaFree (devStates));
    return bestLength;
}
