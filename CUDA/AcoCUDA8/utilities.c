/*

       AAAA    CCCC   OOOO   TTTTTT   SSSSS  PPPPP
      AA  AA  CC     OO  OO    TT    SS      PP  PP
      AAAAAA  CC     OO  OO    TT     SSSS   PPPPP
      AA  AA  CC     OO  OO    TT        SS  PP
      AA  AA   CCCC   OOOO     TT    SSSSS   PP

######################################################
####ACO algorithm tailored to GPUs for the TSP #######
######################################################

      Version: 1.0
      File:    utilities.c
      Author:  Jose M. Cecilia
      Purpose: some additional useful procedures
      Check:   README and gpl.txt
      Copyright (C) 2013  Jose M. Cecilia
*/

/***************************************************************************

    Program's name: gpuacotsp

    Ant Colony Optimization algorithms (AS, ACS, EAS, RAS, MMAS, BWAS) for the
    symmetric TSP

    Copyright (C) 2013 Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain
***************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "InOut.h"
#include "utilities.h"



long int seed = 12345678;


__host__ void setting_kernel_parameters (dim3 * grid, dim3 * block, unsigned int reference, unsigned int block_size)
{

    int num_blocks, num_threads, rounding;

    num_threads = (reference <= block_size)?reference:block_size;
    num_blocks = ((reference%num_threads) == 0)?(reference/num_threads):(reference/num_threads)+1;

    if (num_blocks>=MAX_BLOCKS) {
        rounding= (int)ceil(sqrt(num_blocks));
        grid->x=rounding;
        grid->y=rounding;
        block->x = num_threads;
    }
    else {
        grid->x=num_blocks;
        block->x = num_threads;
    }
}


/**
  * This function sets the kernel parameter for the tour kernel.
  * We set one ant per warp so we are gonna have numberofants x WARP_SIZE
  * threads per block.
  */
__host__ void setting_kernel_parameters_tour(dim3 *grid_tour, dim3 *block_tour, unsigned int * iterationsInTour, int  n, int n_ants)
{

/*    //We assign 1 block per ant and as many threads as cities or fixed number of them
    if (((BLOCK_SIZE_NEXT_TOUR % WARP_SIZE) != 0) ) {
         (stderr, "The number of threads per block in the next tour kernel must be multiple of the WARP_SIZE\n");
        exit (-1);
    }

    if (BLOCK_SIZE_NEXT_TOUR/WARP_SIZE != NUM_ANTS) {
        fprintf (stderr, "The NUM_ANTS MACRO SHOULD BE BLOCK_SIZE/WARP_SIZE\n");
        exit (-1);
    }
*/

    //The number of ants per block is given by the number of threads per block in the next tour
//    int antsPerBlock = BLOCK_SIZE_NEXT_TOUR / WARP_SIZE;

    int antsPerBlock = BLOCK_SIZE_NEXT_TOUR / SUPER_WARP;
    grid_tour->x = ceil((float)n_ants/antsPerBlock);
    block_tour->x = BLOCK_SIZE_NEXT_TOUR;

    //Number of iterations each warp shall do. They should chekc all the cities
    *iterationsInTour = ceil((float)n/SUPER_WARP);
    printf ("Iterations in Kernel Tour %d, number of cities %d, ants per block %d  and number of blocks %d\n", *iterationsInTour, n, antsPerBlock,grid_tour->x);
}

/**
  * This function sets the kernel parameter of pheromone update
  * kernel. We set one block per each ant and a thread per each
  * couples of cities.
  */
__host__ void setting_kernel_parameters_phero(dim3 * grid_phero, dim3 *block_phero, unsigned int * iterationsInPhero, int n, int n_ants){

    grid_phero->x = n_ants;
    block_phero->x  = (n<BLOCK_SIZE_ATOMIC_DEVICE)?n:BLOCK_SIZE_ATOMIC_DEVICE;
    *iterationsInPhero= (unsigned int)ceil((float)n/BLOCK_SIZE_ATOMIC_DEVICE);
    pdebug ("Threads in the Pheromone Update with atomic instructions kernel 6 (x)= (%d) and blocks (%d). Number of iterations per block is %d", block_phero->x, grid_phero->x, *iterationsInPhero);
}


/**
* It returns the best in the current iteration,
* and controls the best so far, doing a copy in device memory
* for the best tour
*/
__host__ unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, unsigned int * d_tour, unsigned int * d_best, unsigned int * bestLenght, unsigned int number_of_cities)
{

  unsigned int bestIt=lenght[0];
  unsigned int antBest = 0;
  unsigned int i;
  for (i=1; i < number_of_ants; ++i) {
    if (lenght[i]<bestIt) {
        bestIt = lenght[i];
        antBest = i;
    }
  }

  unsigned int * aux = d_tour;
  aux=aux+antBest*(number_of_cities+1);
  if (bestIt < *bestLenght) {
    *bestLenght = bestIt;
    cudaMemcpy(d_best, aux, (number_of_cities+1)*sizeof(unsigned int), cudaMemcpyDeviceToDevice);
  }

  return bestIt;
}



