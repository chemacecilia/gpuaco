/*

       AAAA    CCCC   OOOO   TTTTTT   SSSSS  PPPPP
      AA  AA  CC     OO  OO    TT    SS      PP  PP
      AAAAAA  CC     OO  OO    TT     SSSS   PPPPP
      AA  AA  CC     OO  OO    TT        SS  PP
      AA  AA   CCCC   OOOO     TT    SSSSS   PP

######################################################
##### ACO algorithm tailored to GPUs for the TSP####
######################################################

      Version: 1.0
      File:    utilities.c
      Author:  Jose M. Cecilia
      Purpose: some additional useful procedures
      Check:   README and gpl.txt
      Copyright (C) 2013  Jose M. Cecilia
*/

/***************************************************************************

    Program's name: gpuacotsp

    Ant Colony Optimization algorithms (AS, ACS, EAS, RAS, MMAS, BWAS) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. ceciliae

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: stuetzle no@spam informatik.tu-darmstadt.de
    mail address: Universitaet Darmstadt
                  Fachbereich Informatik
                  Hochschulstr. 10
                  D-64283 Darmstadt
		  Germany

***************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "InOut.h"
#include "utilities.h"



long int seed = 12345678;


__host__ void setting_kernel_parameters (dim3 * grid, dim3 * block, unsigned int reference, unsigned int block_size)
{

    int num_blocks, num_threads, rounding;

    num_threads = (reference <= block_size)?reference:block_size;
    num_blocks = ((reference%num_threads) == 0)?(reference/num_threads):(reference/num_threads)+1;

    if (num_blocks>=MAX_BLOCKS) {
        rounding= (int)ceil(sqrt(num_blocks));
        grid->x=rounding;
        grid->y=rounding;
        block->x = num_threads;
    }
    else {
        grid->x=num_blocks;
        block->x = num_threads;
    }
}


/**
  * This function sets the kernel parameter for the tour kernel.
  * We set one block per each ant and a thread per each
  * city.
  */
__host__ void setting_kernel_parameters_tour(dim3 *grid_tour, dim3 *block_tour, unsigned int * iterationsInTour, int  n, int n_ants)
{
    //We assign 1 block per ant and as many threads as cities or fixed number of them
    grid_tour->x = n_ants;
    block_tour->x = BLOCK_SIZE_NEXT_TOUR;
    if (log2(BLOCK_SIZE_NEXT_TOUR) != LOG_BLOCK_SIZE_NEXT_TOUR){
        printf ("You should change the LOG_BLOCK_SIZE_NEXT_TOUR MACRO in utilities.h\n");
        exit (1);
    }
    *iterationsInTour = ceil((float)n/BLOCK_SIZE_NEXT_TOUR);
    pdebug("Setting parameter for the Next tour kernel. Number of threads is %d , blocks %d and iterations per block %d\n", block_tour->x, grid_tour->x, *iterationsInTour);
}

/**
  * This function sets the kernel parameter of pheromone update
  * kernel. We set one block per each ant and a thread per each
  * couples of cities.
  */
__host__ void setting_kernel_parameters_phero(dim3 * grid_phero, dim3 *block_phero, unsigned int * iterationsInPhero, int n, int n_ants){

    grid_phero->x = n_ants;
    block_phero->x  = (n<BLOCK_SIZE_ATOMIC_DEVICE)?n:BLOCK_SIZE_ATOMIC_DEVICE;
    *iterationsInPhero= (unsigned int)ceil((float)n/BLOCK_SIZE_ATOMIC_DEVICE);
    pdebug ("Threads in the Pheromone Update with atomic instructions kernel 6 (x)= (%d) and blocks (%d). Number of iterations per block is %d", block_phero->x, grid_phero->x, *iterationsInPhero);
}


/**
* It returns the best in the current iteration,
* and controls the best so far, doing a copy in device memory
* for the best tour
*/
__host__ unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, unsigned int * d_tour, unsigned int * d_best, unsigned int * bestLenght, unsigned int number_of_cities, int iter)
{

  unsigned int bestIt=lenght[0];
  unsigned int antBest = 0;
  unsigned int i;
  for (i=1; i < number_of_ants; ++i) {
    if (lenght[i]<bestIt) {
        bestIt = lenght[i];
        antBest = i;
    }
  }

  unsigned int * aux = d_tour;
  aux=aux+antBest*(number_of_cities+1);
  if (bestIt < *bestLenght) {
    printf ("Best Lenght updated: New value %d given by ant %d at iteration %d\n", bestIt, antBest, iter);
    *bestLenght = bestIt;
    cudaMemcpy(d_best, aux, (number_of_cities+1)*sizeof(unsigned int), cudaMemcpyDeviceToDevice);
  }

  return bestIt;
}

/**It checks erros in the generation of tours
 * Actually a city previously visted is visted again
 */
__host__ void checkForErrors (unsigned int * h_tours, int n, int m){

            int rep = -1, j, k, l;
            for (j = 0; j < m; ++j){
                for (k=0; k < n; ++k) {
                    for (l=k+1; (l < n); ++l){
                        if (h_tours[k+j*(n+1)] == h_tours[l+j*(n+1)]){
                            rep = h_tours[k+j*(n+1)];
                        }
                    }
                    if (rep!=-1)
                        printf ("%d, ",h_tours[k+j*(n+1)]);
                }
                if (rep!= -1){
                    printf ("\nThe element %d is repeated", rep);
                    rep =-1;
                }
                printf ("\n");
            }

}



















