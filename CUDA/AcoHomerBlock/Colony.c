/***************************************************************************
    File Purpose: implementation of procedures for ant's behavious in the colony
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <limits.h>
#include <time.h>
#include <cuda_runtime.h>
#include "InOut.h"
#include "utilities.h"

float   * pheromone; /* pheromone matrix, one entry for each arc */
float   * total;     /* combination of pheromone times heuristic information */
float   * d_pheromone; /* Pheromone matrix on the device */
float   * d_total; /* total information on the GPU */
unsigned int * d_tour; /* Tours ants have taken in the current tour*/
unsigned int * lengthList; /* List of lengths of the tours taken by the ants*/
unsigned int * d_lengthList; /* List of lengths on the GPU*/
unsigned int * h_bestTour; /* The best tour so far*/
unsigned int * d_bestTour; /*The best tour so far on the GPU*/
int n_ants; /* number of ants in the colony*/
float rho; /* parameter for evaporation*/
float alpha; /* importance of trail*/
float beta; /* Importance of heuristic value*/
int n_tries; /* Number of tries */


/**
* Allocates pinned memory in the host
*/
void host_memory_allocation (void ** ptr, size_t size){

    cudaError_t err;
    printf("\nhost size:%d\n",size);
    if ((err=cudaMallocHost(ptr, size))!= cudaSuccess) {
        fprintf(stderr, "Error Host Memory Allocation %d\n", err);
        exit(1);
    }
}

/**
* Allocate device memory
*/
void device_memory_allocation (void ** ptr, size_t size) {

    cudaError_t err;
    printf("\ndevice size:%d\n",size);
    if ((err=cudaMalloc(ptr, size))!= cudaSuccess) {
        fprintf(stderr, "Error Device Memory Allocation %d\n", err);
        exit(1);
    }
}

/**
* Transfer memory from/to CPU from/to GPU
*/
void memory_transfer (float * destiny, float * source, size_t size, enum cudaMemcpyKind dir) {

    cudaError_t err;
    if ((err=cudaMemcpy(destiny, source, size, dir))!= cudaSuccess) {
        fprintf(stderr, "Error Memory Transfer %d\n", err);
        exit(1);
    }
}

/**
* Initialize pheromone trails and total or choice information on both
* CPU and GPU. The memory is pinned memory.
*/
void init_pheromone_trails ( float initial_trail, struct problem * problem, struct colony * colony){

    unsigned int i;
    unsigned int size = problem->n * problem->n;
    size_t  memsize = sizeof(float) * size;

    pdebug (".....Initializating pheromone trails on the CPU and GPU.....");
    host_memory_allocation ((void **)&colony->pheromone, memsize);
    host_memory_allocation ((void **)&colony->total, memsize);
    device_memory_allocation ((void **)&d_pheromone, memsize);
    device_memory_allocation ((void **)&d_total,memsize);

    //Initialize pheromone trails
    for (i = 0; i < size; ++i){
            colony->pheromone[i] = initial_trail;
            colony->total[i] = initial_trail;
    }

    pdebug("Memory copy of Pheromone and Total from the CPU to the GPU");
    memory_transfer (d_pheromone, colony->pheromone, memsize, cudaMemcpyHostToDevice);
    memory_transfer (d_total, colony->total, memsize, cudaMemcpyHostToDevice);

    pdebug (".....Done.....");
}

/*
void compute_total_information( void ) {

    unsigned int i, j,index = 0;


    pdebug (".....Computing total information.....");

    for (i = 0; i < instance.n; ++i) {
        for (j = 0; j< i; ++j){
            total[index] = pow (pheromone[index], alpha) * pow (HEURISTIC(i,j), beta);
            total[index] = total[index];
            ++index;
        }
        index+=instance.n;
    }

    pdebug (".....Done...........");
}
*/

/**
* This function initializes the structures needed by the ants
* to keep track of the tour information and their length
*/
void init_tour_information (struct problem * problem, struct colony * colony) {

    unsigned int memsize_tour = sizeof(unsigned int) * colony->n_ants * (problem->n+1);
    unsigned int memsize_lengthList = sizeof(unsigned int) * colony->n_ants;
    unsigned int memsize_bestTour = sizeof(unsigned int) * (problem->n+1);

    device_memory_allocation ((void **)&d_tour, memsize_tour);
///////    printf("\n*******\nmemsize_lengthList:%d",memsize_lengthList);
    host_memory_allocation((void**)&colony->lengthList, memsize_lengthList);
    device_memory_allocation ((void **)&d_lengthList, memsize_lengthList);
    device_memory_allocation ((void **)&d_bestTour, memsize_bestTour);
    //Need add best tour information
}
