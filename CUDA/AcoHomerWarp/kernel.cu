/*
 * Version: 1.0
 * File: kernel.cu
 * Author: Jose M. Cecilia
 * Purpose: TheGPU kernels to manage the kernel execution
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. cecilia
 */
/***************************************************************************
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain

*******************************************************************************/
#include <sys/time.h>
#include <stdio.h>
#include <curand_kernel.h>
#include "kernel.h"
#include "utilities.h"

// Print device properties
void printDevProp(cudaDeviceProp devProp){
    printf("Major revision number:         %d\n",  devProp.major);
    printf("Minor revision number:         %d\n",  devProp.minor);
    printf("Name:                          %s\n",  devProp.name);
    printf("Total global memory:           %d\n",  devProp.totalGlobalMem);
    printf("Total shared memory per block: %d\n",  devProp.sharedMemPerBlock);
    printf("Total registers per block:     %d\n",  devProp.regsPerBlock);
    printf("Warp size:                     %d\n",  devProp.warpSize);
    printf("Maximum memory pitch:          %d\n",  devProp.memPitch);
    printf("Maximum threads per block:     %d\n",  devProp.maxThreadsPerBlock);
    for (int i = 0; i < 3; ++i)
        printf("Maximum dimension %d of block:  %d\n", i, devProp.maxThreadsDim[i]);
    for (int i = 0; i < 3; ++i)
        printf("Maximum dimension %d of grid:   %d\n", i, devProp.maxGridSize[i]);
    printf("Clock rate:                    %d\n",  devProp.clockRate);
    printf("Total constant memory:         %d\n",  devProp.totalConstMem);
    printf("Texture alignment:             %d\n",  devProp.textureAlignment);
    printf("Concurrent copy and execution: %s\n",  (devProp.deviceOverlap ? "Yes" : "No"));
    printf("Number of multiprocessors:     %d\n",  devProp.multiProcessorCount);
    printf("Kernel execution timeout:      %s\n",  (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
    return;
}


/****************** CUDA Kernels ***************/

/**
  * This kernel set ups the generation of random number on the GPU
  * It sets a different seed for each thread
  */
__global__ void setup_kernel(curandState *state, unsigned int m) {

  int id = threadIdx.x + blockIdx.x * blockDim.x; /* Each thread gets same seed, a different sequence number,
						     no offset */
  if (id<m)
    curand_init(1234*blockIdx.x, id, 0, &state[id]);
}


/**
  * Kernel to compute the total information
  * and it also performs the evaporation stage.
  */
__global__ void computeTotalkernel (float *d_choiceInfo, float *d_pheromone, unsigned int * d_distance, unsigned int n, float alpha, float beta, float rho)
{

    int tid = (blockIdx.y*blockDim.y*n)+(blockDim.x*blockIdx.x)+threadIdx.x;

    //if (tid== 0) printf ("Prueba con tid %d,", tid);
    if (tid<n*n){
        float pheromone;
        pheromone = d_pheromone[tid];
        float heuristic = 1.0 / ((float) d_distance[tid] + 0.1);
        d_choiceInfo[tid]=__powf(pheromone, alpha)*__powf(heuristic, beta);
        //Evaporation stage
        pheromone= pheromone*(1.0-rho);
        d_pheromone[tid] = pheromone;
    }
}



/*
	FUNCTION:       This function obtains the maximum of the array and the index of that maximum
	INPUT:          pointer to the probabilistic information and the relative index
	OUTPUT:         Relative index of the maximum probability
	(SIDE)EFFECTS:  The maximum probability per each ant is in the line 0
*/

__device__ inline void reduceMax(volatile float *g_idata, volatile unsigned int * index_sh, unsigned int index, unsigned int n, int line_id)
{

  index_sh[threadIdx.x]= index;
  g_idata[threadIdx.x] = (index>=n)?0.0:g_idata[threadIdx.x];

  __syncthreads();

  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
    if (line_id < s){
      if(g_idata[threadIdx.x] < g_idata[threadIdx.x + s]) {
	    g_idata[threadIdx.x] = g_idata[threadIdx.x + s];
	    index_sh[threadIdx.x] = index_sh[threadIdx.x + s];
      }
    }
    __syncthreads();
  }
}


/**
 * This funtion set a city as a visited in a bitwise
 */
__device__ inline void setCityAsVisited(unsigned int city, unsigned long long int * tabul){

  //Division by 64 is the bit that represents that city.
  unsigned int bitTabul = (unsigned int) city>>5;
  //Modulo is the thread that manages that city. Modulo 64 as we are dealing with WARPS
  unsigned int threadTabul = (unsigned int) city& 0x1f;
  unsigned int lane_id = threadIdx.x&0x1f;

  // Set the city as a visited in the corresponding threads
  if (lane_id == threadTabul) {
   //     printf ("En setCityAsvisited thread %d is setting in the bit %d\n", threadTabul,bitTabul);
    unsigned long long int aux = (unsigned long long int) MACROBITS<<bitTabul;
    *tabul = *tabul & (~aux);
  }
}

/**
 * Reduction
 */
__device__ inline void reduction(volatile float *g_idata, unsigned int n, unsigned int index){

  int tid = threadIdx.x;
  int lane_id = threadIdx.x & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = (warp_id*WARP_SIZE)+lane_id;

  g_idata[warpIndex] = (index >= n) ? 0.0f : g_idata[warpIndex];

  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
    if (lane_id < s){
	    g_idata[warpIndex] += g_idata[warpIndex + s];
      }
    }
}


/**
 * Reduction
 */
__device__ inline void reduction_shfl(float * prob, unsigned int n, unsigned int index){

#if __CUDA_ARCH__ >=350
  float aux;
  *prob = (index >= n) ? 0.0f : *prob;

  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
	    aux = __shfl_down(*prob, s);
        *prob+=aux;
    }
#endif
}


/**
 * Simple parallel scan of 2exp(k) elements with 2exp(k) threads
 */
inline __device__ void simplescan_shfl(volatile float * idata, int index, int n, int numBlocks){

#if __CUDA_ARCH__ >=350
    int tid = threadIdx.x;
    int lane_id = tid & 0x1f;
    int warp_id = tid >> 5;
    int warpIndex = warp_id*WARP_SIZE*numBlocks;

    float interpolate = 0.0f;

    for (int i=lane_id; i<(numBlocks*WARP_SIZE); i+=WARP_SIZE){
        
        float value = ((index+i)<n) ? idata[warpIndex+i]:0.0f;//Some cities may be null
        value = (lane_id==0)? value+interpolate:value;

        for (int offset = 1; offset < WARP_SIZE; offset <<= 1) {
            float temp= __shfl_up(value,offset);
            if (lane_id >= offset)
                 value+=temp;
        }
    
        idata[warpIndex+i]=value;
        interpolate = __shfl(value, WARP_SIZE-1);
    }
#else 
    printf ("You shall compile with -arch>=35\n");
    exit(-1);
#endif
}

/**
 * Simple parallel scan of 2exp(k) elements with 2exp(k) threads
 */
inline __device__ void simplescan(volatile float * idata, int index, int n){

    int tid = threadIdx.x;
    int lane_id = tid & 0x1f;
    int warp_id = tid >> 5;
    int warpIndex = (warp_id*WARP_SIZE)+lane_id;

    idata[warpIndex] = (index<n) ? idata[warpIndex]:0.0f;//Some cities may be null

    if (index < n) {
        for (int offset = 1; offset < n; offset <<= 1) {
            float temp;
            if (lane_id >= offset)
               temp = idata[warpIndex-offset];
         __syncthreads();
            if (lane_id >= offset)
                idata[warpIndex]+=temp;
            __syncthreads();
        }
    }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * This function do a stencil check in order to select a city. Only
 * one thread will be in the frontier between positive and negative
 * values.
 */
__device__ inline bool stencilCheck (volatile float * sidata, float p, int n ,  int index) {

    int tid = threadIdx.x;
    int lane_id = tid & 0x1f;
    int warp_id = tid >> 5;
    int warpIndex = (warp_id*WARP_SIZE);

    bool check = false;
    //Stencil to check which city to go next. Only one thread can get in.
    //Thread 0 should be stencil
    for (int i = lane_id; i < n; i+=WARP_SIZE){
        float left_value = (i > 0) ? sidata[(warpIndex+i)-1] : 0.0f; // Prepare the stencil for thread 0.
        left_value -= p;
        float center_value = sidata[warpIndex+i] - p;
        check = ((left_value * center_value)<= 0);
    }
    return check;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/******************************* TOUR IMPLEMENTATIONS *********************************************/
/////////////////////////////////////////////////////////////////////////////////////////////////////


/**
  * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions
  */
__global__ void nextTour_kernel_warp_iroulette_k20(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads, int * proximaciudad){

#if __CUDA_ARCH__ >=350

  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced

  float random; 
  int next_city, city_ini;
  unsigned int tourLenght;
  unsigned long long int tabul1=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  unsigned long long int tabul2=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = warp_id*WARP_SIZE;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;

  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
        localState = state[globalIndex];
        random = curand_uniform(&localState);
        next_city = (int) number_of_cities*random;
        tour_sh[warpIndex] = next_city;
        tourLenght = 0;
    }

    next_city = __shfl(next_city,0);
    city_ini = next_city;
    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
        setCityAsVisited (next_city, &tabul1);
    else 
        setCityAsVisited ((next_city-2048),&tabul2);

    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){

        int indexChoiceInfo = next_city*number_of_cities;
        float choice;
        int indexaux;//Number of iterations
        unsigned int auxtabul;

        //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
        if ((tourIndex & 0x1f) == 0) {
            int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
            d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
        }

        indexaux=0;
        
        float probabilities = 0.0f;
        float maxProbabilities = 0.0f;
        int maxIndex=lane_id;
        //Iterate over all cities, in tiles of 32-width
        for (int index=lane_id; index < number_of_cities; index+=WARP_SIZE){
            //Calculates the values for the chunk
            auxtabul = (index<2048)?tabul1>>indexaux:tabul2>>(indexaux&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
            choice = choice_info[indexChoiceInfo+index];
            random = curand_uniform(&localState);     
            probabilities = (float) choice*auxtabul*random;
        
            if (probabilities > maxProbabilities) {
                maxIndex=index;
                maxProbabilities=probabilities;
            }
            indexaux++;
        }

        //Now, we have the maximum values of probabilities and the city associated with it for a 32-chunk
        float auxshfl;
        int auxMaxIndex;
        auxMaxIndex = __shfl_xor(maxIndex,16); 
        auxshfl = __shfl_xor(maxProbabilities, 16);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }
  
        auxMaxIndex = __shfl_xor(maxIndex,8); 
        auxshfl = __shfl_xor(maxProbabilities, 8);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }

        auxMaxIndex = __shfl_xor(maxIndex,4); 
        auxshfl = __shfl_xor(maxProbabilities, 4);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }

        auxMaxIndex = __shfl_xor(maxIndex,2); 
        auxshfl = __shfl_xor(maxProbabilities, 2);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }
        
        auxMaxIndex = __shfl_xor(maxIndex,1); 
        auxshfl = __shfl_xor(maxProbabilities, 1);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }
        
        next_city = __shfl(maxIndex,0);
         
        if (next_city < 2048){
            setCityAsVisited (next_city, &tabul1);         
        }
        else { 
            setCityAsVisited((next_city-2048),&tabul2);
        }
             
        if (lane_id == 0 ) {
            tourLenght += d_distance[indexChoiceInfo+next_city];
            tour_sh[warpIndex+(tourIndex& 0x1f)] = next_city;                
        }
     
    }//END FOR: ALL CITIES ARE VISITED

    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

    if (indexaux < number_of_cities){
        int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
        d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
    }

    //Complete the tour getting the beginning city
    if (lane_id == 0) {     
        state[globalIndex] = localState;
        tourLenght += d_distance[next_city*number_of_cities+city_ini];
        d_lenghtList[globalIndex] = tourLenght;
        int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
        d_tour[indexdtour] = city_ini;
    }
  //  if (lane_id == 0)
 	proximaciudad[globalIndex]=next_city;

	if ((lane_id == 0)&&(globalIndex==0)){
		printf("%d <-- citywarp",next_city);
	}
  }//END IF 

#else 
    //printf ("You shall compile with -arch>=35 make arch=1\n");
    //exit(-1);
#endif
}



/**
  * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions
  */
__global__ void nextTour_kernel_warp_reducing_random_k20(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

#if __CUDA_ARCH__ >=350

  __shared__ volatile float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  extern __shared__ volatile float partial_sums_sh[];//This can be NUMWARPS*iterationsInTour with
  __shared__ volatile unsigned int current_chunk_sh[NUM_ANTS];

  float random,p; 
  unsigned int next_city, city_ini, current_chunk;
  unsigned int tourLenght;
  unsigned long long int tabul1=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  unsigned long long int tabul2=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  float probabilities = 0.0f;
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = warp_id*WARP_SIZE;
  unsigned int chunkPartialSum = ceil((float)iterathreads/WARP_SIZE);///1
  unsigned int warpPartialSumIndex = warp_id*chunkPartialSum*WARP_SIZE;///32
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;


  printf("\ntotalindex:%d\t thread.x:%d\t warp_id:%d\t lane_id:%d\t warpIndex:%d\t block.x:%d\t globlaIndex:%d\t",(threadIdx.x+(blockIdx.x*BLOCK_SIZE_NEXT_TOUR)),threadIdx.x, warp_id,lane_id,warpIndex,blockIdx.x,globalIndex);

  __syncthreads();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
        localState = state[globalIndex];
        random = curand_uniform(&localState);
        state[globalIndex] = localState;
        next_city = (unsigned int) number_of_cities*random;
        tour_sh[warpIndex] = next_city;
        tourLenght = 0;
    }


    random = __shfl (random, 0);
    next_city = (int)__shfl((float)next_city,0);
    city_ini = next_city;




    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
        setCityAsVisited (next_city, &tabul1);
    else 
        setCityAsVisited ((next_city-2048),&tabul2);

    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){

        int indexChoiceInfo = next_city*number_of_cities;
        float choice;
        int indexaux;
        unsigned int auxtabul;

       // printf ("The current_city is %d\n", next_city);

        //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
        if ((tourIndex & 0x1f) == 0) {
            int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
            d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
        }



        //Iterate over all cities, in tiles of 32-width
        for (int index=0 ; index < (iterathreads-1); ++index){
            
            indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first til
            auxtabul = (indexaux<2048)?tabul1>>index : tabul2>>(index&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
            choice = choice_info[indexChoiceInfo+indexaux];
            
            probabilities = (float) choice*auxtabul;
            reduction_shfl(&probabilities, number_of_cities, indexaux);

            if (lane_id == 0){
                //////////////////// printf ("\nglobal %d The warp %d the index partial sum  %d reductions is %f in tourIteration %d\n", globalIndex, warp_id, warpPartialSumIndex, probabilities, tourIndex);
		////////////arriba
                partial_sums_sh[warpPartialSumIndex+index] = probabilities;
            }            
        }


        //The last chunk may not divisible by block size
        indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

        if (indexaux < number_of_cities){
           // auxtabul = tabul >>(iterathreads-1);
            auxtabul = (indexaux<2048)?tabul1>>(iterathreads-1) : tabul2>>((iterathreads-1)&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int)MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+indexaux];
            probabilities = (float)choice*auxtabul;
        }

        //Reduction of the rest elements
        reduction_shfl(&probabilities, number_of_cities, indexaux);

        if (lane_id == 0){
            //printf ("The %d reductions is %f \n", (iterathreads-1), probabilities);
            partial_sums_sh[warpPartialSumIndex+(iterathreads-1)] = probabilities;
        }

        // Now, scan indexes stored in partial_sums_sh

        simplescan_shfl (partial_sums_sh, 0, iterathreads, chunkPartialSum);
      

	


        //Calculate p value to do the roulette. 
        if (lane_id == 0){
            p = random * partial_sums_sh[warpPartialSumIndex+(iterathreads-1)];
         //   if (blockIdx.x==0 && warp_id==0)
           //     printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+(iterathreads-1)]);
        }
        p = __shfl(p,0);

        ///////////////////////STENCIL CHECK FOR THE CHUNK//////////////////
        ////////////////////CHUNK MAY BE BIGGER THAN 32/////////////////

        for (int chunkIndex=lane_id; chunkIndex<iterathreads; chunkIndex+=WARP_SIZE) {
            
            float left_value = (chunkIndex > 0) ? partial_sums_sh[(warpPartialSumIndex+chunkIndex)-1]:0.0f;
            left_value -=p;
            float center_value = partial_sums_sh[warpPartialSumIndex+chunkIndex] - p;
            if ((left_value * center_value) <= 0){
                current_chunk_sh[warp_id] = chunkIndex;        
                //printf ("I am the warp %d lane_id %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warp_id, lane_id, chunkIndex, left_value,warpIndex+chunkIndex-1, partial_sums_sh[warpIndex+chunkIndex-1], center_value,warpIndex+chunkIndex, partial_sums_sh[warpIndex+chunkIndex]);
            }
        }
        current_chunk = current_chunk_sh[warp_id];

        //if (lane_id ==0)
          //  printf ("The current_chunk is %d\n", current_chunk);

        //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk

        indexaux = lane_id + (current_chunk*WARP_SIZE);

        if (indexaux < number_of_cities){
            //unsigned int auxtabul = tabul>>current_chunk;

            unsigned int auxtabul = ((indexaux)<2048)?tabul1>>(current_chunk) : tabul2>>(current_chunk&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int) MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+indexaux];
            probabilities_sh[warpIndex+lane_id] = (float) choice*auxtabul;
           // printf ("For the city: Thread %d index %d prob %f and chunk %d and auxtabul %d\n", lane_id, warpIndex+lane_id, probabilities_sh[warpIndex+lane_id], current_chunk, auxtabul);
        }

        //Scan over the probabilities. The result is in probabilities_sh
        simplescan_shfl (probabilities_sh, (current_chunk*WARP_SIZE), number_of_cities, 1);
        //Calculate the total probability to proceed with the roulette selection

        p = random * probabilities_sh[warpIndex+(WARP_SIZE-1)];   
        
        //if (blockIdx.x==0 && warp_id==0)
          //  printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+WARP_SIZE-1]);
        
        //Check which city within the select chunk is being selected
        //////////////////////STENCIL CHECK to select the city//////////////////
        //////////////////////////////////////////////////////////////////////
     

///////////////////////////////////////////////////
	__syncthreads();

	printf("\n**************************************************************************************************\n,totalindex:%d\twarp_id:%d\tlane_id:%d\twarp_index:%d\tprobs[warpIndex]%f",(threadIdx.x+(blockIdx.x*BLOCK_SIZE_NEXT_TOUR)),warp_id,lane_id,warpIndex,probabilities_sh[warpIndex+lane_id]);

	__syncthreads();
	if ((threadIdx.x+(blockIdx.x*BLOCK_SIZE_NEXT_TOUR)==0)&&(tourIndex==1)) {
		for (int i=0;i<number_of_cities-1;i++){
			printf("\ni%d:\tchoice:%f\tprobs:%f\tpartialsums:%f\t random:%f,cityini:%d",i,choice_info[i],probabilities_sh[i],partial_sums_sh[i],random,city_ini);
		}
	}
////////////////////////////////////////////////////
////////  printf("\ntotalindex:%d\t thread.x:%d\t warp_id:%d\t lane_id:%d\t warpIndex:%d\t block.x:%d\t globlaIndex:%d\t",(threadIdx.x+(blockIdx.x*BLOCK_SIZE_NEXT_TOUR)),threadIdx.x, warp_id,lane_id,warpIndex,blockIdx.x,globalIndex);

///////////////////////////////////////////////////



        float left_value = (lane_id > 0) ? probabilities_sh[warpIndex+lane_id-1]:0.0f;
        left_value -=p;
        float center_value = probabilities_sh[warpIndex+lane_id] - p;
        if ((left_value * center_value) <= 0){
            tour_sh[warpIndex+(tourIndex& 0x1f)] = indexaux;                
            if (indexaux < 2048){
                setCityAsVisited (indexaux, &tabul1);         
                //printf ("Setting city as visited in tabul1\n");
            }
            else { 
                setCityAsVisited((indexaux-2048),&tabul2);
                //printf ("Setting city %d as visited in tabul2\n",indexaux-2048 );
            }
          /////printf ("\nFor the city: lane_id %d and the city is %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warpIndex+(tourIndex& 0x1f), tour_sh[warpIndex+(tourIndex& 0x1f)], left_value, warpIndex+lane_id-1, probabilities_sh[warpIndex+lane_id-1], center_value,warpIndex+lane_id, partial_sums_sh[warpIndex+lane_id]);
        }
        
        
        next_city = tour_sh[warpIndex + (tourIndex & 0x1f)];
        
       // printf ("For thread %d is Next cituy is %d\n", lane_id, next_city);
        if (lane_id == 0 ) {
            tourLenght += d_distance[indexChoiceInfo+next_city];
        }
     
    }//END FOR: ALL CITIES ARE VISITED

    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

    if (indexaux < number_of_cities){
        int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
        d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
    }

    //Complete the tour getting the beginning city
    if (lane_id == 0) {
        tourLenght += d_distance[next_city*number_of_cities+city_ini];
        d_lenghtList[globalIndex] = tourLenght;
        int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
        d_tour[indexdtour] = city_ini;
    }
if (threadIdx.x+(blockIdx.x*BLOCK_SIZE_NEXT_TOUR)==0){
printf("\nEND");}
  }//END IF 

#else 
    //printf ("You shall compile with -arch>=35 make arch=1\n");
    //exit(-1);
#endif
}


/**
  * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions.
  * This is limited to smallest benchmarks
  */
/*__global__ void nextTour_kernel_warp_reducing_random_k20_small(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

#if __CUDA_ARCH__ >=350

  __shared__ volatile float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  __shared__ volatile float partial_sums_sh[BLOCK_SIZE_NEXT_TOUR];//This can be NUMWARPS*iterationsInTour with
  __shared__ volatile unsigned int current_chunk_sh[NUM_ANTS];

  float random,p; 
  unsigned int next_city, city_ini, current_chunk;
  unsigned int tourLenght;
  unsigned int tabul=0xFFFFFFFF; //Tabulist initialize to one every bit
  float probabilities = 0.0;
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = (warp_id*WARP_SIZE) + lane_id;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;

  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.

    if (lane_id == 0){
        localState = state[globalIndex];
        random = curand_uniform(&localState);
        state[globalIndex] = localState;
        next_city = (unsigned int) number_of_cities*random;
        tour_sh[warpIndex] = next_city;
        tourLenght = 0;
    }

    random = __shfl (random, 0);
    next_city = (int)__shfl((float)next_city,0);

    city_ini = next_city;

    // Identifying the thread within each warp manages the initial city.
    setCityAsVisited(next_city, &tabul);

    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){

        int indexChoiceInfo = next_city*number_of_cities;
        float choice;
        int indexaux;
        unsigned int auxtabul;

        //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
        if ((tourIndex & 0x1f) == 0) {
            int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
            d_tour[indexdtour]= tour_sh[warpIndex];
        }

        //Iterate over all cities, in tiles of 32-width
        for (int index=0 ; index < (iterathreads-1); ++index){
            indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first tile
            auxtabul = tabul>>index; //Shift right the tabulist
            auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
            choice = choice_info[indexChoiceInfo+indexaux];
            probabilities = (float) choice*auxtabul;
            reduction_shfl(&probabilities, number_of_cities, indexaux);

            if (lane_id == 0){
                partial_sums_sh[warpIndex+index] = probabilities;
            }
        }

        //The last chunk may not divisible by block size
        indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

        if (indexaux < number_of_cities){
            auxtabul = tabul >>(iterathreads-1);
            auxtabul = (unsigned int)MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+indexaux];
            probabilities = (float)choice*auxtabul;
        }

        //Reduction of the rest elements
        reduction_shfl(&probabilities, number_of_cities, indexaux);

        if (lane_id == 0)
            partial_sums_sh[warpIndex+(iterathreads-1)] = probabilities;
    
        // Now, scan indexes stored in partial_sums_sh
        simplescan_shfl (partial_sums_sh, lane_id, iterathreads);

        //Calculate the last probabilities
        if (lane_id == (iterathreads-1))
            p = random * partial_sums_sh[warpIndex];
    
        p = __shfl(p,(iterathreads-1));

        //Check in which chunk lies the probability in
        if (stencilCheck(partial_sums_sh, p, iterathreads, lane_id)) 
            current_chunk_sh[warp_id] = lane_id;
        
        current_chunk = current_chunk_sh[warp_id];

        //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk

        indexaux = lane_id + (current_chunk*WARP_SIZE);

        if (indexaux < number_of_cities){
            unsigned int auxtabul = tabul>>current_chunk;
            auxtabul = (unsigned int) MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+indexaux];
            probabilities_sh[warpIndex] = (float) choice*auxtabul;
        }

        //Scan over the probabilities. The result is in probabilities_sh
        simplescan_shfl (probabilities_sh, indexaux, number_of_cities);
        //Calculate the total probability to proceed with the roulette selection

        p = random * probabilities_sh[warp_id*WARP_SIZE+(WARP_SIZE-1)];   
        //Check which city within the select chunk is being selected
        if (stencilCheck(probabilities_sh, p, number_of_cities, indexaux)) {
            setCityAsVisited (indexaux, &tabul);
            tour_sh[(warp_id*WARP_SIZE) + (tourIndex & 0x1f)] = indexaux;
        }

        next_city = tour_sh[(warp_id*WARP_SIZE) + (tourIndex & 0x1f)];
    
        if (lane_id == 0 ) {
            tourLenght += d_distance[indexChoiceInfo+next_city];
        }
        
    }//END FOR: ALL CITIES ARE VISITED

    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

    if (indexaux < number_of_cities){
        int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
        d_tour[indexdtour]= tour_sh[warpIndex]; 
    }

    //Complete the tour getting the beginning city
    if (lane_id == 0) {
        tourLenght += d_distance[next_city*number_of_cities+city_ini];
        d_lenghtList[globalIndex] = tourLenght;
        int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
        d_tour[indexdtour] = city_ini;
    }
  }//END IF 
#endif
}*******/

/***********************************Implementation Scan + Stencil *********************************/

/**
  * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern.
  */
/*__global__ void nextTour_kernel_warp_reducing_random(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){


  __shared__ volatile float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ volatile unsigned int tourLenghtCum_sh[NUM_ANTS]; //Tour's Lenght for each ant
  __shared__ volatile unsigned int current_city_sh[NUM_ANTS];  // Current city of each ant
  __shared__ volatile  unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  __shared__ volatile float partial_sums_sh[BLOCK_SIZE_NEXT_TOUR];//This can be NUMWARPS*iterationsInTour with
  __shared__ volatile float random_sh[NUM_ANTS];
  __shared__ volatile float p[NUM_ANTS];
  __shared__ volatile unsigned int current_chunk_sh[NUM_ANTS];


  unsigned int next_city, city_ini, current_chunk;
  unsigned int tabul=0xFFFFFFFF; //Tabulist initialize to one every bit

  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  //unsigned int warpIndex = (warp_id*WARP_SIZE) + lane_id;
  curandState localState;

  //Each ant is placed in a initial random city.
  if (lane_id == 0){
    localState = state[globalIndex];
    random_sh [warp_id] = curand_uniform(&localState);
    state[globalIndex] = localState;
    tour_sh[threadIdx.x] = (unsigned int) number_of_cities*random_sh[warp_id];
    tourLenghtCum_sh[warp_id] = 0;
  }
  city_ini = tour_sh[warp_id*WARP_SIZE];
  next_city = city_ini;

  // Identifying the thread within each warp manages the initial city.
  setCityAsVisited(city_ini, (unsigned long long int *)&tabul);

  //Repeat until tabu list is full
  for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){

    int indexChoiceInfo = next_city*number_of_cities;
    float choice;
    int indexaux;
    unsigned int auxtabul;

    //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
    if ((tourIndex & 0x1f) == 0) {
        int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
        d_tour[indexdtour]= tour_sh[threadIdx.x];
    }

    //Iterate over all cities, in tiles of 32-width
    for (int index=0 ; index < (iterathreads-1); ++index){

        indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first tile
        auxtabul = tabul>>index; //Shift right the tabulist
        auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
        choice = choice_info[indexChoiceInfo+indexaux];
        probabilities_sh[threadIdx.x] = (float) choice*auxtabul;
    
        reduction(probabilities_sh, number_of_cities, indexaux);

        if (lane_id == 0){
            partial_sums_sh[threadIdx.x+index] = probabilities_sh[threadIdx.x];
        }
    }

    //The last chunk may not divisible by block size
    indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

    if (indexaux < number_of_cities){
      auxtabul = tabul >>(iterathreads-1);
      auxtabul = (unsigned int)MACROBITS & auxtabul;
      float choice = choice_info[indexChoiceInfo+indexaux];
      probabilities_sh[threadIdx.x] = (float)choice*auxtabul;
    }
    //Reduction of the rest elements
    reduction(probabilities_sh, number_of_cities, indexaux);

    if (lane_id == 0){
        partial_sums_sh[threadIdx.x+(iterathreads-1)] = probabilities_sh[threadIdx.x];
    }

    // Now, scan indexes stored in partial_sums_sh
    simplescan (partial_sums_sh, lane_id, iterathreads);

    //Calculate the last probabilities
    if (lane_id == (iterathreads-1)){
        p[warp_id] = random_sh[warp_id] * partial_sums_sh[threadIdx.x];
    }

    //Check in which chunk lies the probability in
    if (stencilCheck(partial_sums_sh, p[warp_id], iterathreads, lane_id)) {
        current_chunk_sh[warp_id] = lane_id;
    }

    current_chunk = current_chunk_sh[warp_id];

    //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk

    indexaux = lane_id + (current_chunk*WARP_SIZE);

    if (indexaux < number_of_cities){
        unsigned int auxtabul = tabul>>current_chunk;
        auxtabul = (unsigned int) MACROBITS & auxtabul;
        float choice = choice_info[indexChoiceInfo+indexaux];
        probabilities_sh[threadIdx.x] = (float) choice*auxtabul;
    }

    //Scan over the probabilities. The result is in probabilities_sh
    simplescan (probabilities_sh, indexaux, number_of_cities);
    //Calculate the total probability to proceed with the roulette selection

    p[warp_id] = random_sh[warp_id] * probabilities_sh[warp_id*WARP_SIZE+(WARP_SIZE-1)];

    //p[warp_id] = (current_chunk == (iterathreads-1)) ? random_sh[warp_id]* probabilities_sh[warp_id*WARP_SIZE+WARP_SIZE-(blockDim.x-number_of_cities)-1] : 
    //random_sh[warp_id] * probabilities_sh[warp_id*WARP_SIZE+(WARP_SIZE-1)];

    //Check which city within the select chunk is being selected
    if (stencilCheck(probabilities_sh, p[warp_id], number_of_cities, indexaux)) {
        setCityAsVisited (indexaux,(unsigned long long int *) &tabul);
        tour_sh[(warp_id*WARP_SIZE) + (tourIndex & 0x1f)] = indexaux;
        tourLenghtCum_sh[warp_id]+= d_distance[indexChoiceInfo+indexaux];
    }
    next_city = tour_sh[(warp_id*WARP_SIZE) + (tourIndex & 0x1f)];


  }//END FOR: ALL CITIES ARE VISITED


  //We store the last chunk
  int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

  if (indexaux < number_of_cities){
        int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
        d_tour[indexdtour]= tour_sh[threadIdx.x];
  }

  if (threadIdx.x < NUM_ANTS) {
    tourLenghtCum_sh[threadIdx.x]+=d_distance[next_city*number_of_cities+city_ini];
    d_lenghtList[globalIndex]=tourLenghtCum_sh[threadIdx.x]; // Keep the lenght of the current tour
  }

  //Complete the tour getting the beginning city
  if (lane_id == 0) {
      int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
      d_tour[indexdtour] = city_ini;
  }
}
*/





//The optimized code in this sample (and also in reduction and scan) uses a technique known as warp-synchronous programming,
//which relies on the fact that within a warp of threads running on a CUDA GPU, all threads execute instructions synchronously.
//The code uses this to avoid __syncthreads() when threads within a warp are sharing data via __shared__ memory.
//It is important to note that for this to work correctly without race conditions on all GPUs, the shared memory used in these warp-synchronous expressions must be declared volatile. If it is not declared volatile, then in the absence of __syncthreads(), the compiler is free to delay stores to __shared__ memory and keep the data in registers (an optimization technique), which will result in incorrect execution.
//So please heed the use of volatile in these samples and use it in the same way in any code you derive from them.


/**
  * FUNCTION: This function performs the tour generation by m ants
  * In this implementation, we assign one warp per each ant. We use
  * tiling to store the tour
  */
/*__global__ void nextTour_kernel_warp_tile(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

  __shared__ float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ unsigned int tourLenghtCum_sh[NUM_ANTS]; //Tour's Lenght for each ant
  __shared__ volatile unsigned int current_city_sh[NUM_ANTS];  // Current city of each ant
  __shared__ unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  __shared__ unsigned int index_sh [BLOCK_SIZE_NEXT_TOUR];

  unsigned int next_city, city_ini;
  unsigned int tabul=0xFFFFFFFF; //Tabulist initialize to one every bit
  float random;
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int globalIndex = ((blockIdx.x * NUM_ANTS) + warp_id);
  unsigned int warpIndex = (warp_id*WARP_SIZE) + lane_id;
  curandState localState = state[globalIndex];

  //Each ant is placed in a initial random city.
  if (lane_id == 0){
    random = curand_uniform(&localState);
    current_city_sh[warp_id] = (unsigned int) number_of_cities*random;
    tourLenghtCum_sh[warp_id] = 0;
    tour_sh[warpIndex] = current_city_sh[warp_id];
  }

  city_ini = current_city_sh[warp_id];
  next_city = city_ini;
  // Identifying the thread within each warp that manages the initial city.
  setCityAsVisited(city_ini,(unsigned long long int *) &tabul);

  //Repeat until tabu list is full
  for (int tourIndex=1; tourIndex<number_of_cities;++tourIndex){

    int indexChoiceInfo = next_city*number_of_cities;
    float current_prob = 0.0f;
    float choice;
    int indexaux;
    unsigned int auxtabul;

    //Iterate over all cities, in tiles of 32-width
    for (int index=0 ; index < (iterathreads-1); ++index){

        indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first tile
        auxtabul = tabul>>index; //Shift right the tabulist
        auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
        choice = choice_info[indexChoiceInfo+indexaux];
        random = curand_uniform(&localState);

        probabilities_sh[warpIndex] = (float) choice*auxtabul*random;

        reduceMax(probabilities_sh, index_sh, indexaux, number_of_cities, lane_id);

        if (lane_id == 0) {
            float prob_in_iteration = probabilities_sh[threadIdx.x];
            if (prob_in_iteration >= current_prob) {
                current_prob = prob_in_iteration;
                current_city_sh[warp_id] = index_sh[threadIdx.x];
            }
        }
    }

    //The last chunk may not divisible by block size
    indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

    if (indexaux < number_of_cities){
      auxtabul = tabul >>(iterathreads-1);
      auxtabul = (unsigned int)MACROBITS & auxtabul;
      float choice= choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float)choice*auxtabul*random;
    }

    reduceMax(probabilities_sh, index_sh, indexaux, number_of_cities, lane_id);

    if (lane_id == 0) {
        float prob_in_iteration = probabilities_sh[threadIdx.x];
        if (prob_in_iteration >= current_prob) {
            current_prob = prob_in_iteration;
            current_city_sh[warp_id] = index_sh[threadIdx.x];
        }
    }
    //All cities have been checked at this point. Ants already know which city to visit next.

    // Set the city as visited and include in the tour list
    next_city = current_city_sh[warp_id];

    setCityAsVisited(next_city, (unsigned long long int *)&tabul);

    //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
    if ((tourIndex & 0x1f) == 0) {
        int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
        d_tour[indexdtour]= tour_sh[threadIdx.x];
    }

    if (lane_id == 0) {
      tour_sh[warpIndex + (tourIndex & 0x1f)] = next_city;
      tourLenghtCum_sh[warp_id] += d_distance[indexChoiceInfo+next_city];
    }


  }//END FOR: ALL CITIES ARE VISITED


  //We store the last chunk
  int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

  if (indexaux < number_of_cities){
        int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
        d_tour[indexdtour]= tour_sh[threadIdx.x];
  }

  if (threadIdx.x < NUM_ANTS) {
    tourLenghtCum_sh[threadIdx.x]+=d_distance[next_city*number_of_cities+city_ini];
    d_lenghtList[globalIndex]=tourLenghtCum_sh[threadIdx.x]; // Keep the lenght of the current tour
  }

  //Complete the tour getting the beginning city
  if (lane_id == 0) {
      d_tour[(globalIndex*(number_of_cities+1))+number_of_cities] = city_ini;
  }
}*/

/**
  * FUNCTION: This function performs the tour generation by m ants
  * In this implementation, we assign one warp per each ant
  */
/*__global__ void nextTour_kernel_warp(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

  __shared__ volatile float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ volatile  unsigned int tourLenghtCum_sh[NUM_ANTS]; //Tour's Lenght for each ant
  __shared__ volatile unsigned int current_city_sh[NUM_ANTS];  // Current city of each ant
  __shared__ volatile unsigned int index_sh [BLOCK_SIZE_NEXT_TOUR];

  unsigned int next_city, city_ini;
  unsigned int tabul=0xFFFFFFFF; //Tabulist initialize to one every bit
  float random;

  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  //Thread within the Warp. Lane id
  int line_id = threadIdx.x & 0x1f;
  unsigned int globalIndex = ((blockIdx.x * NUM_ANTS) + warp_id);
  unsigned int warpIndex = (warp_id*WARP_SIZE) + line_id;
  curandState localState = state[globalIndex];

  //Each ant is placed in a initial random city.
  if (line_id == 0){
    random = curand_uniform(&localState);
    current_city_sh[warp_id] = (unsigned int) number_of_cities*random;
    tourLenghtCum_sh[warp_id] = 0;
    d_tour[globalIndex*(number_of_cities+1)] = current_city_sh[warp_id];
  }

  city_ini = current_city_sh[warp_id];
  next_city = city_ini;
  // Identifying the thread within each warp that manages the initial city.

  setCityAsVisited(city_ini, (unsigned long long int *)&tabul);


  //Repeat until tabu list is full
  for (int tourIndex=1; tourIndex<number_of_cities;++tourIndex){

    int indexChoiceInfo = next_city*number_of_cities;
    float current_prob = 0.0f;
    float choice;
    int indexaux;
    unsigned int auxtabul;

    //Iterate over all cities, in tiles of 32-width
    for (int index=0 ; index < (iterathreads-1); ++index){

        indexaux = threadIdx.x & 0x1f; //Threads for each warp has to start from the beginning
        indexaux += (index*WARP_SIZE); // Tiling 32-width
        auxtabul = tabul>>index; //Shift right the tabulist
        auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
        choice = choice_info[indexChoiceInfo+indexaux];
        random = curand_uniform(&localState);
        probabilities_sh[warpIndex] = (float) choice*auxtabul*random;

        reduceMax(probabilities_sh, index_sh, indexaux, number_of_cities, line_id);


       if (line_id == 0) {
            float prob_in_iteration = probabilities_sh[threadIdx.x];
            if (prob_in_iteration >= current_prob) {
               // printf ("I am thread %d y mi index_sh es %d mi index aux is %d\n", threadIdx.x, index_sh[threadIdx.x], indexaux);
                current_prob = prob_in_iteration;
                current_city_sh[warp_id] = index_sh[threadIdx.x];
            }
        }
    }

    //The last chunk may not divisible by block size
    indexaux = threadIdx.x & 0x1f;
    indexaux += ((iterathreads-1)*WARP_SIZE);

    if (indexaux < number_of_cities){
      auxtabul = tabul >>(iterathreads-1);
      auxtabul = (unsigned int)MACROBITS & auxtabul;
      float choice= choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float)choice*auxtabul*random;
    }


    reduceMax(probabilities_sh, index_sh, indexaux, number_of_cities, line_id);
    if (line_id == 0) {
        float prob_in_iteration = probabilities_sh[threadIdx.x];
        if (prob_in_iteration >= current_prob) {
            current_prob = prob_in_iteration;
            current_city_sh[warp_id] = index_sh[threadIdx.x];
        }
    }

    next_city = current_city_sh[warp_id];

    //Current City is the next city to go, so it is marked as a visted
    setCityAsVisited(next_city, (unsigned long long int *)&tabul);

    if (line_id == 0) {
      //the bit that represents the city in this thread is marked as zero
      d_tour[(globalIndex*(number_of_cities+1))+tourIndex] = next_city;
      tourLenghtCum_sh[warp_id] += d_distance[indexChoiceInfo+next_city];
    }

  }//End of FOR

  //Complete the tour getting the beginning city
  if (line_id == 0) {
    d_tour[(globalIndex*(number_of_cities+1))+number_of_cities] = city_ini;
    tourLenghtCum_sh[warp_id]+=d_distance[next_city*number_of_cities+city_ini];
    d_lenghtList[globalIndex]=tourLenghtCum_sh[warp_id]; // Keep the lenght of the current tour
  }

}
*/


/**
  * This kernel performs the pheromone stage
  */
__global__ void pheromone_update_with_atomic (float * d_pheromone, const unsigned int number_of_cities, unsigned int * d_tour, const unsigned int number_of_ants, unsigned int * d_lenghtList, int ite){

  const int xIndex = blockIdx.x* (number_of_cities+1) + threadIdx.x;
  int index,indexThread;

  for (int i=0; i<ite; i++) {
    indexThread = i*blockDim.x;
    index =xIndex+(indexThread);
    if ((threadIdx.x+indexThread)<number_of_cities){
        unsigned int coordx=d_tour[index];
        unsigned int coordy=d_tour[index+1];
        unsigned int pos = (coordy*number_of_cities)+coordx;
        unsigned int posim = (coordx*number_of_cities)+coordy;
        float pheromone = 0.0;
        pheromone=(float)1.0*(1.0/d_lenghtList[blockIdx.x]);
        atomicAdd(&d_pheromone[pos],pheromone);
        atomicAdd(&d_pheromone[posim],pheromone);
    }
  }
}


/***** External function to launch the kernels **********/
int antSystemTSP (problem instance, colony colony){

    dim3 grid_choice, block_choice;
    dim3 grid_tour, block_tour;
    dim3 grid_phero, block_phero;
    dim3 grid_random, block_random;
    unsigned int iterationsInTour = 0;
    unsigned int iterationsInPhero = 0;
    size_t mem_size_llist = (colony.n_ants)*sizeof(unsigned int);
    int i;
    unsigned int bestLength= UINT_MAX; // the length of the best tour so far
    unsigned int bestIt; // Length of the tour obtained in the current iteration
    unsigned int mem_size_seed = colony.n_ants*sizeof(curandState);
    unsigned int mem_size_h_tours = (instance.n+1) * colony.n_ants * sizeof (unsigned int);
    unsigned int mem_size_h_total = (colony.n_ants*colony.n_ants) * sizeof(float);
    cudaEvent_t start, stop; // events
    cudaEvent_t startiroulette,stopiroulette;
    unsigned int * h_tours = (unsigned int *) malloc (mem_size_h_tours);
    float * h_total = (float *) malloc (mem_size_h_total);
    float * h_total_2 = (float *) malloc (mem_size_h_total);
    float * h_total_SS = (float *) malloc (mem_size_h_total);//para las sumas parciales del SSRoulette
    pdebug ("In the launching kernel function");
    curandState * devStates;
    unsigned int size_proximaciudad = instance.n * sizeof(int);
    int * proximaciudad = (int *) malloc (size_proximaciudad);
    int * d_proximaciudad;

    cudaMalloc(&d_proximaciudad,size_proximaciudad);
    cudaMemset(d_proximaciudad,0,size_proximaciudad);
//    cudaMallochost((int*)&proximaciudad,size_proximaciudad);


    // Event creation
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventCreate(&startiroulette);
    cudaEventCreate(&stopiroulette);


    //initialize_random_numbers(devStates, colony.n_ants);

    if (cudaMalloc ((void **)&devStates, mem_size_seed) != cudaSuccess) {
        fprintf (stderr, "Error allocating device memory \n");
        exit (1);
    }

    setting_kernel_parameters(&grid_random, &block_random, colony.n_ants, BLOCK_SIZE_CHOICE);
    setup_kernel<<<grid_random,block_random>>> (devStates, colony.n_ants);
    setting_kernel_parameters(&grid_choice, &block_choice, instance.n*instance.n, BLOCK_SIZE_CHOICE);

    pdebug ("Choice kernel parameter are number of blocks %d number of threads %d\n", grid_choice.x, block_choice.x);

    setting_kernel_parameters_tour(&grid_tour, &block_tour, &iterationsInTour, instance.n, colony.n_ants);
    setting_kernel_parameters_phero(&grid_phero, &block_phero, &iterationsInPhero, instance.n, colony.n_ants);
    size_t sharedmem = ceil((float)iterationsInTour/WARP_SIZE)*WARP_SIZE*NUM_ANTS*sizeof(float);
    float elapsedTime, total = 0.0f;
    float elapsedTimeiroulette = 0.0f;

//    for (i = 0; i < colony.n_tries; ++i) {
    for (i = 0; i<1;i++){

        computeTotalkernel<<<grid_choice, block_choice>>>(d_total, d_pheromone, d_distance, instance.n, colony.alpha, colony.beta,colony.rho);
        cudaDeviceSynchronize();
        pdebug("Number of blocks %d, number of threads per block %d, number of iterations %d and shared Memory allocated %d bytes\n", grid_tour.x,block_tour.x, iterationsInTour, sharedmem);

/////////        
	int yyy=cudaMemcpy(h_total,d_total, mem_size_h_total, cudaMemcpyDeviceToHost);
	printf("\nAntes del kernel..... error=%d\n",yyy);
/////////

        cudaEventRecord(start,0);
        //nextTour_kernel_warp_iroulette_k20<<< grid_tour, block_tour, sharedmem>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour);

	cudaEventRecord(startiroulette,0);

/////////////////////        nextTour_kernel_warp_iroulette_k20<<< grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour,d_proximaciudad);
	nextTour_kernel_warp_reducing_random_k20<<<grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour);
/////////////////////////////////__global__ void nextTour_kernel_warp_reducing_random_k20(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){
        cudaDeviceSynchronize();
        cudaEventRecord (stop);
	cudaEventRecord (stopiroulette);

        cudaEventSynchronize(stop);
	cudaEventSynchronize(stopiroulette);
        
        cudaEventElapsedTime(&elapsedTime, start, stop);
	cudaEventElapsedTime(&elapsedTimeiroulette, startiroulette,stopiroulette);
        //printf ("Elapsed Time is %f\n", elapsedTime);
        total+=elapsedTime;
        
        int zz=cudaMemcpy(colony.lengthList, d_lengthList, mem_size_llist, cudaMemcpyDeviceToHost);
        int xyz=cudaMemcpy(h_tours, d_tour, mem_size_h_tours, cudaMemcpyDeviceToHost);
	int yy=cudaMemcpy(h_total,d_total, mem_size_h_total, cudaMemcpyDeviceToHost);
//	cudaMemcpyFromSymbol(&nextcity,"proximaciudad",sizeof(proximaciudad),0,cudaMemcpyDeviceToHost);
	int xx=cudaMemcpy(proximaciudad,d_proximaciudad, size_proximaciudad, cudaMemcpyDeviceToHost);

	printf("\n-----error d_total:%d------\n ----- error proximacudad:%d\t error d_legthList:%d\t error d_tour:%d\n",yy,xx,zz,xyz);

    //    for (int l = 0; l< colony.n_ants; ++l) 
      //      printf ("%d, ", colony.lengthList[l]);

       // printf ("\n");
       // #ifdef DEBUG



/*********************************************
        int rep = -1;
        for (int j = 0; j < colony.n_ants; ++j){
            if (rep!=-1)
            printf ("*******Tour for the Ant %d*******\n", j);
            for (int k=0; k < instance.n+1; ++k) {
                for (int l=k+1; (l < instance.n); ++l){
                    if (h_tours[k+j*(instance.n+1)] == h_tours[l+j*(instance.n+1)]){
                        rep = h_tours[k+j*(instance.n+1)];
                    }
                }
                if (rep!=-1)
                    printf ("%d, ",h_tours[k+j*(instance.n+1)]);
            }
            if (rep!= -1){
                printf ("\nThe element %d is repeated", rep);
                rep =-1;
            }
            if (rep!= -1)
            printf ("\n");
        }

***********************************************/
       // #endif

	float probaux=0;
	float probaux2=0;
	float maxprobaux=0;
	int siguiente;
//	for(int x=0;x<colony.n_ants;x++){

	float aleatorio=0;
	struct timeval t1;
	gettimeofday(&t1, NULL);
	srand(t1.tv_usec * t1.tv_sec);
	printf("\nusec:%dsec%d\n",t1.tv_usec,t1.tv_sec);




//	FILE * fp;
//	fp = fopen ("rl5934.txt","w+");
	for(int x=0;x<1;x++){
	   printf("********** I:%d **********\n",i);
	   maxprobaux=0;
	   probaux=0;
	   for (int y=1;y<(instance.n);y++){
		aleatorio= ((float)rand()/RAND_MAX);/// multiples aleatorios
		probaux=h_total[x*(instance.n)+y];
		h_total_2[x*(instance.n)+y]=(h_total[x*(instance.n)+y])*aleatorio;
			// se aplica aquí un cambio de escala al choice_info, para dar más peso al valor del choice que al del aleatorio
		probaux2=h_total_2[x*(instance.n)+y];
//////////////////////////////////////////	       printf("ant x:%d\t en la ciuad y:%d\t choice_total:%f\t choicefinal:%f\n",x,y,h_total[(x*(instance.n))+y],h_total_2[(x*instance.n)+y]);
	

//	   fprintf(fp,"%f ",h_total[(x*instance.n)+y]);

	   }
//	   fclose(fp);
	   for (int z=1;z<(instance.n);z++){
		probaux2=h_total_2[(x*instance.n)+z];
		if (probaux2>maxprobaux){
		    maxprobaux=probaux2;
		    siguiente=z;
		}
	   }
	   printf("%d:nexcity aqui\tpara la hormiga x:%d\n",siguiente,x);//debería coincidir con la nextcity del kernel a expensas del random, no???
	}// esto es el choice_info
//	for (int z=0;z<colony.n_ants;z++){
	for (int z=0;z<1;z++){
		printf("%d:ciudad en el kernel ant z:%d\n",proximaciudad[z],z);
	}//se supone que es la que se eligió para cada warp, no???

	printf("instance.n:%d\n",instance.n);
	printf("colony.n_ants:%d\n",colony.n_ants);

	int ciudadnormalroulette;
	float sumahtotal=0;
	for (int ii=1;ii<colony.n_ants;ii++){//empiezo en 1 xq es para la hormiga 0, para no sumar la prob de la ciudad 0.
		sumahtotal+=h_total[ii];
	}
//	printf("\n***\nsumahtotal:%f\n",sumahtotal);
//	srand(time(NULL));



//	srand(getpid());
//	aleatorio = rand()%(1);

//	printf("\n***\n%daleatorio:\n",aleatorio);
	float umbral=0;
	float umbralSS=0;
	float leftSS=0;
	float centerSS=0;
	int seleccionfinalSS=0;
	umbral=aleatorio*sumahtotal;	
	
	struct timeval t2;
	gettimeofday(&t2, NULL);
	printf("\nusec:%d\tsec%d\n",t2.tv_usec,t2.tv_sec);
	srand(t2.tv_usec * t2.tv_sec + t2.tv_usec * t2.tv_sec);
	aleatorio= ((float)rand()/RAND_MAX);/// genero otro aleatorio para q se escoja "a priori" otra ciudad distinta en SS.
	umbralSS=aleatorio*sumahtotal;//multiplicando por el mismo aleatorio se consigue EXACTAMENTE la misma ciudad.
	float seleccionSS=0;
	//ciudadnormalroulette= (ceil)(sumahtotal * aleatorio);
	float seleccion=0;
	int seleccionfinal=0;
	for (int xx=1;xx<colony.n_ants;xx++){
		seleccion+=h_total[xx];
		if (seleccion>umbral){
			seleccionfinal=xx;
			printf("\n seleccion:%f\tumbral:%f\tseleccionfinal:%d",seleccion,umbral,seleccionfinal);
			break;
		
		}
	}
	printf("\n***\n%d:proximanormalroulette sumahtotal:%f, aleatorio:%f umbral:%f\n",seleccionfinal,sumahtotal,aleatorio,umbral);

	for (int i=0;i<colony.n_ants;i++){
		h_total_SS[i]=0;
	}



	float sumaauxSS=0;
	for (int i=1;i<colony.n_ants;i++){
		sumaauxSS=0;
		for (int j=1;j<=i;j++){//lo del 1 es x evitar la probab de la ciudad 0, q es 1, como en el resto de bucles.
			sumaauxSS+=h_total[j];
		}
		h_total_SS[i]=sumaauxSS;
	}

//	for (int i=0;i<colony.n_ants;i++){
//		printf("\nh_total_SS:%f",h_total_SS[i]);	
//	}




	for (int zz=2; zz<colony.n_ants;zz++){ //dsd 2 para evitar el leftSS del 1
		leftSS=h_total_SS[zz-1]-umbralSS;
		centerSS=h_total_SS[zz]-umbralSS;
		if ((leftSS*centerSS)<=0){
			seleccionfinalSS=zz;
////////////////////////////////////////////			printf("\n%f<-leftSS%f<-centerSS\t%d:yelzz\n",leftSS,centerSS,zz);
		}
	}
	if (seleccionfinalSS==0){
		seleccionfinalSS=1;
	}

	printf("\n\n%d:SSRoulette:%f sumadh_tot, aleatorio:%f umbralSS:%f\n",seleccionfinalSS,sumahtotal,aleatorio,umbralSS);



        bestIt = getLenghtIterationBestTour(colony.lengthList, colony.n_ants, d_tour, d_bestTour,&bestLength, instance.n);
        printf("Best Length found in iteration %d is %d\n",i, bestIt);
        pheromone_update_with_atomic<<<grid_phero, block_phero>>> (d_pheromone, instance.n, d_tour, colony.n_ants, d_lengthList, iterationsInPhero);

        cudaDeviceSynchronize();
    }

    printf("\n%f tiempo en nextTourKernelIRoulette--SSRoulette:\n",elapsedTimeiroulette);
    printf ("Execution on average %f\n",total/colony.n_tries);

    /*cudaEventRecord (stop);
    cudaEventSynchronize(stop);
    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf ("Elapsed Time is %f\n", elapsedTime);
    */

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    cudaFree (devStates);
    return bestLength;
}
