#include <string>
#include <cstdlib>
#include <cmath>
#include <acotsp/tsp.h>
#include <liblocalsearch/localsearch.h>

 
///TspProblem Class constructor definition
TspProblem::TspProblem(unsigned int *distances, unsigned int number_of_cities) {
  distances_ = distances;
	number_of_cities_=number_of_cities;	
}

///TspProblem Class destroyer definition
TspProblem::~TspProblem() {			
  delete distances_;
}

///The maximum tour size is number of cities less 1
unsigned int TspProblem::get_max_tour_size() {
  return	number_of_cities_-1;
}

unsigned int TspProblem::number_of_vertices() {
  return number_of_cities_;
}

unsigned int * TspProblem::get_distances() {
	return distances_;

}
 
FileNotFoundException::FileNotFoundException(const char *filepath) {
  filepath_ = filepath;
}

const char *FileNotFoundException::what() const throw() {
  return filepath_;
}

//Matrix<unsigned int> * 

unsigned int * Parser::parse_tsplib(const char *filepath, unsigned int * size) throw(FileNotFoundException) {

  enum section {
    TYPE,
    DIMENSION,
    EDGE_WEIGHT_TYPE,
    NODE_COORD_SECTION,
    NONE
  };

	unsigned int* distances;

  //Matrix<unsigned int> *distances;
  std::vector<city> cities;
  section s = NONE;
  std::string keyword;
  std::ifstream file(filepath);

  if(!file) {
    throw FileNotFoundException(filepath);
  }

  while(file.good()) {
    file >> keyword;
    if(keyword.find("DIMENSION") != std::string::npos) {
      s = DIMENSION;
      if(keyword.length() == std::string("DIMENSION").length()) {
        file >> keyword; // read colon
      }
    } else if(keyword.find("NODE_COORD_SECTION") != std::string::npos) {
      s = NODE_COORD_SECTION;
    }

    if (s == DIMENSION) {
      int number_of_vertices;
      file >> number_of_vertices;
			//distances = new Matrix<unsigned int>(number_of_vertices, number_of_vertices, 0);
			distances = (unsigned int *) calloc (number_of_vertices*number_of_vertices,sizeof(unsigned int));
			*size=number_of_vertices;
    } else if (s == NODE_COORD_SECTION) {
      unsigned int i=0;
      while(i < *size) {				
        city c;
        file >> c.num >> c.coord_x >> c.coord_y;
        cities.push_back(c);
        i++;
      }
      for(unsigned int j=0;j<cities.size();j++) {
        for(unsigned int k=j+1;k<cities.size();k++) {
          city a = cities[j];
          city b = cities[k];
          double xd = a.coord_x - b.coord_x;
          double yd = a.coord_y - b.coord_y;
          unsigned int dab = int(round(sqrt(xd*xd + yd*yd)));
//					int indexA = (a.num-1)*(*size)+(b.num-1);
//					int indexB = (b.num-1)*(*size)+(a.num-1);
          distances[(a.num-1)*(*size)+(b.num-1)] = dab;
          distances[(b.num-1)*(*size)+(a.num-1)] = dab;

					//(*distances)[a.num-1][b.num-1] = dab;
          //(*distances)[b.num-1][a.num-1] = dab;
        }
      }	
    }

    s = NONE;
  }
  file.close();
  return distances;
}
