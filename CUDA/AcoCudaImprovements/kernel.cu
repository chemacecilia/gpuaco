/*
 * Version: 1.0
 * File: kernel.cu
 * Author: Jose M. Cecilia
 * Purpose: TheGPU kernels to manage the kernel execution
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. cecilia
 */
/***************************************************************************
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain

*******************************************************************************/

#include <stdio.h>
#include <curand_kernel.h>
#include "kernel.h"
#include "utilities.h"

#define TIMING

<<<<<<< .mine
// Print device properties
void printDevProp(cudaDeviceProp devProp){
  printf("Major revision number:         %d\n",  devProp.major);
  printf("Minor revision number:         %d\n",  devProp.minor);
  printf("Name:                          %s\n",  devProp.name);
  printf("Total global memory:           %d\n",  devProp.totalGlobalMem);
  printf("Total shared memory per block: %d\n",  devProp.sharedMemPerBlock);
  printf("Total registers per block:     %d\n",  devProp.regsPerBlock);
  printf("Warp size:                     %d\n",  devProp.warpSize);
  printf("Maximum memory pitch:          %d\n",  devProp.memPitch);
  printf("Maximum threads per block:     %d\n",  devProp.maxThreadsPerBlock);
  for (int i = 0; i < 3; ++i)
    printf("Maximum dimension %d of block:  %d\n", i, devProp.maxThreadsDim[i]);
  for (int i = 0; i < 3; ++i)
    printf("Maximum dimension %d of grid:   %d\n", i, devProp.maxGridSize[i]);
  printf("Clock rate:                    %d\n",  devProp.clockRate);
  printf("Total constant memory:         %d\n",  devProp.totalConstMem);
  printf("Texture alignment:             %d\n",  devProp.textureAlignment);
  printf("Concurrent copy and execution: %s\n",  (devProp.deviceOverlap ? "Yes" : "No"));
  printf("Number of multiprocessors:     %d\n",  devProp.multiProcessorCount);
  printf("Kernel execution timeout:      %s\n",  (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
  return;
}
||||||| .r104
// Print device properties
void printDevProp(cudaDeviceProp devProp){
    printf("Major revision number:         %d\n",  devProp.major);
    printf("Minor revision number:         %d\n",  devProp.minor);
    printf("Name:                          %s\n",  devProp.name);
    printf("Total global memory:           %d\n",  devProp.totalGlobalMem);
    printf("Total shared memory per block: %d\n",  devProp.sharedMemPerBlock);
    printf("Total registers per block:     %d\n",  devProp.regsPerBlock);
    printf("Warp size:                     %d\n",  devProp.warpSize);
    printf("Maximum memory pitch:          %d\n",  devProp.memPitch);
    printf("Maximum threads per block:     %d\n",  devProp.maxThreadsPerBlock);
    for (int i = 0; i < 3; ++i)
        printf("Maximum dimension %d of block:  %d\n", i, devProp.maxThreadsDim[i]);
    for (int i = 0; i < 3; ++i)
        printf("Maximum dimension %d of grid:   %d\n", i, devProp.maxGridSize[i]);
    printf("Clock rate:                    %d\n",  devProp.clockRate);
    printf("Total constant memory:         %d\n",  devProp.totalConstMem);
    printf("Texture alignment:             %d\n",  devProp.textureAlignment);
    printf("Concurrent copy and execution: %s\n",  (devProp.deviceOverlap ? "Yes" : "No"));
    printf("Number of multiprocessors:     %d\n",  devProp.multiProcessorCount);
    printf("Kernel execution timeout:      %s\n",  (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
    return;
}
=======
>>>>>>> .r111


/****************** CUDA Kernels ***************/




/**
 * This kernel set ups the generation of random number on the GPU
 * It sets a different seed for each thread
 */
__global__ void setup_kernel(curandState *state, unsigned int m) {
  
  int id = threadIdx.x + blockIdx.x * blockDim.x; /* Each thread gets same seed, a different sequence number,
						     no offset */
  if (id<m)
    curand_init(1234*blockIdx.x, id, 0, &state[id]);
}


/**
<<<<<<< .mine
 * Kernel to compute the total information
 * and it also performs the evaporation stage.
 */
||||||| .r104
  * Kernel to compute the total information
  * and it also performs the evaporation stage.
  */
=======
  * FUCTION: This Kernel computes the total information and it also performs the evaporation stage.
  * INPUT: 
  * OUTPUT: _d_choiceInfo: Information about heuristic + pheromone, d_pheromone after evaporation 
  */
>>>>>>> .r111
__global__ void computeTotalkernel (float *d_choiceInfo, float *d_pheromone, unsigned int * d_distance, unsigned int n, float alpha, float beta, float rho)
{
<<<<<<< .mine
  
  int tid = (blockIdx.y*blockDim.y*n)+(blockDim.x*blockIdx.x)+threadIdx.x;
  
  //if (tid== 0) printf ("Prueba con tid %d,", tid);
  if (tid<n*n){
    float pheromone;
    pheromone = d_pheromone[tid];
    float heuristic = 1.0 / ((float) d_distance[tid] + 0.1);
    d_choiceInfo[tid]=__powf(pheromone, alpha)*__powf(heuristic, beta);
    //Evaporation stage
    pheromone= pheromone*(1.0-rho);
    d_pheromone[tid] = pheromone;
  }
||||||| .r104

    int tid = (blockIdx.y*blockDim.y*n)+(blockDim.x*blockIdx.x)+threadIdx.x;

    //if (tid== 0) printf ("Prueba con tid %d,", tid);
    if (tid<n*n){
        float pheromone;
        pheromone = d_pheromone[tid];
        float heuristic = 1.0 / ((float) d_distance[tid] + 0.1);
        d_choiceInfo[tid]=__powf(pheromone, alpha)*__powf(heuristic, beta);
        //Evaporation stage
        pheromone= pheromone*(1.0-rho);
        d_pheromone[tid] = pheromone;
    }
=======

    int tid = (blockIdx.y*blockDim.y*n)+(blockDim.x*blockIdx.x)+threadIdx.x;

    if (tid<n*n){
        float pheromone;
        pheromone = d_pheromone[tid]+0.00001f;
        float heuristic = 1.0f / ((float) d_distance[tid] + 0.1f);
        d_choiceInfo[tid]=__powf(pheromone, alpha)*__powf(heuristic, beta);
        //printf ("El choice info es %f, la pheromona es %f  y la heuristica es %f, alpha %f and beta%f\n", d_choiceInfo[tid], pheromone, heuristic, alpha, beta);
        //Evaporation stage
        pheromone*=(1.0f-rho);
        d_pheromone[tid] = pheromone;
    }
>>>>>>> .r111
}



/*
<<<<<<< .mine
  FUNCTION:       This function obtains the maximum of the array and the index of that maximum
  INPUT:          pointer to the probabilistic information and the relative index
  OUTPUT:         Relative index of the maximum probability
  (SIDE)EFFECTS:  The maximum probability per each ant is in the line 0
||||||| .r104
	FUNCTION:       This function obtains the maximum of the array and the index of that maximum
	INPUT:          pointer to the probabilistic information and the relative index
	OUTPUT:         Relative index of the maximum probability
	(SIDE)EFFECTS:  The maximum probability per each ant is in the line 0
=======
	FUNCTION:       This function obtains the maximum of the array and the index of that maximum
	INPUT:          pointer to the probabilistic information and the relative index
	OUTPUT:         Relative index of the maximum probability
	(SIDE)EFFECTS:  The maximum probability is in g_idata[0]
   	ORIGIN:         Partially based on NVIDIA_CUDA_SDK
>>>>>>> .r111
*/
__device__ inline unsigned int reduceMax(float *g_idata, unsigned int index, unsigned int n)
{
  
  __shared__ unsigned int index_sh [BLOCK_SIZE_NEXT_TOUR];

  index_sh[threadIdx.x]= index;
  g_idata[threadIdx.x] = (index>=n)?0.0:g_idata[threadIdx.x];
  
  __syncthreads();
  
  // do reduction in shared mem
<<<<<<< .mine
  for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
    if (line_id < s){
      if(g_idata[threadIdx.x] < g_idata[threadIdx.x + s]) {
	g_idata[threadIdx.x] = g_idata[threadIdx.x + s];
	index_sh[threadIdx.x] = index_sh[threadIdx.x + s];
||||||| .r104
  for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
    if (line_id < s){
      if(g_idata[threadIdx.x] < g_idata[threadIdx.x + s]) {
	    g_idata[threadIdx.x] = g_idata[threadIdx.x + s];
	    index_sh[threadIdx.x] = index_sh[threadIdx.x + s];
=======
  for(unsigned int s=blockDim.x/2; s>0; s>>=1) {
    if (threadIdx.x < s){
      if (g_idata[threadIdx.x]<g_idata[threadIdx.x + s]) {
	    g_idata[threadIdx.x]=g_idata[threadIdx.x + s];
	    index_sh[threadIdx.x]=index_sh[threadIdx.x + s];
>>>>>>> .r111
      }
    }
    __syncthreads();
  }


/**
 * This funtion set a city as a visited in a bitwise
 */
__device__ inline void setCityAsVisited(unsigned int city, unsigned long long int * tabul){
  
  //Division by 64 is the bit that represents that city.
  unsigned int bitTabul = (unsigned int) city>>5;
  //Modulo is the thread that manages that city. Modulo 64 as we are dealing with WARPS
  unsigned int threadTabul = (unsigned int) city& 0x1f;
  unsigned int lane_id = threadIdx.x&0x1f;
  
  // Set the city as a visited in the corresponding threads
  if (lane_id == threadTabul) {
    //     printf ("En setCityAsvisited thread %d is setting in the bit %d\n", threadTabul,bitTabul);
    unsigned long long int aux = (unsigned long long int) MACROBITS<<bitTabul;
    *tabul = *tabul & (~aux);
  }
}

/**
 * Reduction
 */
__device__ inline void reduction(float *g_idata, unsigned int index, unsigned int n){
  
  int tid = threadIdx.x;
  //printf ("En reduction the n element is %d\n", n);
  
  g_idata[tid] = (index >= n) ? 0.0 : g_idata[tid];
  
  __syncthreads();

  // do reduction in shared mem
<<<<<<< .mine
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
    if (lane_id < s){
      g_idata[warpIndex] += g_idata[warpIndex + s];
||||||| .r104
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
    if (lane_id < s){
	    g_idata[warpIndex] += g_idata[warpIndex + s];
      }
=======
  for(unsigned int s=blockDim.x>>1; s>0; s>>=1) {
    if (tid < s){
	    g_idata[tid]+=g_idata[tid + s];
      }
>>>>>>> .r111
    }
<<<<<<< .mine
  }
||||||| .r104
=======
  __syncthreads();
>>>>>>> .r111
}


/**
<<<<<<< .mine
 * Reduction
 */
__device__ inline void reduction_shfl(float * prob, unsigned int n, unsigned int index){
  
#if __CUDA_ARCH__ >=350
  float aux;
  *prob = (index >= n) ? 0.0f : *prob;
  
  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
    aux = __shfl_down(*prob, s);
    *prob+=aux;
  }
#endif
}


/**
||||||| .r104
 * Reduction
 */
__device__ inline void reduction_shfl(float * prob, unsigned int n, unsigned int index){

#if __CUDA_ARCH__ >=350
  float aux;
  *prob = (index >= n) ? 0.0f : *prob;

  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
	    aux = __shfl_down(*prob, s);
        *prob+=aux;
    }
#endif
}


/**
=======
>>>>>>> .r111
 * Simple parallel scan of 2exp(k) elements with 2exp(k) threads
 */
inline __device__ void simplescan(float * idata, unsigned int index, unsigned int n){
  
<<<<<<< .mine
  int tid = threadIdx.x;
  int lane_id = tid & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = warp_id*WARP_SIZE*numBlocks;
  
  float interpolate = 0.0f;
  
  for (int i=lane_id; i<(numBlocks*WARP_SIZE); i+=WARP_SIZE){
    
    float value = ((index+i)<n) ? idata[warpIndex+i]:0.0f;//Some cities may be null
    value = (lane_id==0)? value+interpolate:value;
    
    for (int offset = 1; offset < WARP_SIZE; offset <<= 1) {
      float temp= __shfl_up(value,offset);
      if (lane_id >= offset)
	value+=temp;
    }
    
    idata[warpIndex+i]=value;
    interpolate = __shfl(value, WARP_SIZE-1);
  }
#else 
  printf ("You shall compile with -arch>=35\n");
  exit(-1);
#endif
||||||| .r104
    int tid = threadIdx.x;
    int lane_id = tid & 0x1f;
    int warp_id = tid >> 5;
    int warpIndex = warp_id*WARP_SIZE*numBlocks;

    float interpolate = 0.0f;

    for (int i=lane_id; i<(numBlocks*WARP_SIZE); i+=WARP_SIZE){
        
        float value = ((index+i)<n) ? idata[warpIndex+i]:0.0f;//Some cities may be null
        value = (lane_id==0)? value+interpolate:value;

        for (int offset = 1; offset < WARP_SIZE; offset <<= 1) {
            float temp= __shfl_up(value,offset);
            if (lane_id >= offset)
                 value+=temp;
        }
    
        idata[warpIndex+i]=value;
        interpolate = __shfl(value, WARP_SIZE-1);
    }
#else 
    printf ("You shall compile with -arch>=35\n");
    exit(-1);
#endif
=======
    int tid = threadIdx.x;
    idata[tid] = (index<n) ? idata[tid]:0.0f;//Some cities may be null

    for (int offset = 1; offset < n; offset <<= 1) {
        float temp;
        if (tid >= offset)
            temp = idata[tid-offset];
        __syncthreads();
        if (tid >= offset)
            idata[tid]+=temp;
        __syncthreads();
    }

>>>>>>> .r111
}


/**
 * This funtion set a city as a visited in a bitwise
 */
__device__ inline void setCityAsVisited(unsigned int city, unsigned int * tabul){
<<<<<<< .mine
  
  int tid = threadIdx.x;
  int lane_id = tid & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = (warp_id*WARP_SIZE)+lane_id;
  
  idata[warpIndex] = (index<n) ? idata[warpIndex]:0.0f;//Some cities may be null
  
  if (index < n) {
    for (int offset = 1; offset < n; offset <<= 1) {
      float temp;
      if (lane_id >= offset)
	temp = idata[warpIndex-offset];
      __syncthreads();
      if (lane_id >= offset)
	idata[warpIndex]+=temp;
      __syncthreads();
    }
||||||| .r104

    int tid = threadIdx.x;
    int lane_id = tid & 0x1f;
    int warp_id = tid >> 5;
    int warpIndex = (warp_id*WARP_SIZE)+lane_id;

    idata[warpIndex] = (index<n) ? idata[warpIndex]:0.0f;//Some cities may be null

    if (index < n) {
        for (int offset = 1; offset < n; offset <<= 1) {
            float temp;
            if (lane_id >= offset)
               temp = idata[warpIndex-offset];
         __syncthreads();
            if (lane_id >= offset)
                idata[warpIndex]+=temp;
            __syncthreads();
        }
    }
=======

  //Division is the bit that represents that city
  unsigned int bitTabul = (unsigned int) city>>LOG_BLOCK_SIZE_NEXT_TOUR;
  //Modulo is the thread that manages that city
  unsigned int threadTabul = (unsigned int) city & (BLOCK_SIZE_NEXT_TOUR-1);

  if (threadIdx.x == threadTabul) {
    //the bit that represents the city in this thread is marked as zero
    unsigned int aux = (unsigned int)MACROBITS<<bitTabul;
    *tabul = *tabul & (~aux);
  }
>>>>>>> .r111
  }
}


/**
 * This function do a stencil check in order to select a city. Only
 * one thread will be in the frontier between positive and negative
 * values.
 */
__device__ inline bool stencilCheck (float * sidata, float p, int n ,  int index) {
<<<<<<< .mine
  
  int tid = threadIdx.x;
  int lane_id = tid & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = (warp_id*WARP_SIZE);
  
  bool check = false;
  //Stencil to check which city to go next. Only one thread can get in.
  //Thread 0 should be stencil
  for (int i = lane_id; i < n; i+=WARP_SIZE){
    float left_value = (i > 0) ? sidata[(warpIndex+i)-1] : 0.0f; // Prepare the stencil for thread 0.
    left_value -= p;
    float center_value = sidata[warpIndex+i] - p;
    check = ((left_value * center_value)<= 0);
  }
  return check;
||||||| .r104

    int tid = threadIdx.x;
    int lane_id = tid & 0x1f;
    int warp_id = tid >> 5;
    int warpIndex = (warp_id*WARP_SIZE);

    bool check = false;
    //Stencil to check which city to go next. Only one thread can get in.
    //Thread 0 should be stencil
    for (int i = lane_id; i < n; i+=WARP_SIZE){
        float left_value = (i > 0) ? sidata[(warpIndex+i)-1] : 0.0f; // Prepare the stencil for thread 0.
        left_value -= p;
        float center_value = sidata[warpIndex+i] - p;
        check = ((left_value * center_value)<= 0);
    }
    return check;
=======

    int tid = threadIdx.x;
    bool check = false;
    //Stencil to check which city to go next. Only one thread can get in.
    //Thread 0 sh@ould be stencil

    if (index < n) {
        float left_value = (tid > 0) ? sidata[tid-1] : 0.0f; // Prepare the stencil for thread 0.
        left_value -= p;
        float center_value = sidata[tid] - p;
        check = ((left_value * center_value)<= 0);
    }

    return check;
>>>>>>> .r111

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/******************************* TOUR IMPLEMENTATIONS *********************************************/
/////////////////////////////////////////////////////////////////////////////////////////////////////

/***********************************Implementation Scan + Stencil *********************************/

/**
<<<<<<< .mine
 * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
 * also represents a city or set of cities. However, the selection process is now performed using scan
 * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions
 */
||||||| .r104
  * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions
  */
=======
  * This kernel performs the tour generation by m ants. An ant is represented by a block, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern.
  */
>>>>>>> .r111
__global__ void nextTour_kernel_reducing_random(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){
  
    // We need an extra cell to better do stencil
  __shared__ float probabilities_sh[BLOCK_SIZE_NEXT_TOUR];
  __shared__ float partial_sums_sh[BLOCK_SIZE_NEXT_TOUR];//This can be iterathreads width
  __shared__ unsigned int tour_sh [BLOCK_SIZE_NEXT_TOUR];
  
  int tid = threadIdx.x;
  __shared__ unsigned int tourLenghtCum_sh;
  __shared__ unsigned int current_chunk_sh;
  unsigned int current_city, beginning_city, current_chunk;
  unsigned int btourindex = blockIdx.x*(number_of_cities+1);
  unsigned int tabul=0xFFFFFFFF;
  __shared__ float random;
  __shared__ float p;
  curandState localState = state[blockIdx.x];
  
  //Each ant is placed in a initial random city.
  if (tid == 0){
    random = curand_uniform(&localState);
    tour_sh[0] = (unsigned int)number_of_cities*random;
    tourLenghtCum_sh=0;
  }
  __syncthreads();
  
<<<<<<< .mine
  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
      localState = state[globalIndex];
      random = curand_uniform(&localState);
      next_city = (int) number_of_cities*random;
      tour_sh[warpIndex] = next_city;
      tourLenght = 0;
    }
||||||| .r104
  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
        localState = state[globalIndex];
        random = curand_uniform(&localState);
        next_city = (int) number_of_cities*random;
        tour_sh[warpIndex] = next_city;
        tourLenght = 0;
    }
=======
  current_city = tour_sh[0];
  beginning_city = current_city;
  setCityAsVisited (beginning_city, &tabul);
>>>>>>> .r111
    
<<<<<<< .mine
    next_city = __shfl(next_city,0);
    city_ini = next_city;
    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
      setCityAsVisited (next_city, &tabul1);
    else 
      setCityAsVisited ((next_city-2048),&tabul2);
    
||||||| .r104
    next_city = __shfl(next_city,0);
    city_ini = next_city;
    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
        setCityAsVisited (next_city, &tabul1);
    else 
        setCityAsVisited ((next_city-2048),&tabul2);

=======
  __syncthreads();

>>>>>>> .r111
  //Repeat until tabu list is full
  for (int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){
<<<<<<< .mine
      
      int indexChoiceInfo = next_city*number_of_cities;
      float choice;
      int indexaux;//Number of iterations
      unsigned int auxtabul;
      
      //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
      if ((tourIndex & 0x1f) == 0) {
	int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
	d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
      }
      
      indexaux=0;
      
      float probabilities = 0.0f;
      float maxProbabilities = 0.0f;
      int maxIndex=lane_id;
      //Iterate over all cities, in tiles of 32-width
      for (int index=lane_id; index < number_of_cities; index+=WARP_SIZE){
	//Calculates the values for the chunk
	auxtabul = (index<2048)?tabul1>>indexaux:tabul2>>(indexaux&0x3F);// We use 64 bits for the tabu list
	auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
	choice = choice_info[indexChoiceInfo+index];
	random = curand_uniform(&localState);     
	probabilities = (float) choice*auxtabul*random;
||||||| .r104

        int indexChoiceInfo = next_city*number_of_cities;
        float choice;
        int indexaux;//Number of iterations
        unsigned int auxtabul;

        //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
        if ((tourIndex & 0x1f) == 0) {
            int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
            d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
        }

        indexaux=0;
=======

    int indexChoiceInfo = current_city*number_of_cities;
    int indexaux;

    // Check if tour_sh is full.
    if ((tourIndex & (BLOCK_SIZE_NEXT_TOUR-1)) == 0) {
        int indexdtour = blockIdx.x*(number_of_cities+1) + (((tourIndex >> LOG_BLOCK_SIZE_NEXT_TOUR)-1) * BLOCK_SIZE_NEXT_TOUR)+ threadIdx.x;
        d_tour[indexdtour]= tour_sh[threadIdx.x];
    }

        indexaux=0;
>>>>>>> .r111
        
<<<<<<< .mine
	if (probabilities > maxProbabilities) {
	  maxIndex=index;
	  maxProbabilities=probabilities;
	}
	indexaux++;
      }
      
      //Now, we have the maximum values of probabilities and the city associated with it for a 32-chunk
      float auxshfl;
      int auxMaxIndex;
      auxMaxIndex = __shfl_xor(maxIndex,16); 
      auxshfl = __shfl_xor(maxProbabilities, 16);      
      if (maxProbabilities < auxshfl) {
	maxIndex = auxMaxIndex;
	maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,8); 
      auxshfl = __shfl_xor(maxProbabilities, 8);      
      if (maxProbabilities < auxshfl) {
	maxIndex = auxMaxIndex;
	maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,4); 
      auxshfl = __shfl_xor(maxProbabilities, 4);      
      if (maxProbabilities < auxshfl) {
	maxIndex = auxMaxIndex;
	maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,2); 
      auxshfl = __shfl_xor(maxProbabilities, 2);      
      if (maxProbabilities < auxshfl) {
	maxIndex = auxMaxIndex;
	maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,1); 
      auxshfl = __shfl_xor(maxProbabilities, 1);      
      if (maxProbabilities < auxshfl) {
	maxIndex = auxMaxIndex;
	maxProbabilities = auxshfl; 
      }
      
      next_city = __shfl(maxIndex,0);
      
      if (next_city < 2048){
	setCityAsVisited (next_city, &tabul1);         
      }
      else { 
	setCityAsVisited((next_city-2048),&tabul2);
      }
      
      if (lane_id == 0 ) {
	tourLenght += d_distance[indexChoiceInfo+next_city];
	tour_sh[warpIndex+(tourIndex& 0x1f)] = next_city;                
      }
      
    }//END FOR: ALL CITIES ARE VISITED
    
    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);
    
    if (indexaux < number_of_cities){
      int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
      d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
||||||| .r104
        float probabilities = 0.0f;
        float maxProbabilities = 0.0f;
        int maxIndex=lane_id;
        //Iterate over all cities, in tiles of 32-width
        for (int index=lane_id; index < number_of_cities; index+=WARP_SIZE){
            //Calculates the values for the chunk
            auxtabul = (index<2048)?tabul1>>indexaux:tabul2>>(indexaux&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
            choice = choice_info[indexChoiceInfo+index];
            random = curand_uniform(&localState);     
            probabilities = (float) choice*auxtabul*random;
        
            if (probabilities > maxProbabilities) {
                maxIndex=index;
                maxProbabilities=probabilities;
            }
            indexaux++;
        }

        //Now, we have the maximum values of probabilities and the city associated with it for a 32-chunk
        float auxshfl;
        int auxMaxIndex;
        auxMaxIndex = __shfl_xor(maxIndex,16); 
        auxshfl = __shfl_xor(maxProbabilities, 16);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }
  
        auxMaxIndex = __shfl_xor(maxIndex,8); 
        auxshfl = __shfl_xor(maxProbabilities, 8);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }

        auxMaxIndex = __shfl_xor(maxIndex,4); 
        auxshfl = __shfl_xor(maxProbabilities, 4);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }

        auxMaxIndex = __shfl_xor(maxIndex,2); 
        auxshfl = __shfl_xor(maxProbabilities, 2);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }
        
        auxMaxIndex = __shfl_xor(maxIndex,1); 
        auxshfl = __shfl_xor(maxProbabilities, 1);      
        if (maxProbabilities < auxshfl) {
            maxIndex = auxMaxIndex;
            maxProbabilities = auxshfl; 
        }
        
        next_city = __shfl(maxIndex,0);
         
        if (next_city < 2048){
            setCityAsVisited (next_city, &tabul1);         
        }
        else { 
            setCityAsVisited((next_city-2048),&tabul2);
        }
             
        if (lane_id == 0 ) {
            tourLenght += d_distance[indexChoiceInfo+next_city];
            tour_sh[warpIndex+(tourIndex& 0x1f)] = next_city;                
        }
     
    }//END FOR: ALL CITIES ARE VISITED

    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

    if (indexaux < number_of_cities){
        int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
        d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
=======
        float probabilities = 0.0f;
        float maxProbabilities = 0.0f;
        int maxIndex=lane_id;
        //Iterate over all cities, in tiles of 32-width
        for (int index=lane_id; index < number_of_cities; index+=WARP_SIZE){
            //Calculates the values for the chunk
            auxtabul = (index<2048)?tabul1>>indexaux:tabul2>>(indexaux&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
            choice = choice_info[indexChoiceInfo+index];
            random = curand_uniform(&localState);     
            probabilities = (float) choice*auxtabul*random;
        
            if (probabilities > maxProbabilities) {
                maxIndex=index;
                maxProbabilities=probabilities;
            }
            indexaux++;
        }
        __syncthreads();
        //Scan over the probabilities. The result is in probabilities_sh
        simplescan (probabilities_sh,threadIdx.x, number_of_cities);

        //Calculate the total probability to proceed with the roulette selection
        if (tid == (number_of_cities-1))
            p = random * probabilities_sh[tid];

        __syncthreads();

        if (stencilCheck(probabilities_sh, p, number_of_cities, tid)) {
           // printf ("I am the ant %d, thread %d in the tourIndex %d\n", blockIdx.x, tid, tourIndex);
            setCityAsVisited(tid, &tabul);
            tour_sh[tourIndex] = tid;
            tourLenghtCum_sh+= d_distance[indexChoiceInfo+tid];
        }
        __syncthreads();
        current_city = tour_sh[tourIndex];
>>>>>>> .r111
    }
    
    //Complete the tour getting the beginning city
    if (lane_id == 0) {     
      state[globalIndex] = localState;
      tourLenght += d_distance[next_city*number_of_cities+city_ini];
      d_lenghtList[globalIndex] = tourLenght;
      int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
      d_tour[indexdtour] = city_ini;
    }
  }//END IF 
  
#else 
  //printf ("You shall compile with -arch>=35 make arch=1\n");
  //exit(-1);
#endif
}



/**
 * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
 * also represents a city or set of cities. However, the selection process is now performed using scan
 * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions
 */
__global__ void nextTour_kernel_warp_reducing_random_k20(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){
  
#if __CUDA_ARCH__ >=350
  
  __shared__ volatile float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  extern __shared__ volatile float partial_sums_sh[];//This can be NUMWARPS*iterationsInTour with
  __shared__ volatile unsigned int current_chunk_sh[NUM_ANTS];
  
  float random,p; 
  unsigned int next_city, city_ini, current_chunk;
  unsigned int tourLenght;
  unsigned long long int tabul1=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  unsigned long long int tabul2=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  float probabilities = 0.0f;
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = warp_id*WARP_SIZE;
  unsigned int chunkPartialSum = ceil((float)iterathreads/WARP_SIZE);
  unsigned int warpPartialSumIndex = warp_id*chunkPartialSum*WARP_SIZE;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;
  
  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
      localState = state[globalIndex];
      random = curand_uniform(&localState);
      state[globalIndex] = localState;
      next_city = (unsigned int) number_of_cities*random;
      tour_sh[warpIndex] = next_city;
      tourLenght = 0;
    }
    
    random = __shfl (random, 0);
    next_city = (int)__shfl((float)next_city,0);
    city_ini = next_city;
    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
      setCityAsVisited (next_city, &tabul1);
    else 
      setCityAsVisited ((next_city-2048),&tabul2);
    
    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){
<<<<<<< .mine
      
      int indexChoiceInfo = next_city*number_of_cities;
      float choice;
      int indexaux;
      unsigned int auxtabul;
      
      // printf ("The current_city is %d\n", next_city);
      
      //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
      if ((tourIndex & 0x1f) == 0) {
	int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
	d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
      }
      
      //Iterate over all cities, in tiles of 32-width
      for (int index=0 ; index < (iterathreads-1); ++index){
	
	indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first til
	auxtabul = (indexaux<2048)?tabul1>>index : tabul2>>(index&0x3F);// We use 64 bits for the tabu list
	auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
	choice = choice_info[indexChoiceInfo+indexaux];
        
	probabilities = (float) choice*auxtabul;
	reduction_shfl(&probabilities, number_of_cities, indexaux);
	
	if (lane_id == 0){
	  // printf ("The warp %d the index partial sum  %d reductions is %f in tourIteration %d\n", warp_id, warpPartialSumIndex, probabilities, tourIndex);
	  partial_sums_sh[warpPartialSumIndex+index] = probabilities;
	}            
      }
      
      //The last chunk may not divisible by block size
      indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);
      
      if (indexaux < number_of_cities){
	// auxtabul = tabul >>(iterathreads-1);
	auxtabul = (indexaux<2048)?tabul1>>(iterathreads-1) : tabul2>>((iterathreads-1)&0x3F);// We use 64 bits for the tabu list
	auxtabul = (unsigned int)MACROBITS & auxtabul;
	float choice = choice_info[indexChoiceInfo+indexaux];
	probabilities = (float)choice*auxtabul;
      }
      
      //Reduction of the rest elements
      reduction_shfl(&probabilities, number_of_cities, indexaux);
      
      if (lane_id == 0){
	//printf ("The %d reductions is %f \n", (iterathreads-1), probabilities);
	partial_sums_sh[warpPartialSumIndex+(iterathreads-1)] = probabilities;
      }
      
      // Now, scan indexes stored in partial_sums_sh
      
      simplescan_shfl (partial_sums_sh, 0, iterathreads, chunkPartialSum);
      
      
      //Calculate p value to do the roulette. 
      if (lane_id == 0){
	p = random * partial_sums_sh[warpPartialSumIndex+(iterathreads-1)];
	//   if (blockIdx.x==0 && warp_id==0)
	//     printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+(iterathreads-1)]);
      }
      p = __shfl(p,0);
      
      ///////////////////////STENCIL CHECK FOR THE CHUNK//////////////////
        ////////////////////CHUNK MAY BE BIGGER THAN 32/////////////////
      
      for (int chunkIndex=lane_id; chunkIndex<iterathreads; chunkIndex+=WARP_SIZE) {
	
	float left_value = (chunkIndex > 0) ? partial_sums_sh[(warpPartialSumIndex+chunkIndex)-1]:0.0f;
	left_value -=p;
	float center_value = partial_sums_sh[warpPartialSumIndex+chunkIndex] - p;
            if ((left_value * center_value) <= 0){
	      current_chunk_sh[warp_id] = chunkIndex;        
	      //printf ("I am the warp %d lane_id %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warp_id, lane_id, chunkIndex, left_value,warpIndex+chunkIndex-1, partial_sums_sh[warpIndex+chunkIndex-1], center_value,warpIndex+chunkIndex, partial_sums_sh[warpIndex+chunkIndex]);
            }
        }
      current_chunk = current_chunk_sh[warp_id];
      
      //if (lane_id ==0)
      //  printf ("The current_chunk is %d\n", current_chunk);
      
        //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk
      
      indexaux = lane_id + (current_chunk*WARP_SIZE);
      
      if (indexaux < number_of_cities){
	//unsigned int auxtabul = tabul>>current_chunk;
	
	unsigned int auxtabul = ((indexaux)<2048)?tabul1>>(current_chunk) : tabul2>>(current_chunk&0x3F);// We use 64 bits for the tabu list
	auxtabul = (unsigned int) MACROBITS & auxtabul;
	float choice = choice_info[indexChoiceInfo+indexaux];
	probabilities_sh[warpIndex+lane_id] = (float) choice*auxtabul;
	// printf ("For the city: Thread %d index %d prob %f and chunk %d and auxtabul %d\n", lane_id, warpIndex+lane_id, probabilities_sh[warpIndex+lane_id], current_chunk, auxtabul);
      }
      
      //Scan over the probabilities. The result is in probabilities_sh
      simplescan_shfl (probabilities_sh, (current_chunk*WARP_SIZE), number_of_cities, 1);
      //Calculate the total probability to proceed with the roulette selection
      
      p = random * probabilities_sh[warpIndex+(WARP_SIZE-1)];   
      
      //if (blockIdx.x==0 && warp_id==0)
      //  printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+WARP_SIZE-1]);
      
      //Check which city within the select chunk is being selected
      //////////////////////STENCIL CHECK to select the city//////////////////
      //////////////////////////////////////////////////////////////////////
      
      float left_value = (lane_id > 0) ? probabilities_sh[warpIndex+lane_id-1]:0.0f;
      left_value -=p;
      float center_value = probabilities_sh[warpIndex+lane_id] - p;
      if ((left_value * center_value) <= 0){
	tour_sh[warpIndex+(tourIndex& 0x1f)] = indexaux;                
	if (indexaux < 2048){
	  setCityAsVisited (indexaux, &tabul1);         
	  //printf ("Setting city as visited in tabul1\n");
	}
	else { 
	  setCityAsVisited((indexaux-2048),&tabul2);
	  //printf ("Setting city %d as visited in tabul2\n",indexaux-2048 );
	}
	//  printf ("For the city: lane_id %d and the city is %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warpIndex+(tourIndex& 0x1f), tour_sh[warpIndex+(tourIndex& 0x1f)], left_value, warpIndex+lane_id-1, probabilities_sh[warpIndex+lane_id-1], center_value,warpIndex+lane_id, partial_sums_sh[warpIndex+lane_id]);
      }
      
      
      next_city = tour_sh[warpIndex + (tourIndex & 0x1f)];
      
      // printf ("For thread %d is Next cituy is %d\n", lane_id, next_city);
      if (lane_id == 0 ) {
	tourLenght += d_distance[indexChoiceInfo+next_city];
      }
      
    }//END FOR: ALL CITIES ARE VISITED
||||||| .r104

        int indexChoiceInfo = next_city*number_of_cities;
        float choice;
        int indexaux;
        unsigned int auxtabul;

       // printf ("The current_city is %d\n", next_city);

        //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
        if ((tourIndex & 0x1f) == 0) {
            int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
            d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
        }

        //Iterate over all cities, in tiles of 32-width
        for (int index=0 ; index < (iterathreads-1); ++index){
            
            indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first til
            auxtabul = (indexaux<2048)?tabul1>>index : tabul2>>(index&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
            choice = choice_info[indexChoiceInfo+indexaux];
            
            probabilities = (float) choice*auxtabul;
            reduction_shfl(&probabilities, number_of_cities, indexaux);

            if (lane_id == 0){
               // printf ("The warp %d the index partial sum  %d reductions is %f in tourIteration %d\n", warp_id, warpPartialSumIndex, probabilities, tourIndex);
                partial_sums_sh[warpPartialSumIndex+index] = probabilities;
            }            
        }

        //The last chunk may not divisible by block size
        indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

        if (indexaux < number_of_cities){
           // auxtabul = tabul >>(iterathreads-1);
            auxtabul = (indexaux<2048)?tabul1>>(iterathreads-1) : tabul2>>((iterathreads-1)&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int)MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+indexaux];
            probabilities = (float)choice*auxtabul;
        }

        //Reduction of the rest elements
        reduction_shfl(&probabilities, number_of_cities, indexaux);

        if (lane_id == 0){
            //printf ("The %d reductions is %f \n", (iterathreads-1), probabilities);
            partial_sums_sh[warpPartialSumIndex+(iterathreads-1)] = probabilities;
        }

        // Now, scan indexes stored in partial_sums_sh

        simplescan_shfl (partial_sums_sh, 0, iterathreads, chunkPartialSum);
       

        //Calculate p value to do the roulette. 
        if (lane_id == 0){
            p = random * partial_sums_sh[warpPartialSumIndex+(iterathreads-1)];
         //   if (blockIdx.x==0 && warp_id==0)
           //     printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+(iterathreads-1)]);
        }
        p = __shfl(p,0);

        ///////////////////////STENCIL CHECK FOR THE CHUNK//////////////////
        ////////////////////CHUNK MAY BE BIGGER THAN 32/////////////////

        for (int chunkIndex=lane_id; chunkIndex<iterathreads; chunkIndex+=WARP_SIZE) {
            
            float left_value = (chunkIndex > 0) ? partial_sums_sh[(warpPartialSumIndex+chunkIndex)-1]:0.0f;
            left_value -=p;
            float center_value = partial_sums_sh[warpPartialSumIndex+chunkIndex] - p;
            if ((left_value * center_value) <= 0){
                current_chunk_sh[warp_id] = chunkIndex;        
                //printf ("I am the warp %d lane_id %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warp_id, lane_id, chunkIndex, left_value,warpIndex+chunkIndex-1, partial_sums_sh[warpIndex+chunkIndex-1], center_value,warpIndex+chunkIndex, partial_sums_sh[warpIndex+chunkIndex]);
            }
        }
        current_chunk = current_chunk_sh[warp_id];

        //if (lane_id ==0)
          //  printf ("The current_chunk is %d\n", current_chunk);

        //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk

        indexaux = lane_id + (current_chunk*WARP_SIZE);

        if (indexaux < number_of_cities){
            //unsigned int auxtabul = tabul>>current_chunk;

            unsigned int auxtabul = ((indexaux)<2048)?tabul1>>(current_chunk) : tabul2>>(current_chunk&0x3F);// We use 64 bits for the tabu list
            auxtabul = (unsigned int) MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+indexaux];
            probabilities_sh[warpIndex+lane_id] = (float) choice*auxtabul;
           // printf ("For the city: Thread %d index %d prob %f and chunk %d and auxtabul %d\n", lane_id, warpIndex+lane_id, probabilities_sh[warpIndex+lane_id], current_chunk, auxtabul);
        }

        //Scan over the probabilities. The result is in probabilities_sh
        simplescan_shfl (probabilities_sh, (current_chunk*WARP_SIZE), number_of_cities, 1);
        //Calculate the total probability to proceed with the roulette selection

        p = random * probabilities_sh[warpIndex+(WARP_SIZE-1)];   
        
        //if (blockIdx.x==0 && warp_id==0)
          //  printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+WARP_SIZE-1]);
        
        //Check which city within the select chunk is being selected
        //////////////////////STENCIL CHECK to select the city//////////////////
        //////////////////////////////////////////////////////////////////////
     
        float left_value = (lane_id > 0) ? probabilities_sh[warpIndex+lane_id-1]:0.0f;
        left_value -=p;
        float center_value = probabilities_sh[warpIndex+lane_id] - p;
        if ((left_value * center_value) <= 0){
            tour_sh[warpIndex+(tourIndex& 0x1f)] = indexaux;                
            if (indexaux < 2048){
                setCityAsVisited (indexaux, &tabul1);         
                //printf ("Setting city as visited in tabul1\n");
            }
            else { 
                setCityAsVisited((indexaux-2048),&tabul2);
                //printf ("Setting city %d as visited in tabul2\n",indexaux-2048 );
            }
         //  printf ("For the city: lane_id %d and the city is %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warpIndex+(tourIndex& 0x1f), tour_sh[warpIndex+(tourIndex& 0x1f)], left_value, warpIndex+lane_id-1, probabilities_sh[warpIndex+lane_id-1], center_value,warpIndex+lane_id, partial_sums_sh[warpIndex+lane_id]);
        }
        
        
        next_city = tour_sh[warpIndex + (tourIndex & 0x1f)];
        
       // printf ("For thread %d is Next cituy is %d\n", lane_id, next_city);
        if (lane_id == 0 ) {
            tourLenght += d_distance[indexChoiceInfo+next_city];
        }
     
    }//END FOR: ALL CITIES ARE VISITED
=======

        int indexChoiceInfo = next_city*number_of_cities;
        float choice;
        int indexaux;
        unsigned int auxtabul;

       // printf ("The current_city is %d\n", next_city);

        //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
        if ((tourIndex & 0x1f) == 0) {
            int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
            d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
        }

        //Iterate over all cities, in tiles of 32-width
        for (int index=0 ; index < (iterathreads-1); ++index){
            
            reduction(probabilities_sh, indexaux, number_of_cities);
            
            if (threadIdx.x == 0){
                partial_sums_sh[i] = probabilities_sh[0];
            }
            __syncthreads();
        }
        // Last Chunk may be not divisible by the TAMBLOCK
        indexaux = threadIdx.x + ((iterathreads-1)*blockDim.x);
        if (indexaux < number_of_cities){
            unsigned int auxtabul = tabul >>(iterathreads-1);
            auxtabul = (unsigned int)MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+(indexaux)];
            probabilities_sh[threadIdx.x] = (float)choice*auxtabul;
        }
        
        __syncthreads ();
        //Reduction of the rest elements
        reduction(probabilities_sh, indexaux, number_of_cities);
        
        if (threadIdx.x == 0){
            partial_sums_sh[(iterathreads-1)] = probabilities_sh[0];
        }
        __syncthreads();
>>>>>>> .r111
    
    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);
    
    if (indexaux < number_of_cities){
      int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
      d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
    }
    
    //Complete the tour getting the beginning city
    if (lane_id == 0) {
      tourLenght += d_distance[next_city*number_of_cities+city_ini];
      d_lenghtList[globalIndex] = tourLenght;
      int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
      d_tour[indexdtour] = city_ini;
    }
  }//END IF 
  
#else 
  //printf ("You shall compile with -arch>=35 make arch=1\n");
  //exit(-1);
#endif
}


/**
  * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
  * also represents a city or set of cities. However, the selection process is now performed using scan
  * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions.
  * This is limited to smallest benchmarks
  */
/*__global__ void nextTour_kernel_warp_reducing_random_k20_small(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

#if __CUDA_ARCH__ >=350

  __shared__ volatile float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  __shared__ volatile float partial_sums_sh[BLOCK_SIZE_NEXT_TOUR];//This can be NUMWARPS*iterationsInTour with
  __shared__ volatile unsigned int current_chunk_sh[NUM_ANTS];

  float random,p; 
  unsigned int next_city, city_ini, current_chunk;
  unsigned int tourLenght;
  unsigned int tabul=0xFFFFFFFF; //Tabulist initialize to one every bit
  float probabilities = 0.0;
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = (warp_id*WARP_SIZE) + lane_id;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;

  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.

    if (lane_id == 0){
        localState = state[globalIndex];
        random = curand_uniform(&localState);
        state[globalIndex] = localState;
        next_city = (unsigned int) number_of_cities*random;
        tour_sh[warpIndex] = next_city;
        tourLenght = 0;
    }

    random = __shfl (random, 0);
    next_city = (int)__shfl((float)next_city,0);

    city_ini = next_city;

    // Identifying the thread within each warp manages the initial city.
    setCityAsVisited(next_city, &tabul);

    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){

        int indexChoiceInfo = next_city*number_of_cities;
        float choice;
        int indexaux;
        unsigned int auxtabul;

        //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
        if ((tourIndex & 0x1f) == 0) {
            int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
            d_tour[indexdtour]= tour_sh[warpIndex];
        }

        //Iterate over all cities, in tiles of 32-width
        for (int index=0 ; index < (iterathreads-1); ++index){
            indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first tile
            auxtabul = tabul>>index; //Shift right the tabulist
            auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
            choice = choice_info[indexChoiceInfo+indexaux];
            probabilities = (float) choice*auxtabul;
            reduction_shfl(&probabilities, number_of_cities, indexaux);

            if (lane_id == 0){
                partial_sums_sh[warpIndex+index] = probabilities;
            }
        }

        //The last chunk may not divisible by block size
        indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);

        if (indexaux < number_of_cities){
            auxtabul = tabul >>(iterathreads-1);
            auxtabul = (unsigned int)MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+indexaux];
            probabilities = (float)choice*auxtabul;
        }

        //Reduction of the rest elements
        reduction_shfl(&probabilities, number_of_cities, indexaux);

        if (lane_id == 0)
            partial_sums_sh[warpIndex+(iterathreads-1)] = probabilities;
    
        // Now, scan indexes stored in partial_sums_sh
        simplescan (partial_sums_sh, threadIdx.x, iterathreads);

        //Calculate the last probabilities
        if (tid == (iterathreads-1)){
            p = random * partial_sums_sh[iterathreads-1];
        }
        __syncthreads();

        //Check in which chunk lies the probability in
        if (stencilCheck(partial_sums_sh, p, iterathreads, tid)) {
            current_chunk_sh = tid;
        }
        __syncthreads ();
        current_chunk = current_chunk_sh;

        //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk
        indexaux = threadIdx.x + current_chunk*blockDim.x;

        if (indexaux < number_of_cities){
            unsigned int auxtabul = tabul>>current_chunk;
            auxtabul = (unsigned int) MACROBITS & auxtabul;
            float choice = choice_info[indexChoiceInfo+(indexaux)];
            probabilities_sh[threadIdx.x]= (float) choice*auxtabul;
        }

        __syncthreads ();
        //Scan over the probabilities. The result is in probabilities_sh
        simplescan (probabilities_sh, indexaux, number_of_cities);
         
        if (tid == 0) {
           p = random * probabilities_sh[BLOCK_SIZE_NEXT_TOUR-1];
        }        
        __syncthreads();

        //Check which city within the select chunk is being selected
        if (stencilCheck(probabilities_sh, p, number_of_cities, indexaux)) {
            setCityAsVisited (indexaux, &tabul);
            tour_sh[tourIndex & (BLOCK_SIZE_NEXT_TOUR-1)] = indexaux;
            tourLenghtCum_sh+= d_distance[indexChoiceInfo+indexaux];
        }
        __syncthreads();
        
        current_city = tour_sh[tourIndex & (BLOCK_SIZE_NEXT_TOUR-1)]; 
    }
  }

  int indexaux = threadIdx.x + ((iterathreads-1)* BLOCK_SIZE_NEXT_TOUR);

  //Write in device memory the tour
  if ((indexaux < number_of_cities)) {
     int indexdtour = blockIdx.x*(number_of_cities+1) + threadIdx.x + (iterathreads-1)* BLOCK_SIZE_NEXT_TOUR;
     d_tour[indexdtour]= tour_sh[threadIdx.x];
  }

  //Complete the tour getting the beginning city
  if (threadIdx.x==0) {
    d_tour[btourindex+number_of_cities]=beginning_city;
    tourLenghtCum_sh+= d_distance[current_city*number_of_cities+beginning_city];
    d_lenghtList[blockIdx.x]=tourLenghtCum_sh; // Keep the lenght of the current tour
  }
}

/***********************************Baseline Implementation (JPDC). Ant=Block; random per city*********************************/
/**
  * FUNCTION: This function performs the tour generation by m ants. Each ant is represented by one block
  * each thread represents one city or set of cities. The selection procedure is implemented by launching
  * one random number per thread and performing a hierarchical reduction.
  */
__global__ void nextTour_kernel(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

  __shared__ float probabilities_sh[BLOCK_SIZE_NEXT_TOUR];
  __shared__ unsigned int tourLenghtCum;
  __shared__ unsigned int current_city_sh;
  unsigned int current_city;
  unsigned int btourindex = blockIdx.x*(number_of_cities+1);
  unsigned int tabul=0xFFFFFFFF;
  float random;
  curandState localState = state[blockIdx.x];

  //Each ant is placed in a initial random city.
  if (threadIdx.x == 0){
    random = curand_uniform(&localState);
    current_city_sh = (unsigned int)number_of_cities*random;
    tourLenghtCum=0;
    d_tour[btourindex] = current_city_sh;
    //printf("I am block %d y la ciudad inicial es %d\n", blockIdx.x, current_city_sh);
  }

  __syncthreads();

  current_city = current_city_sh;
  unsigned int beginning_city = current_city;

  setCityAsVisited (beginning_city, &tabul);

  __syncthreads();

  //Repeat until tabu list is full
  for (int tourIndex=1; tourIndex<number_of_cities;++tourIndex){

    int indexChoiceInfo = current_city*number_of_cities;
    float current_prob = 0.0f;
    int indexaux;
   // printf ("EL numero de iteraciones es %d\n",iterathreads-1);
    for (int i=0; i<iterathreads-1; i++){

      indexaux = threadIdx.x+(i*blockDim.x);
      unsigned int auxtabul = tabul>>i; //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int) MACROBITS & auxtabul; //auxbits debe ser 0 o 1.
      float choice = choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float) choice*auxtabul*random;

      //printf("I am thread %d y auxtabul es %d, choice es %f y random %f y la probabilidad es %f\n", threadIdx.x, auxtabul, choice, random, probabilities_sh[threadIdx.x]);

      __syncthreads();

      unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);
      float prob_local = probabilities_sh[0];
      ////printf ("Para la ant %d la prob que se obtiene de reduccion es %f\n", blockIdx.x, prob_local);
      if (prob_local>=current_prob) {
  	    current_prob = prob_local;
  	    current_city = aux_city;
      }
    }

    indexaux = threadIdx.x+((iterathreads-1)*blockDim.x);
    if (indexaux < number_of_cities){
      unsigned int auxtabul = tabul >>(iterathreads-1); //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int)MACROBITS & auxtabul; //auxbits debe ser 0 o 1.
      float choice= choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float)choice*auxtabul*random;
    }
    __syncthreads();
    unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);
    float prob_local = probabilities_sh[0];
    if (prob_local>=current_prob) {
      current_prob = prob_local;
      current_city = aux_city;
    }
    __syncthreads();

    setCityAsVisited (current_city, &tabul);
    if (threadIdx.x==0){
      d_tour[btourindex+tourIndex]=current_city; //Adding the city to the tour
      tourLenghtCum+= d_distance[indexChoiceInfo+current_city];
    }
    __syncthreads();
  }

  //Complete the tour getting the beginning city
  if (threadIdx.x==0) {
    d_tour[btourindex+number_of_cities]=beginning_city;
    tourLenghtCum+= d_distance[current_city*number_of_cities+beginning_city];
    d_lenghtList[blockIdx.x]=tourLenghtCum; // Keep the lenght of the current tour
    //printf ("Para la ant %d La longitud del camino es %d\n", blockIdx.x,tourLenghtCum);
  }

}



/**
  * This kernel performs the pheromone stage
  */
__global__ void pheromone_update_with_atomic (float * d_pheromone, const unsigned int number_of_cities, unsigned int * d_tour, const unsigned int number_of_ants, unsigned int * d_lenghtList, int ite){

  const int xIndex = blockIdx.x* (number_of_cities+1) + threadIdx.x;
  int index,indexThread;

  for (int i=0; i<ite; i++) {
    indexThread = i*blockDim.x;
    index =xIndex+(indexThread);
    if ((threadIdx.x+indexThread)<number_of_cities){
        unsigned int coordx=d_tour[index];
        unsigned int coordy=d_tour[index+1];
        unsigned int pos = (coordy*number_of_cities)+coordx;
        unsigned int posim = (coordx*number_of_cities)+coordy;
        float pheromone = 0.0;
        pheromone=(float)1.0*(1.0/d_lenghtList[blockIdx.x]);
        d_pheromone[pos]+=pheromone;
        d_pheromone[posim]+=pheromone;
    }
  }
}

void deviceQuery () {

    // Number of CUDA devices
    int devCount;
    cudaGetDeviceCount(&devCount);
    printf("CUDA Device Query...\n");
    printf("There are %d CUDA devices.\n", devCount);

    //First, we check how many threads can be allocated per block
    cudaDeviceProp devProp;
    for (int i=0; i<devCount; ++i){
        cudaGetDeviceProperties(&devProp,i);
        printf("Major revision number:         %zd\n",  devProp.major);
        printf("Minor revision number:         %zd\n",  devProp.minor);
        printf("Name:                          %s\n",  devProp.name);
        printf("Total global memory:           %zd\n",  devProp.totalGlobalMem);
        printf("Total shared memory per block: %zd\n",  devProp.sharedMemPerBlock);
        printf("Total registers per block:     %zd\n",  devProp.regsPerBlock);
        printf("Warp size:                     %zd\n",  devProp.warpSize);
        printf("Maximum memory pitch:          %zd\n",  devProp.memPitch);
        printf("Maximum threads per block:     %zd\n",  devProp.maxThreadsPerBlock);
        for (int i = 0; i < 3; ++i)
            printf("Maximum dimension %d of block:  %zd\n", i, devProp.maxThreadsDim[i]);
        for (int i = 0; i < 3; ++i)
            printf("Maximum dimension %d of grid:   %zd\n", i, devProp.maxGridSize[i]);
        printf("Clock rate:                    %zd\n",  devProp.clockRate);
        printf("Total constant memory:         %zd\n",  devProp.totalConstMem);
        printf("Texture alignment:             %zd\n",  devProp.textureAlignment);
        printf("Concurrent copy and execution: %s\n",  (devProp.deviceOverlap ? "Yes" : "No"));
        printf("Number of multiprocessors:     %zd\n",  devProp.multiProcessorCount);
        printf("Kernel execution timeout:      %s\n",  (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
        printf("*************************************************\n");
    }


}

/***** External function to launch the kernels **********/
int antSystemTSP (problem instance, colony colony){

    dim3 grid_choice, block_choice;
    dim3 grid_tour, block_tour;
    dim3 grid_phero, block_phero;
    dim3 grid_random, block_random;
    unsigned int iterationsInTour = 0;
    unsigned int iterationsInPhero = 0;
    size_t mem_size_llist = (colony.n_ants)*sizeof(unsigned int);
    int i,j;
    unsigned int bestLength= UINT_MAX; // the length of the best tour so far
    unsigned int bestIt=0; // Length of the tour obtained in the current iteration
    unsigned int mem_size_seed = colony.n_ants*sizeof(curandState);
    unsigned int mem_size_h_tours = (instance.n+1) * colony.n_ants * sizeof (unsigned int);
    unsigned int * h_tours = (unsigned int *) malloc (mem_size_h_tours);
    unsigned int * h_bestTour = (unsigned int *) malloc ((instance.n+1)*sizeof(unsigned int));
    pdebug ("In the launching kernel function");
    curandState * devStates;
    #ifdef DEBUG
    deviceQuery();
    #endif

    //// new
    cudaDeviceProp nombre;
    int devi;
    cudaGetDevice(&devi);
    cudaGetDeviceProperties(&nombre,devi);
    printf("Con Get Device:\n%s\t%d\t%d\n",nombre.name,nombre.major,nombre.minor);
    
    #ifdef TIMING
    // Event creation
    cudaEvent_t start, stop; // events
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    float elapsedTime, total = 0.0f;
    #endif

    //initialize_random_numbers(devStates, colony.n_ants);

    CHECK_CUDA(cudaMalloc ((void **)&devStates, mem_size_seed)); 

    setting_kernel_parameters(&grid_random, &block_random, colony.n_ants, BLOCK_SIZE_CHOICE);
    setup_kernel<<<grid_random,block_random>>> (devStates, colony.n_ants);

    setting_kernel_parameters(&grid_choice, &block_choice, instance.n*instance.n, BLOCK_SIZE_CHOICE);
    setting_kernel_parameters_tour(&grid_tour, &block_tour, &iterationsInTour, instance.n, colony.n_ants);
    setting_kernel_parameters_phero(&grid_phero, &block_phero, &iterationsInPhero, instance.n, colony.n_ants);

    pdebug ("Compute Total parameters are number of blocks %d number of threads %d\n", grid_choice.x, block_choice.x);
    pdebug ("Next Tour parameters: blocks %d, threads %d, number of tiles %d\n", grid_tour.x, block_tour.x, iterationsInTour);
    pdebug ("Pheromone kernel parameters are number of blocks %d number of threads %d\n", grid_phero.x, block_phero.x);


/*    cudaEvent_t starttodo, stoptodo; // events
    cudaEventCreate(&startodo);
    cudaEventCreate(&stoptodo);
    float elapsedTimetodo, totaltodo = 0.0f;
*/



    for (i = 0; i < colony.n_tries; ++i) {

        CHECK_CUDA(cudaMemset (d_pheromone,0, instance.n*instance.n*sizeof(float))); //Reset Pheromone matrix
        pdebug ("------Running the %d try, consisting on %d tours---------------\n ", i, colony.n_tours);
        for (j = 0; j< colony.n_tours; ++j){
            
            computeTotalkernel<<<grid_choice, block_choice>>>(d_total, d_pheromone, d_distance, instance.n, colony.alpha, colony.beta,colony.rho);
            cudaThreadSynchronize();          
            #ifdef TIMING
            cudaEventRecord(start,0);
            #endif

	    /// I-Roulette
           /////////// nextTour_kernel<<< grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour);
            /// Scan + Stencil
            nextTour_kernel_reducing_random<<< grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour);
            
	    #ifdef TIMING        
            cudaDeviceSynchronize();
            cudaEventRecord (stop);
            cudaEventSynchronize(stop);

            cudaEventElapsedTime(&elapsedTime, start, stop);
	    if (j%100==0){	
              printf ("Elapsed Time is %f\n", elapsedTime);
	    }
            total+=elapsedTime;
            #endif

            CHECK_CUDA(cudaMemcpy(colony.lengthList, d_lengthList, mem_size_llist, cudaMemcpyDeviceToHost));

            #ifdef DEBUG
            CHECK_CUDA(cudaMemcpy(h_tours, d_tour, mem_size_h_tours, cudaMemcpyDeviceToHost));
            checkForErrors (h_tours, instance.n, colony.n_ants);
            #endif 

            bestIt = getLenghtIterationBestTour(colony.lengthList, colony.n_ants, d_tour, d_bestTour,&bestLength,instance.n, j);
            
            pheromone_update_with_atomic<<<grid_phero, block_phero>>> (d_pheromone, instance.n, d_tour, colony.n_ants, d_lengthList, iterationsInPhero);
            cudaThreadSynchronize();
        }
    }

    printf ("Total Time is: \n%f\n",total);
    printf ("The best tour found is:\n");
    cudaMemcpy (h_bestTour, d_bestTour, (instance.n+1)*sizeof(unsigned int), cudaMemcpyDeviceToHost);
    for (int k=0; k<(instance.n+1); ++k) {
        printf ("%d, ", h_bestTour[k]);
    }
    printf ("\n");
    
    #ifdef TIMING
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    #endif
    CHECK_CUDA(cudaFree (devStates));
    return bestLength;
}
