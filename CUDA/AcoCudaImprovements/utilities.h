/*
 * Version: 1.0
 * File: utilities.h
 * Author: Jose M. Cecilia
 * Purpose: Some addtional utilities to manage both CPU and GPUModel TSP problem
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. cecilia
 */

/***************************************************************************

    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain
***************************************************************************/
#include <cuda_runtime.h>

#define INFTY                 LONG_MAX
#define MAXIMUM_NO_TRIES      100

#define TRUE  1
#define FALSE 0

/* general macros */

#define MAX(x,y)        ((x)>=(y)?(x):(y))
#define MIN(x,y)        ((x)<=(y)?(x):(y))

#ifdef DEBUG
#define pdebug(...) do{if(DEBUG)printf(__VA_ARGS__);printf(". In file: %s. ",__FILE__); printf("In Line: %d\n",__LINE__);} while(0);
#else
#define pdebug(...)
#endif


#ifdef __cplusplus
#define __kernel__ extern "C"
#else
#define __kernel__
#endif




/* constants for a random number generator, for details see numerical recipes in C */

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define MASK 123459876

#define BLOCK_SIZE_CHOICE 512
#define MAX_BLOCKS 65535
#define TAM_BLOCK_PHERO3 512
#define BLOCK_SIZE_ATOMIC_DEVICE 512
#define MACROBITS 0x0000000000000001
///YOU SHOULD CHANGE NUM_ANTS
#define BLOCK_SIZE_NEXT_TOUR 320
#define NUM_ANTS 10
#define WARP_SIZE 32


//Number of ants should be BLOCK_SIZE_NEXT_TOUR/WARP SIZE4
extern long int seed;


__kernel__ void setting_kernel_parameters (dim3 * grid, dim3 * block, unsigned int reference, unsigned int block_size);

__kernel__ void setting_kernel_parameters_tour(dim3 *grid_tour, dim3 *block_tour, unsigned int * iterationsInTour, int  n, int n_ants);

__kernel__ void setting_kernel_parameters_phero(dim3 * grid_phero, dim3 *block_phero, unsigned int * iterationsInPhero, int n, int n_ants);

__kernel__ unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, unsigned int * d_tour, unsigned int * d_best, unsigned int * bestLenght, unsigned int number_of_cities);

//__kernel__ void printDevProp(cudaDeviceProp devProp);
