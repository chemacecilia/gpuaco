// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
// includes, project
#include "cutil_inline.h"
#include "include/libaco/util.h"

#define TIMING

// includes, kernels
#include "simpleAntGPU_kernel.cu"
//
////////////////////////////////////////////////////////////////////////////////
// declaration, forward 

#define BLOCK_SIZE_CHOICE 256
#define MAX_BLOCKS 65535
#define TAM_BLOCK_PHERO3 256
#define BLOCK_SIZE_ATOMIC_DEVICE 256

void setting_kernel_parameters(int * threads, int * blocks, unsigned int reference, unsigned int block_size);
void printTour(unsigned int * best, unsigned int number_of_cities);
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, unsigned int * d_tabu, unsigned int * d_best, unsigned int * bestLenght, unsigned int number_of_cities);
 

////////////////////////////////////////////////////////////////////////////////
//! Entry point for Cuda functionality on host side
//! @param  
////////////////////////////////////////////////////////////////////////////////
extern "C" void
antColonyGPULauncher(const unsigned int number_of_ants, const unsigned int number_of_cities, unsigned int * distance, float * pheromone, float * choice_info, unsigned int * ants, float alpha, float beta, float evaporation_rate, unsigned int iterations, float elitist_weight, unsigned int elitist_ants, unsigned int best_so_far_frequency, float a, AcoType acotype)
{ 
    cudaSetDevice(1);	
    int device;
    cudaGetDevice(&device);

    printf ("\n...................In the GPU launcher on the device %d...................\n", device);	
    
    const unsigned int mem_size_distance = sizeof(unsigned int) * number_of_cities * number_of_cities;
    const unsigned int mem_size_pheromone = sizeof(float) * number_of_cities * number_of_cities;
    const unsigned int mem_size_tour = sizeof(unsigned int) * number_of_ants * (number_of_cities+1);
    const unsigned int mem_size_lenghtList = sizeof(unsigned int) * number_of_ants;
    const unsigned int mem_size_choiceinfo = sizeof(float)*number_of_cities*number_of_cities;
    const unsigned int mem_size_seed = number_of_ants * number_of_cities* sizeof(curandState); 
    
    unsigned int totalBytesAllocated =0; //Bytes allocated on the GPU
    
    
    //////////////////////////////////////////////////////////////////
    ///////////////////Initialize Distances///////////////////////////
    /////////////////////////////////////////////////////////////////
    
    // allocate memory for distance
    unsigned int * d_distance; 
    cutilSafeCall(cudaMalloc((void**) &d_distance, mem_size_distance));
    // copy host memory to device		
 
    cutilSafeCall(cudaMemcpy(d_distance, distance, mem_size_distance,
			     cudaMemcpyHostToDevice) );
    
    totalBytesAllocated+=mem_size_distance;

    ////////////////////////////////////////////////////////////////////////
    ///////////////Initialize Choice Information////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    float * d_choiceInfo;
    cutilSafeCall(cudaMalloc((void**) &d_choiceInfo, mem_size_choiceinfo));
 
    totalBytesAllocated+=mem_size_choiceinfo;
		
    ///////////////////////////////////////////////////////////////////////////
    ///////////////Initialize Pheromone Information////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    
    float* d_pheromone; 					
 		
    cutilSafeCall(cudaMalloc((void**) &d_pheromone, mem_size_pheromone));
    // copy host memory to device
    cutilSafeCall(cudaMemcpy(d_pheromone, pheromone, mem_size_pheromone,
			     cudaMemcpyHostToDevice) );
    
    ///////////////////////////////////////////////////////////////////////////
    /////////////////Initialize Tour information//////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    
    unsigned int* d_tour;
    cutilSafeCall(cudaMalloc((void**) &d_tour, mem_size_tour)); 
    totalBytesAllocated+=mem_size_tour;

    ///////////////////////////////////////////////////////////////////////////
    /////////////////Initialize Tour Lenght information////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    //the lenght of the best tour so far
    unsigned int bestLenght=UINT_MAX;  		
    
    // allocate device memory for the lenght list
    unsigned int * d_lenghtList;
    cutilSafeCall(cudaMalloc((void**) &d_lenghtList,mem_size_lenghtList));
    //Lenghts of all tour taken by each Ant			
    unsigned int * h_lenghtList = (unsigned int *) malloc (mem_size_lenghtList);
    totalBytesAllocated+=mem_size_lenghtList;

    ///////////////////////////////////////////////////////////////////////////
    /////////////////////////Best tour information ////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    
    //the best tour so far
    unsigned int * d_bestTour;
    cutilSafeCall(cudaMalloc((void **) &d_bestTour, mem_size_tour));
    unsigned int * h_bestTour= (unsigned int *)malloc (mem_size_tour);
    totalBytesAllocated+=mem_size_tour;
    
    ///////////////////////////////////////////////////////////////////////////
    ///////////////Initialize for random numbers on the GPU////////////////////
    ///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
  ///////////////Initialize for random numbers on the GPU////////////////////
  ///////////////////////////////////////////////////////////////////////////
  
    curandState *devStates;
  // Allocate space for prng states on device, one for each ant
    cutilSafeCall(cudaMalloc((void **)&devStates,mem_size_seed));
    totalBytesAllocated+=mem_size_seed;
    
    int num_blocks, num_threads;		
    setting_kernel_parameters(&num_threads, &num_blocks, number_of_cities*number_of_ants, BLOCK_SIZE_CHOICE); 
  
    dim3 gridRandom (1,1);
    dim3 threadsRandom(1,1);
    
    if (num_blocks>=MAX_BLOCKS) {
      float rounding = ceil(sqrt(num_blocks));
      gridRandom.x=rounding;
      gridRandom.y=rounding;
      threadsRandom.x = num_threads;						
      std::cout << "Setting Random kernel parameters. Number of threads is "<< threadsRandom.x <<", and blocks " << gridRandom.x << ", "<< gridRandom.y << std::endl;		   
    }
    else {
      gridRandom.x=num_blocks;
      threadsRandom.x = num_threads;						
      std::cout << "Setting Random kernel parameters. Number of threads is "<< threadsRandom.x <<", and blocks " << gridRandom.x << std::endl;		   
    }  
    
    setup_kernel<<<gridRandom, threadsRandom>>>(devStates, number_of_ants*number_of_cities);
    cutilCheckMsg("Kernel execution failed Setup");
    cudaThreadSynchronize(); 
    
    totalBytesAllocated+=mem_size_seed;
    
    
    ///////////////////////////////////////////////////////////////////////////
    ///////////////Setting GPU parameter //////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    
    //////////////////////////////////////////////////////////////////////////
    ////////////Setting parameters for the Choiceinfokernel//////////////////
    /////////////////////////////////////////////////////////////////////////
    		
    setting_kernel_parameters(&num_threads, &num_blocks, number_of_cities*number_of_cities, BLOCK_SIZE_CHOICE); 
    
    dim3 grid0 (1,1);
    dim3 threads0(1,1);
    
    if (num_blocks>=MAX_BLOCKS) {
      float rounding= ceil(sqrt(num_blocks));
      grid0.x=rounding;
      grid0.y=rounding;
      threads0.x = num_threads;						
      std::cout << "Choice info kernel. Number of threads is "<< threads0.x <<", and blocks " << grid0.x << ", "<< grid0.y << std::endl;    
    }
    else {
      grid0.x=num_blocks;
      threads0.x = num_threads;						
      std::cout << "Choice info kernel. Number of threads is "<< threads0.x <<", and blocks " << grid0.x << std::endl;			
    }
    
    //////////////////////////////////////////////////////////////////////////
    ////////////Setting parameters for the Next Tour kernel//////////////////
    /////////////////////////////////////////////////////////////////////////
    
    ///1 block per ant. And as many threads as cities or fixed number of them 
    dim3 grid1(number_of_ants);
    if (BLOCK_SIZE_NEXT_TOUR > number_of_cities) {
       std::cout << "The block size macro should be smaills than the number of cities\n";
       return;		
    }	
    
    int threads = BLOCK_SIZE_NEXT_TOUR;	
    
    dim3 threads1(threads);
    unsigned int iterNextTour=ceil((float)number_of_cities/threads1.x);
    std::cout << "Next tour kernel. Number of threads is "<< threads1.x <<", blocks " << grid1.x << ", and iterations " << iterNextTour << std::endl;			
    
    //////////////////////////////////////////////////////////////////////////
    ////////////Setting parameters for the Pheromone Update kernel////////////
    /////////////////////////////////////////////////////////////////////////
    
    dim3 gridPheromone (1,1);
    dim3 threadsPheromone (1,1);
    int iterationsPheromones6;	
   
    gridPheromone.x= number_of_ants;
    threadsPheromone.x = (number_of_cities<BLOCK_SIZE_ATOMIC_DEVICE)?number_of_cities:BLOCK_SIZE_ATOMIC_DEVICE; 	
    iterationsPheromones6 = ceil((float)number_of_cities/BLOCK_SIZE_ATOMIC_DEVICE);
    printf ("Threads in the Pheromone Update with atomic instructions kernel 6 (x)= (%d) and blocks (%d). Number of iterations per block is %d\n", threadsPheromone.x, gridPheromone.x, iterationsPheromones6);		
    
    iterations=1;    
    std::cout << "Number of iterations is " << iterations << "\n";
    cudaEvent_t startChoice, stopChoice, startReduc, stopReduc, startNext, stopNext, startPher, stopPher;
    cudaEventCreate(&startChoice);
    cudaEventCreate(&stopChoice);
    cudaEventCreate(&startReduc);
    cudaEventCreate(&stopReduc);
    cudaEventCreate(&startNext);
    cudaEventCreate(&stopNext);
    cudaEventCreate(&startPher);
    cudaEventCreate(&stopPher);
    cudaEventRecord(startChoice, 0);
    float choiceTime, nextTime, pherTime, elapsedTime, accu;
    choiceTime=nextTime=pherTime=accu=0.0;
    for (int i=0; i<iterations; i++) {	

      cudaEventRecord(startChoice, 0);    	
      init_choiceinfo_kernel <<<grid0,threads0>>> (d_choiceInfo,d_pheromone,d_distance, number_of_cities, alpha, beta,evaporation_rate);   
      cutilCheckMsg("Kernel execution failed Choice information");	
      cudaThreadSynchronize(); 	
      cudaEventRecord(stopChoice, 0);
      cudaEventSynchronize(stopChoice);
      cudaEventElapsedTime(&elapsedTime, startChoice, stopChoice);
      std::cout << "Total execution time for Init Choice: " <<elapsedTime <<", msec \n"; 			
      choiceTime+=elapsedTime;
      accu+= elapsedTime;				
      
      cudaEventRecord(startNext, 0);
      cudaFuncSetCacheConfig(nextTour_kernel, cudaFuncCachePreferShared);
      nextTour_kernel<<< grid1, threads1>>>(number_of_cities, number_of_ants, d_distance,d_tour,d_lenghtList,d_choiceInfo, devStates, iterNextTour); 	  		          
      cutilCheckMsg("Kernel execution failed Next tour kernel");
      cudaThreadSynchronize(); 						
           
      cudaEventRecord(stopNext, 0);
      cudaEventSynchronize(stopNext);
      cudaEventElapsedTime(&elapsedTime, startNext, stopNext);
      std::cout << "Total execution time for Next: " <<elapsedTime <<", msec \n"; 			
      accu+= elapsedTime;
      nextTime+=elapsedTime;

      //cudaEventRecord(startReduc, 0);		
      
      
      cutilSafeCall(cudaMemcpy(h_lenghtList, d_lenghtList, mem_size_lenghtList, cudaMemcpyDeviceToHost) ); 
 
      /*std::cout << "Las long de los tour para la iteracion" << i << std::endl;      
      for (int k=0; k<number_of_ants; k++) {
	std::cout << h_lenghtList[k] << ", ";
	}*/
      
      unsigned int bestIt = getLenghtIterationBestTour(h_lenghtList, number_of_ants, d_tour, d_bestTour,&bestLenght, number_of_cities); 		
      /*cudaEventRecord(stopReduc, 0);
      cudaEventSynchronize(stopReduc);
      cudaEventElapsedTime(&elapsedTime, startReduc, stopReduc);
      std::cout << "Total execution time for Reduction: " <<elapsedTime <<", msec \n"; 	*/		
 
      cudaEventRecord(startPher, 0);				  
      
      // Kernel parameters for the Pheromone Update kernel using atomic operations with only shared				 
      //std::cout << "Pheromone update sin shared memory\n";
      
      pheromone_update_with_atomic<<<gridPheromone, threadsPheromone>>> (d_pheromone, number_of_cities, d_tour, number_of_ants, d_lenghtList,iterationsPheromones6);		
      cutilCheckMsg("Kernel execution failed Pheromone Update");
      cudaThreadSynchronize(); 			
      
      cudaEventRecord(stopPher, 0);
      cudaEventSynchronize(stopPher);
      cudaEventElapsedTime(&elapsedTime, startPher, stopPher);
      std::cout << "Total execution time for Pheromone: " <<elapsedTime <<", msec \n";
      accu+=elapsedTime;
      pherTime+=elapsedTime;
    }

    cudaEventElapsedTime(&elapsedTime, startChoice, stopChoice);
    std::cout << "Total execution time: " <<accu/iterations <<", msec \n"; 		 	
    
    //std::cout << "Total execution time: " <<timer() << "sec \t";
    std::cout << std::endl;
    std::cout << "Best Tour Lenght" << std::endl;
    std::cout << bestLenght << "\t";
    std::cout << std::endl;
    
    std::cout << "Best Ordering" << std::endl;
    cutilSafeCall(cudaMemcpy(h_bestTour, d_bestTour, (number_of_cities+1)*sizeof(unsigned int), cudaMemcpyDeviceToHost));				
    printTour(h_bestTour, number_of_cities);
    std::cout << std::endl;
    
    #ifdef TIMING
    FILE *f1,*f2,*f3,*f4,*f5;
    f1 = fopen("out/choice","a");
    f2 = fopen("out/tour","a");
    f3 = fopen("out/pher","a");
    f4 = fopen("out/total","a");
    f5 = fopen("out/result","a");
    fprintf(f1,"%f ",choiceTime/iterations);
    fprintf(f2,"%f ",nextTime/iterations);
    fprintf(f3,"%f ",pherTime/iterations);
    fprintf(f4,"%f ",accu/iterations);
    fprintf(f5,"%d ",bestLenght);
    if (number_of_ants==2392) {
      fprintf(f1,"\n");
      fprintf(f2,"\n");
      fprintf(f3,"\n");
      fprintf(f4,"\n");
      fprintf(f5,"\n");
    }
    #endif

    cudaEventDestroy(startChoice);
    cudaEventDestroy(stopChoice);
    cudaEventDestroy(startReduc);
    cudaEventDestroy(stopReduc);
    cudaEventDestroy(startNext);
    cudaEventDestroy(stopNext);
    cudaEventDestroy(startPher);
    cudaEventDestroy(stopPher);
    cutilSafeCall (cudaFree(d_distance));
    cutilSafeCall (cudaFree(d_pheromone));
    cutilSafeCall (cudaFree(d_tour));
    cutilSafeCall (cudaFree(d_lenghtList));
    cutilSafeCall (cudaFree(d_bestTour));
    cutilSafeCall (cudaFree(devStates));
    free (h_bestTour);	
    free (h_lenghtList);	
    
    cudaThreadExit();
}


void setting_kernel_parameters(int * threads, int * blocks, unsigned int reference, unsigned int block_size) {
  
  *threads= (reference <= block_size)?reference:block_size;
  *blocks = (reference%*threads==0)?(reference / *threads):(reference / *threads)+1; 
  
}



void printTour(unsigned int * best, unsigned int number_of_cities) {

  for (int i=0; i<=number_of_cities;i++)
    std::cout << best[i] << ", ";
		
  std::cout << std::endl;		
}

///
///It returns the best in the current iteration, and controls the best so far, doing a copy in device memory for the best tour 
///
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, unsigned int * d_tour, unsigned int * d_best, unsigned int * bestLenght, unsigned int number_of_cities) 
{
  
  unsigned int bestIt=lenght[0];
  unsigned int antBest = 0;
  
  for (int i=1;i<number_of_ants;i++) {
    if (lenght[i]<bestIt) {
      bestIt = lenght[i];
      antBest =i;
    }
  }
  
  
  /* int size = (number_of_cities+1)*number_of_ants*sizeof(uint);

  unsigned int * partialbest = (unsigned int * ) malloc (size);
  cutilSafeCall(cudaMemcpy(partialbest, d_tour, size, cudaMemcpyDeviceToHost) );
  
  std::cout << "Todas los tours para todas las ants \n";
  for (int j = 0; j< number_of_ants; j++) {
    std::cout << "Para la ant " << j << std::endl;
    for (int i=0; i<=number_of_cities; i++) {
      std::cout << partialbest[(j*(number_of_cities+1))+i] <<", ";
    }
    std::cout << std::endl;
  }
  */

  unsigned int * aux = d_tour;
  aux=aux+antBest*(number_of_cities+1);
  if (bestIt < *bestLenght) {
    // std::cout << "La mejor ant es " << antBest << std::endl;
    *bestLenght = bestIt;			
    cutilSafeCall(cudaMemcpy(d_best, aux, (number_of_cities+1)*sizeof(unsigned int), cudaMemcpyDeviceToDevice) );				
  }     	
  
  return bestIt;		  
}





