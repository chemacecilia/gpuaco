#!/bin/bash
 
for i in 16 32 64 128 256 512 1024 
do
  cd ../../libaco/trunk/
  cp src/simpleAntColonyGPU_launcher_$i.cu src/simpleAntColonyGPU_launcher.cu
  cp src/simpleAntGPU_kernel_$i.cu src/simpleAntGPU_kernel.cu
  make clean; make && cd ../../gpuacotsp/trunk/ && make clean && make

  ./bin/acotsp --simple -f ./benchmarks/tsplib/d198.tsp -m 198 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/a280.tsp -m 280 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/lin318.tsp -m 318 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/pcb442.tsp -m 442 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/rat783.tsp -m 783 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/pr1002.tsp -m 1002 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/pcb1173.tsp -m 1173 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/d1291.tsp -m 1291 >> results/out_$i 2>> results/err_$i
  ./bin/acotsp --simple -f ./benchmarks/tsplib/pr2392.tsp -m 2392 >> results/out_$i 2>> results/err_$i
done
 
