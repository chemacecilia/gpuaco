/*
 * Version: 1.0
 * File: utilities.h
 * Author: Jose M. Cecilia
 * Purpose: Some addtional utilities to manage both CPU and GPUModel TSP problem
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. cecilia
 */

/***************************************************************************
    File Purpose: implementation of actual computation of the main routines on the GPU
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain
 *******************************************************************************/
#ifdef __cplusplus
#define __kernel__ extern "C"
#else
#define __kernel__
#endif


#include "InOut.h"

#define CHECK_CUDA(x) do {\
    cudaError_t retval = (x);\
    if (retval != cudaSuccess) { \
        fprintf (stderr, "CUDA error with code %d \n", retval);\
        exit (-1);\
    }\
} while (0)


//#define BLOCK_SIZE_CHOICE 512
//#define MAX_BLOCKS 65535
//#define TAM_BLOCK_PHERO3 512
//#define BLOCK_SIZE_ATOMIC_DEVICE 512
//#define MACROBITS 0x00000001
///OJO CAMBIA EL LOGARITMO EN BASE 2
//#define BLOCK_SIZE_NEXT_TOUR 32
//#define LOG_BLOCK_SIZE_NEXT_TOUR 5

__kernel__
int antSystemTSP (problem instance, colony colony);

