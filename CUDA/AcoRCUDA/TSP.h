/*
 * Version: 1.0
 * File: TSP.h
 * Author: Jose M. Cecilia
 * Purpose: Model TSP problem
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. cecilia
 */

/***************************************************************************

    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain

 **************************************************************************/

#define LINE_BUF_LEN 100

struct point {
    double x;
    double y;
};

struct problem{
    char name[LINE_BUF_LEN]; //Instance name
    char edge_weight_type[LINE_BUF_LEN];
    unsigned int optimum; //Optimal tour length if known, otherwise a bounf
    unsigned int n; //number of cities
    struct point * nodeptr; //Array of struct containig coordinates of nodes
    unsigned int * distance; //distance matrix: distance[i][j] gives distance between city i and j
};

typedef struct problem  problem;
extern unsigned int * d_distance;

unsigned int (*distance) (unsigned int i, unsigned int j, struct point *); // pointer to function returning distance
unsigned int round_distance (unsigned int i, unsigned int j, struct point *);
unsigned int ceil_distance (unsigned int i, unsigned int j, struct point *);
unsigned int geo_distance (unsigned int i, unsigned int j, struct point *);
unsigned int att_distance (unsigned int i, unsigned int j, struct point *);
unsigned int round_distance (unsigned int i, unsigned int j, struct point *);
void compute_distances(struct problem * problem);
