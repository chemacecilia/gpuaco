/***************************************************************************

    Program's name: gpuacotsp

    Ant Colony Optimization algorithm tailored to the GPU for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos
                  30107 Murcia
                  Spain

***************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <limits.h>
#include "InOut.h"
#include "utilities.h"
#include "parse.h"



/* ------------------------------------------------------------------------ */

FILE *report, *comp_report, *stat_report;

char name_buf[LINE_BUF_LEN];
int  opt;


/**
      FUNCTION: parse and read instance file
      INPUT:    instance name
      OUTPUT:   list of coordinates for all nodes
      COMMENTS: Instance files have to be in TSPLIB format, otherwise procedure fails
*/
struct point * read_etsp(const char *tsp_file_name, problem * problem){

    FILE * tsp_file;
    char buf[LINE_BUF_LEN];
    int i, j,n;

    struct point *nodeptr;

    tsp_file = fopen(tsp_file_name, "r");
    if ( tsp_file == NULL ) {
	    fprintf(stderr,"No instance file specified, abort\n");
	    exit(1);
    }
    printf("\nreading tsp-file %s ... \n\n", tsp_file_name);

    fscanf(tsp_file,"%s", buf);

    while ( strcmp("NODE_COORD_SECTION", buf) != 0 ) {
	    if ( strcmp("NAME", buf) == 0 ) {
	        fscanf(tsp_file, "%s", buf);
	        fscanf(tsp_file, "%s", buf);
	        strcpy(problem->name, buf);
	        buf[0]=0;
	    }
	    else if ( strcmp("NAME:", buf) == 0 ) {
	        fscanf(tsp_file, "%s", buf);
	        strcpy(problem->name, buf);
	        buf[0]=0;
	    }
	    else if ( strcmp("COMMENT", buf) == 0 ){
	        fgets(buf, LINE_BUF_LEN, tsp_file);
	        buf[0]=0;
	    }
	    else if ( strcmp("TYPE", buf) == 0 ) {
	        fscanf(tsp_file, "%s", buf);
	        fscanf(tsp_file, "%s", buf);
	        if( strcmp("TSP", buf) != 0 ) {
		        fprintf(stderr,"\n Not a TSP instance in TSPLIB format !!\n");
		        exit(1);
	        }
	        buf[0]=0;
	    }
	    else if( strcmp("DIMENSION", buf) == 0 ){
	        fscanf(tsp_file, "%s", buf);
	        fscanf(tsp_file, "%d", &n);
	        problem->n = n;
	        assert ( n > 2 && n < 6000);
	        buf[0]=0;
	    }
	    else if( strcmp("DISPLAY_DATA_TYPE", buf) == 0 ){
	        fgets(buf, LINE_BUF_LEN, tsp_file);
	        buf[0]=0;
	    }
	    else if( strcmp("EDGE_WEIGHT_TYPE", buf) == 0 ){
	        buf[0]=0;
	        fscanf(tsp_file, "%s", buf);
	        buf[0]=0;
	        fscanf(tsp_file, "%s", buf);
	        if ( strcmp("EUC_2D", buf) == 0 ) {
		        distance = round_distance;
	        }
	        else if ( strcmp("CEIL_2D", buf) == 0 ) {
		        distance = ceil_distance;
	        }
	        else if ( strcmp("GEO", buf) == 0 ) {
		        distance = geo_distance;
	        }
	        else if ( strcmp("ATT", buf) == 0 ) {
		        distance = att_distance;
	        }
	        else
		        fprintf(stderr,"EDGE_WEIGHT_TYPE %s not implemented\n",buf);
	        strcpy(problem->edge_weight_type, buf);
	        buf[0]=0;
	    }
	    buf[0]=0;
	    fscanf(tsp_file,"%s", buf);
    }

    if( strcmp("NODE_COORD_SECTION", buf) == 0 ){
	}
    else{
	    fprintf(stderr,"\n\nSome error ocurred finding start of coordinates from tsp file !!\n");
	    exit(1);
    }

    if( (nodeptr = (struct point *) malloc(sizeof(struct point) * n)) == NULL )
	    exit(EXIT_FAILURE);
    else {
	    for ( i = 0 ; i < n ; i++ ) {
	        fscanf(tsp_file,"%d %lf %lf", &j, &nodeptr[i].x, &nodeptr[i].y );
	    }
    }
	return (nodeptr);
}



void assignValues (options * opt, problem * problem, colony * colony) {

    colony->alpha = opt->arg_alpha;
    colony->beta = opt->arg_beta;
    colony->n_ants = opt->arg_ants;
    colony->rho = opt->arg_rho;
    colony->n_tries = opt->arg_tries;
    problem->n = opt->arg_tours;
    problem->nodeptr = read_etsp(opt->arg_tsplibfile, problem);

    printf("calculating distance matrix ..\n\n");
    compute_distances(problem);
    printf(" .. done\n");

}



/**
 * Function initialize the program in the host
 * @input: program arguments, need for parsing command line
 */
void init_program( int argc, char *argv[], problem * problem, colony * colony)
{

  options options;
  char temp_buffer[LINE_BUF_LEN];

  printf(PROG_ID_STR);
  //set_default_parameters();
  //setbuf(stdout,NULL);

  parseValues(argc, argv, &options);

  pdebug (".......Values are parsed.......");
  assert (options.arg_ants < MAX_ANTS-1);
  assert (options.arg_tries <= MAXIMUM_NO_TRIES);


  int devCount;
  cudaGetDeviceCount(&devCount);
  printf("There are %d CUDA devices.\n", devCount);
  cudaSetDevice (options.device);
  int dev;
  cudaGetDevice(&dev);
  printf ("******The device chosen is %d****\n", dev);
  assignValues (&options, problem, colony);


  //Reading the TSP
  sprintf(temp_buffer,"best.%s",problem->name);
  report = fopen(temp_buffer, "w");
  sprintf(temp_buffer,"cmp.%s",problem->name);
  comp_report = fopen(temp_buffer, "w");
  sprintf(temp_buffer,"stat.%s",problem->name);
  stat_report = fopen(temp_buffer, "w");

  //Pheromone update initialization
  pdebug ("Calling initialization of pheromone Trails\n");
  init_pheromone_trails(0.1f, problem, colony);

  pdebug ("Calling initilization of tour information\n");
  init_tour_information(problem, colony);
  printf("\nFinally set ACO algorithm specific parameters, typically done as proposed in literature\n\n");

}


 void exit_program (problem * instance, colony * colony) {

   cudaFreeHost(instance->distance);
   cudaFree (d_distance);
   free(instance->nodeptr);
   cudaFreeHost (colony->pheromone);
   cudaFreeHost (colony->total);
   cudaFreeHost (colony->lengthList);
   cudaFree(d_tour);
   cudaFree (d_lengthList);
   cudaFree (d_bestTour);
   cudaFree (d_pheromone);
   cudaFree (d_total);

 }


/*
      FUNCTION:       print distance matrix
      INPUT:          none
      OUTPUT:         none
*/
/*void printDist(void){
  long int i,j;

  printf("Distance Matrix:\n");
  for ( i = 0 ; i < instance.n ; i++) {
    printf("From %ld:  ",i);
    for ( j = 0 ; j < instance.n - 1 ; j++ ) {
      printf(" %ld", instance.distance[i*instance.n+j]);
    }
    printf(" %ld\n", instance.distance[i*instance.n+instance.n-1]);
    printf("\n");
  }
  printf("\n");
}
*/


/*
      FUNCTION:       print heuristic information
      INPUT:          none
      OUTPUT:         none
*/
/*void printHeur(void){
  long int i, j;

  printf("Heuristic information:\n");
  for ( i = 0 ; i < instance.n ; ++i) {
    printf("From %ld:  ",i);
    for ( j = 0 ; j < instance.n - 1 ; ++j ) {
      printf(" %.3f ", HEURISTIC(i,j));
    }
    printf(" %.3f\n", HEURISTIC(i,j));
    printf("\n");
  }
  printf("\n");
}
*/


/*
      FUNCTION:       print pheromone trail values
      INPUT:          none
      OUTPUT:         none
*/
/*void printTrail(void){
  long int i,j;

  //printf("pheromone Trail matrix, iteration: %ld\n\n",iteration);
  for ( i = 0 ; i < instance.n ; ++i) {
    printf("From %ld:  ",i);
    for ( j = 0 ; j < instance.n ; ++j ) {
      printf(" %.10f ", pheromone[i*instance.n+j]);
      if ( pheromone[i*instance.n+j] > 1.0 )
	    printf("XXXXX\n");
    }
    printf("\n");
  }
  printf("\n");
}
*/





























