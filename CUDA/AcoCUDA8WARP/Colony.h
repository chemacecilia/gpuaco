/***************************************************************************
    File Purpose: implementation of procedures for ant's behavious in the colony
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*******************************************************************************/
#define HEURISTIC(i, j) (1.0/((double) instance.distance[(i*instance.n)+j]+0.1))
//add a small constant to avoid division by zero if a distance is zero

#define EPSILON     0.00000000000000000000000000000001
#define MAX_ANTS    20000000

/* Note that *tour needs to be allocated for length n+1 since the first city of
 * a tour (at position 0) is repeated at position n. This is done to make the
 * computation of the tour length easier
 * */

extern float   * d_pheromone; /* Pheromone matrix on the device */
extern float   * d_total; /* total information on the GPU */
extern unsigned int * d_tour; /* Tours ants have taken in the current tour*/
extern unsigned int * d_lengthList; /* List of lengths on the GPU*/
extern unsigned int * d_bestTour; /*The best tour so far on the GPU*/

struct colony {
    float   * pheromone; /* pheromone matrix, one entry for each arc */
    float   * total;     /* combination of pheromone times heuristic information */
    int n_ants; /* number of ants in the colony*/
    unsigned int * bestTour; /* The best tour so far*/
    unsigned int * lengthList; /* List of lengths of the tours taken by the ants*/
    float rho; /* parameter for evaporation*/
    float alpha; /* importance of trail*/
    float beta; /* Importance of heuristic value*/
    int n_tries; /* Number of tries */
};

typedef struct colony colony;
/* Pheromone manipulation etc. */

void init_pheromone_trails ( float initial_trail, struct problem *, struct colony *);

//void compute_total_information( void );

/* Information about the Tour information*/
void init_tour_information (struct problem *, struct colony *);


