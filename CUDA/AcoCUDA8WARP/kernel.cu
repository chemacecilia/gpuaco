/*
 * Version: 1.0
 * File: kernel.cu
 * Author: Jose M. Cecilia
 * Purpose: TheGPU kernels to manage the kernel execution
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. cecilia
 */
/***************************************************************************
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to the GPU) for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain

*******************************************************************************/

#include <stdio.h>
#include <curand_kernel.h>
#include "kernel.h"
#include "utilities.h"


// Print device properties
void printDevProp(cudaDeviceProp devProp){
  printf("Major revision number:         %d\n",  devProp.major);
  printf("Minor revision number:         %d\n",  devProp.minor);
  printf("Name:                          %s\n",  devProp.name);
  printf("Total global memory:           %d\n",  devProp.totalGlobalMem);
  printf("Total shared memory per block: %d\n",  devProp.sharedMemPerBlock);
  printf("Total registers per block:     %d\n",  devProp.regsPerBlock);
  printf("Warp size:                     %d\n",  devProp.warpSize);
  printf("Maximum memory pitch:          %d\n",  devProp.memPitch);
  printf("Maximum threads per block:     %d\n",  devProp.maxThreadsPerBlock);
  for (int i = 0; i < 3; ++i)
    printf("Maximum dimension %d of block:  %d\n", i, devProp.maxThreadsDim[i]);
  for (int i = 0; i < 3; ++i)
    printf("Maximum dimension %d of grid:   %d\n", i, devProp.maxGridSize[i]);
  printf("Clock rate:                    %d\n",  devProp.clockRate);
  printf("Total constant memory:         %d\n",  devProp.totalConstMem);
  printf("Texture alignment:             %d\n",  devProp.textureAlignment);
  printf("Concurrent copy and execution: %s\n",  (devProp.deviceOverlap ? "Yes" : "No"));
  printf("Number of multiprocessors:     %d\n",  devProp.multiProcessorCount);
  printf("Kernel execution timeout:      %s\n",  (devProp.kernelExecTimeoutEnabled ? "Yes" : "No"));
  return;
}


/****************** CUDA Kernels ***************/

/**
 * This kernel set ups the generation of random number on the GPU
 * It sets a different seed for each thread
 */
__global__ void setup_kernel(curandState *state, unsigned int m) {
  
  int id = threadIdx.x + blockIdx.x * blockDim.x; /* Each thread gets same seed, a different sequence number,
						     no offset */
  if (id<m)
    curand_init(1234*blockIdx.x, id, 0, &state[id]);
}


/**
 * Kernel to compute the total information
 * and it also performs the evaporation stage.
 */
__global__ void computeTotalkernel (float *d_choiceInfo, float *d_pheromone, unsigned int * d_distance, unsigned int n, float alpha, float beta, float rho)
{
  
  int tid = (blockIdx.y*blockDim.y*n)+(blockDim.x*blockIdx.x)+threadIdx.x;
  
  //if (tid== 0) printf ("Prueba con tid %d,", tid);
  if (tid<n*n){
    float pheromone;
    pheromone = d_pheromone[tid];
    float heuristic = 1.0 / ((float) d_distance[tid] + 0.1);
    d_choiceInfo[tid]=__powf(pheromone, alpha)*__powf(heuristic, beta);
    //Evaporation stage
    pheromone= pheromone*(1.0-rho);
    d_pheromone[tid] = pheromone;
  }
}



/*
  FUNCTION:       This function obtains the maximum of the array and the index of that maximum
  INPUT:          pointer to the probabilistic information and the relative index
  OUTPUT:         Relative index of the maximum probability
  (SIDE)EFFECTS:  The maximum probability per each ant is in the line 0
*/

__device__ inline void reduceMax(volatile float *g_idata, volatile unsigned int * index_sh, unsigned int n, int line_id)
{
  
//  index_sh[threadIdx.x]= index;
//  g_idata[threadIdx.x] = (index>=n)?0.0:g_idata[threadIdx.x];
  
  int warp_id = threadIdx.x >> 5; 
  asm("bar.sync %0, 32;" :: "r"(warp_id)); 
  
  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE/2; s>0; s>>=1) {
    if (line_id < s){
      if(g_idata[threadIdx.x] < g_idata[threadIdx.x + s]) {
	    g_idata[threadIdx.x] = g_idata[threadIdx.x + s];
	    index_sh[threadIdx.x] = index_sh[threadIdx.x + s];
      }
    }
  asm("bar.sync %0, 32;" :: "r"(warp_id)); 
  }
}


/**
 * This funtion set a city as a visited in a bitwise
 */
__device__ inline void setCityAsVisited(unsigned int city, unsigned long long int * tabul){
  
  //Division by 64 is the bit that represents that city.
  unsigned int bitTabul = (unsigned int) city>>5;
  //Modulo is the thread that manages that city. Modulo 64 as we are dealing with WARPS
  unsigned int threadTabul = (unsigned int) city& 0x1f;
  unsigned int lane_id = threadIdx.x&0x1f;
  
  // Set the city as a visited in the corresponding threads
  if (lane_id == threadTabul) {
    //     printf ("En setCityAsvisited thread %d is setting in the bit %d\n", threadTabul,bitTabul);
    unsigned long long int aux = (unsigned long long int) MACROBITS<<bitTabul;
    *tabul = *tabul & (~aux);
  }
}

/**
 * Reduction
 */
__device__ inline void reduction(volatile float *g_idata, unsigned int n, unsigned int index){
  
  int tid = threadIdx.x;
  int lane_id = threadIdx.x & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = (warp_id*WARP_SIZE)+lane_id;
  
  g_idata[warpIndex] = (index >= n) ? 0.0f : g_idata[warpIndex];
  
  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
    if (lane_id < s){
      g_idata[warpIndex] += g_idata[warpIndex + s];
    }
  }
}


/**
 * Reduction
 */
__device__ inline void reduction_shfl(float * prob, unsigned int n, unsigned int index){
  
#if __CUDA_ARCH__ >=350
  float aux;
  *prob = (index >= n) ? 0.0f : *prob;
  
  // do reduction in shared mem
  for(unsigned int s=WARP_SIZE>>1; s>0; s>>=1) {
    aux = __shfl_down(*prob, s);
    *prob+=aux;
  }
#endif
}


/**
 * Simple parallel scan of 2exp(k) elements with 2exp(k) threads
 */
inline __device__ void simplescan_shfl(volatile float * idata, int index, int n, int numBlocks){
  
#if __CUDA_ARCH__ >=350
  int tid = threadIdx.x;
  int lane_id = tid & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = warp_id*WARP_SIZE*numBlocks;
  
  float interpolate = 0.0f;
  
  for (int i=lane_id; i<(numBlocks*WARP_SIZE); i+=WARP_SIZE){
    
    float value = ((index+i)<n) ? idata[warpIndex+i]:0.0f;//Some cities may be null
    value = (lane_id==0)? value+interpolate:value;
    
    for (int offset = 1; offset < WARP_SIZE; offset <<= 1) {
      float temp= __shfl_up(value,offset);
      if (lane_id >= offset)
	value+=temp;
    }
    
    idata[warpIndex+i]=value;
    interpolate = __shfl(value, WARP_SIZE-1);
  }
#else 
  printf ("You shall compile with -arch>=35\n");
  exit(-1);
#endif
}

/**
 * Simple parallel scan of 2exp(k) elements with 2exp(k) threads
 */
inline __device__ void simplescan(volatile float * idata, int index, int n){
  
  int tid = threadIdx.x;
  int lane_id = tid & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = (warp_id*WARP_SIZE)+lane_id;
  
  idata[warpIndex] = (index<n) ? idata[warpIndex]:0.0f;//Some cities may be null
  
  if (index < n) {
    for (int offset = 1; offset < n; offset <<= 1) {
      float temp;
      if (lane_id >= offset)
	    temp = idata[warpIndex-offset];
      __syncthreads();
      if (lane_id >= offset)
	    idata[warpIndex]+=temp;
      __syncthreads();
    }
  }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * This function do a stencil check in order to select a city. Only
 * one thread will be in the frontier between positive and negative
 * values.
 */
__device__ inline bool stencilCheck (volatile float * sidata, float p, int n ,  int index) {
  
  int tid = threadIdx.x;
  int lane_id = tid & 0x1f;
  int warp_id = tid >> 5;
  int warpIndex = (warp_id*WARP_SIZE);
  
  bool check = false;
  //Stencil to check which city to go next. Only one thread can get in.
  //Thread 0 should be stencil
  for (int i = lane_id; i < n; i+=WARP_SIZE){
    float left_value = (i > 0) ? sidata[(warpIndex+i)-1] : 0.0f; // Prepare the stencil for thread 0.
    left_value -= p;
    float center_value = sidata[warpIndex+i] - p;
    check = ((left_value * center_value)<= 0);
  }
  return check;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/******************************* TOUR IMPLEMENTATIONS *********************************************/
/////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * This kernel performs the tour generation and pheromone by m ants. An ant is represented by a warp, and a thread
 * also represents a city or set of cities. 
 */
__global__ void nextTour_iroulette_pheromone(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads, float * d_pheromone){
  
  
#if __CUDA_ARCH__ >=350
  __shared__ volatile unsigned int tour_sh[NUM_CITIES]; // Tour so far, try to obtain coallesced

  float random; 
  int next_city, city_ini;
  unsigned int tourLenght=0;
  unsigned long long int tabul1=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  unsigned long long int tabul2=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = warp_id*(number_of_cities+1);
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;
  
  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
      localState = state[globalIndex];
      random = curand_uniform(&localState);
      next_city = (int) number_of_cities*random;
      tour_sh[warpIndex] = next_city;
    }
    
    next_city = __shfl(next_city,0);
    city_ini = next_city;
    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
      setCityAsVisited (next_city, &tabul1);
    else 
      setCityAsVisited ((next_city-2048),&tabul2);
    
    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){
      
      int indexChoiceInfo = next_city*number_of_cities;
      float choice;
      int indexaux;//Number of iterations
      unsigned int auxtabul;
      
      indexaux=0;
      
      float probabilities = 0.0f;
      float maxProbabilities = 0.0f;
      int maxIndex=lane_id;
      //Iterate over all cities, in tiles of 32-width
      for (int index=lane_id; index < number_of_cities; index+=WARP_SIZE){
	    //Calculates the values for the chunk
	    auxtabul = (index<2048)?tabul1>>indexaux:tabul2>>(indexaux&0x3F);// We use 64 bits for the tabu list
	    auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
	    choice = choice_info[indexChoiceInfo+index];
	    random = curand_uniform(&localState);     
	    probabilities = (float) choice*auxtabul*random;
        
	    if (probabilities > maxProbabilities) {
	        maxIndex=index;
	        maxProbabilities=probabilities;
	    }
	    indexaux++;
      }

      //Now, we have the maximum values of probabilities and the city associated with it for a 32-chunk
      float auxshfl;
      int auxMaxIndex;
      auxMaxIndex = __shfl_xor(maxIndex,16); 
      auxshfl = __shfl_xor(maxProbabilities, 16);      
      if (maxProbabilities < auxshfl) {
    	maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,8); 
      auxshfl = __shfl_xor(maxProbabilities, 8);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,4); 
      auxshfl = __shfl_xor(maxProbabilities, 4);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,2); 
      auxshfl = __shfl_xor(maxProbabilities, 2);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,1); 
      auxshfl = __shfl_xor(maxProbabilities, 1);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      next_city = __shfl(maxIndex,0);
      
      
      if (next_city < 2048)
	    setCityAsVisited (next_city, &tabul1);         
      else  
	    setCityAsVisited((next_city-2048),&tabul2);
      
      
      if (lane_id == 0 ) {
	    tourLenght += d_distance[indexChoiceInfo+next_city];
	    tour_sh[warpIndex+tourIndex] = next_city;                
      }
      tourLenght = __shfl(tourLenght,0);
      
    }//END FOR: ALL CITIES ARE VISITED
    

    //Empezaria pheromona

    //Complete the tour getting the beginning city
    if (lane_id == 0) {     
      state[globalIndex] = localState;
      tourLenght += d_distance[next_city*number_of_cities+city_ini];
      d_lenghtList[globalIndex] = tourLenght;
      tour_sh[warpIndex+number_of_cities] = city_ini;
    }

    int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1);
    for (int i=lane_id; i < number_of_cities;i+=WARP_SIZE){
        int coordx=tour_sh[warpIndex+i];
        int coordy=tour_sh[warpIndex+i+1];
        float pheromone = 0.0f;
    	d_tour[indexdtour+i]=coordx;//Write the results in device memory
        int pos = (coordy*number_of_cities)+coordx;
        int posim = (coordx*number_of_cities)+coordy;
        pheromone=(float)1.0f*(1.0f/tourLenght);
        atomicAdd(&d_pheromone[pos],pheromone);
        atomicAdd(&d_pheromone[posim],pheromone);
    }
  }//END IF 

#endif
}

/**
 * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
 * also represents a city or set of cities. 
 */
__global__ void nextTour_kernel_warp_iroulette_sf(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){
  
  
#if __CUDA_ARCH__ >=350
  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  
  float random; 
  int next_city, city_ini;
  unsigned int tourLenght;
  unsigned long long int tabul1=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  unsigned long long int tabul2=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = warp_id*WARP_SIZE;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;
  
  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
      localState = state[globalIndex];
      random = curand_uniform(&localState);
      next_city = (int) number_of_cities*random;
      tour_sh[warpIndex] = next_city;
      tourLenght = 0;
    }
    
    next_city = __shfl(next_city,0);
    city_ini = next_city;
    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
      setCityAsVisited (next_city, &tabul1);
    else 
      setCityAsVisited ((next_city-2048),&tabul2);
    
    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){
      
      int indexChoiceInfo = next_city*number_of_cities;
      float choice;
      int indexaux;//Number of iterations
      unsigned int auxtabul;
      
      //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
      if ((tourIndex & 0x1f) == 0) {
	    int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
    	d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
      }
      
      indexaux=0;
      
      float probabilities = 0.0f;
      float maxProbabilities = 0.0f;
      int maxIndex=lane_id;
      //Iterate over all cities, in tiles of 32-width
      for (int index=lane_id; index < number_of_cities; index+=WARP_SIZE){
	    //Calculates the values for the chunk
	    auxtabul = (index<2048)?tabul1>>indexaux:tabul2>>(indexaux&0x3F);// We use 64 bits for the tabu list
	    auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
	    choice = choice_info[indexChoiceInfo+index];
	    random = curand_uniform(&localState);     
	    probabilities = (float) choice*auxtabul*random;
        
	    if (probabilities > maxProbabilities) {
	        maxIndex=index;
	        maxProbabilities=probabilities;
	    }
	    indexaux++;
      }

      //Now, we have the maximum values of probabilities and the city associated with it for a 32-chunk
      float auxshfl;
      int auxMaxIndex;
      auxMaxIndex = __shfl_xor(maxIndex,16); 
      auxshfl = __shfl_xor(maxProbabilities, 16);      
      if (maxProbabilities < auxshfl) {
    	maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,8); 
      auxshfl = __shfl_xor(maxProbabilities, 8);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,4); 
      auxshfl = __shfl_xor(maxProbabilities, 4);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,2); 
      auxshfl = __shfl_xor(maxProbabilities, 2);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      auxMaxIndex = __shfl_xor(maxIndex,1); 
      auxshfl = __shfl_xor(maxProbabilities, 1);      
      if (maxProbabilities < auxshfl) {
	    maxIndex = auxMaxIndex;
	    maxProbabilities = auxshfl; 
      }
      
      next_city = __shfl(maxIndex,0);
      
      
      if (next_city < 2048)
	    setCityAsVisited (next_city, &tabul1);         
      else  
	    setCityAsVisited((next_city-2048),&tabul2);
      
      
      if (lane_id == 0 ) {
	    tourLenght += d_distance[indexChoiceInfo+next_city];
	    tour_sh[warpIndex+(tourIndex& 0x1f)] = next_city;                
      }
      
    }//END FOR: ALL CITIES ARE VISITED
    
    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);
    
    if (indexaux < number_of_cities){
      int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
      d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
    }
    
    //Complete the tour getting the beginning city
    if (lane_id == 0) {     
      state[globalIndex] = localState;
      tourLenght += d_distance[next_city*number_of_cities+city_ini];
      d_lenghtList[globalIndex] = tourLenght;
      int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
      d_tour[indexdtour] = city_ini;
    }
  }//END IF 
#endif
}

/**
 * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
 * also represents a city or set of cities. However, the selection process is now performed using scan
 * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions
 */
__global__ void nextTour_kernel_warp_iroulette(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){
  
  
  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  __shared__ volatile unsigned int  next_city_sh[BLOCK_SIZE_NEXT_TOUR];
  __shared__ volatile float prob_sh[BLOCK_SIZE_NEXT_TOUR]; 
  
  float random; 
  int next_city, city_ini;
  unsigned int tourLenght;
  unsigned long long int tabul1=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  unsigned long long int tabul2=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = warp_id*WARP_SIZE;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;
  
  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
      localState = state[globalIndex];
      random = curand_uniform(&localState);
      
      next_city_sh[warpIndex] = (int) number_of_cities*random;
      tour_sh[warpIndex] = next_city_sh[warpIndex];
      tourLenght = 0;
    }
    
    asm("bar.sync %0, 32;" :: "r"(warp_id)); 
    next_city = next_city_sh[warpIndex];
    city_ini = next_city;

    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
      setCityAsVisited (next_city, &tabul1);
    else 
      setCityAsVisited ((next_city-2048),&tabul2);
    
    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){
      
      int indexChoiceInfo = next_city*number_of_cities;
      float choice;
      int indexaux;//Number of iterations
      unsigned int auxtabul;
      
      //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
      if ((tourIndex & 0x1f) == 0) {
	    int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
    	d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
      }
      
      indexaux=0;
      
      float probabilities = 0.0f;
      float maxProbabilities = 0.0f;
      int maxIndex=lane_id;
      //Iterate over all cities, in tiles of 32-width
      for (int index=lane_id; index < number_of_cities; index+=WARP_SIZE){
	    //Calculates the values for the chunk
	    auxtabul = (index<2048)?tabul1>>indexaux:tabul2>>(indexaux&0x3F);// We use 64 bits for the tabu list
	    auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
	    choice = choice_info[indexChoiceInfo+index];
	    random = curand_uniform(&localState);     
	    probabilities = (float) choice*auxtabul*random;
        
	    if (probabilities > maxProbabilities) {
	        maxIndex=index;
	        maxProbabilities=probabilities;
	    }
	    indexaux++;
      }
      
      next_city_sh[threadIdx.x] = maxIndex; 
      prob_sh [threadIdx.x] = maxProbabilities;
      reduceMax(prob_sh, next_city_sh, number_of_cities, lane_id);
      next_city = next_city_sh[warpIndex]; 

      if (next_city < 2048)
	    setCityAsVisited (next_city, &tabul1);         
      else  
	    setCityAsVisited((next_city-2048),&tabul2);
      
      if (lane_id == 0 ) {
	    tourLenght += d_distance[indexChoiceInfo+next_city];
	    tour_sh[warpIndex+(tourIndex& 0x1f)] = next_city;                
      }
    
    }//END FOR: ALL CITIES ARE VISITED

    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);
    
    if (indexaux < number_of_cities){
      int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
      d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
    }
    
    //Complete the tour getting the beginning city
    if (lane_id == 0) {     
      state[globalIndex] = localState;
      tourLenght += d_distance[next_city*number_of_cities+city_ini];
      d_lenghtList[globalIndex] = tourLenght;
      int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
      d_tour[indexdtour] = city_ini;
    }
  }//END IF 
  
}



/**
 * This kernel performs the tour generation by m ants. An ant is represented by a warp, and a thread
 * also represents a city or set of cities. However, the selection process is now performed using scan
 * and stencil pattern. As threads in a warp works toguether for an ant, we can use shuffle instructions
 */
__global__ void nextTour_kernel_warp_reducing_random_k20(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){
  
#if __CUDA_ARCH__ >=350
  
  __shared__ volatile float probabilities_sh[BLOCK_SIZE_NEXT_TOUR]; //Stores probabilites to visit cities tilling 32 cities per ant (warp)
  __shared__ volatile unsigned int tour_sh[BLOCK_SIZE_NEXT_TOUR]; // Tour so far, try to obtain coallesced
  extern __shared__ volatile float partial_sums_sh[];//This can be NUMWARPS*iterationsInTour with
  __shared__ volatile unsigned int current_chunk_sh[NUM_ANTS];
  
  float random,p; 
  unsigned int next_city, city_ini, current_chunk;
  unsigned int tourLenght;
  unsigned long long int tabul1=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  unsigned long long int tabul2=0xFFFFFFFFFFFFFFFF; //Tabulist initialize to one every bit
  float probabilities = 0.0f;
  //Warp id of each thread in a bitwises
  int warp_id = threadIdx.x >> 5;
  int lane_id = threadIdx.x & 0x1f;
  unsigned int warpIndex = warp_id*WARP_SIZE;
  unsigned int chunkPartialSum = ceil((float)iterathreads/WARP_SIZE);
  unsigned int warpPartialSumIndex = warp_id*chunkPartialSum*WARP_SIZE;
  unsigned int globalIndex = (blockIdx.x * NUM_ANTS) + warp_id;
  curandState localState;
  
  if (globalIndex < number_of_ants){
    //Each ant is placed in a initial random city.
    if (lane_id == 0){
      localState = state[globalIndex];
      random = curand_uniform(&localState);
      state[globalIndex] = localState;
      next_city = (unsigned int) number_of_cities*random;
      tour_sh[warpIndex] = next_city;
      tourLenght = 0;
    }
    
    random = __shfl (random, 0);
    next_city = (int)__shfl((float)next_city,0);
    city_ini = next_city;
    //1024 is the maximum number of cities we can manage with a warp and 64-bit tabulist
    if (next_city < 2048)
      setCityAsVisited (next_city, &tabul1);
    else 
      setCityAsVisited ((next_city-2048),&tabul2);
    
    //Repeat until tabu list is full
    for (unsigned int tourIndex=1; tourIndex < number_of_cities; ++tourIndex){
      
      int indexChoiceInfo = next_city*number_of_cities;
      float choice;
      int indexaux;
      unsigned int auxtabul;
      
      // printf ("The current_city is %d\n", next_city);
      
      //When the tour in shared memory is full (32 cities have been visted), it is stored in global memory
      if ((tourIndex & 0x1f) == 0) {
	int indexdtour = globalIndex * (number_of_cities+1) + lane_id +  (((tourIndex >> 5)-1) * WARP_SIZE);
	d_tour[indexdtour]= tour_sh[warpIndex+lane_id];
      }
      
      //Iterate over all cities, in tiles of 32-width
      for (int index=0 ; index < (iterathreads-1); ++index){
	
	indexaux = lane_id + (index * WARP_SIZE); // Threads from all warps start with the first til
	auxtabul = (indexaux<2048)?tabul1>>index : tabul2>>(index&0x3F);// We use 64 bits for the tabu list
	auxtabul = (unsigned int) MACROBITS & auxtabul; // Check whether the city has been visited (0) or not (1)
	choice = choice_info[indexChoiceInfo+indexaux];
        
	probabilities = (float) choice*auxtabul;
	reduction_shfl(&probabilities, number_of_cities, indexaux);
	
	if (lane_id == 0){
	  // printf ("The warp %d the index partial sum  %d reductions is %f in tourIteration %d\n", warp_id, warpPartialSumIndex, probabilities, tourIndex);
	  partial_sums_sh[warpPartialSumIndex+index] = probabilities;
	}            
      }
      
      //The last chunk may not divisible by block size
      indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);
      
      if (indexaux < number_of_cities){
	// auxtabul = tabul >>(iterathreads-1);
	auxtabul = (indexaux<2048)?tabul1>>(iterathreads-1) : tabul2>>((iterathreads-1)&0x3F);// We use 64 bits for the tabu list
	auxtabul = (unsigned int)MACROBITS & auxtabul;
	float choice = choice_info[indexChoiceInfo+indexaux];
	probabilities = (float)choice*auxtabul;
      }
      
      //Reduction of the rest elements
      reduction_shfl(&probabilities, number_of_cities, indexaux);
      
      if (lane_id == 0){
	//printf ("The %d reductions is %f \n", (iterathreads-1), probabilities);
	partial_sums_sh[warpPartialSumIndex+(iterathreads-1)] = probabilities;
      }
      
      // Now, scan indexes stored in partial_sums_sh
      
      simplescan_shfl (partial_sums_sh, 0, iterathreads, chunkPartialSum);
      
      
      //Calculate p value to do the roulette. 
      if (lane_id == 0){
	p = random * partial_sums_sh[warpPartialSumIndex+(iterathreads-1)];
	//   if (blockIdx.x==0 && warp_id==0)
	//     printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+(iterathreads-1)]);
      }
      p = __shfl(p,0);
      
      ///////////////////////STENCIL CHECK FOR THE CHUNK//////////////////
        ////////////////////CHUNK MAY BE BIGGER THAN 32/////////////////
      
      for (int chunkIndex=lane_id; chunkIndex<iterathreads; chunkIndex+=WARP_SIZE) {
	
	float left_value = (chunkIndex > 0) ? partial_sums_sh[(warpPartialSumIndex+chunkIndex)-1]:0.0f;
	left_value -=p;
	float center_value = partial_sums_sh[warpPartialSumIndex+chunkIndex] - p;
            if ((left_value * center_value) <= 0){
	      current_chunk_sh[warp_id] = chunkIndex;        
	      //printf ("I am the warp %d lane_id %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warp_id, lane_id, chunkIndex, left_value,warpIndex+chunkIndex-1, partial_sums_sh[warpIndex+chunkIndex-1], center_value,warpIndex+chunkIndex, partial_sums_sh[warpIndex+chunkIndex]);
            }
        }
      current_chunk = current_chunk_sh[warp_id];
      
      //if (lane_id ==0)
      //  printf ("The current_chunk is %d\n", current_chunk);
      
        //Once We know the selected chunk we have to calculate again probabilities_sh for the selected chunk
      
      indexaux = lane_id + (current_chunk*WARP_SIZE);
      
      if (indexaux < number_of_cities){
	//unsigned int auxtabul = tabul>>current_chunk;
	
	unsigned int auxtabul = ((indexaux)<2048)?tabul1>>(current_chunk) : tabul2>>(current_chunk&0x3F);// We use 64 bits for the tabu list
	auxtabul = (unsigned int) MACROBITS & auxtabul;
	float choice = choice_info[indexChoiceInfo+indexaux];
	probabilities_sh[warpIndex+lane_id] = (float) choice*auxtabul;
	// printf ("For the city: Thread %d index %d prob %f and chunk %d and auxtabul %d\n", lane_id, warpIndex+lane_id, probabilities_sh[warpIndex+lane_id], current_chunk, auxtabul);
      }
      
      //Scan over the probabilities. The result is in probabilities_sh
      simplescan_shfl (probabilities_sh, (current_chunk*WARP_SIZE), number_of_cities, 1);
      //Calculate the total probability to proceed with the roulette selection
      
      p = random * probabilities_sh[warpIndex+(WARP_SIZE-1)];   
      
      //if (blockIdx.x==0 && warp_id==0)
      //  printf ("Random %f con probabilidad p %f y scan %f\n", random, p,partial_sums_sh[warpIndex+WARP_SIZE-1]);
      
      //Check which city within the select chunk is being selected
      //////////////////////STENCIL CHECK to select the city//////////////////
      //////////////////////////////////////////////////////////////////////
      
      float left_value = (lane_id > 0) ? probabilities_sh[warpIndex+lane_id-1]:0.0f;
      left_value -=p;
      float center_value = probabilities_sh[warpIndex+lane_id] - p;
      if ((left_value * center_value) <= 0){
	tour_sh[warpIndex+(tourIndex& 0x1f)] = indexaux;                
	if (indexaux < 2048){
	  setCityAsVisited (indexaux, &tabul1);         
	  //printf ("Setting city as visited in tabul1\n");
	}
	else { 
	  setCityAsVisited((indexaux-2048),&tabul2);
	  //printf ("Setting city %d as visited in tabul2\n",indexaux-2048 );
	}
	//  printf ("For the city: lane_id %d and the city is %d my left value is %f, indexLeft %d y valor scan %f y center %f, indexCenter %d y valor scan %f\n", warpIndex+(tourIndex& 0x1f), tour_sh[warpIndex+(tourIndex& 0x1f)], left_value, warpIndex+lane_id-1, probabilities_sh[warpIndex+lane_id-1], center_value,warpIndex+lane_id, partial_sums_sh[warpIndex+lane_id]);
      }
      
      
      next_city = tour_sh[warpIndex + (tourIndex & 0x1f)];
      
      // printf ("For thread %d is Next cituy is %d\n", lane_id, next_city);
      if (lane_id == 0 ) {
	tourLenght += d_distance[indexChoiceInfo+next_city];
      }
      
    }//END FOR: ALL CITIES ARE VISITED
    
    //We store the last chunk
    int indexaux = lane_id + ((iterathreads-1)*WARP_SIZE);
    
    if (indexaux < number_of_cities){
      int indexdtour = globalIndex * (number_of_cities + 1) + lane_id + (iterathreads-1) * WARP_SIZE;
      d_tour[indexdtour]= tour_sh[warpIndex+lane_id]; 
    }
    
    //Complete the tour getting the beginning city
    if (lane_id == 0) {
      tourLenght += d_distance[next_city*number_of_cities+city_ini];
      d_lenghtList[globalIndex] = tourLenght;
      int indexdtour = blockIdx.x*NUM_ANTS*(number_of_cities+1)+warp_id*(number_of_cities+1)+number_of_cities;
      d_tour[indexdtour] = city_ini;
    }
  }//END IF 
  
#else 
  //printf ("You shall compile with -arch>=35 make arch=1\n");
  //exit(-1);
#endif
}



/**
  * This kernel performs the pheromone stage
  */
__global__ void pheromone_update_with_atomic (float * d_pheromone, const unsigned int number_of_cities, unsigned int * d_tour, const unsigned int number_of_ants, unsigned int * d_lenghtList, int ite){

  const int xIndex = blockIdx.x* (number_of_cities+1) + threadIdx.x;
  int index,indexThread;

  for (int i=0; i<ite; i++) {
    indexThread = i*blockDim.x;
    index =xIndex+(indexThread);
    if ((threadIdx.x+indexThread)<number_of_cities){
        unsigned int coordx=d_tour[index];
        unsigned int coordy=d_tour[index+1];
        unsigned int pos = (coordy*number_of_cities)+coordx;
        unsigned int posim = (coordx*number_of_cities)+coordy;
        float pheromone = 0.0;
        pheromone=(float)1.0*(1.0/d_lenghtList[blockIdx.x]);
        atomicAdd(&d_pheromone[pos],pheromone);
        atomicAdd(&d_pheromone[posim],pheromone);
    }
  }
}


/***** External function to launch the kernels **********/
int antSystemTSP (problem instance, colony colony){

    dim3 grid_choice, block_choice;
    dim3 grid_tour, block_tour;
    dim3 grid_phero, block_phero;
    dim3 grid_random, block_random;
    unsigned int iterationsInTour = 0;
    unsigned int iterationsInPhero = 0;
    size_t mem_size_llist = (colony.n_ants)*sizeof(unsigned int);
    int i;
    unsigned int bestLength= UINT_MAX; // the length of the best tour so far
    unsigned int bestIt; // Length of the tour obtained in the current iteration
    unsigned int mem_size_seed = colony.n_ants*sizeof(curandState);
    unsigned int mem_size_h_tours = (instance.n+1) * colony.n_ants * sizeof (unsigned int);
    cudaEvent_t start, stop; // events
    unsigned int * h_tours = (unsigned int *) malloc (mem_size_h_tours);
    pdebug ("In the launching kernel function");
    curandState * devStates;



    // Event creation
    cudaEventCreate(&start);
    cudaEventCreate(&stop);


    //initialize_random_numbers(devStates, colony.n_ants);

    if (cudaMalloc ((void **)&devStates, mem_size_seed) != cudaSuccess) {
        fprintf (stderr, "Error allocating device memory \n");
        exit (1);
    }

    setting_kernel_parameters(&grid_random, &block_random, colony.n_ants, BLOCK_SIZE_CHOICE);
    setup_kernel<<<grid_random,block_random>>> (devStates, colony.n_ants);
    setting_kernel_parameters(&grid_choice, &block_choice, instance.n*instance.n, BLOCK_SIZE_CHOICE);

    pdebug ("Choice kernel parameter are number of blocks %d number of threads %d\n", grid_choice.x, block_choice.x);

    setting_kernel_parameters_tour(&grid_tour, &block_tour, &iterationsInTour, instance.n, colony.n_ants);
    setting_kernel_parameters_phero(&grid_phero, &block_phero, &iterationsInPhero, instance.n, colony.n_ants);
    size_t sharedmem = ceil((float)iterationsInTour/WARP_SIZE)*WARP_SIZE*NUM_ANTS*sizeof(float);
    float elapsedTime, total = 0.0f;

    for (i = 0; i < colony.n_tries; ++i) {

        computeTotalkernel<<<grid_choice, block_choice>>>(d_total, d_pheromone, d_distance, instance.n, colony.alpha, colony.beta,colony.rho);
        cudaDeviceSynchronize();
        pdebug("Number of blocks %d, number of threads per block %d, number of iterations %d and shared Memory allocated %d bytes\n", grid_tour.x,block_tour.x, iterationsInTour, sharedmem);
        
        cudaEventRecord(start,0);
//      nextTour_kernel_warp_iroulette_sf<<< grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour);
//        nextTour_kernel_warp_iroulette<<< grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour);
        nextTour_iroulette_pheromone<<< grid_tour, block_tour>>>(instance.n, colony.n_ants, d_distance, d_tour, d_lengthList, d_total, devStates, iterationsInTour, d_pheromone);
        cudaDeviceSynchronize();
        cudaEventRecord (stop);

        cudaEventSynchronize(stop);
        
        cudaEventElapsedTime(&elapsedTime, start, stop);
        //printf ("Elapsed Time is %f\n", elapsedTime);
        total+=elapsedTime;
        
        cudaMemcpy(colony.lengthList, d_lengthList, mem_size_llist, cudaMemcpyDeviceToHost);
        cudaMemcpy (h_tours, d_tour, mem_size_h_tours, cudaMemcpyDeviceToHost);

    //    for (int l = 0; l< colony.n_ants; ++l) 
      //      printf ("%d, ", colony.lengthList[l]);

       // printf ("\n");
       // #ifdef DEBUG
        int rep = -1;
        for (int j = 0; j < colony.n_ants; ++j){
            if (rep!=-1)
            printf ("*******Tour for the Ant %d*******\n", j);
            for (int k=0; k < instance.n+1; ++k) {
                for (int l=k+1; (l < instance.n); ++l){
                    if (h_tours[k+j*(instance.n+1)] == h_tours[l+j*(instance.n+1)]){
                        rep = h_tours[k+j*(instance.n+1)];
                    }
                }
                if (rep!=-1)
                    printf ("%d, ",h_tours[k+j*(instance.n+1)]);
            }
            if (rep!= -1){
                printf ("\nThe element %d is repeated", rep);
                rep =-1;
            }
            if (rep!= -1)
            printf ("\n");
        }
       // #endif

        bestIt = getLenghtIterationBestTour(colony.lengthList, colony.n_ants, d_tour, d_bestTour,&bestLength, instance.n);
        printf("Best Length found in iteration %d is %d\n",i, bestIt);
//        pheromone_update_with_atomic<<<grid_phero, block_phero>>> (d_pheromone, instance.n, d_tour, colony.n_ants, d_lengthList, iterationsInPhero);

        cudaDeviceSynchronize();
    }

    printf ("Execution on average %f\n",total/colony.n_tries);

    /*cudaEventRecord (stop);
    cudaEventSynchronize(stop);
    float elapsedTime;
    cudaEventElapsedTime(&elapsedTime, start, stop);
    printf ("Elapsed Time is %f\n", elapsedTime);
    */

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    cudaFree (devStates);
    return bestLength;
}
