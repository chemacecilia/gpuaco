#ifndef _ANT_COLONY_KERNEL_H_
#define _ANT_COLONY_KERNEL_H_

#define TILE_SIZE_X 8
#define TILE_SIZE_Y 8
#define TILE_DIM 32

///OJO CAMBIA EL LOGARITMO EN BASE 2
#define BLOCK_SIZE_NEXT_TOUR 32
#define LOG_BLOCK_SIZE_NEXT_TOUR 5

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836

#define MACROBITS 0x00000001
#define LENGHT 5.0f

#pragma OPENCL EXTENSION cl_amd_printf : enable
#pragma OPENCL EXTENSION cl_khr_global_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_base_atomics : enable
#pragma OPENCL EXTENSION cl_khr_global_int32_extended_atomics : enable
#pragma OPENCL EXTENSION cl_khr_local_int32_extended_atomics : enable



inline void AtomicAdd(volatile __global float *source, const float operand) {
    union {
        unsigned int intVal;
        float floatVal;
    } newVal;
    union {
        unsigned int intVal;
        float floatVal;
    } prevVal;
    do {
        prevVal.floatVal = *source;
        newVal.floatVal = prevVal.floatVal + operand;
    } while (atomic_cmpxchg((volatile __global unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}



__kernel void pheromone_update(__global float * d_pheromone2, const unsigned int number_of_cities, __global unsigned int * d_tour, const unsigned int number_of_ants, int ite){
  const int xIndex = get_group_id(0) * (number_of_cities+1) + get_local_id(0);
  int index,indexThread; 
  int i=0;
  for(i=0; i < ite ; i++) {
	  indexThread = i*get_local_size(0);         
	  index = xIndex+(indexThread);
	  if ((get_local_id(0)+indexThread)<number_of_cities){
		unsigned int coordx=d_tour[index];
		unsigned int coordy=d_tour[index+1];

		unsigned int pos = (coordx*number_of_cities)+coordy;
		unsigned int posim = (coordy*number_of_cities)+coordx;              
		float pheromone = 0.0f;
		pheromone = 1;
    
		//AtomicAdd(&d_pheromone2[pos], pheromone);
		//AtomicAdd(&d_pheromone2[posim], pheromone);
		d_pheromone2[pos] += pheromone;
		d_pheromone2[posim] += pheromone;
	  }	
  }
}

#endif // #ifndef _ANT_COLONY_KERNEL_H_


