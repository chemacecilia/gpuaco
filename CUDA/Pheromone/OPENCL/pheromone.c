#include <stdio.h>
#include <math.h>
#include <cuda_runtime.h>
#include <stdlib.h>
#include <string.h>
#include <CL/cl.h>
#include <time.h>

#define LENGHT 5.0f

#define MAX_SOURCE_SIZE (0x100000)

#define BLOCK_SIZE_ATOMIC_DEVICE 256

void sacaCPU(float * pheromone, int num_cities, int num_ants){
	int i=0;
	double aux=0;
        for (i = 0; i<(num_cities*num_cities);i++){
                printf("%f\n",pheromone[i]);
		aux+=pheromone[i];
        }
}



void checkErrors (float * h_pheromoneCPU,  float * h_pheromoneGPU, int num_cities) {
  int i=0;
  printf("....Checking Errors........\n");
  for (i=0;i<(num_cities* num_cities);i++){
    if (h_pheromoneCPU[i] != h_pheromoneGPU[i])
      printf("i = %d, CPU = %e and GPU %e\n",i, h_pheromoneCPU[i], h_pheromoneGPU[i]);
  }
  printf (".......OK\n\n");

}


void pheromoneOnCPU (float * pheromone, int * tours, int num_cities, int num_ants) {
    int pos = 0, i=0, j=0;

      for (i=0; i<num_ants; ++i) {
        for (j=0; j<num_cities; ++j) {
            int pos1 = tours[pos+j];
            int pos2 = tours[pos+j+1];

            pheromone[(pos1*num_cities)+pos2] += 1;
            pheromone[(pos2*num_cities)+pos1] += 1;
        }
        pos+=(num_cities+1);
    }
}


void loadfile (int * num_ants, int * num_cities, int** h_tours, char * name_f){

    FILE * fichero;
    fichero = fopen(name_f,"r");
    if (fichero == NULL){
	printf("cagada al abrir el fichero para leerlo");
	exit (-1);
    } 

    if (fscanf(fichero,"%d",num_ants)){};
    *num_cities=*num_ants;

    int aux, i=0,j=0;
    *h_tours = (int *) malloc ((*num_cities+1) * (*num_ants)*sizeof(int));
    
    if (*h_tours == NULL) {
	fprintf (stderr, "Malloc error\n");
	exit (-1);
    }

    for(i=0; i < *num_ants;i++){
       for(j=0; j <= *num_cities;j++){
	   if(fscanf(fichero,"%d",&aux)){}; 
	   *(*h_tours+(i*(*num_cities+1)+j)) = aux; 
       }
       if (feof(fichero)){
	  break;
       }
    }
    fclose(fichero);
}



void allocateHostMemory (float ** h_pheromone, int num_cities, int num_ants) {
    if ((*h_pheromone= (float *) malloc (num_cities*num_cities*sizeof(float))) == NULL) {
        fprintf (stderr,"Malloc h_pheromone error\n");
        exit (-1); 
    }
}


/**
 * This funtion selects the OpenCL platform among those found in the system
 */
cl_platform_id selectPlatform () {

  cl_uint numPlatforms;
  cl_platform_id platform = NULL;
  unsigned int  i;
  char pbuff[100];
  cl_int status = clGetPlatformIDs(0, NULL, &numPlatforms);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error getting the platform id\n");
    exit(-1);
  }

  printf("=== %d OpenCL platform(s) found: ===\n", numPlatforms);

  if(numPlatforms > 0) {
    cl_platform_id* platforms = (cl_platform_id *) malloc (numPlatforms * sizeof(cl_platform_id));
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    if(status != CL_SUCCESS){
        fprintf (stderr, "Error getting the platform id\n");
        exit (-1);
    }
    for(i=0; i < numPlatforms; ++i) {
        status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, sizeof(pbuff), pbuff, NULL);
        if(status != CL_SUCCESS){
            fprintf (stderr, "Error getting the platform id\n");
            exit (-1);
        }
        printf("  VENDOR = %s\n", pbuff);
        status = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, sizeof(pbuff), pbuff, NULL);
        if(status != CL_SUCCESS){
            fprintf (stderr, "Error getting the platform id\n");
            exit (-1);
        }
        printf("  NAME = %s\n", pbuff);
        platform = platforms[i];
        if(!strcmp(pbuff, "Advanced Micro Devices, Inc.")){
           break;
        }
    }
    free (platforms);
  }

  if(NULL == platform){
      fprintf (stderr, "NULL platform found so Exiting Application.\n");
      exit (-1);
  }
  printf ("The selected platform is %s\n", pbuff);
  return platform;
}


///////////// MAIN //////////////////
int main (int argc, char** argv) {

    
    int num_cities = 0, num_ants=0, i=0,j=0;
    int *h_tours, *d_tours; 
    float *h_pheromone, *d_pheromone, *h_pheromone_opencl; 
    int *h_lenghtlist, *d_lenghtlist; 
    char *name_file;
    unsigned int iterationsInPhero=0;


    name_file = argv[1];
    printf ("---------Reading File\n");
    loadfile (&num_ants, &num_cities, &h_tours, name_file);
    printf("-----------OK!\n");

    //********** Reserva de memoria en el host
    allocateHostMemory (&h_pheromone,num_cities,num_ants);
    allocateHostMemory (&h_pheromone_opencl,num_cities,num_ants);
    allocateHostMemory (&d_pheromone,num_cities,num_ants);


    clock_t start = clock(), stop;
    double msecs;

    pheromoneOnCPU (h_pheromone, h_tours, num_cities, num_ants);

    stop = clock();
    msecs = (double)(stop-start)/CLOCKS_PER_SEC;
    printf("\nTiempo CPU: %f\n\n", (msecs)*1000);  


//*********************** HASTA AQUÍ LA PARTE DEL HOST ************************************/
 
//*********************** A PARTIR DE AHORA OPENCL ****************************************/

  cl_int status = 0;

  ////////////////////////////////////////////////////////////////////
  // STEP 1 Getting Platform.
  ////////////////////////////////////////////////////////////////////

  cl_platform_id platform = selectPlatform ();

  ////////////////////////////////////////////////////////////////////
  //// STEP 2 Select the Device to use in the execution
  //////////////////////////////////////////////////////////////////////

  cl_uint num_devices;
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Failed to create a device group\n");
    return -1;
  }

  cl_device_id * devices = (cl_device_id *)malloc(num_devices*sizeof(cl_device_id));
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, num_devices, devices, NULL);

  ////////////////////////////////////////////////////////////////////
  //// STEP 3 Creating context using the platform selected
  ////  Context created from type includes all available
  ////  devices of the specified type from the selected platform
  //////////////////////////////////////////////////////////////////////

  cl_context context = clCreateContext(NULL, 1, &devices[0], NULL, NULL, &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Failed to create a CL context\n");
    return -1;
  }

  ////////////////////////////////////////////////////////////////////
  //// STEP 4 Creating command queue for a single device
  ////    Each device in the context can have a
  ////    dedicated commandqueue object for itself
  //////////////////////////////////////////////////////////////////////

  cl_command_queue commandQueue;
  commandQueue = clCreateCommandQueue(context, devices[0], 0, &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Failed to create a queuet\n");
    return -1;
  }


  ////////////////////////////////////////////////////////////////////
  //// STEP 5 Creating cl_buffer objects from host buffer
  ////    These buffer objects can be passed to the kernel
  ////    as kernel arguments
  //////////////////////////////////////////////////////////////////////

  const unsigned int mem_size_pheromone = sizeof(cl_float) * num_cities * num_cities;
  const unsigned int mem_size_tour = sizeof(cl_uint) * num_ants * (num_cities+1);

  // The memory buffer that is used as input/output for OpenCL kernel
  cl_mem   d_pheromone2;
  cl_mem   d_tour;
  cl_mem   d_bestTour;

 //////////////////////////////////////////////////////////////////
 ///////////////////Initialize Pheromone///////////////////////////
 /////////////////////////////////////////////////////////////////n

  int v=0;
  for (v=0;v<(num_cities*num_cities);v++){
	d_pheromone[v]=0.0f;
      }
  
  d_pheromone2 = clCreateBuffer (context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, mem_size_pheromone, d_pheromone, &status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating pheromone Buffer\n");
    return -1;
  }
 
  d_tour = clCreateBuffer (context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, mem_size_tour, h_tours, &status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating d_tour Buffer\n");
    return -1;
  }
 /////////////////////////////////////////////////////////////////
 //// STEP 6. Building Program
 ////    6.1 Load CL file, using basic file i/o
 ////    6.2 Build CL program object
 ///////////////////////////////////////////////////////////////////

  //Load the kernel source code into the array source_str
  FILE * fp;
  char * source_str;
  size_t source_size;
	       
  fp = fopen ("simpleAntGPU_kernel.cl", "r");
  if (!fp) {
    fprintf (stderr, "Failed to load kernel. \n");
  }
  source_str = (char *) malloc (MAX_SOURCE_SIZE);
  source_size =fread(source_str,1, MAX_SOURCE_SIZE,fp);
  fclose(fp);

  //create a program from the kernel source
  cl_program program = clCreateProgramWithSource(context,1, (const char **)&source_str, (const size_t *) &source_size, &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating de program Buffer\n");
    return -1;
  }

  //Build the program

  char const buildOptions[] = {"-cl-fast-relaxed-math -cl-mad-enable -cl-no-signed-zeros"};
  status = clBuildProgram(program, 1, &devices[0], buildOptions, NULL, NULL);

  if(status != CL_SUCCESS){
    char buffer[1024];
    clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, NULL);
    fprintf (stderr, "Error creating de program Buffer\n%s\n",buffer);
    return -1;
  }

   //////////////////////////////////////////////////////////////////////////
   /////////////        Kernel Pheromone Update kernel            ////////////
   ///////////////////////////////////////////////////////////////////////////

  cl_kernel pheromone_update = clCreateKernel(program, "pheromone_update", &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating the pheromone_update kernel status\n");
    return -1;
  }

  cl_uint iterationsPheromones6;
  status = clSetKernelArg(pheromone_update, 0, sizeof(cl_mem), (void *)&d_pheromone2);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel d_pheromone2\n");
    return -1;
  }

  status = clSetKernelArg(pheromone_update, 1, sizeof(cl_uint), (void *)&num_cities);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel num_cities\n");
    return -1;
  }
  status = clSetKernelArg(pheromone_update, 2, sizeof(cl_mem), (void *)&d_tour);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel d_tour\n");
    return -1;
  }
  status = clSetKernelArg(pheromone_update, 3, sizeof(cl_uint), (void *)&num_ants);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel num_ants\n");
    return -1;
  }

  size_t globalThreads2[1], localThreads2[1];
  localThreads2[0] = (num_cities<BLOCK_SIZE_ATOMIC_DEVICE)?num_cities:BLOCK_SIZE_ATOMIC_DEVICE;
  globalThreads2[0]= num_ants*localThreads2[0];
  iterationsPheromones6=ceil((float)num_cities/BLOCK_SIZE_ATOMIC_DEVICE);
/////////////////////////
  printf("\nITERATIONS:%d\n",iterationsPheromones6);

  status = clSetKernelArg(pheromone_update, 4, sizeof(cl_uint), (void *)&iterationsPheromones6);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel\n");
    return -1;
  }

//****************************************************************************************************
  cl_event event;
    clock_t start2 = clock(), stop2;
    double msecs2;
    status = clEnqueueNDRangeKernel(commandQueue, pheromone_update, 1, NULL, globalThreads2, localThreads2, 0, NULL, &event);
    //clEnqueueBarrier(commandQueue);
    stop2=clock();
    msecs2 = (double)(stop2-start2)/CLOCKS_PER_SEC;
    printf("\nTiempo KERNEL: %f\n", msecs2*1000);
    if(status != CL_SUCCESS){
      fprintf (stderr, "Error: Enqueueing pheromone update kernel\n");
      return -1;
    }

//****************************************************************************************************
  status = clReleaseCommandQueue(commandQueue);
  if(status != CL_SUCCESS) {
     fprintf(stderr, "Error: In clReleasecommandQueue\n");
     return -1;
  }

  int memsizehpheromone=num_cities*num_cities*sizeof(float);
  status = clEnqueueReadBuffer(commandQueue, d_pheromone2, CL_TRUE, 0, memsizehpheromone, h_pheromone_opencl, 0, NULL, NULL);

  status = clReleaseKernel(pheromone_update);

  checkErrors (h_pheromone, h_pheromone_opencl, num_cities);
//  sacaCPU(h_pheromone,num_cities,num_ants);


  return 0;


    

    /////////////////***************
  
}

