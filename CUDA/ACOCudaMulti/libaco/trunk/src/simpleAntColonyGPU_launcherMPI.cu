// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
// includes, project
#include "include/libaco/util.h"

#include <omp.h>
#include <mpi.h>

//#define TIMING

// includes, kernels
#include "simpleAntGPU_kernel.cu"
//
////////////////////////////////////////////////////////////////////////////////
// declaration, forward 

#define BLOCK_SIZE_CHOICE 256
#define MAX_BLOCKS 65535
#define TAM_BLOCK_PHERO3 256
#define BLOCK_SIZE_ATOMIC_DEVICE 256

void setting_kernel_parameters(int * threads, int * blocks, unsigned int reference, unsigned int block_size);
void printTour(unsigned int * best, unsigned int number_of_cities);
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, unsigned int * d_tabu, unsigned int * d_best, unsigned int * bestLenght, unsigned int number_of_cities);
 

////////////////////////////////////////////////////////////////////////////////
//! Entry point for Cuda functionality on host side
//! @param  
////////////////////////////////////////////////////////////////////////////////
extern "C" void
antColonyGPULauncher(const unsigned int number_of_ants, const unsigned int number_of_cities, unsigned int * distance, float * pheromone, float * choice_info, unsigned int * ants, float alpha, float beta, float evaporation_rate, unsigned int iterations, float elitist_weight, unsigned int elitist_ants, unsigned int best_so_far_frequency, float a, AcoType acotype)
{ 
	MPI_Init(NULL,NULL);
        cudaError_t cudaErr; 	
	int num_devices;
	cudaGetDeviceCount(&num_devices); 
	///////////////////////////////////num_devices=1;
	float nodeBestTime = 0.0f;
        float globalBestTime = 0.0f;
	
	#pragma omp parallel num_threads(num_devices) private(iterations)
    {
    
        int tid = omp_get_thread_num();      
	  
        cudaSetDevice(3);	
 	////////cudaSetdevice(tid);
        int device;
        cudaGetDevice(&device);

        //Bytes to allocate in each array 
        const unsigned int mem_size_distance = sizeof(unsigned int) * number_of_cities * number_of_cities;
        const unsigned int mem_size_pheromone = sizeof(float) * number_of_cities * number_of_cities;
        const unsigned int mem_size_tour = sizeof(unsigned int) * number_of_ants * (number_of_cities+1);
        const unsigned int mem_size_lenghtList = sizeof(unsigned int) * number_of_ants;
        const unsigned int mem_size_choiceinfo = sizeof(float)*number_of_cities*number_of_cities;
        const unsigned int mem_size_seed = number_of_ants * number_of_cities* sizeof(curandState);     
        unsigned int totalBytesAllocated =0; //Bytes allocated on the GPU
    
    
        //////////////////////////////////////////////////////////////////
        ///////////////////Initialize Distances///////////////////////////
        /////////////////////////////////////////////////////////////////
    
        unsigned int * d_distance; 
        cudaErr = cudaMalloc((void**) &d_distance, mem_size_distance);
        cudaErr = cudaMemcpy(d_distance, distance, mem_size_distance,cudaMemcpyHostToDevice);
        if (cudaErr != cudaSuccess) {
            fprintf (stderr,"Error managing distances\n");
        }
        totalBytesAllocated+=mem_size_distance;

        ////////////////////////////////////////////////////////////////////////
        ///////////////Initialize Choice Information////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        float * d_choiceInfo;
        cudaErr = cudaMalloc((void**) &d_choiceInfo, mem_size_choiceinfo); 
        if (cudaErr != cudaSuccess) {
            fprintf (stderr,"Error managing choice info\n");
        }
        totalBytesAllocated+=mem_size_choiceinfo;
		
        ///////////////////////////////////////////////////////////////////////////
        ///////////////Initialize Pheromone Information////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        float* d_pheromone; 					 		
        cudaErr = cudaMalloc((void**) &d_pheromone, mem_size_pheromone);
        cudaErr = cudaMemcpy(d_pheromone, pheromone, mem_size_pheromone, cudaMemcpyHostToDevice) ;
        if (cudaErr != cudaSuccess) {
            fprintf (stderr,"Error managing pheromone\n");
        }
        ///////////////////////////////////////////////////////////////////////////
        /////////////////Initialize Tour information//////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        unsigned int* d_tour;
        cudaErr = cudaMalloc((void**) &d_tour, mem_size_tour); 
        if (cudaErr != cudaSuccess) {
            fprintf (stderr,"Error managing tour information\n");
        }
        totalBytesAllocated+=mem_size_tour;

        ///////////////////////////////////////////////////////////////////////////
        /////////////////Initialize Tour Lenght information////////////////////////
        ///////////////////////////////////////////////////////////////////////////
        //the lenght of the best tour so far
    
        unsigned int bestLenght=UINT_MAX; //Best lenght so far 
        unsigned int bestLenghtOld;     
        unsigned int * d_lenghtList; 
        cudaErr = cudaMalloc((void**) &d_lenghtList,mem_size_lenghtList);
        unsigned int * h_lenghtList = (unsigned int *) malloc (mem_size_lenghtList);
        if ((cudaErr != cudaSuccess) || h_lenghtList == NULL) {
            fprintf (stderr,"Error managing lenght List\n");
        }
        totalBytesAllocated+=mem_size_lenghtList;

        ///////////////////////////////////////////////////////////////////////////
        /////////////////////////Best tour information ////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
    
        //the best tour so far
        unsigned int * d_bestTour;
        cudaErr = cudaMalloc((void **) &d_bestTour, mem_size_tour);
        unsigned int * h_bestTour= (unsigned int *)malloc (mem_size_tour);

        if (cudaErr != cudaSuccess) {
            fprintf (stderr,"Error managing Best tour information\n");
        }
        totalBytesAllocated+=mem_size_tour;
    
        ///////////////////////////////////////////////////////////////////////////
        ///////////////Initialize for random numbers on the GPU////////////////////
        ///////////////////////////////////////////////////////////////////////////
  
        curandState *devStates;
        cudaMalloc((void **)&devStates,mem_size_seed); // Allocate space for prng states on device, one for each ant
        totalBytesAllocated+=mem_size_seed;
    
        int num_blocks, num_threads;		
        setting_kernel_parameters(&num_threads, &num_blocks, number_of_cities*number_of_ants, BLOCK_SIZE_CHOICE); 
  
        dim3 gridRandom (1,1);
        dim3 threadsRandom(1,1);
    
        if (num_blocks>=MAX_BLOCKS) {
            float rounding = ceil(sqrt(num_blocks));
            gridRandom.x=rounding;
            gridRandom.y=rounding;
            threadsRandom.x = num_threads;						
        }
        else {
            gridRandom.x=num_blocks;
            threadsRandom.x = num_threads;						
        }  
    
        setup_kernel<<<gridRandom, threadsRandom>>>(devStates, number_of_ants*number_of_cities);
        cudaThreadSynchronize(); 
    
        totalBytesAllocated+=mem_size_seed;
    
    
        ///////////////////////////////////////////////////////////////////////////
        ///////////////Setting GPU parameter //////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////
    
        //////////////////////////////////////////////////////////////////////////
        ////////////Setting parameters for the Choiceinfokernel//////////////////
        /////////////////////////////////////////////////////////////////////////
    		
        setting_kernel_parameters(&num_threads, &num_blocks, number_of_cities*number_of_cities, BLOCK_SIZE_CHOICE); 
    
        dim3 grid0 (1,1);
        dim3 threads0(1,1);
    
        if (num_blocks>=MAX_BLOCKS) {
            float rounding= ceil(sqrt(num_blocks));
            grid0.x=rounding;
            grid0.y=rounding;
            threads0.x = num_threads;						
        }
        else {
            grid0.x=num_blocks;
            threads0.x = num_threads;						
        }
    
        //////////////////////////////////////////////////////////////////////////
        ////////////Setting parameters for the Next Tour kernel//////////////////
        /////////////////////////////////////////////////////////////////////////
    
        ///1 block per ant. And as many threads as cities or fixed number of them 
        dim3 grid1(number_of_ants);
        if (BLOCK_SIZE_NEXT_TOUR > number_of_cities) {
            //std::cout << "The block size macro should be smaills than the number of cities\n";
            //return;		
        }	
    
        int threads = BLOCK_SIZE_NEXT_TOUR;	
    
        dim3 threads1(threads);
        unsigned int iterNextTour=ceil((float)number_of_cities/threads1.x);
        //std::cout << "Next tour kernel. Number of threads is "<< threads1.x <<", blocks " << grid1.x << ", and iterations " << iterNextTour << std::endl;			
    
        //////////////////////////////////////////////////////////////////////////
        ////////////Setting parameters for the Pheromone Update kernel////////////
        /////////////////////////////////////////////////////////////////////////
    
        dim3 gridPheromone (1,1);
        dim3 threadsPheromone (1,1);
        int iterationsPheromones6;	
   
        gridPheromone.x= number_of_ants;
        threadsPheromone.x = (number_of_cities<BLOCK_SIZE_ATOMIC_DEVICE)?number_of_cities:BLOCK_SIZE_ATOMIC_DEVICE; 	
        iterationsPheromones6 = ceil((float)number_of_cities/BLOCK_SIZE_ATOMIC_DEVICE);
        //printf ("Threads in the Pheromone Update with atomic instructions kernel 6 (x)= (%d) and blocks (%d). Number of iterations per block is %d\n", threadsPheromone.x, gridPheromone.x, iterationsPheromones6);		
    
        /////////////////////////////////////////////////////////////
        //////////////////////////Load Balancing////////////////////
        ///////////////////////////////////////////////////////////

        iterations=0;// Private variable to each CPU thread to define number of ACO iterations  //era 1  
	

        cudaEvent_t start, stop;
        cudaEventCreate(&start);//tomar tiempos
        cudaEventCreate(&stop);
        float elapsedTime;
    
	int iteraciondemejora=1;
	//int mejorlongitud=0;
	int veces;
	int intervalo=100;
	int tope=10000;
	//////////veces=(int)tope/intervalo;
	veces=2;
	

        float percent=1.0f;      
        for (uint phase=0; phase<veces; ++phase) {
	    if (phase==0)
                iterations=0;//250;
            else {
		//////////iterations= iterations+intervalo;
                iterations= (int)500;
		////iterations=ceil(100*percent)*10;
                //std::cout << "percent=" << percent << "\t iterations=" << iterations << std::endl;
            }

	bestLenght=UINT_MAX; //Best lenght so far 
        bestLenghtOld=0;

            cudaEventRecord(start, 0); ///// empieza a tomar tiempos
            for (int i=0; i<iterations; ++i) {	
                init_choiceinfo_kernel <<<grid0,threads0>>> (d_choiceInfo,d_pheromone,d_distance, number_of_cities, alpha, beta,evaporation_rate);   
                cudaFuncSetCacheConfig(nextTour_kernel, cudaFuncCachePreferShared);
                nextTour_kernel<<< grid1, threads1>>>(number_of_cities, number_of_ants, d_distance,d_tour,d_lenghtList,d_choiceInfo, devStates, iterNextTour); 	  		          
                cudaThreadSynchronize(); 						
                cudaMemcpy(h_lenghtList, d_lenghtList, mem_size_lenghtList, cudaMemcpyDeviceToHost) ; 
                unsigned int bestIt = getLenghtIterationBestTour(h_lenghtList, number_of_ants, d_tour, d_bestTour,&bestLenght, number_of_cities); 
			     
                //std::cout << 	bestLenght << std::endl;
      
                if (bestLenght<bestLenghtOld) {
		            int mejora;
 		    	    if (i == 0) 
		                mejora = bestLenght;
		            else 
		                mejora = bestLenghtOld - bestLenght;
			    iteraciondemejora=i;
			    //////////////std::cout << "iteration = " << i << " ->\t\t mejora: " << mejora << std::endl;
	        }
      
		/////mejorlongitud=bestIt;
                bestLenghtOld = bestLenght;			  
                pheromone_update_with_atomic<<<gridPheromone, threadsPheromone>>> (d_pheromone, number_of_cities, d_tour, number_of_ants, d_lenghtList,iterationsPheromones6);		
                cudaThreadSynchronize(); 			
            }//END_FOR ACO Iterations
        
        cudaEventRecord(stop, 0);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&elapsedTime, start, stop);
            //Fase de Ajuste 
            if(phase==0) {
                elapsedTime=1/elapsedTime;  
	            #pragma omp critical
	            {
	        	    if (elapsedTime > nodeBestTime)
		                nodeBestTime=elapsedTime;
	            }
	  
	            #pragma omp barrier
	  
	            #pragma omp single
                {
	                MPI_Allreduce(&nodeBestTime, &globalBestTime, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD);
                }
      
	            percent=elapsedTime/globalBestTime;
	  
	        }//END_IF 

    
        cudaDeviceProp devProp;
        cudaGetDeviceProperties(&devProp, tid);
	    printf("%s\t%i\t%i\t%f\t%f\t%d %f\t\n", devProp.name, bestLenght, iteraciondemejora, percent, elapsedTime, iterations, nodeBestTime);////todo esto iba fuera del for_phases

        } //END_FOR_PHASES

        //////////////printf("GPU %s ->\t\tPercent: %f\tResult: %i\tIteracion nº: %d\tTotal execution time: %f ms\tIterations: %d\n", devProp.name, bestLenght, iteraciondemejora, percent, elapsedTime, iterations);
	
  
        //std::cout << "Total execution time: " <<accu/iterations <<", msec \n"; 		 	
    
        //std::cout << "Total execution time: " <<timer() << "sec \t";
        //std::cout << std::endl;
        //std::cout << "Best Tour Lenght" << std::endl;
        //std::cout << bestLenght << "\t";
        //std::cout << std::endl;
    
        //std::cout << "Best Ordering" << std::endl;
        cudaMemcpy(h_bestTour, d_bestTour, (number_of_cities+1)*sizeof(unsigned int), cudaMemcpyDeviceToHost);				
        //printTour(h_bestTour, number_of_cities);
        //std::cout << std::endl;
    
        #ifdef TIMING
        FILE *f1,*f2,*f3,*f4,*f5;
        f1 = fopen("out/choice","a");
        f2 = fopen("out/tour","a");
        f3 = fopen("out/pher","a");
        f4 = fopen("out/total","a");
        f5 = fopen("out/result","a");
        fprintf(f1,"%f ",choiceTime/iterations);
        fprintf(f2,"%f ",nextTime/iterations);
        fprintf(f3,"%f ",pherTime/iterations);
        fprintf(f4,"%f ",accu/iterations);
        fprintf(f5,"%d ",bestLenght);
        if (number_of_ants==2392) {
            fprintf(f1,"\n");
            fprintf(f2,"\n");
            fprintf(f3,"\n");
            fprintf(f4,"\n");
            fprintf(f5,"\n");
        }
        #endif

        cudaEventDestroy(start);
        cudaEventDestroy(stop);
        cudaFree(d_distance);
        cudaFree(d_pheromone);
        cudaFree(d_tour);
        cudaFree(d_lenghtList);
        cudaFree(d_bestTour);
        cudaFree(devStates);
        free (h_bestTour);	
        free (h_lenghtList);	
    
        cudaThreadExit();
    } //END_OMP_PARALLEL
    
    MPI_Finalize(); 
}


void setting_kernel_parameters(int * threads, int * blocks, unsigned int reference, unsigned int block_size) {
  
  *threads= (reference <= block_size)?reference:block_size;
  *blocks = (reference%*threads==0)?(reference / *threads):(reference / *threads)+1; 
  
}



void printTour(unsigned int * best, unsigned int number_of_cities) {

  for (int i=0; i<=number_of_cities;i++)
    std::cout << best[i] << ", ";
		
  std::cout << std::endl;		
}

///
///It returns the best in the current iteration, and controls the best so far, doing a copy in device memory for the best tour 
///
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, unsigned int * d_tour, unsigned int * d_best, unsigned int * bestLenght, unsigned int number_of_cities) 
{
  
  unsigned int bestIt=lenght[0];
  unsigned int antBest = 0;
  
  for (int i=1;i<number_of_ants;i++) {
    if (lenght[i]<bestIt) {
      bestIt = lenght[i];
      antBest =i;
    }
  }
  
  
  /* int size = (number_of_cities+1)*number_of_ants*sizeof(uint);

  unsigned int * partialbest = (unsigned int * ) malloc (size);
  cutilSafeCall(cudaMemcpy(partialbest, d_tour, size, cudaMemcpyDeviceToHost) );
  
  std::cout << "Todas los tours para todas las ants \n";
  for (int j = 0; j< number_of_ants; j++) {
    std::cout << "Para la ant " << j << std::endl;
    for (int i=0; i<=number_of_cities; i++) {
      std::cout << partialbest[(j*(number_of_cities+1))+i] <<", ";
    }
    std::cout << std::endl;
  }
  */

  unsigned int * aux = d_tour;
  aux=aux+antBest*(number_of_cities+1);
  if (bestIt < *bestLenght) {
    // std::cout << "La mejor ant es " << antBest << std::endl;
    *bestLenght = bestIt;			
    cudaMemcpy(d_best, aux, (number_of_cities+1)*sizeof(unsigned int), cudaMemcpyDeviceToDevice);				
  }     	
  
  return bestIt;		  
}





