#include <cstdlib>
#include <ctime>

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define MASK 123459876



enum AcoType { ACO_SIMPLE, ACO_ELITIST, ACO_RANK, ACO_MAX_MIN, ACO_ACS };
namespace Util {
  unsigned int random_number(unsigned int range=RAND_MAX);
	void swap2(unsigned int v[], unsigned int v2[], unsigned int i, unsigned int j);
	void sort2(unsigned int v[], unsigned int v2[], unsigned int left, unsigned int right);
	float ran01( int *idum );
}
