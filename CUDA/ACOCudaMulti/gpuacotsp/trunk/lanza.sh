#!/bin/bash
 
#for i in 16 32 64 128 256 
#do
#  cd ../../libaco/trunk/
#  cp src/simpleAntColonyGPU_launcher_$i.cu src/simpleAntColonyGPU_launcher.cu
#  cp src/simpleAntGPU_kernel_$i.cu src/simpleAntGPU_kernel.cu
#  make clean; make && cd ../../gpuacotsp/trunk/ && make clean && make
  echo Ejecutando d198.tsp con 198 hormigas 
  ./bin/acotsp --simple -f ./benchmarks/tsplib/d198.tsp -m 198 > ./results/d198_1_10000.txt
  echo Ejecutando a280.tsp con 280 hormigas 
  ./bin/acotsp --simple -f ./benchmarks/tsplib/a280.tsp -m 280 > ./results/a280_1_10000.txt
  echo Ejecutando lin318.tsp con 318 hormigas 
#  ./bin/acotsp --simple -f ./benchmarks/tsplib/lin318.tsp -m 318
  echo Ejecutando pcb442.tsp con 442 hormigas 
#  ./bin/acotsp --simple -f ./benchmarks/tsplib/pcb442.tsp -m 442
  echo Ejecutando rat783.tsp con 783 hormigas 
#  ./bin/acotsp --simple -f ./benchmarks/tsplib/rat783.tsp -m 783
  echo Ejecutando pr1002.tsp con 1002 hormigas 
#  ./bin/acotsp --simple -f ./benchmarks/tsplib/pr1002.tsp -m 1002
  echo Ejecutando pcb1173.tsp con 1173 hormigas 
#  ./bin/acotsp --simple -f ./benchmarks/tsplib/pcb1173.tsp -m 1173
  echo Ejecutando d1291.tsp con 1291 hormigas 
#  ./bin/acotsp --simple -f ./benchmarks/tsplib/d1291.tsp -m 1291
  echo Ejecutando pr2392.tsp con 2392 hormigas 
#  ./bin/acotsp --simple -f ./benchmarks/tsplib/pr2392.tsp -m 2392
#done
 
