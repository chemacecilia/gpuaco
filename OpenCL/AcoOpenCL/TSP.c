
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include <assert.h>
#include <cuda_runtime.h>
#include "InOut.h"
#include "utilities.h"


unsigned int * d_distance;


static double dtrunc (double x)
{
    int k;

    k = (int) x;
    x = (double) k;
    return x;
}

unsigned int  (*distance)(unsigned int, unsigned int, struct point *);  /* function pointer */

/********** The following four functions implement different *********
 ********** ways of computing distance for TSPLIB instances **********/

/*
 * FUNCTION: compute Euclidean distances between two nodes rounded to next
 *           integer for TSPLIB instances
 * INPUT:    two node indices
 * OUTPUT:   distance between the two nodes
 * COMMENTS: for the definition of how to compute this distance see TSPLIB
 */
unsigned int round_distance (unsigned int i, unsigned int j, struct point * nodeptr) {
    double xd = nodeptr[i].x - nodeptr[j].x;
    double yd = nodeptr[i].y - nodeptr[j].y;
    double r  = sqrt(xd*xd + yd*yd) + 0.5;
    return (unsigned int) r;
}

/*
 * FUNCTION: compute ceiling distance between two nodes rounded to next
 *           integer for TSPLIB instances
 * INPUT:    two node indices
 * OUTPUT:   distance between the two nodes
 * COMMENTS: for the definition of how to compute this distance see TSPLIB
 *                                            */
unsigned int ceil_distance (unsigned int i, unsigned int j, struct point * nodeptr)
{
    double xd = nodeptr[i].x - nodeptr[j].x;
    double yd = nodeptr[i].y - nodeptr[j].y;
    double r  = sqrt(xd*xd + yd*yd) + 0.000000001;

    return (unsigned int)r;
}

/*
 * FUNCTION: compute geometric distance between two nodes rounded to next
 *           integer for TSPLIB instances
 * INPUT:    two node indices
 * OUTPUT:   distance between the two nodes
 * COMMENTS: adapted from concorde code
 * for the definition of how to compute this distance see TSPLIB
 */
unsigned int geo_distance (unsigned int i, unsigned int j, struct point * nodeptr){
    double deg, min;
    double lati, latj, longi, longj;
    double q1, q2, q3;
    unsigned int dd;

    double x1 = nodeptr[i].x, x2 = nodeptr[j].x,
    y1 = nodeptr[i].y, y2 = nodeptr[j].y;
    deg = dtrunc (x1);
    min = x1 - deg;
    lati = M_PI * (deg + 5.0 * min / 3.0) / 180.0;
    deg = dtrunc (x2);
    min = x2 - deg;
    latj = M_PI * (deg + 5.0 * min / 3.0) / 180.0;
    deg = dtrunc (y1);
    min = y1 - deg;
    longi = M_PI * (deg + 5.0 * min / 3.0) / 180.0;
    deg = dtrunc (y2);
    min = y2 - deg;
    longj = M_PI * (deg + 5.0 * min / 3.0) / 180.0;
    q1 = cos (longi - longj);
    q2 = cos (lati - latj);
    q3 = cos (lati + latj);
    dd = (int) (6378.388 * acos (0.5 * ((1.0 + q1) * q2 - (1.0 - q1) * q3)) + 1.0);
    return dd;
}


/*
 *  FUNCTION: compute ATT distance between two nodes rounded to next
 *            integer for TSPLIB instances
 *  INPUT:    two node indices
 *  OUTPUT:   distance between the two nodes
 *  COMMENTS: for the definition of how to compute this distance see TSPLIB
 */
unsigned int att_distance (unsigned int i, unsigned int j, struct point * nodeptr) {
    double xd = nodeptr[i].x - nodeptr[j].x;
    double yd = nodeptr[i].y - nodeptr[j].y;
    double rij = sqrt ((xd * xd + yd * yd) / 10.0);
    double tij = dtrunc (rij);
    unsigned int dij;
    if (tij < rij)
        dij = (int) tij + 1;
    else
        dij = (int) tij;
    return dij;
}


/*
    FUNCTION: computes the matrix of all intercity distances. Allocates pinned memory in the host
    and device memory for this matrix.
*/

void  compute_distances(struct problem * problem)
{
  unsigned int i, j;
  unsigned int size = problem->n * problem->n;
  size_t memsize = sizeof (unsigned int)* size;
  cudaError_t cudaErr;

  pdebug ("Computing distance on CPU and GPU");

  //Allocating pinned memory in the host
  problem->distance = (unsigned int *) malloc (memsize);

  /*  if ((cudaErr = cudaMallocHost((void **)&problem->distance, memsize)) != cudaSuccess) {
    fprintf(stderr, "Error %d Allocating pinned memory for Distance Matrix\n", cudaErr);
    exit(1);
  }

  if ((cudaErr = cudaMalloc((void**)&d_distance, memsize)) != cudaSuccess) {
    fprintf(stderr, "Error %d Allocating device memory for Distance Matrix\n", cudaErr);
    exit(1);
  }
*/
  for ( i = 0 ; i < problem->n ; ++i ) {
      for ( j = 0  ; j < problem->n ; ++j ) {
        problem->distance[i*problem->n+j] = distance(i, j, problem->nodeptr);
      }
  }

  pdebug ("Transfering distance data from CPU to GPU, memsize %d bytes", memsize);

  /*if ((cudaErr = cudaMemcpy(d_distance, problem->distance, memsize, cudaMemcpyHostToDevice)) != cudaSuccess) {
    fprintf(stderr, "Error %d Transfering device memory for Distance Matrix\n", cudaErr);
    exit(1);
  }*/
}

