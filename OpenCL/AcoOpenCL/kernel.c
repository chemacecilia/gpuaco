/*
 * Version: 1.0
 * File: kernel.cu
 * Author: Jose M. Cecilia and Ginés D. Guerrero
 * Purpose: The GPU kernels to manage the kernel execution
 * Check: README.TXT and legal.txt
 * Copyright (C) 2013 Jose M. Cecilia and Ginés D. Guerrero
 */
/***************************************************************************
    Program's name: gpuacotsp

    Ant Colony Optimization AS algorithm, tailored to OpenCL architectures for the
    symmetric TSP

    Copyright (C) 2013  Jose M. Cecilia

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: jmcecilia at ucam.edu
    mail address: Computer Science Department
                  Catholic University of Murcia
                  Campus de los Jeronimos.
                  30107 Murcia
		          Spain

*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <CL/cl.h>
#include "kernel.h"
#include "utilities.h"
#include <string.h>
#include <math.h>
#define MAX_SOURCE_SIZE (0x100000)
#define UINT_MAX   4294967295U



/**
  * This kernel set ups the generation of random number on the GPU
  * It sets a different seed for each thread
  */
/*__kernel void setup_kernel(curandState *state, unsigned int m) {

  int id = threadIdx.x + blockIdx.x * blockDim.x; //Each thread gets same seed, a different sequence number,no offset
  if (id<m)
    curand_init(1234*blockIdx.x, id, 0, &state[id]);
}
*/

/**
  * Kernel to compute the total information
  * and it also performs the evaporation stage.
  */
/*__kernel void computeTotalkernel (float *d_choiceInfo, float *d_pheromone, unsigned int * d_distance, unsigned int n, float alpha, float beta, float rho)
{

    int tid = (blockIdx.y*blockDim.y*n)+(blockDim.x*blockIdx.x)+threadIdx.x;

    //if (tid== 0) printf ("Prueba con tid %d,", tid);
    if (tid<n*n){
        float pheromone;
        pheromone = d_pheromone[tid];
        float heuristic = 1.0 / ((float) d_distance[tid] + 0.1);
        d_choiceInfo[tid]=__powf(pheromone, alpha)*__powf(heuristic, beta);
        //Evaporation stage
        pheromone= pheromone*(1.0-rho);
        d_pheromone[tid] = pheromone;
    }
}*/



/*
	FUNCTION:       This function obtains the maximum of the array and the index of that maximum
	INPUT:          pointer to the probabilistic information and the relative index
	OUTPUT:         Relative index of the maximum probability
	(SIDE)EFFECTS:  The maximum probability is in g_idata[0]
	ORIGIN:         Partially based on NVIDIA_CUDA_SDK
*/
/*
__device__ inline unsigned int reduceMax(float *g_idata, unsigned int index, unsigned int n)
{

  __shared__ unsigned int index_sh [BLOCK_SIZE_NEXT_TOUR];

  index_sh[threadIdx.x]= index;
  g_idata[threadIdx.x] = (index>=n)?0.0:g_idata[threadIdx.x];

  __syncthreads();

  // do reduction in shared mem
  for(unsigned int s=blockDim.x/2; s>0; s>>=1) {
    if (threadIdx.x < s){
      if (g_idata[threadIdx.x]<g_idata[threadIdx.x + s]) {
	g_idata[threadIdx.x]=g_idata[threadIdx.x + s];
	index_sh[threadIdx.x]=index_sh[threadIdx.x + s];
      }
    }
    __syncthreads();
  }

  // write result for this block to global mem
  return index_sh[0];
}
*/

/**
  * FUNCTION: This function performs the tour generation by m ants
  * INPUT:
  * OUTPUT:
  * (SIDE) EFFECTS:
  : ORIGIN:
  */
/*__global__ void nextTour_kernel(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){

  __shared__ float probabilities_sh[BLOCK_SIZE_NEXT_TOUR];
  __shared__ unsigned int tourLenghtCum;
  __shared__ unsigned int current_city_sh;
  unsigned int current_city;
  unsigned int btourindex = blockIdx.x*(number_of_cities+1);
  unsigned int tabul=0;
  float random;
  curandState localState = state[blockIdx.x];

  //The tabulist is initializes as 1
  // 1 is not visited yet
  // 0 it has been visited
  // itera is the N/BlockDim with the padding
  // The padding cities are marked as visited
  //Set all bits to 1
  tabul=~(tabul & 0);

  //Each ant is placed in a initial random city.
  if (threadIdx.x ==0){
    random = curand_uniform(&localState);
    current_city_sh = (unsigned int)number_of_cities*random;
  //  printf ("The generated random number is %f for the ant %d and the beginning city is %d\n", random, blockIdx.x, current_city_sh);
  }

  __syncthreads();
  current_city = current_city_sh;
  unsigned int beginning_city = current_city;
  ////printf ("The current_city for the ant %d is %d\n", blockIdx.x, current_city);

  //Division is the bit that represents that city
  unsigned int bitTabul = (unsigned int) beginning_city>>LOG_BLOCK_SIZE_NEXT_TOUR;
  //Modulo is the thread that manages that city
  unsigned int threadTabul = (unsigned int) beginning_city & (BLOCK_SIZE_NEXT_TOUR-1);

  if (threadIdx.x == threadTabul) {
    //the bit that represents the city in this thread is marked as zero
    //Pongo el uno en el bit adecuado y hago la negacion de todo
    unsigned int aux = (unsigned int)MACROBITS<<bitTabul;
    tabul = tabul & (~aux);
    tourLenghtCum=0;
    d_tour[btourindex] = beginning_city;
   // printf("d_tour[%d] = %d\n",btourindex,d_tour[btourindex]);
  }
  __syncthreads();

  //Repeat until tabu list is full
  for (int tourIndex=1; tourIndex<number_of_cities;tourIndex++){

    int indexChoiceInfo = current_city*number_of_cities;
    float current_prob = 0.0;
    int indexaux;

    //printf ("EL numero de iteraciones es %d\n",iterathreads-1);
    for (int i=0; i<iterathreads-1; i++){

      indexaux = threadIdx.x+(i*blockDim.x);
      unsigned int auxtabul = tabul>>i; //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int) MACROBITS & auxtabul; //auxbits debe ser 0 o 1.
      float choice = choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float) choice*auxtabul*random;

      //  printf("I am thread %d y la probabilidad es %f\n", threadIdx.x, probabilities_sh[threadIdx.x]);

      __syncthreads();

      unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);
      float prob_local = probabilities_sh[0];
      ////printf ("Para la ant %d la prob que se obtiene de reduccion es %f\n", blockIdx.x, prob_local);
      if (prob_local>=current_prob) {
  	    current_prob = prob_local;
  	    current_city = aux_city;
      }
    }


    indexaux = threadIdx.x+((iterathreads-1)*blockDim.x);
    if (indexaux < number_of_cities){

      unsigned int auxtabul = tabul >>(iterathreads-1); //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int)MACROBITS & auxtabul; //auxbits debe ser 0 o 1.
      float choice= choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float)choice*auxtabul*random;
    }
    __syncthreads();
    unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);
    float prob_local = probabilities_sh[0];
    if (prob_local>=current_prob) {
      current_prob = prob_local;
      current_city = aux_city;
    }
    __syncthreads();

    //Division is the bit that represents that city
    bitTabul = current_city>>LOG_BLOCK_SIZE_NEXT_TOUR;
    //Modulo is the thread that manages that city
    threadTabul = current_city & (BLOCK_SIZE_NEXT_TOUR-1);

    if (threadIdx.x==threadTabul) {
      //printf("Bloque %d hilo %d Ciudad %d de %d\n",blockIdx.x,threadTabul,tourIndex,number_of_cities);
      //the bit that represents the city in this thread is marked as zero
      unsigned int aux = (unsigned int)MACROBITS<<bitTabul;
      tabul = tabul & (~aux);
      d_tour[btourindex+tourIndex]=current_city; //Adding the city to the tour
      //printf("d_tour[%d] = %d\n",btourindex+tourIndex,current_city);
      tourLenghtCum+= d_distance[indexChoiceInfo+current_city];
     // printf ("Para la ant %d La longitud del camino es %d\n", blockIdx.x,tourLenghtCum);
    }
    __syncthreads();

  }//End of FOR

  //Complete the tour getting the beginning city
  if (threadIdx.x==0) {
    d_tour[btourindex+number_of_cities]=beginning_city;
//printf("d_tour[%d] = %d\n",btourindex+number_of_cities,beginning_city);
    tourLenghtCum+= d_distance[current_city*number_of_cities+beginning_city];
    d_lenghtList[blockIdx.x]=tourLenghtCum; // Keep the lenght of the current tour
    printf ("Para la ant %d La longitud del camino es %d\n", blockIdx.x,tourLenghtCum);

  }
}*/

/**
  * This kernel performs the pheromone stage
  */
/*__global__ void pheromone_update_with_atomic (float * d_pheromone, const unsigned int number_of_cities, unsigned int * d_tour, const unsigned int number_of_ants, unsigned int * d_lenghtList, int ite){

  const int xIndex = blockIdx.x* (number_of_cities+1) + threadIdx.x;
  int index,indexThread;

  for (int i=0; i<ite; i++) {
    indexThread = i*blockDim.x;
    index =xIndex+(indexThread);
    if ((threadIdx.x+indexThread)<number_of_cities){
        unsigned int coordx=d_tour[index];
        unsigned int coordy=d_tour[index+1];
        unsigned int pos = (coordy*number_of_cities)+coordx;
        unsigned int posim = (coordx*number_of_cities)+coordy;
        float pheromone = 0.0;
        pheromone=(float)1.0*(1.0/d_lenghtList[blockIdx.x]);
        d_pheromone[pos]+=pheromone;
        d_pheromone[posim]+=pheromone;
    }
  }
}*/


/***** External function to launch the kernels **********/
int antSystemTSP (problem instance, colony colony){

  unsigned int i;
  //char pbuff[100];
  cl_int status = 0;

  ////////////////////////////////////////////////////////////////////
  // STEP 1 Getting Platform.
  ////////////////////////////////////////////////////////////////////

  cl_platform_id platform = selectPlatform ();

  ////////////////////////////////////////////////////////////////////
  //// STEP 2 Select the Device to use in the execution
  //////////////////////////////////////////////////////////////////////

  //Get devices number
  cl_uint num_devices;
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Failed to create a device group\n");
    return -1;
  }

  cl_device_id * devices = (cl_device_id *)malloc(num_devices*sizeof(cl_device_id));
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, num_devices, devices, NULL);
  clPrintDevInfo (devices[0]);


  ////////////////////////////////////////////////////////////////////
  //// STEP 3 Creating context using the platform selected
  ////  Context created from type includes all available
  ////  devices of the specified type from the selected platform
  //////////////////////////////////////////////////////////////////////

  cl_context context = clCreateContext(NULL, 1, &devices[0], NULL, NULL, &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Failed to create a CL context\n");
    return -1;
  }

  ////////////////////////////////////////////////////////////////////
  //// STEP 4 Creating command queue for a single device
  ////    Each device in the context can have a
  ////    dedicated commandqueue object for itself
  //////////////////////////////////////////////////////////////////////

  cl_command_queue commandQueue;
  commandQueue = clCreateCommandQueue(context, devices[0], 0, &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Failed to create a queuet\n");
    return -1;
  }


  ////////////////////////////////////////////////////////////////////
  //// STEP 5 Creating cl_buffer objects from host buffer
  ////    These buffer objects can be passed to the kernel
  ////    as kernel arguments
  //////////////////////////////////////////////////////////////////////

  const unsigned int mem_size_distance = sizeof(cl_uint) * instance.n * instance.n;
  const unsigned int mem_size_pheromone = sizeof(cl_float) * instance.n * instance.n;
  const unsigned int mem_size_tour = sizeof(cl_uint) * colony.n_ants * (instance.n+1);
  const unsigned int mem_size_lenghtList = sizeof(cl_uint) * colony.n_ants;
  const unsigned int mem_size_total = sizeof(cl_float)*instance.n * instance.n;
  const unsigned int mem_size_seed = sizeof(cl_uint)* instance.n * instance.n;

  // The memory buffer that is used as input/output for OpenCL kernel
  cl_mem   d_distance;
  cl_mem   d_total;
  cl_mem   d_pheromone;
  cl_mem   d_tour;
  cl_mem   d_lenghtList;
  cl_mem   d_bestTour;
  cl_mem   d_seed;

 //////////////////////////////////////////////////////////////////
 ///////////////////Initialize Distances///////////////////////////
 //////////////////////////////////////////////////////////////////

  d_distance = clCreateBuffer (context, CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, mem_size_distance, instance.distance,&status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating distance Buffer\n");
    return -1;
  }

 //////////////////////////////////////////////////////////////////
 ///////////////////Initialize Total///////////////////////////
 //////////////////////////////////////////////////////////////////

  d_total = clCreateBuffer (context, CL_MEM_READ_WRITE, mem_size_total, NULL,&status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating total Buffer with status %d\n", status);
    return -1;
  }

 //////////////////////////////////////////////////////////////////
 ///////////////////Initialize Pheromone///////////////////////////
 //////////////////////////////////////////////////////////////////
  d_pheromone = clCreateBuffer (context, CL_MEM_READ_WRITE|CL_MEM_COPY_HOST_PTR, mem_size_pheromone, colony.pheromone, &status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating pheromone Buffer\n");
    return -1;
  }

 //////////////////////////////////////////////////////////////////
 ///////////////////Initialize Tour Information////////////////////////
 //////////////////////////////////////////////////////////////////
  d_tour = clCreateBuffer (context, CL_MEM_READ_WRITE, mem_size_tour, NULL, &status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating d_tour Buffer\n");
    return -1;
  }

 //////////////////////////////////////////////////////////////////
 ///////////////////Initialize Tour Length Information////////////////////////
 //////////////////////////////////////////////////////////////////
  d_lenghtList = clCreateBuffer (context, CL_MEM_READ_WRITE, mem_size_lenghtList, NULL, &status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating d_tour Buffer\n");
    return -1;
  }

 //////////////////////////////////////////////////////////////////
 //////////////////Best tour Length Information////////////////////////
 //////////////////////////////////////////////////////////////////
  d_bestTour = clCreateBuffer (context, CL_MEM_READ_WRITE, mem_size_tour, NULL, &status);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating d_bestTour Buffer\n");
    return -1;
  }

 ///////////////////////////////////////////////////////////////////////////
 /////////////////Initialize for random numbers on the GPU////////////////////
 /////////////////////////////////////////////////////////////////////////////
  cl_uint *h_seed = (cl_uint *) malloc (mem_size_seed);

  //initialize random seed:

  srand ( time(NULL) );

  for (i=0; i<instance.n*instance.n;++i)
    h_seed[i]=rand()%UINT_MAX;//aux;

  d_seed = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, mem_size_seed, h_seed, &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating random Buffer\n");
    return -1;
  }

 /////////////////////////////////////////////////////////////////
 //// STEP 6. Building Program
 ////    6.1 Load CL file, using basic file i/o
 ////    6.2 Build CL program object
 ///////////////////////////////////////////////////////////////////

  //Load the kernel source code into the array source_str
  FILE * fp;
  char * source_str;
  size_t source_size;

  fp = fopen ("simpleAntGPU_kernel.cl", "r");
  if (!fp) {
    fprintf (stderr, "Failed to load kernel. \n");
  }
  source_str = (char *) malloc (MAX_SOURCE_SIZE);
  source_size =fread(source_str,1, MAX_SOURCE_SIZE,fp);
  fclose(fp);

  //create a program from the kernel source
  cl_program program = clCreateProgramWithSource(context,1, (const char **)&source_str, (const size_t *) &source_size, &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating de program Buffer\n");
    return -1;
  }

  //Build the program

  char const buildOptions[] = {"-cl-fast-relaxed-math -cl-mad-enable -cl-no-signed-zeros"};
  status = clBuildProgram(program, 1, &devices[0], buildOptions, NULL, NULL);

  if(status != CL_SUCCESS){

    char buffer[1024];
    clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, NULL);
    fprintf (stderr, "Error creating de program Buffer\n");
    return -1;
  }

  ////////////////////////////////////////////////////////////////////
  //// STEP 7 Create kernels and set appropriate arguments
  //////////////////////////////////////////////////////////////////////

  ////////////////////KERNEL TOTAL INFORMATION///////////////////////////

  cl_kernel total = clCreateKernel (program, "computeTotalkernel", &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating total kernel\n");
    return 1;
  }

  //Set kernel arguments
  if ((status = clSetKernelArg(total, 0, sizeof(cl_mem), (void *)&d_total)) != CL_SUCCESS) {
    fprintf (stderr, "Error setting argument kernel total\n");
    return 1;
  }

  if ((status = clSetKernelArg(total, 1, sizeof(cl_mem), (void *)&d_pheromone)) != CL_SUCCESS) {
    fprintf (stderr, "Error setting argument kernel total\n");
    return 1;
  }

  status = clSetKernelArg(total, 2, sizeof(cl_mem), (void *)&d_distance);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel total\n");
    return 1;
  }

  status = clSetKernelArg(total, 3, sizeof(cl_uint), (void *)&instance.n);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel total\n");
    return 1;
  }

  status = clSetKernelArg(total, 4, sizeof(cl_float), (void *)&colony.alpha);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel total\n");
    return 1;
  }

  status = clSetKernelArg(total, 5, sizeof(cl_float), (void *)&colony.beta);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel total\n");
    return 1;
  }

  status = clSetKernelArg(total, 6, sizeof(cl_float), (void *)&colony.rho);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel total\n");
    return 1;
  }

  int workItemSize, workGroupSize;
  size_t globalThreads0[1], localThreads0[1];
  setting_kernel_parameters(&workItemSize, &workGroupSize, instance.n*instance.n, BLOCK_SIZE_CHOICE);
  localThreads0[0] = workItemSize;
  globalThreads0[0]= workGroupSize*localThreads0[0];


  ////////////////////////////////////////////////////////////////
  ////////////////////NEXT TOUR KERNEL///////////////////////////
  ////////////////////////////////////////////////////////////////
  cl_uint iterNextTour;
  cl_kernel nextTour_kernel = clCreateKernel(program, "nextTour_kernel", &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating the kernel nextTour\n");
    return -1;
  }

  //set kernel arguments
  status = clSetKernelArg(nextTour_kernel, 0, sizeof(cl_uint), (void *)&instance.n);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }

  status = clSetKernelArg(nextTour_kernel, 1, sizeof(cl_uint), (void *)&colony.n_ants);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }

  status = clSetKernelArg(nextTour_kernel, 2, sizeof(cl_mem), (void *)&d_distance);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }

  status = clSetKernelArg(nextTour_kernel, 3, sizeof(cl_mem), (void *)&d_tour);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }

  status = clSetKernelArg(nextTour_kernel, 4, sizeof(cl_mem), (void *)&d_lenghtList);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }

  status = clSetKernelArg(nextTour_kernel, 5, sizeof(cl_mem), (void *)&d_total);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }

  status = clSetKernelArg(nextTour_kernel, 6, sizeof(cl_mem), (void *)&d_seed);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }
  //1 work-group per ant. And as many threads as cities or fixed number of them
  if (BLOCK_SIZE_NEXT_TOUR > instance.n) {
      printf ("The work-group size macro should be smalls than the number of cities\n");
      return -1;
   }
   size_t globalThreads1[1], localThreads1[1];
   localThreads1[0] = BLOCK_SIZE_NEXT_TOUR;
   globalThreads1[0]= colony.n_ants*localThreads1[0];

   iterNextTour = ceil((float)instance.n/BLOCK_SIZE_NEXT_TOUR);

   status = clSetKernelArg(nextTour_kernel, 7, sizeof(cl_uint), (void *)&iterNextTour);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel nextTour_kernel\n");
    return -1;
  }
   //////////////////////////////////////////////////////////////////////////
   /////////////        Kernel Pheromone Update kernel            ////////////
   ///////////////////////////////////////////////////////////////////////////

  cl_kernel pheromone_update = clCreateKernel(program, "pheromone_update", &status);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error creating the pheromone_update kernel\n");
    return -1;
  }

  cl_uint iterationsPheromones6;
  status = clSetKernelArg(pheromone_update, 0, sizeof(cl_mem), (void *)&d_pheromone);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel\n");
    return -1;
  }

  status = clSetKernelArg(pheromone_update, 1, sizeof(cl_uint), (void *)&instance.n);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel\n");
    return -1;
  }
  status = clSetKernelArg(pheromone_update, 2, sizeof(cl_mem), (void *)&d_tour);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel\n");
    return -1;
  }
  status = clSetKernelArg(pheromone_update, 3, sizeof(cl_uint), (void *)&colony.n_ants);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel\n");
    return -1;
  }
  status = clSetKernelArg(pheromone_update, 4, sizeof(cl_mem), (void *)&d_lenghtList);
  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel\n");
    return -1;
  }

  size_t globalThreads2[1], localThreads2[1];
  localThreads2[0] = (instance.n<BLOCK_SIZE_ATOMIC_DEVICE)?instance.n:BLOCK_SIZE_ATOMIC_DEVICE;
  globalThreads2[0]= colony.n_ants*localThreads2[0];
  iterationsPheromones6=ceil((float)instance.n/BLOCK_SIZE_ATOMIC_DEVICE);
  status = clSetKernelArg(pheromone_update, 5, sizeof(cl_uint), (void *)&iterationsPheromones6);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error setting argument kernel Pheromone_kernel\n");
    return -1;
  }
  ////////////////////////////////////////////////////////////////////
  //// STEP 9 Execute main loop
  /////////////////////////////////////////////////////////////////////

  // the lenght of the best tour so far
  unsigned int bestLenght=UINT_MAX;
  // Lenghts of all tour taken by each Ant
  cl_uint *h_lenghtList = (cl_uint *) malloc (mem_size_lenghtList);
  // the best tour so far
  cl_uint *h_bestTour= (cl_uint *)malloc (mem_size_tour);
  //CPerfCounter timer;
  float elapsedTime;
  cl_event event;
  unsigned int bestIt;

  for (i = 0; i < (unsigned int) colony.n_tries; ++i) {
    status = clEnqueueNDRangeKernel(commandQueue, total, 1, NULL, globalThreads0, localThreads0, 0, NULL, &event);

    if(status != CL_SUCCESS){
      fprintf (stderr, "Error: Enqueueing total kernel\n");
      return -1;
    }

    status = clWaitForEvents(1, &event);
    status = clReleaseEvent(event);
    status = clEnqueueNDRangeKernel(commandQueue, nextTour_kernel, 1, NULL, globalThreads1, localThreads1, 0, NULL, &event);

    if(status != CL_SUCCESS){
      fprintf (stderr, "Error: Enqueueing next tour  kernel\n");
      return -1;
    }
    status = clWaitForEvents(1, &event);
    status = clReleaseEvent(event);

    status = clEnqueueReadBuffer(commandQueue, d_lenghtList, CL_TRUE, 0, mem_size_lenghtList, h_lenghtList, 0, NULL, &event);

    if(status != CL_SUCCESS){
      fprintf (stderr, "Error: Enqueueing Read Buffer to read h_lenghtList\n");
      return -1;
    }

    status = clWaitForEvents(1, &event);
    status = clReleaseEvent(event);
    bestIt = getLenghtIterationBestTour(h_lenghtList, colony.n_ants, d_tour, d_bestTour, &bestLenght,instance.n, commandQueue);
    status = clEnqueueNDRangeKernel(commandQueue, pheromone_update, 1, NULL, globalThreads2, localThreads2, 0, NULL, &event);

    if(status != CL_SUCCESS){
      fprintf (stderr, "Error: Enqueueing pheromone update kernel\n");
      return -1;
    }

    status = clWaitForEvents(1, &event);
    status = clReleaseEvent(event);
    printf ("The best tour in iteration %d is %d\n", bestIt, i);
  }

  free (h_bestTour);
  free (h_lenghtList);
  status = clReleaseCommandQueue(commandQueue);
  if(status != CL_SUCCESS) {
     fprintf(stderr, "Error: In clReleasecommandQueue\n");
     return -1;
  }

  status = clReleaseKernel(total);
  status = clReleaseKernel(nextTour_kernel);
  status = clReleaseKernel(pheromone_update);
  status = clReleaseProgram(program);
  status = clReleaseMemObject(d_distance);
  status = clReleaseMemObject(d_total);
  status = clReleaseMemObject(d_pheromone);
  status = clReleaseMemObject(d_tour);
  status = clReleaseMemObject(d_lenghtList);
  status = clReleaseMemObject(d_bestTour);
  status = clReleaseContext(context);

  return 0;

}
