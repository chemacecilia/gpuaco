struct options {

/* Argument to option --tries (-r).  */
    int arg_tries;

/* Argument to option --tours (-s).  */
    int arg_tours;

/* Argument to option --tsplibfile (-i).  */
    char *arg_tsplibfile;

/* Argument to option --ants (-m).  */
    int arg_ants;

/* Argument to option --alpha (-a).  */
    float arg_alpha;

/* Argument to option --beta (-b).  */
    float arg_beta;

/* Argument to option --rho (-e).  */
    float arg_rho;

};
typedef struct options options;


/**
* This method initialize values by default
*/
void initialize_values (struct options * options);

/**
* Diplay help information
*/
void display_info();

/**
 * This function checks if the value introduced is an integer
 */
int check_valid (char * arg);

/**
 *This function prints the values of the ACO algorithm
 */
void printValues(struct options * options);

/**
 * This function parses the command line
 */
void parseValues (int argc, char ** argv, options * options);
