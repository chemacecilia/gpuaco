/*

       AAAA    CCCC   OOOO   TTTTTT   SSSSS  PPPPP
      AA  AA  CC     OO  OO    TT    SS      PP  PP
      AAAAAA  CC     OO  OO    TT     SSSS   PPPPP
      AA  AA  CC     OO  OO    TT        SS  PP
      AA  AA   CCCC   OOOO     TT    SSSSS   PP

######################################################
##########    ACO algorithms for the TSP    ##########
######################################################

      Version: 1.0
      File:    utilities.c
      Author:  Thomas Stuetzle
      Purpose: some additional useful procedures
      Check:   README and gpl.txt
      Copyright (C) 2002  Thomas Stuetzle
*/

/***************************************************************************

    Program's name: acotsp

    Ant Colony Optimization algorithms (AS, ACS, EAS, RAS, MMAS, BWAS) for the
    symmetric TSP

    Copyright (C) 2004  Thomas Stuetzle

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    email: stuetzle no@spam informatik.tu-darmstadt.de
    mail address: Universitaet Darmstadt
                  Fachbereich Informatik
                  Hochschulstr. 10
                  D-64283 Darmstadt
		  Germany

***************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "InOut.h"
#include "utilities.h"
#include <CL/cl.h>



long int seed = 12345678;


/**
 * This funtion selects the OpenCL platform among those found in the system
 */
cl_platform_id selectPlatform () {

  cl_uint numPlatforms;
  cl_platform_id platform = NULL;
  unsigned int  i;
  char pbuff[100];
  cl_int status = clGetPlatformIDs(0, NULL, &numPlatforms);

  if(status != CL_SUCCESS){
    fprintf (stderr, "Error getting the platform id\n");
    exit(-1);
  }

  printf("=== %d OpenCL platform(s) found: ===\n", numPlatforms);

  if(numPlatforms > 0) {
    cl_platform_id* platforms = (cl_platform_id *) malloc (numPlatforms * sizeof(cl_platform_id));
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    if(status != CL_SUCCESS){
        fprintf (stderr, "Error getting the platform id\n");
        exit (-1);
    }
    for(i=0; i < numPlatforms; ++i) {
        status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, sizeof(pbuff), pbuff, NULL);
        if(status != CL_SUCCESS){
            fprintf (stderr, "Error getting the platform id\n");
            exit (-1);
        }
        printf("  VENDOR = %s\n", pbuff);
        status = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, sizeof(pbuff), pbuff, NULL);
        if(status != CL_SUCCESS){
            fprintf (stderr, "Error getting the platform id\n");
            exit (-1);
        }
        printf("  NAME = %s\n", pbuff);
        platform = platforms[i];
        if(!strcmp(pbuff, "Advanced Micro Devices, Inc.")){
           break;
        }
    }
    free (platforms);
  }

  if(NULL == platform){
      fprintf (stderr, "NULL platform found so Exiting Application.\n");
      exit (-1);
  }
  printf ("The selected platform is %s\n", pbuff);
  return platform;
}


void clPrintDevInfo (cl_device_id device) {

    char device_string[1024];

    //CL_DEVICE_NAME
    clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(device_string), &device_string, NULL);
    printf ("  CL_DEVICE_NAME:\t\t\t%s\n", device_string);

    //CL_DEVICE_VENDOR
    clGetDeviceInfo(device, CL_DEVICE_VENDOR, sizeof(device_string), &device_string, NULL);
    printf ("  CL_DEVICE_VENDOR:\t\t\t%s\n", device_string);

    //CL_DRIVER_VERSION
    clGetDeviceInfo(device, CL_DRIVER_VERSION, sizeof(device_string), &device_string, NULL);
    printf ("  CL_DRIVER_VERSION:\t\t\t%s\n", device_string);

    //CL_DEVICE_INFO
    cl_device_type type;
    clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(type), &type, NULL);
    if (type & CL_DEVICE_TYPE_CPU)
        printf ("  CL_ DEVICE_TYPE: \t\t\t%s\n", "CL_DEVICE_TYPE_CPU");
    if (type & CL_DEVICE_TYPE_GPU)
        printf ("  CL_ DEVICE_TYPE: \t\t\t%s\n", "CL_DEVICE_TYPE_GPU");
    if (type & CL_DEVICE_TYPE_ACCELERATOR)
        printf ("  CL_ DEVICE_TYPE: \t\t\t%s\n", "CL_DEVICE_TYPE_ACCELERATOR");
    if (type & CL_DEVICE_TYPE_DEFAULT)
        printf ("  CL_ DEVICE_TYPE: \t\t\t%s\n", "CL_DEVICE_TYPE_DEFAULT");

    //CL_DEVICE_MAX_COMPUTE_UNITS
    cl_uint compute_units;
    clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(compute_units), &compute_units, NULL);
    printf ("  CL_DEVICE_MAX_COMPUTE_UNITS: \t\t\t%u\n", compute_units);

    //CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
    size_t workitem_dims;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(workitem_dims), &workitem_dims, NULL);
    printf ("  CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS: \t\t\t%ld\n", workitem_dims);

    //CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
    size_t workitem_size[3];
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(workitem_size), &workitem_size, NULL);
    printf ("  CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS: \t\t\t%ld / %ld / %ld \n", workitem_size[0], workitem_size[1], workitem_size[2]);


    //CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
    size_t workgroup_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(workgroup_size), &workgroup_size, NULL);
    printf ("  CL_DEVICE_MAX_WORK_GROUP_SIZE: \t\t\t%ld \n", workgroup_size);


    // CL_DEVICE_MAX_CLOCK_FREQUENCY
    cl_uint clock_frequency;
    clGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(clock_frequency), &clock_frequency, NULL);
    printf("  CL_DEVICE_MAX_CLOCK_FREQUENCY:\t%u MHz\n", clock_frequency);

    // CL_DEVICE_ADDRESS_BITS
    cl_uint addr_bits;
    clGetDeviceInfo(device, CL_DEVICE_ADDRESS_BITS, sizeof(addr_bits), &addr_bits, NULL);
    printf("  CL_DEVICE_ADDRESS_BITS:\t\t%u\n", addr_bits);

    // CL_DEVICE_MAX_MEM_ALLOC_SIZE
    cl_ulong max_mem_alloc_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(max_mem_alloc_size), &max_mem_alloc_size, NULL);
    printf("  CL_DEVICE_MAX_MEM_ALLOC_SIZE:\t\t%u MByte\n", (unsigned int)(max_mem_alloc_size / (1024 * 1024)));

    // CL_DEVICE_GLOBAL_MEM_SIZE
    cl_ulong mem_size;
    clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(mem_size), &mem_size, NULL);
    printf("  CL_DEVICE_GLOBAL_MEM_SIZE:\t\t%u MByte\n", (unsigned int)(mem_size / (1024 * 1024)));

    // CL_DEVICE_ERROR_CORRECTION_SUPPORT
    cl_bool error_correction_support;
    clGetDeviceInfo(device, CL_DEVICE_ERROR_CORRECTION_SUPPORT, sizeof(error_correction_support), &error_correction_support, NULL);
    printf("  CL_DEVICE_ERROR_CORRECTION_SUPPORT:\t%s\n", error_correction_support == CL_TRUE ? "yes" : "no");

    // CL_DEVICE_LOCAL_MEM_TYPE
    cl_device_local_mem_type local_mem_type;
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(local_mem_type), &local_mem_type, NULL);
    printf("  CL_DEVICE_LOCAL_MEM_TYPE:\t\t%s\n", local_mem_type == 1 ? "local" : "global");

    // CL_DEVICE_LOCAL_MEM_SIZE
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(mem_size), &mem_size, NULL);
    printf("  CL_DEVICE_LOCAL_MEM_SIZE:\t\t%u KByte\n", (unsigned int)(mem_size / 1024));

    // CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE
    clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(mem_size), &mem_size, NULL);
    printf("  CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:\t%u KByte\n", (unsigned int)(mem_size / 1024));

    // CL_DEVICE_QUEUE_PROPERTIES
    cl_command_queue_properties queue_properties;
    clGetDeviceInfo(device, CL_DEVICE_QUEUE_PROPERTIES, sizeof(queue_properties), &queue_properties, NULL);
    if( queue_properties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE )
        printf("  CL_DEVICE_QUEUE_PROPERTIES:\t\t%s\n", "CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE");
    if( queue_properties & CL_QUEUE_PROFILING_ENABLE )
        printf("  CL_DEVICE_QUEUE_PROPERTIES:\t\t%s\n", "CL_QUEUE_PROFILING_ENABLE");

     // CL_DEVICE_PREFERRED_VECTOR_WIDTH_<type>
    printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_<t>\t");
    cl_uint vec_width [6];
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, sizeof(cl_uint), &vec_width[0], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, sizeof(cl_uint), &vec_width[1], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(cl_uint), &vec_width[2], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, sizeof(cl_uint), &vec_width[3], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, sizeof(cl_uint), &vec_width[4], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, sizeof(cl_uint), &vec_width[5], NULL);
    printf("CHAR %u, SHORT %u, INT %u, FLOAT %u, DOUBLE %u\n\n\n", vec_width[0], vec_width[1], vec_width[2], vec_width[3], vec_width[4]);
}



void setting_kernel_parameters (int * threads, int * blocks, unsigned int reference, unsigned int block_size)
{
    *threads= (reference <= block_size)?reference:block_size;
    *blocks = (reference%*threads==0)?(reference / *threads):(reference / *threads)+1;

}


/**
  * This function sets the kernel parameter for the tour kernel.
  * We set one block per each ant and a thread per each
  * city.
  */
void setting_kernel_parameters_tour(int *grid_tour, int *block_tour, unsigned int * iterationsInTour, int  n, int n_ants)
{
/*    //We assign 1 block per ant and as many threads as cities or fixed number of them
    grid_tour->x = n_ants;
    block_tour->x = BLOCK_SIZE_NEXT_TOUR;
    if (BLOCK_SIZE_NEXT_TOUR > n) {
        printf ("The block size macro should be smaller than the number of cities");
        exit (1);
    }
    *iterationsInTour = ceil((float)n/BLOCK_SIZE_NEXT_TOUR);
    printf ("Iterations in Tour %d, numero de ciudades %d y BLOCK_SIZE NEXT TOUR %d\n", *iterationsInTour, n, BLOCK_SIZE_NEXT_TOUR);
    pdebug("Setting parameter for the Next tour kernel. Number of threads is %d , blocks %d and iterations per block %d\n", block_tour->x, grid_tour->x, *iterationsInTour);
*/
}

/**
  * This function sets the kernel parameter of pheromone update
  * kernel. We set one block per each ant and a thread per each
  * couples of cities.
  */
void setting_kernel_parameters_phero(int * grid_phero, int *block_phero, unsigned int * iterationsInPhero, int n, int n_ants){
/*
    grid_phero->x = n_ants;
    block_phero->x  = (n<BLOCK_SIZE_ATOMIC_DEVICE)?n:BLOCK_SIZE_ATOMIC_DEVICE;
    *iterationsInPhero= (unsigned int)ceil((float)n/BLOCK_SIZE_ATOMIC_DEVICE);
    pdebug ("Threads in the Pheromone Update with atomic instructions kernel 6 (x)= (%d) and blocks (%d). Number of iterations per block is %d", block_phero->x, grid_phero->x, *iterationsInPhero);
*/
}


/**
* It returns the best in the current iteration,
* and controls the best so far, doing a copy in device memory
* for the best tour
*/
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, cl_mem d_tour, cl_mem d_best, unsigned int * bestLenght, unsigned int number_of_cities, cl_command_queue commandQueue)
{
  unsigned int i;
  unsigned int bestIt=lenght[0];
  unsigned int antBest = 0;

  for (i=1; i < number_of_ants; ++i) {
      printf ("%d, ", lenght[i]);
    if (lenght[i]<bestIt) {
        bestIt = lenght[i];
        antBest =i;
    }
  }

  cl_int status = 0;
  cl_event event;
  if (bestIt < *bestLenght) {
    *bestLenght = bestIt;
    status = clEnqueueCopyBuffer(commandQueue, d_tour, d_best, antBest*(number_of_cities+1)*sizeof(cl_uint), 0, (number_of_cities+1)*sizeof(cl_uint), 0, NULL, &event);
    if(status != CL_SUCCESS){
        fprintf (stderr, "Error: clEnqueueCopyBuffer failed. (clEnqueueReadBuffer)\n");
        return -1;
    }
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS){
        fprintf(stderr, "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n");
        return -1;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS){
        fprintf(stderr,"Error: Release event object. (clReleaseEvent)\n");
        return -1;
    }
  }
  return bestIt;
}


double mean( long int *values, long int max )
/*
      FUNCTION:       compute the average value of an integer array of length max
      INPUT:          pointer to array, length of array
      OUTPUT:         average
      (SIDE)EFFECTS:  none
*/
{
  long int j;
  double   m;

  m = 0.;
  for ( j = 0 ; j < max ; j++ ) {
    m += (double)values[j];
  }
  m = m / (double)max;
  return m;
}



double meanr( double *values, long int max )
/*
      FUNCTION:       compute the average value of a floating number array of length max
      INPUT:          pointer to array, length of array
      OUTPUT:         average
      (SIDE)EFFECTS:  none
*/
{
  long int j;
  double   m;

  m = 0.;
  for ( j = 0 ; j < max ; j++ ) {
    m += values[j];
  }
  m = m / (double)max;
  return m;
}



double std_deviation( long int *values, long int max, double mean )
/*
      FUNCTION:       compute the standard deviation of an integer array
      INPUT:          pointer to array, length of array, mean
      OUTPUT:         standard deviation
      (SIDE)EFFECTS:  none
*/
{
  long int j;
  double   dev = 0.;

  if (max <= 1)
    return 0.;
  for ( j = 0 ; j < max; j++ ) {
    dev += ((double)values[j] - mean) * ((double)values[j] - mean);
  }
  return sqrt(dev/(double)(max - 1));
}



double std_deviationr( double *values, long int max, double mean )
/*
      FUNCTION:       compute the standard deviation of a floating number array
      INPUT:          pointer to array, length of array, mean
      OUTPUT:         standard deviation
      (SIDE)EFFECTS:  none
*/
{
  long int j;
  double   dev;

  if (max <= 1)
    return 0.;
  dev = 0.;
  for ( j = 0 ; j < max ; j++ ) {
    dev += ((double)values[j] - mean) * ((double)values[j] - mean);
  }
  return sqrt(dev/(double)(max - 1));
}



long int best_of_vector( long int *values, long int l )
/*
      FUNCTION:       return the minimum value in an integer value
      INPUT:          pointer to array, length of array
      OUTPUT:         smallest number in the array
      (SIDE)EFFECTS:  none
*/
{
  long int min, k;

  k = 0;
  min = values[k];
  for( k = 1 ; k < l ; k++ ) {
    if( values[k] < min ) {
      min = values[k];
    }
  }
  return min;
}



long int worst_of_vector( long int *values, long int l )
/*
      FUNCTION:       return the maximum value in an integer value
      INPUT:          pointer to array, length of array
      OUTPUT:         largest number in the array
      (SIDE)EFFECTS:  none
*/
{
  long int max, k;

  k = 0;
  max = values[k];
  for( k = 1 ; k < l ; k++ ) {
    if( values[k] > max ){
      max = values[k];
    }
  }
  return max;
}



double quantil(long int v[], double q, long int l)
/*
      FUNCTION:       return the q-quantil of an ordered integer array
      INPUT:          one array, desired quantil q, length of array
      OUTPUT:         q-quantil of array
      (SIDE)EFFECTS:  none
*/
{
  long int i,j;
  double tmp;

  tmp = q * (double)l;
  if ((double)((long int)tmp) == tmp) {
    i = (long int)tmp;
    j = (long int)(tmp + 1.);
    return ((double)v[i-1] + (double)v[j-1]) / 2.;
  } else {
    i = (long int)(tmp +1.);
    return v[i-1];
  }
}



void swap(long int v[], long int i, long int j)
/*
      FUNCTION:       auxiliary routine for sorting an integer array
      INPUT:          array, two indices
      OUTPUT:         none
      (SIDE)EFFECTS:  elements at position i and j of array are swapped
*/
{
  long int tmp;

  tmp = v[i];
  v[i] = v[j];
  v[j] = tmp;
}




void sort(long int v[], long int left, long int right)
/*
      FUNCTION:       recursive routine (quicksort) for sorting an array
      INPUT:          one array, two indices
      OUTPUT:         none
      (SIDE)EFFECTS:  elements at position i and j of the two arrays are swapped
*/
{
  long int k, last;

  if (left >= right)
    return;
  swap(v, left, (left + right)/2);
  last = left;
  for (k=left+1; k <= right; k++)
    if (v[k] < v[left])
      swap(v, ++last, k);
  swap(v, left, last);
  sort(v, left, last);
  sort(v, last+1, right);
}



void swap2(long int v[], long int v2[], long int i, long int j)
/*
      FUNCTION:       auxiliary routine for sorting an integer array
      INPUT:          two arraya, two indices
      OUTPUT:         none
      (SIDE)EFFECTS:  elements at position i and j of the two arrays are swapped
*/
{
  long int tmp;

  tmp = v[i];
  v[i] = v[j];
  v[j] = tmp;
  tmp = v2[i];
  v2[i] = v2[j];
  v2[j] = tmp;
}



void sort2(long int v[], long int v2[], long int left, long int right)
/*
      FUNCTION:       recursive routine (quicksort) for sorting one array; second
                      arrays does the same sequence of swaps
      INPUT:          two arrays, two indices
      OUTPUT:         none
      (SIDE)EFFECTS:  elements at position i and j of the two arrays are swapped
*/
{
  long int k, last;

  if (left >= right)
    return;
  swap2(v, v2, left, (left + right)/2);
  last = left;
  for (k=left+1; k <= right; k++)
    if (v[k] < v[left])
      swap2(v, v2, ++last, k);
  swap2(v, v2, left, last);
  sort2(v, v2, left, last);
  sort2(v, v2, last+1, right);
}



double ran01( long *idum )
/*
      FUNCTION:       generate a random number that is uniformly distributed in [0,1]
      INPUT:          pointer to variable with the current seed
      OUTPUT:         random number uniformly distributed in [0,1]
      (SIDE)EFFECTS:  random number seed is modified (important, this has to be done!)
      ORIGIN:         numerical recipes in C
*/
{
  long k;
  double ans;

  k =(*idum)/IQ;
  *idum = IA * (*idum - k * IQ) - IR * k;
  if (*idum < 0 ) *idum += IM;
  ans = AM * (*idum);
  return ans;
}



long int random_number( long *idum )
/*
      FUNCTION:       generate an integer random number
      INPUT:          pointer to variable containing random number seed
      OUTPUT:         integer random number uniformly distributed in {0,2147483647}
      (SIDE)EFFECTS:  random number seed is modified (important, has to be done!)
      ORIGIN:         numerical recipes in C
*/
{
  long k;

  k =(*idum)/IQ;
  *idum = IA * (*idum - k * IQ) - IR * k;
  if (*idum < 0 ) *idum += IM;
  return *idum;
}



long int ** generate_int_matrix( long int n, long int m)
/*
      FUNCTION:       malloc a matrix and return pointer to it
      INPUT:          size of matrix as n x m
      OUTPUT:         pointer to matrix
      (SIDE)EFFECTS:
*/
{
  long int i;
  long int **matrix;

  if((matrix = malloc(sizeof(long int) * n * m +
		      sizeof(long int *) * n	 )) == NULL){
    printf("Out of memory, exit.");
    exit(1);
  }
  for ( i = 0 ; i < n ; i++ ) {
    matrix[i] = (long int *)(matrix + n) + i*m;
  }

  return matrix;
}



double ** generate_double_matrix( long int n, long int m)
/*
      FUNCTION:       malloc a matrix and return pointer to it
      INPUT:          size of matrix as n x m
      OUTPUT:         pointer to matrix
      (SIDE)EFFECTS:
*/
{

  long int i;
  double **matrix;

  if((matrix = malloc(sizeof(double) * n * m +
		      sizeof(double *) * n	 )) == NULL){
    printf("Out of memory, exit.");
    exit(1);
  }
  for ( i = 0 ; i < n ; i++ ) {
    matrix[i] = (double *)(matrix + n) + i*m;
  }
  return matrix;
}
