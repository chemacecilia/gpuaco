// includes, project
#include "simpleAntColonyGPU_launcher.hpp"


/*
 * Converts the contents of a file into a string
 */
std::string
convertToString(const char *filename)
{
  size_t size;
  char*  str;
  std::string s;

  std::fstream f(filename, (std::fstream::in | std::fstream::binary));

  if(f.is_open())
  {
    size_t fileSize;
    f.seekg(0, std::fstream::end);
    size = fileSize = (size_t)f.tellg();
    f.seekg(0, std::fstream::beg);

    str = new char[size+1];
    if(!str)
    {
      f.close();
      std::cout << "Memory allocation failed";
      return NULL;
    }

    f.read(str, fileSize);
    f.close();
    str[size] = '\0';
  
    s = str;
    delete[] str;
    return s;
  }
  else
  {
    std::cout << "\nFile containg the kernel code(\"" << filename << "\") not found. Please copy the required file in the folder containg the executable.\n";
    exit(1);
  }
  return NULL;
} 

////////////////////////////////////////////////////////////////////////////////
//! Entry point for Cuda functionality on host side
//! @param  
////////////////////////////////////////////////////////////////////////////////
extern "C" int
antColonyGPULauncher(const unsigned int number_of_ants, const unsigned int number_of_cities, unsigned int * distance, float * pheromone, float * choice_info, unsigned int * ants, float alpha, float beta, float evaporation_rate, unsigned int iterations, float elitist_weight, unsigned int elitist_ants, unsigned int best_so_far_frequency, float a, AcoType acotype)
{
  cl_int status = 0;

  //////////////////////////////////////////////////////////////////// 
  // STEP 1 Getting Platform.
  //////////////////////////////////////////////////////////////////// 
  /*
   * Have a look at the available platforms and pick either
   * the AMD one if available or a reasonable default.
   */
  cl_uint numPlatforms;
  cl_platform_id platform = NULL;
  status = clGetPlatformIDs(0, NULL, &numPlatforms);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: Getting Platforms. (clGetPlatformsIDs)\n";
    return ACO_FAILURE;
  }
  
  if(numPlatforms > 0)
  {
    cl_platform_id* platforms = new cl_platform_id[numPlatforms];
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    if(status != CL_SUCCESS)
    {
      std::cout << "Error: Getting Platform Ids. (clGetPlatformsIDs)\n";
      return ACO_FAILURE;
    }
    for(unsigned int i=0; i < numPlatforms; ++i)
    {
      char pbuff[100];
      status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, sizeof(pbuff), pbuff, NULL);
      if(status != CL_SUCCESS)
      {
        std::cout << "Error: Getting Platform Info.(clGetPlatformInfo)\n";
        return ACO_FAILURE;
      }
      platform = platforms[i];
      std::cout << "Plat " << i << " = " << pbuff << "\n";  //andres
     // if(!strcmp(pbuff, "Advanced Micro Devices, Inc."))
      if(!strcmp(pbuff, "ARM"))
      {
        break;
      }
    }
    delete platforms;
  }

  if(NULL == platform)
  {
    std::cout << "NULL platform found so Exiting Application." << std::endl;
    return ACO_FAILURE;
  }

  //////////////////////////////////////////////////////////////////// 
  // STEP 2 Creating context using the platform selected
  //  Context created from type includes all available
  //  devices of the specified type from the selected platform 
  //////////////////////////////////////////////////////////////////// 
  /*
   * If we could find our platform, use it. Otherwise use just available platform.
   */
  cl_context_properties cps[3] = { CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 0 };

  cl_context context = clCreateContextFromType(cps, CL_DEVICE_TYPE_GPU, NULL, NULL, &status);  // was CPU andres
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Creating Context. (clCreateContextFromType)\n";
    return ACO_FAILURE; 
  }

  //////////////////////////////////////////////////////////////////// 
  // STEP 3
  //  3.1 Query context for the device list size,
  //  3.2 Allocate that much memory using malloc or new
  //  3.3 Again query context info to get the array of device
  //    available in the created context
  //////////////////////////////////////////////////////////////////// 
  size_t deviceListSize;

  // First, get the size of device list data
  status = clGetContextInfo(context, CL_CONTEXT_DEVICES, 0, NULL, &deviceListSize);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Getting Context Info (device list size, clGetContextInfo)\n";
    return ACO_FAILURE;
  }

  cl_device_id * devices = (cl_device_id *)malloc(deviceListSize);
  if(devices == 0)
  {
    std::cout << "Error: No devices found.\n";
    return ACO_FAILURE;
  }

  // Now, get the device list data
  status = clGetContextInfo(context, CL_CONTEXT_DEVICES, deviceListSize, devices, NULL);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Getting Context Info (device list, clGetContextInfo)\n";
    return ACO_FAILURE;
  }

  //////////////////////////////////////////////////////////////////// 
  // STEP 4 Creating command queue for a single device
  //    Each device in the context can have a 
  //    dedicated commandqueue object for itself
  //////////////////////////////////////////////////////////////////// 
  commandQueue = clCreateCommandQueue(context, devices[0], 0, &status);
  if(status != CL_SUCCESS) 
  {    
    std::cout << "Creating Command Queue. (clCreateCommandQueue)\n";
    return ACO_FAILURE;
  }

  /////////////////////////////////////////////////////////////////
  // STEP 5 Creating cl_buffer objects from host buffer
  //      These buffer objects can be passed to the kernel
  //      as kernel arguments
  /////////////////////////////////////////////////////////////////
  const unsigned int mem_size_distance = sizeof(cl_uint) * number_of_cities * number_of_cities;
  const unsigned int mem_size_pheromone = sizeof(cl_float) * number_of_cities * number_of_cities;
  const unsigned int mem_size_tour = sizeof(cl_uint) * number_of_ants * (number_of_cities+1);
  const unsigned int mem_size_lenghtList = sizeof(cl_uint) * number_of_ants;
  const unsigned int mem_size_choiceinfo = sizeof(cl_float)*number_of_cities * number_of_cities;
  const unsigned int mem_size_seed = sizeof(cl_uint)*number_of_cities * number_of_cities;
  unsigned int totalBytesAllocated = 0; //Bytes allocated on the GPU
  
  /* The memory buffer that is used as input/output for OpenCL kernel */
  cl_mem   d_distance;
  cl_mem   d_choiceInfo;
  cl_mem   d_pheromone;
  cl_mem   d_tour;
  cl_mem   d_lenghtList;
  cl_mem   d_bestTour;
  cl_mem   d_seed;

  //////////////////////////////////////////////////////////////////
  ///////////////////Initialize Distances///////////////////////////
  /////////////////////////////////////////////////////////////////
  d_distance = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, mem_size_distance, distance, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_distance)\n";
    return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_distance;
  
  ////////////////////////////////////////////////////////////////////////  
  ///////////////Initialize Choice Information////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  d_choiceInfo = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_choiceinfo, NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_choiceInfo)\n";
    return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_choiceinfo;
  
  ///////////////////////////////////////////////////////////////////////////
  ///////////////Initialize Pheromone Information////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  d_pheromone = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, mem_size_pheromone, pheromone, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_pheromone)\n";
    return ACO_FAILURE;
  }
  
  totalBytesAllocated+=mem_size_pheromone;
  
  ///////////////////////////////////////////////////////////////////////////
  /////////////////Initialize Tour information//////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  d_tour = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_tour, NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_tour)\n";
    return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_tour;
  
  ///////////////////////////////////////////////////////////////////////////
  /////////////////Initialize Tour Lenght information////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  // the lenght of the best tour so far
  unsigned int bestLenght=UINT_MAX;    
  
  // Lenghts of all tour taken by each Ant      
  cl_uint *h_lenghtList = (cl_uint *) malloc (mem_size_lenghtList);
  
  // allocate device memory for the lenght list
  d_lenghtList = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_lenghtList,  NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_lenghtList)\n";
    return ACO_FAILURE;
  }
  
  totalBytesAllocated+=mem_size_lenghtList;
  
  ///////////////////////////////////////////////////////////////////////////
  /////////////////////////Best tour information ////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  // the best tour so far
  cl_uint *h_bestTour= (cl_uint *)malloc (mem_size_tour);
  
  d_bestTour = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_tour, NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_bestTour)\n";
    return ACO_FAILURE;
  }
  
  totalBytesAllocated+=mem_size_tour;
  
  ///////////////////////////////////////////////////////////////////////////
  ///////////////Initialize for random numbers on the GPU////////////////////
  ///////////////////////////////////////////////////////////////////////////
  cl_uint *h_seed = (cl_uint *) malloc (mem_size_seed);

  /* initialize random seed: */
  srand ( time(NULL) );
  //uint aux = rand()%UINT_MAX;

  for (uint i=0; i<number_of_cities*number_of_cities;i++)
    h_seed[i]=rand()%UINT_MAX;//aux;
    
  d_seed = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, mem_size_seed, h_seed, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_seed)\n";
    return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_seed;
  
  /////////////////////////////////////////////////////////////////
  // STEP 6. Building Program
  //    6.1 Load CL file, using basic file i/o
  //    6.2 Build CL program object
  /////////////////////////////////////////////////////////////////
  const char * filename  = "simpleAntGPU_kernel.cl";
  std::string  sourceStr = convertToString(filename);
  const char * source    = sourceStr.c_str();
  size_t sourceSize[]    = {strlen(source)};

  cl_program program = clCreateProgramWithSource(context, 1, &source, sourceSize, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Loading Binary into cl_program (clCreateProgramWithBinary)\n";
    return ACO_FAILURE;
  }

  std::string BuildOptions = "-cl-fast-relaxed-math";
  BuildOptions += " -cl-mad-enable";
  BuildOptions += " -cl-no-signed-zeros";
  
  // create a cl program executable for all the devices specified
  status = clBuildProgram(program, 1, devices, BuildOptions.c_str(), NULL, NULL);
  if(status != CL_SUCCESS) 
  {
    char buffer[10240];
    clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, NULL);
    std::cout << "CL Compilation failed" << std::endl << buffer << std::endl;
    return ACO_FAILURE; 
  }
  
  //////////////////////////////////////////////////////////////////// 
  // STEP 7 Create kernels and set appropriate arguments
  //////////////////////////////////////////////////////////////////// 
  
  //////////////////////////////////////////////////////////////////////////
  //////////////          Kernel Choiceinfokernel          /////////////////
  //////////////////////////////////////////////////////////////////////////
  initChoiceInfo = clCreateKernel(program, "init_choiceinfo_kernel", &status);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Creating Kernel init_choiceinfo_kernel from program. (clCreateKernel)\n";
    return ACO_FAILURE;
  }

  status = clSetKernelArg(initChoiceInfo, 0, sizeof(cl_mem), (void *)&d_choiceInfo);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Setting initChoiceInfo aernel argument. (d_choiceInfo)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 1, sizeof(cl_mem), (void *)&d_pheromone); 
  if(status != CL_SUCCESS) 
  { 
     std::cout << "Error: Setting initChoiceInfo kernel argument. (d_pheromone)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 2, sizeof(cl_mem), (void *)&d_distance);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (d_distance)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 3, sizeof(cl_uint), (void *)&number_of_cities);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (number_of_cities)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 4, sizeof(cl_float), (void *)&alpha);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (alpha)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 5, sizeof(cl_float), (void *)&beta);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (beta)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 6, sizeof(cl_float), (void *)&evaporation_rate);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (evaporation_rate)\n";
    return ACO_FAILURE;
  }
  
  int workItemSize, workGroupSize;
  size_t globalThreads0[1], localThreads0[1];
  setting_kernel_parameters(&workItemSize, &workGroupSize, number_of_cities*number_of_cities, BLOCK_SIZE_CHOICE);
  localThreads0[0] = workItemSize;  
  globalThreads0[0]= workGroupSize*localThreads0[0];
  
  //////////////////////////////////////////////////////////////////////////
  //////////////              Kernel Next Tour             /////////////////
  //////////////////////////////////////////////////////////////////////////
  nextTour_kernel = clCreateKernel(program, "nextTour_kernel", &status);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Creating Kernel nextTour_kernel from program. (clCreateKernel)\n";
    return ACO_FAILURE;
  }
  
  cl_uint iterNextTour;
  status = clSetKernelArg(nextTour_kernel, 0, sizeof(cl_uint), (void *)&number_of_cities);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (number_of_cities)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 1, sizeof(cl_uint), (void *)&number_of_ants);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (number_of_ants)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 2, sizeof(cl_mem), (void *)&d_distance);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (d_distance)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 3, sizeof(cl_mem), (void *)&d_tour);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Setting nextTour_kernel aernel argument. (d_tour)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 4, sizeof(cl_mem), (void *)&d_lenghtList); 
  if(status != CL_SUCCESS) 
  { 
     std::cout << "Error: Setting nextTour_kernel kernel argument. (d_lenghtList)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 5, sizeof(cl_mem), (void *)&d_choiceInfo);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (d_choiceInfo)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 6, sizeof(cl_mem), (void *)&d_seed);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (d_seed)\n";
    return ACO_FAILURE;
  }

  // 1 work-group per ant. And as many threads as cities or fixed number of them 
  if (BLOCK_SIZE_NEXT_TOUR > number_of_cities) {
     std::cout << "The work-group size macro should be smaills than the number of cities\n";
     return CL_SUCCESS;    
  }
  size_t globalThreads1[1], localThreads1[1];
  localThreads1[0] = BLOCK_SIZE_NEXT_TOUR;  
  globalThreads1[0]= number_of_ants*localThreads1[0];  
  iterNextTour=ceil((float)number_of_cities/BLOCK_SIZE_NEXT_TOUR);
  
  status = clSetKernelArg(nextTour_kernel, 7, sizeof(cl_uint), (void *)&iterNextTour);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (iterNextTour)\n";
    return ACO_FAILURE;
  }
  
  //////////////////////////////////////////////////////////////////////////
  ////////////        Kernel Pheromone Update kernel            ////////////
  //////////////////////////////////////////////////////////////////////////
  pheromone_update = clCreateKernel(program, "pheromone_update", &status);
  if(status != CL_SUCCESS)
  {  
    std::cout << "Error: Creating Kernel pheromone_update from program. (clCreateKernel)\n";
    return ACO_FAILURE;
  }
  
  cl_uint iterationsPheromones6;
  status = clSetKernelArg(pheromone_update, 0, sizeof(cl_mem), (void *)&d_pheromone);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (d_pheromone)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 1, sizeof(cl_uint), (void *)&number_of_cities);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (number_of_cities)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 2, sizeof(cl_mem), (void *)&d_tour);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (d_tour)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 3, sizeof(cl_uint), (void *)&number_of_ants);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Setting pheromone_update aernel argument. (number_of_ants)\n";
    return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 4, sizeof(cl_mem), (void *)&d_lenghtList); 
  if(status != CL_SUCCESS) 
  { 
     std::cout << "Error: Setting pheromone_update kernel argument. (d_lenghtList)\n";
    return ACO_FAILURE;
  }

  size_t globalThreads2[1], localThreads2[1];
  localThreads2[0] = (number_of_cities<BLOCK_SIZE_ATOMIC_DEVICE)?number_of_cities:BLOCK_SIZE_ATOMIC_DEVICE;
  globalThreads2[0]= number_of_ants*localThreads2[0];  
  iterationsPheromones6=ceil((float)number_of_cities/BLOCK_SIZE_ATOMIC_DEVICE);
  
  status = clSetKernelArg(pheromone_update, 5, sizeof(cl_uint), (void *)&iterationsPheromones6);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (iterationsPheromones6)\n";
    return ACO_FAILURE;
  }
  
  
  //////////////////////////////////////////////////////////////////// 
  // STEP 9 Execute main loop
  ////////////////////////////////////////////////////////////////////  
  iterations=100;   // andres
  std::cout << "Number of iterations is " << iterations << "\n";
  CPerfCounter choice, reduc, next, pher, transf;
  float elapsedTime, accu=0.0;

  cl_event event;
  for (uint i=0; i<iterations; i++) {   
    choice.Reset();
    choice.Start();
    status = clEnqueueNDRangeKernel(commandQueue, initChoiceInfo, 1, NULL, globalThreads0, localThreads0, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Enqueueing initChoiceInfo kernel onto command queue. (clEnqueueNDRangeKernel)\n";
      return ACO_FAILURE;
    }
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. (clReleaseEvent)\n";
      return ACO_FAILURE;
    }
    choice.Stop();
    elapsedTime = choice.GetElapsedTime()*1000; 
    std::cout << "Total execution time for Init Choice: " << elapsedTime <<", msec \n";      
    accu+= elapsedTime;

    next.Reset();
    next.Start();
    status = clEnqueueNDRangeKernel(commandQueue, nextTour_kernel, 1, NULL, globalThreads1, localThreads1, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Enqueueing nextTour_kernel kernel onto command queue.(clEnqueueNDRangeKernel)\n";
      return ACO_FAILURE;
    }
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. (clReleaseEvent)\n";
      return ACO_FAILURE;
    }
    next.Stop();
    elapsedTime = next.GetElapsedTime()*1000; 
    std::cout << "Total execution time for Next: " << elapsedTime <<", msec \n";      
    accu+= elapsedTime;
    
    /*transf.Reset();
    transf.Start();*/
    status = clEnqueueReadBuffer(commandQueue, d_lenghtList, CL_TRUE, 0, mem_size_lenghtList, h_lenghtList, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: clEnqueueReadBuffer failed. (clEnqueueReadBuffer)\n";
      return ACO_FAILURE;
    }
    // Wait for the read buffer to finish execution
   std::cout << "primero espero d_lenghtList\n";
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. (clReleaseEvent)\n";
      return ACO_FAILURE;
    }         
    /*transf.Stop();
    elapsedTime = transf.GetElapsedTime()*1000; 
    std::cout << "Transference time for Length List: " << elapsedTime <<", msec \n";      
    accu+= elapsedTime;*/

    unsigned int bestIt = getLenghtIterationBestTour(h_lenghtList, number_of_ants, d_tour, d_bestTour,&bestLenght, number_of_cities);     
    std::cout << "bestIt " << bestIt << "\n";
    
      status = clEnqueueReadBuffer(commandQueue, d_bestTour, CL_TRUE, 0, (number_of_cities+1)*sizeof(cl_uint), h_bestTour, 0, NULL, &event);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: clEnqueueReadBuffer failed. (clEnqueueReadBuffer)\n";
    return ACO_FAILURE;
  }
  // Wait for the read buffer to finish execution
  status = clWaitForEvents(1, &event);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: Waiting for read buffer call to finish.(clWaitForEvent)\n";
    return ACO_FAILURE;
  }
  status = clReleaseEvent(event);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: Release event object. (clReleaseEvent)\n";
    return ACO_FAILURE;
  }
    printTour(h_bestTour, number_of_cities);
    std::cout << std::endl;
 
    pher.Reset();
    pher.Start();
    status = clEnqueueNDRangeKernel(commandQueue, pheromone_update, 1, NULL, globalThreads2, localThreads2, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Enqueueing pheromone kernel onto command queue. (clEnqueueNDRangeKernel)\n";
      return ACO_FAILURE;
    }
std::cout << "ahora espero pheromone_update\n";
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. clReleaseEvent)\n";
      return ACO_FAILURE;
    } 
    pher.Stop();
    elapsedTime = pher.GetElapsedTime()*1000; 
    std::cout << "Total execution time for Pheromone: " << elapsedTime <<", msec \n";      
    accu+= elapsedTime;
  }
  
  std::cout << "Total execution time: " <<accu/iterations <<", msec \n";   
  
  std::cout << std::endl;
  std::cout << "Best Tour Lenght" << std::endl;
  std::cout << bestLenght << "\t";
  std::cout << std::endl;  

  std::cout << "Best Ordering" << std::endl;
  status = clEnqueueReadBuffer(commandQueue, d_bestTour, CL_TRUE, 0, (number_of_cities+1)*sizeof(cl_uint), h_bestTour, 0, NULL, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clEnqueueReadBuffer failed. (clEnqueueReadBuffer)\n";
    return ACO_FAILURE;
  }
  // Wait for the read buffer to finish execution
  status = clWaitForEvents(1, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
    return ACO_FAILURE;
  }
  status = clReleaseEvent(event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Release event object. (clReleaseEvent)\n";
    return ACO_FAILURE;
  }      
  printTour(h_bestTour, number_of_cities);
  std::cout << std::endl;
  
  status = clReleaseKernel(initChoiceInfo);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseKernel initChoiceInfo\n";
    return ACO_FAILURE; 
  }
  status = clReleaseKernel(nextTour_kernel);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseKernel nextTour_kernel\n";
    return ACO_FAILURE; 
  }
  status = clReleaseKernel(pheromone_update);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseKernel pheromone_update\n";
    return ACO_FAILURE; 
  }
  status = clReleaseProgram(program);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseProgram\n";
    return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_distance);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_distance)\n";
    return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_choiceInfo);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_choiceInfo)\n";
    return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_pheromone);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_pheromone)\n";
    return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_tour);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_tour)\n";
    return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_lenghtList);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_lenghtList)\n";
    return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_bestTour);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_bestTour)\n";
    return ACO_FAILURE; 
  }
  status = clReleaseCommandQueue(commandQueue);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseCommandQueue\n";
    return ACO_FAILURE;
  }
  status = clReleaseContext(context);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseContext\n";
    return ACO_FAILURE;
  }

  free (h_bestTour);  
  free (h_lenghtList);

  return ACO_SUCCESS;
}


void setting_kernel_parameters(int * threads, int * blocks, unsigned int reference, unsigned int block_size) {
  
  *threads= (reference <= block_size)?reference:block_size;
  *blocks = (reference%*threads==0)?(reference / *threads):(reference / *threads)+1; 
  
}



void printTour(unsigned int * best, unsigned int number_of_cities) {

  for (uint i=0; i<=number_of_cities;i++)
  std::cout << best[i] << ", ";
    
  std::cout << std::endl;    
}

///
///It returns the best in the current iteration, and controls the best so far, doing a copy in device memory for the best tour 
///
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, cl_mem d_tour, cl_mem d_best, unsigned int * bestLenght, unsigned int number_of_cities) 
{
  
  unsigned int bestIt=lenght[0];
  unsigned int antBest = 0;
  
  for (uint i=1;i<number_of_ants;i++) {
  if (lenght[i]<bestIt) {
    bestIt = lenght[i];
    antBest =i;
  }
  }
  
  
  /* int size = (number_of_cities+1)*number_of_ants*sizeof(uint);

  unsigned int * partialbest = (unsigned int * ) malloc (size);
  cutilSafeCall(cudaMemcpy(partialbest, d_tour, size, cudaMemcpyDeviceToHost) );
  
  std::cout << "Todas los tours para todas las ants \n";
  for (int j = 0; j< number_of_ants; j++) {
  std::cout << "Para la ant " << j << std::endl;
  for (int i=0; i<=number_of_cities; i++) {
    std::cout << partialbest[(j*(number_of_cities+1))+i] <<", ";
  }
  std::cout << std::endl;
  }
  */

  cl_int status = 0;
  cl_event event;
  if (bestIt < *bestLenght) {
  //std::cout << "La mejor ant es " << antBest << std::endl;
  *bestLenght = bestIt;  

  status = clEnqueueCopyBuffer(commandQueue, d_tour, d_best, antBest*(number_of_cities+1)*sizeof(cl_uint), 0, (number_of_cities+1)*sizeof(cl_uint), 0, NULL, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clEnqueueCopyBuffer failed. (clEnqueueReadBuffer)\n";
    return ACO_FAILURE;
  }
  // Wait for the copy buffer to finish execution
  status = clWaitForEvents(1, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
    return ACO_FAILURE;
  }
  status = clReleaseEvent(event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Release event object. (clReleaseEvent)\n";
    return ACO_FAILURE;
  }        
  }      

//fin
  return bestIt;      
}
