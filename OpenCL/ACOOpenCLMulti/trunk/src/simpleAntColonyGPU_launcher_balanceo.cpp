// includes, project
#include "simpleAntColonyGPU_launcher.hpp"
#include <omp.h>
#include <mpi.h>

//#define TIMING

void clPrintDevInfo(cl_device_id device)
{
    char device_string[1024];


    // CL_DEVICE_NAME
    clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(device_string), &device_string, NULL);
   printf("  CL_DEVICE_NAME: \t\t\t%s\n", device_string);

    // CL_DEVICE_VENDOR
    clGetDeviceInfo(device, CL_DEVICE_VENDOR, sizeof(device_string), &device_string, NULL);
   printf("  CL_DEVICE_VENDOR: \t\t\t%s\n", device_string);
 
    // CL_DRIVER_VERSION
    clGetDeviceInfo(device, CL_DRIVER_VERSION, sizeof(device_string), &device_string, NULL);
   printf("  CL_DRIVER_VERSION: \t\t\t%s\n", device_string);

    // CL_DEVICE_INFO
    cl_device_type type;
    clGetDeviceInfo(device, CL_DEVICE_TYPE, sizeof(type), &type, NULL);
    if( type & CL_DEVICE_TYPE_CPU )
       printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_CPU");
    if( type & CL_DEVICE_TYPE_GPU )
       printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_GPU");
    if( type & CL_DEVICE_TYPE_ACCELERATOR )
       printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_ACCELERATOR");
    if( type & CL_DEVICE_TYPE_DEFAULT )
       printf("  CL_DEVICE_TYPE:\t\t\t%s\n", "CL_DEVICE_TYPE_DEFAULT");
    
    // CL_DEVICE_MAX_COMPUTE_UNITS
    cl_uint compute_units;
    clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(compute_units), &compute_units, NULL);
   printf("  CL_DEVICE_MAX_COMPUTE_UNITS:\t\t%u\n", compute_units);

    // CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
    size_t workitem_dims;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, sizeof(workitem_dims), &workitem_dims, NULL);
   printf("  CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:\t%u\n", workitem_dims);

    // CL_DEVICE_MAX_WORK_ITEM_SIZES
    size_t workitem_size[3];
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(workitem_size), &workitem_size, NULL);
   printf("  CL_DEVICE_MAX_WORK_ITEM_SIZES:\t%u / %u / %u \n", workitem_size[0], workitem_size[1], workitem_size[2]);
    
    // CL_DEVICE_MAX_WORK_GROUP_SIZE
    size_t workgroup_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(workgroup_size), &workgroup_size, NULL);
   printf("  CL_DEVICE_MAX_WORK_GROUP_SIZE:\t%u\n", workgroup_size);

    // CL_DEVICE_MAX_CLOCK_FREQUENCY
    cl_uint clock_frequency;
    clGetDeviceInfo(device, CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(clock_frequency), &clock_frequency, NULL);
   printf("  CL_DEVICE_MAX_CLOCK_FREQUENCY:\t%u MHz\n", clock_frequency);

    // CL_DEVICE_ADDRESS_BITS
    cl_uint addr_bits;
    clGetDeviceInfo(device, CL_DEVICE_ADDRESS_BITS, sizeof(addr_bits), &addr_bits, NULL);
   printf("  CL_DEVICE_ADDRESS_BITS:\t\t%u\n", addr_bits);

    // CL_DEVICE_MAX_MEM_ALLOC_SIZE
    cl_ulong max_mem_alloc_size;
    clGetDeviceInfo(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE, sizeof(max_mem_alloc_size), &max_mem_alloc_size, NULL);
   printf("  CL_DEVICE_MAX_MEM_ALLOC_SIZE:\t\t%u MByte\n", (unsigned int)(max_mem_alloc_size / (1024 * 1024)));

    // CL_DEVICE_GLOBAL_MEM_SIZE
    cl_ulong mem_size;
    clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(mem_size), &mem_size, NULL);
   printf("  CL_DEVICE_GLOBAL_MEM_SIZE:\t\t%u MByte\n", (unsigned int)(mem_size / (1024 * 1024)));

    // CL_DEVICE_ERROR_CORRECTION_SUPPORT
    cl_bool error_correction_support;
    clGetDeviceInfo(device, CL_DEVICE_ERROR_CORRECTION_SUPPORT, sizeof(error_correction_support), &error_correction_support, NULL);
   printf("  CL_DEVICE_ERROR_CORRECTION_SUPPORT:\t%s\n", error_correction_support == CL_TRUE ? "yes" : "no");

    // CL_DEVICE_LOCAL_MEM_TYPE
    cl_device_local_mem_type local_mem_type;
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_TYPE, sizeof(local_mem_type), &local_mem_type, NULL);
   printf("  CL_DEVICE_LOCAL_MEM_TYPE:\t\t%s\n", local_mem_type == 1 ? "local" : "global");

    // CL_DEVICE_LOCAL_MEM_SIZE
    clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(mem_size), &mem_size, NULL);
   printf("  CL_DEVICE_LOCAL_MEM_SIZE:\t\t%u KByte\n", (unsigned int)(mem_size / 1024));

    // CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE
    clGetDeviceInfo(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(mem_size), &mem_size, NULL);
   printf("  CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:\t%u KByte\n", (unsigned int)(mem_size / 1024));

    // CL_DEVICE_QUEUE_PROPERTIES
    cl_command_queue_properties queue_properties;
    clGetDeviceInfo(device, CL_DEVICE_QUEUE_PROPERTIES, sizeof(queue_properties), &queue_properties, NULL);
    if( queue_properties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE )
       printf("  CL_DEVICE_QUEUE_PROPERTIES:\t\t%s\n", "CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE");    
    if( queue_properties & CL_QUEUE_PROFILING_ENABLE )
       printf("  CL_DEVICE_QUEUE_PROPERTIES:\t\t%s\n", "CL_QUEUE_PROFILING_ENABLE");

    // CL_DEVICE_IMAGE_SUPPORT
    cl_bool image_support;
    clGetDeviceInfo(device, CL_DEVICE_IMAGE_SUPPORT, sizeof(image_support), &image_support, NULL);
   printf("  CL_DEVICE_IMAGE_SUPPORT:\t\t%u\n", image_support);

    // CL_DEVICE_MAX_READ_IMAGE_ARGS
    cl_uint max_read_image_args;
    clGetDeviceInfo(device, CL_DEVICE_MAX_READ_IMAGE_ARGS, sizeof(max_read_image_args), &max_read_image_args, NULL);
   printf("  CL_DEVICE_MAX_READ_IMAGE_ARGS:\t%u\n", max_read_image_args);

    // CL_DEVICE_MAX_WRITE_IMAGE_ARGS
    cl_uint max_write_image_args;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WRITE_IMAGE_ARGS, sizeof(max_write_image_args), &max_write_image_args, NULL);
   printf("  CL_DEVICE_MAX_WRITE_IMAGE_ARGS:\t%u\n", max_write_image_args);
    
    // CL_DEVICE_IMAGE2D_MAX_WIDTH, CL_DEVICE_IMAGE2D_MAX_HEIGHT, CL_DEVICE_IMAGE3D_MAX_WIDTH, CL_DEVICE_IMAGE3D_MAX_HEIGHT, CL_DEVICE_IMAGE3D_MAX_DEPTH
    size_t szMaxDims[5];
   printf("\n  CL_DEVICE_IMAGE <dim>"); 
    clGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_WIDTH, sizeof(size_t), &szMaxDims[0], NULL);
   printf("\t\t\t2D_MAX_WIDTH\t %u\n", szMaxDims[0]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE2D_MAX_HEIGHT, sizeof(size_t), &szMaxDims[1], NULL);
   printf("\t\t\t\t\t2D_MAX_HEIGHT\t %u\n", szMaxDims[1]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_WIDTH, sizeof(size_t), &szMaxDims[2], NULL);
   printf("\t\t\t\t\t3D_MAX_WIDTH\t %u\n", szMaxDims[2]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_HEIGHT, sizeof(size_t), &szMaxDims[3], NULL);
   printf("\t\t\t\t\t3D_MAX_HEIGHT\t %u\n", szMaxDims[3]);
    clGetDeviceInfo(device, CL_DEVICE_IMAGE3D_MAX_DEPTH, sizeof(size_t), &szMaxDims[4], NULL);
   printf("\t\t\t\t\t3D_MAX_DEPTH\t %u\n", szMaxDims[4]);
    


 

    // CL_DEVICE_PREFERRED_VECTOR_WIDTH_<type>
   printf("  CL_DEVICE_PREFERRED_VECTOR_WIDTH_<t>\t"); 
    cl_uint vec_width [6];
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, sizeof(cl_uint), &vec_width[0], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, sizeof(cl_uint), &vec_width[1], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, sizeof(cl_uint), &vec_width[2], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, sizeof(cl_uint), &vec_width[3], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, sizeof(cl_uint), &vec_width[4], NULL);
    clGetDeviceInfo(device, CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, sizeof(cl_uint), &vec_width[5], NULL);
   printf("CHAR %u, SHORT %u, INT %u, FLOAT %u, DOUBLE %u\n\n\n", 
           vec_width[0], vec_width[1], vec_width[2], vec_width[3], vec_width[4]); 
}


/*
 * Converts the contents of a file into a string
 */
std::string
convertToString(const char *filename)
{
  size_t size;
  char*  str;
  std::string s;

  std::fstream f(filename, (std::fstream::in | std::fstream::binary));

  if(f.is_open())
  {
    size_t fileSize;
    f.seekg(0, std::fstream::end);
    size = fileSize = (size_t)f.tellg();
    f.seekg(0, std::fstream::beg);

    str = new char[size+1];
    if(!str)
    {
      f.close();
      std::cout << "Memory allocation failed";
      return NULL;
    }

    f.read(str, fileSize);
    f.close();
    str[size] = '\0';
  
    s = str;
    delete[] str;
    return s;
  }
  else
  {
    std::cout << "\nFile containg the kernel code(\"" << filename << "\") not found. Please copy the required file in the folder containg the executable.\n";
    exit(1);
  }
  return NULL;
} 

////////////////////////////////////////////////////////////////////////////////
//! Entry point for Cuda functionality on host side
//! @param  
////////////////////////////////////////////////////////////////////////////////
extern "C" int
antColonyGPULauncher(const unsigned int number_of_ants, const unsigned int number_of_cities, unsigned int * distance, float * pheromone, float * choice_info, unsigned int * ants, float alpha, float beta, float evaporation_rate, unsigned int iterations, float elitist_weight, unsigned int elitist_ants, unsigned int best_so_far_frequency, float a, AcoType acotype)
{
  MPI_Init(NULL,NULL);
  		  	
  cl_int status = 0;
  
  float nodeBestTime = 0.0f;
  float globalBestTime = 0.0f;
  
  //////////////////////////////////////////////////////////////////// 
  // STEP 1 Getting Platform.
  //////////////////////////////////////////////////////////////////// 
  /*
   * Have a look at the available platforms and pick either
   * the AMD one if available or a reasonable default.
   */
   
  cl_uint numPlatforms;
  cl_platform_id platform = NULL;
  status = clGetPlatformIDs(0, NULL, &numPlatforms);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: Getting Platforms. (clGetPlatformsIDs)\n";
    return ACO_FAILURE;
  }
 
  //printf("=== %d OpenCL platform(s) found: ===\n", numPlatforms); 
  if(numPlatforms > 0)
  {
    cl_platform_id* platforms = new cl_platform_id[numPlatforms];
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);
    if(status != CL_SUCCESS)
    {
      std::cout << "Error: Getting Platform Ids. (clGetPlatformsIDs)\n";
      return ACO_FAILURE;
    }
    for(unsigned int i=0; i < numPlatforms; ++i)
    {
      char pbuff[100];
      status = clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, sizeof(pbuff), pbuff, NULL);
      if(status != CL_SUCCESS)
      {
        std::cout << "Error: Getting Platform Info.(clGetPlatformInfo)\n";
        return ACO_FAILURE;
      }
      //printf("  VENDOR = %s\n", pbuff);
      status = clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, sizeof(pbuff), pbuff, NULL);
      if(status != CL_SUCCESS)
      {
        std::cout << "Error: Getting Platform Info.(clGetPlatformInfo)\n";
        return ACO_FAILURE;
      } 
      //printf("  NAME = %s\n", pbuff);
      platform = platforms[i];
      if(!strcmp(pbuff, "Advanced Micro Devices, Inc."))
      {
        break;
      }
    }
    delete platforms;
  }

  if(NULL == platform)
  {
    std::cout << "NULL platform found so Exiting Application." << std::endl;
    return ACO_FAILURE;
  }

  //////////////////////////////////////////////////////////////////// 
  // STEP 2 Creating context using the platform selected
  //  Context created from type includes all available
  //  devices of the specified type from the selected platform 
  //////////////////////////////////////////////////////////////////// 
  /*
   * If we could find our platform, use it. Otherwise use just available platform.
   */
  cl_context_properties cps[3] = {CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 0};
  //Get devices number
  cl_uint num_devices;
  status = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);
  if (status != CL_SUCCESS) 
  {
    std::cout << "Error: Failed to create a device group:" << std::endl;
    return ACO_FAILURE;
  }
  cl_device_id * devices = (cl_device_id *)malloc(num_devices*sizeof(double));
  clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, num_devices, devices, NULL);
  
  #pragma omp parallel num_threads(num_devices) private(status, iterations)
  {
  int tid = omp_get_thread_num();
  cl_context context = clCreateContext(cps, 1, &devices[tid], NULL, NULL, &status);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Creating Context. (clCreateContext)\n";
    std::cout << "status " << status << std::endl;
    //return ACO_FAILURE; 
  }

  //////////////////////////////////////////////////////////////////// 
  // STEP 4 Creating command queue for a single device
  //    Each device in the context can have a 
  //    dedicated commandqueue object for itself
  ////////////////////////////////////////////////////////////////////

  cl_command_queue commandQueue; 

  //clPrintDevInfo(devices[tid]);
  commandQueue = clCreateCommandQueue(context, devices[tid], 0, &status);
  if(status != CL_SUCCESS) 
  {    
    std::cout << "Creating Command Queue. (clCreatecommandQueue)\n";
    //return ACO_FAILURE;
  }

  /////////////////////////////////////////////////////////////////
  // STEP 5 Creating cl_buffer objects from host buffer
  //      These buffer objects can be passed to the kernel
  //      as kernel arguments
  /////////////////////////////////////////////////////////////////
  const unsigned int mem_size_distance = sizeof(cl_uint) * number_of_cities * number_of_cities;
  const unsigned int mem_size_pheromone = sizeof(cl_float) * number_of_cities * number_of_cities;
  const unsigned int mem_size_tour = sizeof(cl_uint) * number_of_ants * (number_of_cities+1);
  const unsigned int mem_size_lenghtList = sizeof(cl_uint) * number_of_ants;
  const unsigned int mem_size_choiceinfo = sizeof(cl_float)*number_of_cities * number_of_cities;
  const unsigned int mem_size_seed = sizeof(cl_uint)*number_of_cities * number_of_cities;
  unsigned int totalBytesAllocated = 0; //Bytes allocated on the GPU
  
  /* The memory buffer that is used as input/output for OpenCL kernel */
  cl_mem   d_distance;
  cl_mem   d_choiceInfo;
  cl_mem   d_pheromone;
  cl_mem   d_tour;
  cl_mem   d_lenghtList;
  cl_mem   d_bestTour;
  cl_mem   d_seed;

  //////////////////////////////////////////////////////////////////
  ///////////////////Initialize Distances///////////////////////////
  /////////////////////////////////////////////////////////////////
  d_distance = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, mem_size_distance, distance, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_distance)\n";
    //return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_distance;
  
  ////////////////////////////////////////////////////////////////////////  
  ///////////////Initialize Choice Information////////////////////////////
  ////////////////////////////////////////////////////////////////////////
  d_choiceInfo = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_choiceinfo, NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_choiceInfo)\n";
    //return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_choiceinfo;
  
  ///////////////////////////////////////////////////////////////////////////
  ///////////////Initialize Pheromone Information////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  d_pheromone = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, mem_size_pheromone, pheromone, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_pheromone)\n";
    //return ACO_FAILURE;
  }
  
  totalBytesAllocated+=mem_size_pheromone;
  
  ///////////////////////////////////////////////////////////////////////////
  /////////////////Initialize Tour information//////////////////////////////
  ///////////////////////////////////////////////////////////////////////////
  d_tour = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_tour, NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_tour)\n";
    //return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_tour;
  
  ///////////////////////////////////////////////////////////////////////////
  /////////////////Initialize Tour Lenght information////////////////////////
  ///////////////////////////////////////////////////////////////////////////  
  // allocate device memory for the lenght list
  d_lenghtList = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_lenghtList,  NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_lenghtList)\n";
    //return ACO_FAILURE;
  }
  
  totalBytesAllocated+=mem_size_lenghtList;
  
  ///////////////////////////////////////////////////////////////////////////
  /////////////////////////Best tour information ////////////////////////////
  ///////////////////////////////////////////////////////////////////////////  
  d_bestTour = clCreateBuffer(context, CL_MEM_READ_WRITE, mem_size_tour, NULL, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_bestTour)\n";
    //return ACO_FAILURE;
  }
  
  totalBytesAllocated+=mem_size_tour;
  
  ///////////////////////////////////////////////////////////////////////////
  ///////////////Initialize for random numbers on the GPU////////////////////
  ///////////////////////////////////////////////////////////////////////////
  cl_uint *h_seed = (cl_uint *) malloc (mem_size_seed);

  /* initialize random seed: */
  srand ( time(NULL) );
  //uint aux = rand()%UINT_MAX;

  for (uint i=0; i<number_of_cities*number_of_cities;i++)
    h_seed[i]=rand()%UINT_MAX;//aux;
    
  d_seed = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, mem_size_seed, h_seed, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clCreateBuffer (d_seed)\n";
    //return ACO_FAILURE;
  }

  totalBytesAllocated+=mem_size_seed;
  
  /////////////////////////////////////////////////////////////////
  // STEP 6. Building Program
  //    6.1 Load CL file, using basic file i/o
  //    6.2 Build CL program object
  /////////////////////////////////////////////////////////////////
  const char * filename  = "simpleAntGPU_kernel.cl";
  std::string  sourceStr = convertToString(filename);
  const char * source    = sourceStr.c_str();
  size_t sourceSize[]    = {strlen(source)};

  cl_program program = clCreateProgramWithSource(context, 1, &source, sourceSize, &status);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Loading Binary into cl_program (clCreateProgramWithBinary)\n";
    //return ACO_FAILURE;
  }

  std::string BuildOptions = "-cl-fast-relaxed-math";
  BuildOptions += " -cl-mad-enable";
  BuildOptions += " -cl-no-signed-zeros";
  
  // create a cl program executable for all the devices specified
  status = clBuildProgram(program, 1, &devices[tid], BuildOptions.c_str(), NULL, NULL);
  if(status != CL_SUCCESS) 
  {
    char buffer[10240];
    clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, NULL);
    std::cout << "CL Compilation failed" << std::endl << buffer << std::endl;
    //return ACO_FAILURE; 
  }
  
  //////////////////////////////////////////////////////////////////// 
  // STEP 7 Create kernels and set appropriate arguments
  //////////////////////////////////////////////////////////////////// 
  
  //////////////////////////////////////////////////////////////////////////
  //////////////          Kernel Choiceinfokernel          /////////////////
  //////////////////////////////////////////////////////////////////////////
  cl_kernel initChoiceInfo = clCreateKernel(program, "init_choiceinfo_kernel", &status);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Creating Kernel init_choiceinfo_kernel from program. (clCreateKernel)\n";
    //return ACO_FAILURE;
  }

  status = clSetKernelArg(initChoiceInfo, 0, sizeof(cl_mem), (void *)&d_choiceInfo);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Setting initChoiceInfo aernel argument. (d_choiceInfo)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 1, sizeof(cl_mem), (void *)&d_pheromone); 
  if(status != CL_SUCCESS) 
  { 
     std::cout << "Error: Setting initChoiceInfo kernel argument. (d_pheromone)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 2, sizeof(cl_mem), (void *)&d_distance);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (d_distance)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 3, sizeof(cl_uint), (void *)&number_of_cities);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (number_of_cities)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 4, sizeof(cl_float), (void *)&alpha);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (alpha)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 5, sizeof(cl_float), (void *)&beta);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (beta)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(initChoiceInfo, 6, sizeof(cl_float), (void *)&evaporation_rate);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting initChoiceInfo kernel argument. (evaporation_rate)\n";
    //return ACO_FAILURE;
  }
  
  int workItemSize, workGroupSize;
  size_t globalThreads0[1], localThreads0[1];
  setting_kernel_parameters(&workItemSize, &workGroupSize, number_of_cities*number_of_cities, BLOCK_SIZE_CHOICE);
  localThreads0[0] = workItemSize;  
  globalThreads0[0]= workGroupSize*localThreads0[0];
  
  //////////////////////////////////////////////////////////////////////////
  //////////////              Kernel Next Tour             /////////////////
  //////////////////////////////////////////////////////////////////////////
  cl_kernel nextTour_kernel = clCreateKernel(program, "nextTour_kernel", &status);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Creating Kernel nextTour_kernel from program. (clCreateKernel)\n";
    //return ACO_FAILURE;
  }
  
  cl_uint iterNextTour;
  status = clSetKernelArg(nextTour_kernel, 0, sizeof(cl_uint), (void *)&number_of_cities);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (number_of_cities)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 1, sizeof(cl_uint), (void *)&number_of_ants);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (number_of_ants)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 2, sizeof(cl_mem), (void *)&d_distance);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (d_distance)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 3, sizeof(cl_mem), (void *)&d_tour);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Setting nextTour_kernel aernel argument. (d_tour)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 4, sizeof(cl_mem), (void *)&d_lenghtList); 
  if(status != CL_SUCCESS) 
  { 
     std::cout << "Error: Setting nextTour_kernel kernel argument. (d_lenghtList)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 5, sizeof(cl_mem), (void *)&d_choiceInfo);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (d_choiceInfo)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(nextTour_kernel, 6, sizeof(cl_mem), (void *)&d_seed);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (d_seed)\n";
    //return ACO_FAILURE;
  }

  // 1 work-group per ant. And as many threads as cities or fixed number of them 
  if (BLOCK_SIZE_NEXT_TOUR > number_of_cities) {
     std::cout << "The work-group size macro should be smalls than the number of cities\n";
     //return CL_SUCCESS;    
  }
  size_t globalThreads1[1], localThreads1[1];
  localThreads1[0] = BLOCK_SIZE_NEXT_TOUR;  
  globalThreads1[0]= number_of_ants*localThreads1[0];  
  iterNextTour=ceil((float)number_of_cities/BLOCK_SIZE_NEXT_TOUR);
  
  status = clSetKernelArg(nextTour_kernel, 7, sizeof(cl_uint), (void *)&iterNextTour);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting nextTour_kernel kernel argument. (iterNextTour)\n";
    //return ACO_FAILURE;
  }
  
  //////////////////////////////////////////////////////////////////////////
  ////////////        Kernel Pheromone Update kernel            ////////////
  //////////////////////////////////////////////////////////////////////////
  cl_kernel pheromone_update = clCreateKernel(program, "pheromone_update", &status);
  if(status != CL_SUCCESS)
  {  
    std::cout << "Error: Creating Kernel pheromone_update from program. (clCreateKernel)\n";
    //return ACO_FAILURE;
  }
  
  cl_uint iterationsPheromones6;
  status = clSetKernelArg(pheromone_update, 0, sizeof(cl_mem), (void *)&d_pheromone);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (d_pheromone)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 1, sizeof(cl_uint), (void *)&number_of_cities);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (number_of_cities)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 2, sizeof(cl_mem), (void *)&d_tour);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (d_tour)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 3, sizeof(cl_uint), (void *)&number_of_ants);
  if(status != CL_SUCCESS) 
  {  
    std::cout << "Error: Setting pheromone_update aernel argument. (number_of_ants)\n";
    //return ACO_FAILURE;
  }
  status = clSetKernelArg(pheromone_update, 4, sizeof(cl_mem), (void *)&d_lenghtList); 
  if(status != CL_SUCCESS) 
  { 
     std::cout << "Error: Setting pheromone_update kernel argument. (d_lenghtList)\n";
    //return ACO_FAILURE;
  }

  size_t globalThreads2[1], localThreads2[1];
  localThreads2[0] = (number_of_cities<BLOCK_SIZE_ATOMIC_DEVICE)?number_of_cities:BLOCK_SIZE_ATOMIC_DEVICE;
  globalThreads2[0]= number_of_ants*localThreads2[0];  
  iterationsPheromones6=ceil((float)number_of_cities/BLOCK_SIZE_ATOMIC_DEVICE);
  
  status = clSetKernelArg(pheromone_update, 5, sizeof(cl_uint), (void *)&iterationsPheromones6);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Setting pheromone_update kernel argument. (iterationsPheromones6)\n";
    //return ACO_FAILURE;
  }
  
  
  //////////////////////////////////////////////////////////////////// 
  // STEP 9 Execute main loop
  ////////////////////////////////////////////////////////////////////  
  // the lenght of the best tour so far
  unsigned int bestLenght=UINT_MAX;    
  
  // Lenghts of all tour taken by each Ant      
  cl_uint *h_lenghtList = (cl_uint *) malloc (mem_size_lenghtList);
  
  // the best tour so far
  cl_uint *h_bestTour= (cl_uint *)malloc (mem_size_tour);
 
  //std::cout << "Number of iterations is " << iterations << "\n";
  CPerfCounter timer;
  float elapsedTime;

  cl_event event;
  float percent=1.0f;
  
  for (uint phase=0; phase<2; phase++) {
     
  if (phase==0)
    iterations=250;
  else {
    iterations= (int)ceil(100*percent)*10;
    //std::cout << "percent=" << percent << "\t iterations=" << iterations << std::endl;
  }
     
  timer.Reset();
  timer.Start();
  for (uint i=0; i<iterations; i++) {   
    status = clEnqueueNDRangeKernel(commandQueue, initChoiceInfo, 1, NULL, globalThreads0, localThreads0, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
	  std::cout << "Error: status " << status << std::endl;
      std::cout << "Error: Enqueueing initChoiceInfo kernel onto command queue. (clEnqueueNDRangeKernel)\n";
      //return ACO_FAILURE;
    }
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      //return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. (clReleaseEvent)\n";
      //return ACO_FAILURE;
	}
    status = clEnqueueNDRangeKernel(commandQueue, nextTour_kernel, 1, NULL, globalThreads1, localThreads1, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Enqueueing nextTour_kernel kernel onto command queue.(clEnqueueNDRangeKernel)\n";
      //return ACO_FAILURE;
    }
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      //return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. (clReleaseEvent)\n";
      //return ACO_FAILURE;
    }
    
    status = clEnqueueReadBuffer(commandQueue, d_lenghtList, CL_TRUE, 0, mem_size_lenghtList, h_lenghtList, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: clEnqueueReadBuffer failed. (clEnqueueReadBuffer)\n";
      //return ACO_FAILURE;
    }
    // Wait for the read buffer to finish execution
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      //return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. (clReleaseEvent)\n";
      //return ACO_FAILURE;
    }         

    unsigned int bestIt = getLenghtIterationBestTour(h_lenghtList, number_of_ants, d_tour, d_bestTour,&bestLenght, number_of_cities, commandQueue);     
 
    status = clEnqueueNDRangeKernel(commandQueue, pheromone_update, 1, NULL, globalThreads2, localThreads2, 0, NULL, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Enqueueing pheromone kernel onto command queue. (clEnqueueNDRangeKernel)\n";
      //return ACO_FAILURE;
    }
    status = clWaitForEvents(1, &event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
      //return ACO_FAILURE;
    }
    status = clReleaseEvent(event);
    if(status != CL_SUCCESS) 
    { 
      std::cout << "Error: Release event object. clReleaseEvent)\n";
      //return ACO_FAILURE;
    } 
    }
  
    if(phase==0) {
	  timer.Stop();
      elapsedTime=timer.GetElapsedTime()*1000;
      elapsedTime=1/elapsedTime;
  
	  #pragma omp critical
	  {
		if (elapsedTime > nodeBestTime)
		  nodeBestTime=elapsedTime;
	  }
	  
	  #pragma omp barrier
	  
	  #pragma omp single
	  {
	    MPI_Allreduce(&nodeBestTime, &globalBestTime, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD);
      }
      
	  percent=elapsedTime/globalBestTime;
	  
	  timer.Reset();
      timer.Start();
      
	}
  
  }
  
  timer.Stop();
  elapsedTime = timer.GetElapsedTime()*1000;   
  char device_string[1024];
  clGetDeviceInfo(devices[tid], CL_DEVICE_NAME, sizeof(device_string), &device_string, NULL);
  printf("GPU %s ->\t\tPercent: %f\tResult: %d\tTotal execution time: %f ms\tIterations: %d\n", device_string, bestLenght, percent, elapsedTime, iterations);
  
 /* std::cout << "Best Tour Lenght" << std::endl;
  std::cout << bestLenght << "\t";
  std::cout << std::endl;  

  std::cout << "Best Ordering" << std::endl;
  status = clEnqueueReadBuffer(commandQueue, d_bestTour, CL_TRUE, 0, (number_of_cities+1)*sizeof(cl_uint), h_bestTour, 0, NULL, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clEnqueueReadBuffer failed. (clEnqueueReadBuffer)\n";
    //return ACO_FAILURE;
  }
  // Wait for the read buffer to finish execution
  status = clWaitForEvents(1, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
    //return ACO_FAILURE;
  }
  status = clReleaseEvent(event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Release event object. (clReleaseEvent)\n";
    //return ACO_FAILURE;
  }      
  printTour(h_bestTour, number_of_cities);
  std::cout << std::endl;*/

  #ifdef TIMING
    FILE *f1,*f2,*f3,*f4,*f5;
    f1 = fopen("out/choice","a");
    f2 = fopen("out/tour","a");
    f3 = fopen("out/pher","a");
    f4 = fopen("out/total","a");
    f5 = fopen("out/result","a");
    fprintf(f1,"%f ",choiceTime/iterations);
    fprintf(f2,"%f ",nextTime/iterations);
    fprintf(f3,"%f ",pherTime/iterations);
    fprintf(f4,"%f ",accu/iterations);
    fprintf(f5,"%d ",bestLenght);
    if (number_of_ants==2392) {
      fprintf(f1,"\n");
      fprintf(f2,"\n");
      fprintf(f3,"\n");
      fprintf(f4,"\n");
      fprintf(f5,"\n");
    }
  #endif
  
  free (h_bestTour);  
  free (h_lenghtList);
  
  status = clReleaseCommandQueue(commandQueue);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleasecommandQueue\n";
    //return ACO_FAILURE;
  }
  
  status = clReleaseKernel(initChoiceInfo);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseKernel initChoiceInfo\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseKernel(nextTour_kernel);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseKernel nextTour_kernel\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseKernel(pheromone_update);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseKernel pheromone_update\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseProgram(program);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseProgram\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_distance);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_distance)\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_choiceInfo);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_choiceInfo)\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_pheromone);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_pheromone)\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_tour);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_tour)\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_lenghtList);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_lenghtList)\n";
    //return ACO_FAILURE; 
  }
  status = clReleaseMemObject(d_bestTour);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseMemObject (d_bestTour)\n";
    //return ACO_FAILURE; 
  }
    
  status = clReleaseContext(context);
  if(status != CL_SUCCESS)
  {
    std::cout << "Error: In clReleaseContext\n";
    //return ACO_FAILURE;
  }
  }
  
  MPI_Finalize();
  
  return ACO_SUCCESS;
}


void setting_kernel_parameters(int * threads, int * blocks, unsigned int reference, unsigned int block_size) {
  
  *threads= (reference <= block_size)?reference:block_size;
  *blocks = (reference%*threads==0)?(reference / *threads):(reference / *threads)+1; 
  
}



void printTour(unsigned int * best, unsigned int number_of_cities) {

  for (uint i=0; i<=number_of_cities;i++)
  std::cout << best[i] << ", ";
    
  std::cout << std::endl;    
}

///
///It returns the best in the current iteration, and controls the best so far, doing a copy in device memory for the best tour 
///
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, cl_mem d_tour, cl_mem d_best, unsigned int * bestLenght, unsigned int number_of_cities, cl_command_queue commandQueue) 
{
  
  unsigned int bestIt=lenght[0];
  unsigned int antBest = 0;
  
  for (uint i=1;i<number_of_ants;i++) {
  if (lenght[i]<bestIt) {
    bestIt = lenght[i];
    antBest =i;
  }
  }
  
  
  /* int size = (number_of_cities+1)*number_of_ants*sizeof(uint);

  unsigned int * partialbest = (unsigned int * ) malloc (size);
  cutilSafeCall(cudaMemcpy(partialbest, d_tour, size, cudaMemcpyDeviceToHost) );
  
  std::cout << "Todas los tours para todas las ants \n";
  for (int j = 0; j< number_of_ants; j++) {
  std::cout << "Para la ant " << j << std::endl;
  for (int i=0; i<=number_of_cities; i++) {
    std::cout << partialbest[(j*(number_of_cities+1))+i] <<", ";
  }
  std::cout << std::endl;
  }
  */

  cl_int status = 0;
  cl_event event;
  if (bestIt < *bestLenght) {
  //std::cout << "La mejor ant es " << antBest << std::endl;
  *bestLenght = bestIt;  

  status = clEnqueueCopyBuffer(commandQueue, d_tour, d_best, antBest*(number_of_cities+1)*sizeof(cl_uint), 0, (number_of_cities+1)*sizeof(cl_uint), 0, NULL, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: clEnqueueCopyBuffer failed. (clEnqueueReadBuffer)\n";
    return ACO_FAILURE;
  }
  // Wait for the copy buffer to finish execution
  status = clWaitForEvents(1, &event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Waiting for read buffer call to finish. (clWaitForEvents)\n";
    return ACO_FAILURE;
  }
  status = clReleaseEvent(event);
  if(status != CL_SUCCESS) 
  { 
    std::cout << "Error: Release event object. (clReleaseEvent)\n";
    return ACO_FAILURE;
  }        
  }      

//fin
  return bestIt;      
}
