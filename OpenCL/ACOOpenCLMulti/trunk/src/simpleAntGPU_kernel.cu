#ifndef _ANT_COLONY_KERNEL_H_
#define _ANT_COLONY_KERNEL_H_


#include <curand_kernel.h>
#include "src/cuPrintf.cu"

#define TILE_SIZE_X 8
#define TILE_SIZE_Y 8
#define TILE_DIM 32

///OJO CAMBIA EL LOGARITMO EN BASE 2
#define BLOCK_SIZE_NEXT_TOUR 1024
#define LOG_BLOCK_SIZE_NEXT_TOUR 10

#define MACROBITS 0x00000001

 

__global__ void setup_kernel(curandState *state, unsigned int m) {
  
  int id = threadIdx.x + blockIdx.x * blockDim.x; /* Each thread gets same seed, a different sequence number,
						     no offset */ 
  if (id<m)
    curand_init(1234*blockIdx.x, id, 0, &state[id]);
}



///This kernel creates the choice info matrix and also evaporates the pheromone
/// It creates a single thread per cell 
__global__ void init_choiceinfo_kernel (float * d_choiceInfo, float * d_pheromone, unsigned int * d_distance, unsigned int number_of_cities, const float alpha, const float beta, const float evaporation_rate) {
  
  unsigned int threadIndex = (blockIdx.y*blockDim.y*number_of_cities)+(blockDim.x*blockIdx.x)+threadIdx.x;
 			
  if (threadIndex<number_of_cities*number_of_cities){
    float pheromone;
    pheromone = d_pheromone[threadIndex];
    float heuristic = 1.0 / ((float) d_distance[threadIndex] + 0.1);   			
    d_choiceInfo[threadIndex]=__powf(pheromone, alpha)*__powf(heuristic, beta);				
    //Evaporation stage	
    pheromone= pheromone*(1.0-evaporation_rate);			
    d_pheromone[threadIndex] = pheromone;
  }
}
 

__device__ inline unsigned int reduceMax(float *g_idata, unsigned int index, unsigned int n) {
  /*    
	FUNCTION:       This fuction obtains the maximum of the array and the index of that maximum 
	INPUT:          pointer to the probabilistic information and the relative index
	OUTPUT:         Relative index of the maximum probability
	(SIDE)EFFECTS:  The maximum probability is in g_idata[0]
	ORIGIN:         Partially based on NVIDIA_CUDA_SDK
  */
  
  __shared__ unsigned int index_sh [BLOCK_SIZE_NEXT_TOUR];
  
  index_sh[threadIdx.x]= index;
  g_idata[threadIdx.x] = (index>=n)?0.0:g_idata[threadIdx.x];

  __syncthreads();
  
  // do reduction in shared mem
  for(unsigned int s=blockDim.x/2; s>0; s>>=1) {
    if (threadIdx.x < s){
      if (g_idata[threadIdx.x]<g_idata[threadIdx.x + s]) {
	g_idata[threadIdx.x]=g_idata[threadIdx.x + s];
	index_sh[threadIdx.x]=index_sh[threadIdx.x + s];
      }
    }
    __syncthreads();
  }
  
  // write result for this block to global mem
  return index_sh[0];
}



__global__ void nextTour_kernel(unsigned  int number_of_cities, const unsigned int number_of_ants, unsigned int * d_distance, unsigned int * d_tour, unsigned int * d_lenghtList, float * choice_info, curandState *state, const unsigned int iterathreads){
   
  __shared__ float probabilities_sh[BLOCK_SIZE_NEXT_TOUR];		
  __shared__ unsigned int tourLenghtCum;					
  __shared__ unsigned int current_city_sh;
  unsigned int current_city;
  unsigned int btourindex = blockIdx.x*(number_of_cities+1);
  unsigned int tabul=0;
  float random; 
  curandState localState = state[(blockIdx.x*number_of_cities)+threadIdx.x];

  //The tabulist is initializes as 1 
  // 1 is not visited yet
  // 0 it has been visited 
  // itera is the N/BlockDim with the padding 
  // The padding cities are marked as visited
  //Set all bits to 1
  tabul=~(tabul & 0); 
		
  if (threadIdx.x==0) {
    random = curand_uniform(&localState);
    //cuPrintf ("The generated random number is %f for the ant %d\n", random, blockIdx.x);
    current_city_sh = (unsigned int)number_of_cities*random;
  }

  __syncthreads(); 
  current_city = current_city_sh;
  unsigned int beginning_city = current_city;
  // cuPrintf ("The current_city for the ant %d is %d\n", blockIdx.x, current_city);
  
  //Division is the bit that represents that city
  unsigned int bitTabul = (unsigned int) beginning_city>>LOG_BLOCK_SIZE_NEXT_TOUR; 	
  //Modulo is the thread that manages that city 
  unsigned int threadTabul = (unsigned int) beginning_city & (BLOCK_SIZE_NEXT_TOUR-1);		
  
  if (threadIdx.x == threadTabul) {
    //the bit that represents the city in this thread is marked as zero 
    //Pongo el uno en el bit adecuado y hago la negacion de todo 
    unsigned int aux = (unsigned int)MACROBITS<<bitTabul; 			
    tabul = tabul & (~aux);
    tourLenghtCum=0;
    d_tour[btourindex] = beginning_city;
  }
  __syncthreads();
  
  //Repeat until tabu list is full 
  for (int tourIndex=1; tourIndex<number_of_cities;tourIndex++){

    int indexChoiceInfo = current_city*number_of_cities; 
    float current_prob = 0.0;
    int indexaux;
    
    for (int i=0; i<iterathreads-1; i++){
   
      indexaux = threadIdx.x+(i*blockDim.x);
      unsigned int auxtabul = tabul>>i; //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int) MACROBITS & auxtabul; //auxbits debe ser 0 o 1. 
      float choice = choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float) choice*auxtabul*random;  	  
	
      __syncthreads();

      unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);			       
      float prob_local = probabilities_sh[0];
      //cuPrintf ("Para la ant %d la prob que se obtiene de reduccion es %f\n", blockIdx.x, prob_local);
      if (prob_local>=current_prob) {
  	current_prob = prob_local;
  	current_city = aux_city;
      }
    }
    
    indexaux = threadIdx.x+((iterathreads-1)*blockDim.x);
    if (indexaux < number_of_cities){
      unsigned int auxtabul = tabul >>(iterathreads-1); //Desplazo a la derecha el posible 0
      auxtabul = (unsigned int)MACROBITS & auxtabul; //auxbits debe ser 0 o 1. 		 
      float choice= choice_info[indexChoiceInfo+(indexaux)];
      random = curand_uniform(&localState);
      probabilities_sh[threadIdx.x]= (float)choice*auxtabul*random;  	  	  
    }	
    __syncthreads();
    unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities);			      
    float prob_local = probabilities_sh[0];
    if (prob_local>=current_prob) {
      current_prob = prob_local;
      current_city = aux_city;
    }
    __syncthreads();
    
    //Division is the bit that represents that city
    bitTabul = current_city>>LOG_BLOCK_SIZE_NEXT_TOUR; 	
    //Modulo is the thread that manages that city 
    threadTabul = current_city & (BLOCK_SIZE_NEXT_TOUR-1);		
    
    if (threadIdx.x==threadTabul) {
      //the bit that represents the city in this thread is marked as zero 
      unsigned int aux = (unsigned int)MACROBITS<<bitTabul;			
      tabul = tabul & (~aux);
      d_tour[btourindex+tourIndex]=current_city; //Adding the city to the tour	
      tourLenghtCum+= d_distance[indexChoiceInfo+current_city];					
    }
    __syncthreads();		
    
  }//End of FOR  
  
  //Complete the tour getting the beginning city		
  if (threadIdx.x==0) {
    d_tour[btourindex+number_of_cities]=beginning_city; 
    tourLenghtCum+= d_distance[current_city*number_of_cities+beginning_city];					 
    d_lenghtList[blockIdx.x]=tourLenghtCum; // Keep the lenght of the current tour
  }
}	

 
__global__ void pheromone_update_with_atomic (float * d_pheromone, const unsigned int number_of_cities, unsigned int * d_tour, const unsigned int number_of_ants, unsigned int * d_lenghtList, int ite){

  const int xIndex = blockIdx.x* (number_of_cities+1) + threadIdx.x;
  int index,indexThread; 
  for (int i=0; i<ite; i++) {
    indexThread = i*blockDim.x;             
    index =xIndex+(indexThread);
    if ((threadIdx.x+indexThread)<number_of_cities){
      unsigned int coordx=d_tour[index];
      unsigned int coordy=d_tour[index+1];
      unsigned int pos = (coordy*number_of_cities)+coordx;
      unsigned int posim = (coordx*number_of_cities)+coordy;                          
      float pheromone = 0.0;
      pheromone=(float)1.0*(1.0/d_lenghtList[blockIdx.x]);
      //cuPrintf ("El valor de pheromone es %f en la pos %d, %d\n", pheromone, pos, posim);
      
	//Incluir Operaciones atomica
	d_pheromone[pos]+=pheromone;
      d_pheromone[posim]+=pheromone;
    }
  }  
}

#endif // #ifndef _ANT_COLONY_KERNEL_H_


