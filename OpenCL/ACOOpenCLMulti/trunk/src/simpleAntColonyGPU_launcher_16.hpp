#ifndef SIMPLE_ANT_COLONY_GPU_LAUNCHER_HPP_
#define SIMPLE_ANT_COLONY_GPU_LAUNCHER_HPP_


// includes, system
#include <cstdlib>
#include <string>
#include <cmath>
#include <iostream>
#include <climits>
#include <fstream>

// includes, project
#include <CL/cl.hpp>
#include "include/libaco/util.h"
#include "include/libaco/Timer.h"

// GLOBALS
#define ACO_SUCCESS 0
#define ACO_FAILURE 1 
#define BLOCK_SIZE_CHOICE 16
#define BLOCK_SIZE_NEXT_TOUR 16
#define BLOCK_SIZE_ATOMIC_DEVICE 16

cl_command_queue commandQueue;

/* This program uses only one kernel and this serves as a handle to it */
cl_kernel  initChoiceInfo;
cl_kernel  nextTour_kernel;
cl_kernel  pheromone_update;

// FUNCTION DECLARATIONS 
void setting_kernel_parameters(int * threads, int * blocks, unsigned int reference, unsigned int block_size);
void printTour(unsigned int * best, unsigned int number_of_cities);
unsigned int getLenghtIterationBestTour (unsigned int * lenght, unsigned int number_of_ants, cl_mem d_tour, cl_mem d_best, unsigned int * bestLenght, unsigned int number_of_cities);

#endif
