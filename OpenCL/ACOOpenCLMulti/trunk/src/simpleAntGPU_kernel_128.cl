#ifndef _ANT_COLONY_KERNEL_H_
#define _ANT_COLONY_KERNEL_H_

#define TILE_SIZE_X 8
#define TILE_SIZE_Y 8
#define TILE_DIM 32

///OJO CAMBIA EL LOGARITMO EN BASE 2
#define BLOCK_SIZE_NEXT_TOUR 128
#define LOG_BLOCK_SIZE_NEXT_TOUR 7

#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836

#define MACROBITS 0x00000001

#pragma OPENCL EXTENSION cl_amd_printf : enable


///This kernel creates the choice info matrix and also evaporates the pheromone
/// It creates a single thread per cell 
__kernel void init_choiceinfo_kernel (__global float * d_choiceInfo, __global float * d_pheromone, __global unsigned int * d_distance, unsigned int number_of_cities, const float alpha, const float beta, const float evaporation_rate) {
  //unsigned int threadIndex = (get_group_id(1)*get_local_size(1)*number_of_cities)+(get_local_size(0)*get_group_id(0))+get_local_id(0);
  unsigned int threadIndex = get_local_size(0)*get_group_id(0)+get_local_id(0);
      
  if (threadIndex<number_of_cities*number_of_cities){
  float pheromone;
  pheromone = d_pheromone[threadIndex];
  float heuristic = 1.0f / ((float) d_distance[threadIndex] + 0.1f);        
  d_choiceInfo[threadIndex]= native_powr(pheromone, alpha)*native_powr(heuristic, beta);        
  //Evaporation stage  
  pheromone= pheromone*(1.0f-evaporation_rate);      
  d_pheromone[threadIndex] = pheromone;
  }
}
 

unsigned int reduceMax(__local float *g_idata, unsigned int index, unsigned int n, __local uint * index_sh) {
  /*  
  FUNCTION:    This fuction obtains the maximum of the array and the index of that maximum 
  INPUT:      pointer to the probabilistic information and the relative index
  OUTPUT:      Relative index of the maximum probability
  (SIDE)EFFECTS:  The maximum probability is in g_idata[0]
  ORIGIN:      Partially based on NVIDIA_CUDA_SDK
  */
 
  index_sh[get_local_id(0)]= index;
  g_idata[get_local_id(0)] = (index>=n)?0.0f:g_idata[get_local_id(0)];

  barrier(CLK_LOCAL_MEM_FENCE);
  
  // do reduction in shared mem
  for(unsigned int s=get_local_size(0)/2; s>0; s>>=1) {
  if (get_local_id(0) < s){
    if (g_idata[get_local_id(0)]<g_idata[get_local_id(0) + s]) {
  g_idata[get_local_id(0)]=g_idata[get_local_id(0) + s];
  index_sh[get_local_id(0)]=index_sh[get_local_id(0) + s];
    }
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  }

  // write result for this block to global mem
  return index_sh[0];
}

float ran01( int *idum )
/*    
    FUNCTION:      generate a random number that is uniformly distributed in [0,1]
    INPUT:      pointer to variable with the current seed
    OUTPUT:      random number uniformly distributed in [0,1]
    (SIDE)EFFECTS:  random number seed is modified (important, this has to be done!)
    ORIGIN:      numerical recipes in C
*/
{
  int k;
  float ans;

  k =(*idum)/IQ;
  *idum = IA * (*idum - k * IQ) - IR * k;
  if (*idum < 0 ) *idum += IM;
  ans = AM * (*idum);
  return ans;
}

__kernel void nextTour_kernel(unsigned  int number_of_cities, const unsigned int number_of_ants, __global unsigned int * d_distance, __global unsigned int * d_tour, __global unsigned int * d_lenghtList, __global float * choice_info, __global int * d_seed, const unsigned int iterathreads){
   
  __local float probabilities_sh[BLOCK_SIZE_NEXT_TOUR];    
  __local unsigned int tourLenghtCum;          
  __local unsigned int current_city_sh;
  __local uint index_sh[BLOCK_SIZE_NEXT_TOUR];
  unsigned int current_city;
  unsigned int btourindex = get_group_id(0)*(number_of_cities+1);
  unsigned int tabul=0;
  unsigned int antindex = (get_group_id(0)*number_of_cities)+get_local_id(0);
  float random;
  int seed = d_seed[antindex];

  //The tabulist is initializes as 1 
  // 1 is not visited yet
  // 0 it has been visited 
  // itera is the N/get_local_size() with the padding 
  // The padding cities are marked as visited
  //Set all bits to 1
  tabul=~(tabul & 0); 
    
  if (get_local_id(0)==0) {
  random = ran01(&seed);
  current_city_sh = (unsigned int)number_of_cities*random;
  }

  barrier(CLK_LOCAL_MEM_FENCE);
  current_city = current_city_sh;
  unsigned int beginning_city = current_city;
  
  //Division is the bit that represents that city
  unsigned int bitTabul = (unsigned int) beginning_city>>LOG_BLOCK_SIZE_NEXT_TOUR;  
  //Modulo is the thread that manages that city 
  unsigned int threadTabul = (unsigned int) beginning_city & (BLOCK_SIZE_NEXT_TOUR-1);    
  
  if (get_local_id(0) == threadTabul) {
  //the bit that represents the city in this thread is marked as zero 
  //Pongo el uno en el bit adecuado y hago la negacion de todo 
  unsigned int aux = (unsigned int)MACROBITS<<bitTabul;      
  tabul = tabul & (~aux);
  tourLenghtCum=0;
  d_tour[btourindex] = beginning_city;
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  
  //Repeat until tabu list is full 
  for (int tourIndex=1; tourIndex<number_of_cities;tourIndex++){
  int indexChoiceInfo = current_city*number_of_cities; 
  float current_prob = 0.0f;
  int indexaux;
  
  for (int i=0; i<iterathreads-1; i++){   
    indexaux = get_local_id(0)+(i*get_local_size(0));
    unsigned int auxtabul = tabul>>i; //Desplazo a la derecha el posible 0
    auxtabul = (unsigned int) MACROBITS & auxtabul; //auxbits debe ser 0 o 1. 
    float choice = choice_info[indexChoiceInfo+(indexaux)];
    random = ran01(&seed);
    probabilities_sh[get_local_id(0)]= (float) choice*auxtabul*random;    
  
    barrier(CLK_LOCAL_MEM_FENCE);

    unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities, index_sh);           
    float prob_local = probabilities_sh[0];
    
    if (prob_local>=current_prob) {
      current_prob = prob_local;
      current_city = aux_city;
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }
  
  indexaux = get_local_id(0)+((iterathreads-1)*get_local_size(0));
  if (indexaux < number_of_cities){
    unsigned int auxtabul = tabul >>(iterathreads-1); //Desplazo a la derecha el posible 0
    auxtabul = (unsigned int)MACROBITS & auxtabul; //auxbits debe ser 0 o 1.     
    float choice= choice_info[indexChoiceInfo+(indexaux)];
    random = ran01(&seed);
    probabilities_sh[get_local_id(0)]= (float)choice*auxtabul*random;      
  }  

  barrier(CLK_LOCAL_MEM_FENCE);

  unsigned int aux_city= reduceMax(probabilities_sh,indexaux, number_of_cities, index_sh);          
  float prob_local = probabilities_sh[0];

  if (prob_local>=current_prob) {
    current_prob = prob_local;
    current_city = aux_city;
  }

  barrier(CLK_LOCAL_MEM_FENCE);
    
  //Division is the bit that represents that city
  bitTabul = current_city>>LOG_BLOCK_SIZE_NEXT_TOUR;  
  //Modulo is the thread that manages that city 
  threadTabul = current_city & (BLOCK_SIZE_NEXT_TOUR-1);

  if (get_local_id(0)==threadTabul) {
    //the bit that represents the city in this thread is marked as zero 
    unsigned int aux = (unsigned int)MACROBITS<<bitTabul;      
    tabul = tabul & (~aux);
    d_tour[btourindex+tourIndex]=current_city; //Adding the city to the tour  
    tourLenghtCum+= d_distance[indexChoiceInfo+current_city];        
  }  
  barrier(CLK_LOCAL_MEM_FENCE);   
  }//End of FOR  
  
  //Complete the tour getting the beginning city    
  if (get_local_id(0)==0) {
  d_tour[btourindex+number_of_cities]=beginning_city;
  tourLenghtCum+= d_distance[current_city*number_of_cities+beginning_city];           
  d_lenghtList[get_group_id(0)]=tourLenghtCum; // Keep the lenght of the current tour
  }

  d_seed[antindex] = seed;
}  

inline void AtomicAdd(volatile __global float *source, const float operand) {
  union {
    unsigned int intVal;
    float floatVal;
  } newVal;
  union {
    unsigned int intVal;
    float floatVal;
  } prevVal;
  do {
    prevVal.floatVal = *source;
    newVal.floatVal = prevVal.floatVal + operand;
  } while (atomic_cmpxchg((volatile __global unsigned int *)source, prevVal.intVal, newVal.intVal) != prevVal.intVal);
}
 
__kernel void pheromone_update(__global float * d_pheromone, const unsigned int number_of_cities, __global unsigned int * d_tour, const unsigned int number_of_ants, __global unsigned int * d_lenghtList, int ite){
  const int xIndex = get_group_id(0)* (number_of_cities+1) + get_local_id(0);
  int index,indexThread; 
  for (int i=0; i<ite; i++) {
  indexThread = i*get_local_size(0);         
  index =xIndex+(indexThread);
  if ((get_local_id(0)+indexThread)<number_of_cities){
    unsigned int coordx=d_tour[index];
    unsigned int coordy=d_tour[index+1];

    unsigned int pos = (coordy*number_of_cities)+coordx;
    unsigned int posim = (coordx*number_of_cities)+coordy;              
    float pheromone = 0.0f;
    pheromone=(float)1.0f*(1.0f/d_lenghtList[get_group_id(0)]);
    
    //AtomicAdd(&d_pheromone[pos], pheromone);
    //AtomicAdd(&d_pheromone[posim], pheromone);
    d_pheromone[pos]+=pheromone;
    d_pheromone[posim]+=pheromone;
  }
  }
}

#endif // #ifndef _ANT_COLONY_KERNEL_H_


