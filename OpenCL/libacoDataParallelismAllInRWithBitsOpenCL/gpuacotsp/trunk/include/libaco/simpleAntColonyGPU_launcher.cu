// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>
// includes, project
#include <cutil_inline.h>

// includes, kernels
#include "simpleAntGPU_kernel.cu"
//
////////////////////////////////////////////////////////////////////////////////
// declaration, forward 

#define BLOCK_SIZE 256

void printTour(unsigned int * best, unsigned int number_of_cities);
float getLenghtIterationBestTour (float * lenght, unsigned int number_of_ants, unsigned int * d_tabu, unsigned int * h_best, float * bestLenght, unsigned int number_of_cities);

////////////////////////////////////////////////////////////////////////////////
//! Entry point for Cuda functionality on host side
//! @param  
////////////////////////////////////////////////////////////////////////////////
extern "C" void
antColonyGPULauncher(const unsigned int number_of_ants, const unsigned int number_of_cities, unsigned int * distance, float * pheromone,
										 unsigned int * ants, float alpha, float beta, float evaporation_rate, unsigned int iterations)
{ 

		cudaSetDevice(cutGetMaxGflopsDeviceId());
		
    const unsigned int mem_size_distance = sizeof(unsigned int) * number_of_cities * number_of_cities;

    const unsigned int mem_size_pheromone = sizeof(float) * number_of_cities * number_of_cities;

    const unsigned int mem_size_ants = sizeof(unsigned int) * number_of_ants;

		const unsigned int mem_size_tabul = sizeof (unsigned int) * number_of_ants * number_of_cities;
		const unsigned int mem_size_lenghtList = sizeof (float) * number_of_ants;


    // allocate device memory for distance
    unsigned int * d_distance; 
    cutilSafeCall(cudaMalloc((void**) &d_distance, mem_size_distance));
    // copy host memory to device
    cutilSafeCall(cudaMemcpy(d_distance, distance, mem_size_distance,
                            cudaMemcpyHostToDevice) );

    // allocate device memory for pheromone
    float* d_pheromone; 							
    cutilSafeCall(cudaMalloc((void**) &d_pheromone, mem_size_pheromone));
    // copy host memory to device
    cutilSafeCall(cudaMemcpy(d_pheromone, pheromone, mem_size_pheromone,
                            cudaMemcpyHostToDevice) );

    // allocate device memory for ants
    unsigned int* d_ants;
    cutilSafeCall(cudaMalloc((void**) &d_ants, mem_size_ants));
    // copy host memory to device
    cutilSafeCall(cudaMemcpy(d_ants, ants, mem_size_ants,
                            cudaMemcpyHostToDevice) );

    // allocate device memory for tabulist
    unsigned int* d_tabulist;
    cutilSafeCall(cudaMalloc((void**) &d_tabulist, mem_size_tabul));
		
		// allocate device memory for the tabu list 
		float * d_lenghtList;
    cutilSafeCall(cudaMalloc((void**) &d_lenghtList,mem_size_lenghtList));


		//the lenght of the best tour so far
		float bestLenght;  		

		//the best tour so far
		unsigned int * h_bestTour= (unsigned int *)malloc (number_of_cities*sizeof(unsigned int));

		//Lenghts of all tour taken by each Ant			
		float * h_lenghtList = (float *) malloc (mem_size_lenghtList);
  
	  std::cout << "iter\ttime\tbest\tbest_it";
 	  std::cout << std::endl;

    // create and start timer
    unsigned int timer = 0;
    cutilCheckError(cutCreateTimer(&timer));
		
		unsigned int block_size = (number_of_ants < BLOCK_SIZE)?number_of_ants:BLOCK_SIZE;	

		unsigned int num_blocks  = number_of_ants % block_size;
		
    // setup execution parameters for nextTour kernel. 
    dim3 grid1(num_blocks);
    dim3 threads1(block_size);

		// setup execution parameters for updating pheromone kernel. 

		block_size = (number_of_cities < BLOCK_SIZE) ? number_of_cities:BLOCK_SIZE;
		num_blocks  = number_of_cities % block_size;

    dim3 grid2(num_blocks);
    dim3 threads2(block_size);

		for (int i = 0; i<iterations; i++) {

	    cutilCheckError(cutStartTimer(timer));
	    nextTour_kernel<<< grid1, threads1 >>>(number_of_cities, d_distance, d_pheromone, d_ants, alpha, beta, d_tabulist, d_lenghtList); 						
			
			cutilSafeCall(cudaMemcpy(h_lenghtList, d_lenghtList, mem_size_lenghtList,
                            cudaMemcpyDeviceToHost) );

			pheromone_update<<<grid2,threads2>>>(d_pheromone,evaporation_rate, number_of_cities);	
	  // check if kernel execution generated and error
  	  cutilCheckMsg("Kernel execution failed");

			cudaThreadSynchronize();

	    // stop
  	  cutilCheckError(cutStopTimer(timer));

			std::cout << (i+1) << "\t";
	    std::cout << cutGetTimerValue(timer) << "msec \t";
			
			float bestIt = getLenghtIterationBestTour(h_lenghtList, number_of_ants, d_tabulist, h_bestTour,&bestLenght, number_of_cities);

    	std::cout << bestLenght << "\t";
    	std::cout << bestIt << "\t";
  	  cutilCheckError(cutResetTimer(timer));
	
	 	} 	

	  std::cout << std::endl;
	  std::cout << "best\tordering" << std::endl;
	  std::cout << bestLenght << "\t";

	  printTour(h_bestTour, number_of_cities);
	  std::cout << std::endl;

 	  cutilCheckError(cutDeleteTimer(timer));
		cutilSafeCall (cudaFree(d_distance));
		cutilSafeCall (cudaFree(d_pheromone));
		cutilSafeCall (cudaFree(d_ants));
		cutilSafeCall (cudaFree(d_tabulist));
		cutilSafeCall (cudaFree(d_lenghtList));
		free (h_bestTour);	


    cudaThreadExit();
}

void printTour(unsigned int * best, unsigned int number_of_cities) {

	for (int i=0; i<number_of_cities;i++)
		std::cout << best[i] << ", ";
		
  std::cout << std::endl;		
}


float getLenghtIterationBestTour (float * lenght, unsigned int number_of_ants, unsigned int * d_tabu, unsigned int * h_best, float * bestLenght, unsigned int number_of_cities) 
{
	
	float bestIt=lenght[0];
	unsigned int antBest = 0;
		
	for (int i=1;i<number_of_ants;i++) {
			if (lenght[i]<bestIt) {
				bestIt = lenght[i];
				antBest =i;
			}
	}

	if (bestIt < *bestLenght) {
		*bestLenght = bestIt;			
		cutilSafeCall(cudaMemcpy(h_best, d_tabu+antBest*number_of_cities, number_of_cities*sizeof(unsigned int), cudaMemcpyDeviceToHost) );				
	}     	
	
			
	return bestIt;		

}
