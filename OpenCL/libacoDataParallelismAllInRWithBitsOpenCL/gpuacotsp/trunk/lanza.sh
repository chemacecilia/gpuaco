#!/bin/bash
 
./bin/acotsp --simple -f ./benchmarks/tsplib/d198.tsp -m 198
./bin/acotsp --simple -f ./benchmarks/tsplib/a280.tsp -m 280
./bin/acotsp --simple -f ./benchmarks/tsplib/lin318.tsp -m 318
./bin/acotsp --simple -f ./benchmarks/tsplib/pcb442.tsp -m 442
./bin/acotsp --simple -f ./benchmarks/tsplib/rat783.tsp -m 783

./bin/acotsp --simple -f ./benchmarks/tsplib/pr1002.tsp -m 1002
./bin/acotsp --simple -f ./benchmarks/tsplib/pcb1173.tsp -m 1173
./bin/acotsp --simple -f ./benchmarks/tsplib/d1291.tsp -m 1291
./bin/acotsp --simple -f ./benchmarks/tsplib/pr2392.tsp -m 2392
 
